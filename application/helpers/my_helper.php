<?php
    
    function curlpost($parameters, $path)
    {
		$apiUrl = $path; 
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $apiUrl);
		curl_setopt($curl_handle, CURLOPT_BUFFERSIZE, 1024);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, TRUE);
		curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_PROXY, '');
        curl_setopt($curl_handle, CURLOPT_SSLVERSION, 3);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $parameters);
        $response = curl_exec($curl_handle);
        #$info=curl_getinfo($curl_handle);
        #p($response);    
        #print_R($info); exit();
		//	p($response);  exit;
		$newresponse = (object) json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);	
		//$response  = json_decode($newresponse);
		curl_close($curl_handle);
		return $newresponse;
	}
	function curlput($parameters, $path)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $path,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "PUT",
		  CURLOPT_POSTFIELDS => $parameters,
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "postman-token: b6e5cedb-72dc-0e8f-9941-5d707af2d0b2"
		  ),
		));
		$response = curl_exec($curl);
		$newresponse = (object) json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);	
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
		return $newresponse;
	}
	function curlget($myurl)
	{
	   	$curl = curl_init();
        $option=array(
        	CURLOPT_RETURNTRANSFER =>true,
            CURLOPT_URL => $myurl,
            CURLOPT_USERAGENT =>'Medhoop',
            CURLOPT_SSL_VERIFYPEER => TRUE,
            CURLOPT_SSL_VERIFYHOST => TRUE,
            CURLOPT_PROXY => '',
            CURLOPT_SSLVERSION => 3
        );
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl,$option);
		// Send the request & save response to $resp
		$response = curl_exec($curl);
        #$info=curl_getinfo($curl_handle);
        #p($response);    
        #print_R($info); 
		$newresponse = (object) json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true);	
                #p($newresponse);exit;
		//$response  = json_decode($newresponse);
		curl_close($curl);
		return $newresponse;
	}
	function curldel($myurl)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $myurl,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "DELETE",
		  // CURLOPT_HTTPHEADER => array(
		  //   "cache-control: no-cache",
		  //   "content-type: application/x-www-form-urlencoded",
		  //   "postman-token: e0f15f10-319f-a146-fd41-bb742c1a240f"
		  // ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) 
		{
		  echo "cURL Error #:" . $err;
		} 
		else 
		{
		  echo $response;
		}
	}
    function api_url()
    {
		$api_url ='https://localhost:8080/activezone/';
		$result = $api_url;
		return $result;
	}

	?>