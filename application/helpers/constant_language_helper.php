<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('global_text_method')) {
  function global_text_method($vars = '') {
    if($vars == 'danger-class') {
      return $vars = 'alert alert-danger';
    }
    if($vars == 'success-class') {
      return $vars = 'alert alert-success';
    }
    if($vars == 'require-field') {
      return $vars = 'Fields are required!!! Please try again.';
    }
    if($vars == 'correct-details') {
      return $vars = 'Please Enter Correct Details.';
    }
    if($vars == 'captcha-error') {
      return $vars = 'Sorry, please enter the correct captcha code!';
    }
    
  }   
}

