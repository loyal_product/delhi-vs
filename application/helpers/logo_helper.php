<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('getlogo')) {
  function getlogo() {
    $CI = &get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('MyModel');
    // Call a function of the model
    $result = $CI->MyModel->getAdminLogo();
    return $result[0]['meta_value'];
  }
}