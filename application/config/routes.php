<?php
defined('BASEPATH') || exit('No direct script access allowed');
/*error_reporting(E_ALL);
ini_set("display_errors", 1);*/
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//backend
$route['default_controller'] 			= 'MainController';
//$route['site/core/micro/site/lib/controller/type'] 						= 'backend/UserController';
$route['site/core/micro/site/lib/controller/type'] 						= 'backend/LoginController';
$route['site/core/micro/site/lib/controller/type/logout'] 					= "backend/LoginController/logout";
$route['site/core/micro/site/lib/controller/type/login'] 					= "backend/LoginController/login";

$route['site/core/micro/site/lib/controller/type/error'] 					= "backend/UserController/error_page";			
$route['404_override'] 					= '';
$route['translate_uri_dashes'] 			= FALSE;
$route['site/core/micro/site/lib/controller/type/insert'] 					= "backend/UserController/insert";
/*$route['site/core/micro/site/lib/controller/type/login'] 					= "backend/UserController/login";
$route['site/core/micro/site/lib/controller/type/otp'] 					= "backend/UserController/otp";
$route['site/core/micro/site/lib/controller/type/otpSubmit'] 				= "backend/UserController/otpSubmit";*/
$route['site/core/micro/site/lib/controller/type/dashboard'] 				= "backend/UserController/dashboard";
//$route['site/core/micro/site/lib/controller/type/logout'] 					= "backend/UserController/logout";
$route['site/core/micro/site/lib/controller/type/changePasswordUpdate'] 	= "backend/UserController/changePasswordUpdate";
$route['site/core/micro/site/lib/controller/type/changePassword'] 			= "backend/UserController/changePassword";

// Users -----------------------------------------------
$route['site/core/micro/site/lib/controller/type/registration'] 			= "backend/UserController/registration";
$route['site/core/micro/site/lib/controller/type/userList'] 				= "backend/UserController/userList";
$route['site/core/micro/site/lib/controller/type/userList/(:any)'] 		= "backend/UserController/userList/$1";
$route['site/core/micro/site/lib/controller/type/deleteUserList/(:any)'] 	= "backend/UserController/deleteUserList/$1";
$route['site/core/micro/site/lib/controller/type/editUserList/(:any)'] 	= "backend/UserController/editUserList/$1";
$route['site/core/micro/site/lib/controller/type/updateUser/(:any)'] 		= "backend/UserController/updateUser/$1";
$route['site/core/micro/site/lib/controller/type/registrationmorethan5sameip'] 		= "backend/UserController/registrationMoreThan5SameIP";
$route['site/core/micro/site/lib/controller/type/bookingmorethan5sameip'] 		= "backend/UserController/bookingMoreThan5SameIP";
$route['site/core/micro/site/lib/controller/type/registrationmorethan3sameemail'] 		= "backend/UserController/registrationMoreThan3SameEmail";
$route['site/core/micro/site/lib/controller/type/bookingmorethan3sameemail'] 		= "backend/UserController/bookingMoreThan3SameEmail";

$route['site/core/micro/site/lib/controller/type/registrationdownloadmorethan5sameip'] 		= "backend/UserController/registrationDownloadMoreThan5SameIP";
$route['site/core/micro/site/lib/controller/type/bookingdownloadmorethan5sameip'] 		= "backend/UserController/bookingDownloadMoreThan5SameIP";
$route['site/core/micro/site/lib/controller/type/registrationdownloadmorethan3sameemail'] 		= "backend/UserController/registrationDownloadMoreThan3SameEmail";
$route['site/core/micro/site/lib/controller/type/bookingdownloadmorethan3sameemail'] 		= "backend/UserController/bookingDownloadMoreThan3SameEmail";


//Settings
$route['site/core/micro/site/lib/controller/type/cityvenue'] 				= "backend/SettingController/CityVenueList";
$route['site/core/micro/site/lib/controller/type/cityinsert'] 				= "backend/SettingController/cityInsert";
$route['site/core/micro/site/lib/controller/type/venueinsert'] 			= "backend/SettingController/venueInsert";
$route['site/core/micro/site/lib/controller/type/salonvenueinsert'] 			= "backend/SettingController/salonVenueInsert";
$route['site/core/micro/site/lib/controller/type/citylistupload'] 			= "backend/SettingController/CityListUpload";
$route['site/core/micro/site/lib/controller/type/venuelistupload'] 		= "backend/SettingController/VenueListUpload";
$route['site/core/micro/site/lib/controller/type/cityuploadcsv'] 			= "backend/SettingController/cityUploadCSV";
$route['site/core/micro/site/lib/controller/type/venueuploadcsv'] 			= "backend/SettingController/venueUploadCSV";
$route['site/core/micro/site/lib/controller/type/generalsetting'] 			= "backend/SettingController";
$route['site/core/micro/site/lib/controller/type/generalsettinginsert'] 	= "backend/SettingController/generalSettingInsert";
$route['site/core/micro/site/lib/controller/type/citylisting'] 			  = "backend/SettingController/CityListing";
$route['site/core/micro/site/lib/controller/type/citylisting/(:any)'] 	= "backend/SettingController/CityListing/$1";
$route['site/core/micro/site/lib/controller/type/venuelisting'] 			  = "backend/SettingController/VenueListing";
$route['site/core/micro/site/lib/controller/type/venuelisting/(:any)'] = "backend/SettingController/VenueListing/$1";
$route['site/core/micro/site/lib/controller/type/deletecity/(:any)'] 	= "backend/SettingController/deleteCity/$1";
$route['site/core/micro/site/lib/controller/type/editcity/(:any)/(:any)'] 	= "backend/SettingController/editCity/$1/$2";
$route['site/core/micro/site/lib/controller/type/cityupdate/(:any)'] 		= "backend/SettingController/cityUpdate/$1";
$route['site/core/micro/site/lib/controller/type/stateupdate/(:any)'] 		= "backend/SettingController/stateUpdate/$1";
$route['site/core/micro/site/lib/controller/type/deletevenue/(:any)'] 	= "backend/SettingController/deleteVenue/$1";
$route['site/core/micro/site/lib/controller/type/deletestate/(:any)'] 	= "backend/SettingController/deleteState/$1";
$route['site/core/micro/site/lib/controller/type/editvenue/(:any)/(:any)'] = "backend/SettingController/editVenue/$1/$2";
$route['site/core/micro/site/lib/controller/type/editstate/(:any)'] = "backend/SettingController/editState/$1";
$route['site/core/micro/site/lib/controller/type/venueupdate/(:any)'] 		= "backend/SettingController/venueUpdate/$1";
$route['site/core/micro/site/lib/controller/type/venueuploadingbulk']  = "backend/SettingController/venueUploadingBulk";
$route['site/core/micro/site/lib/controller/type/allcitylist']  				= "backend/SettingController/downloadAllCityList";

$route['site/core/micro/site/lib/controller/type/allstatelist']  				= "backend/SettingController/downloadAllStateList";
$route['site/core/micro/site/lib/controller/type/stateuploadcsv'] 			= "backend/SettingController/stateUploadCSV";
$route['site/core/micro/site/lib/controller/type/stateuploadcsvinsert'] 			= "backend/SettingController/stateUploadCSVInsert";

$route['site/core/micro/site/lib/controller/type/venueuploadingbulkinsert']  = "backend/SettingController/venueUploadingBulkInsert";
$route['site/core/micro/site/lib/controller/type/searchbycityname'] 		= "backend/SettingController/searchByCityName";
$route['site/core/micro/site/lib/controller/type/multipledelete']		=	"backend/SettingController/multipleDelete";
$route['site/core/micro/site/lib/controller/type/multiplevenuedelete'] = "backend/SettingController/multipleVenueDelete";
$route['site/core/micro/site/lib/controller/type/getcitylist'] 	= "backend/SettingController/getCityList";
$route['site/core/micro/site/lib/controller/type/getstatelist'] 	= "backend/SettingController/getStateList";
$route['site/core/micro/site/lib/controller/type/venueadd']		= "backend/SettingController/venueAdd";
$route['site/core/micro/site/lib/controller/type/salonvenueadd']		= "backend/SettingController/salonVenueAdd";
$route['site/core/micro/site/lib/controller/type/stateadd']		= "backend/SettingController/stateAdd";
$route['site/core/micro/site/lib/controller/type/stateinsert'] 			= "backend/SettingController/stateInsert";
$route['site/core/micro/site/lib/controller/type/statelisting'] 			  = "backend/SettingController/StateListing";
// Settings for Offers
$route['site/core/micro/site/lib/controller/type/settingsforofferadd'] 		= "backend/SettingController/settingsForOfferAdd";
$route['site/core/micro/site/lib/controller/type/settingsforofferinsert'] 		= "backend/SettingController/settingsForOfferInsert";
$route['site/core/micro/site/lib/controller/type/settingsforofferlist'] 		= "backend/SettingController/settingsForOfferList";
$route['site/core/micro/site/lib/controller/type/settingsforofferlist/(:any)'] = "backend/SettingController/settingsForOfferList/$1";
$route['site/core/micro/site/lib/controller/type/settingsforofferdelete/(:any)'] = "backend/SettingController/settingsForOfferDelete/$1";
$route['site/core/micro/site/lib/controller/type/settingsforofferedit/(:any)'] = "backend/SettingController/settingsForOfferEdit/$1";
$route['site/core/micro/site/lib/controller/type/settingsforofferupdate/(:any)'] 	= "backend/SettingController/settingsForOfferUpdate/$1";

$route['site/core/micro/site/lib/controller/type/getcitylists'] = "backend/SettingController/getCityLists";
$route['site/core/micro/site/lib/controller/type/searchbyvenuelists'] = "backend/SettingController/VenueListingAfterSearching";
$route['site/core/micro/site/lib/controller/type/searchbyvenuelists/(:any)'] = "backend/SettingController/VenueListingAfterSearching/$1";


$route['site/core/micro/site/lib/controller/type/multiplevenuedeactivate'] = "backend/SettingController/multipleVenueDeactivate";
$route['site/core/micro/site/lib/controller/type/multiplevenueactivate'] = "backend/SettingController/multipleVenueActivate";

//--------CIRCLE-----------
$route['site/core/micro/site/lib/controller/type/circlelist']              = "backend/SettingController/circleList";
$route['site/core/micro/site/lib/controller/type/circleadd']               = "backend/SettingController/circleAdd";
$route['site/core/micro/site/lib/controller/type/circleinsert']            = "backend/SettingController/circleInsert";
$route['site/core/micro/site/lib/controller/type/circleupload']            = "backend/SettingController/circleUpload";
$route['site/core/micro/site/lib/controller/type/circleuploadadd']         = "backend/SettingController/circleUploadAdd";
$route['site/core/micro/site/lib/controller/type/searchbycirclename']      = "backend/SettingController/searchByCircleName";
$route['site/core/micro/site/lib/controller/type/multipledeletecircle']	   = "backend/SettingController/multipleDeleteCircle";
$route['site/core/micro/site/lib/controller/type/editcircle/(:any)/(:any)']= "backend/SettingController/editCircle/$1/$2";
$route['site/core/micro/site/lib/controller/type/circleupdate/(:any)'] 	   = "backend/SettingController/circleUpdate/$1";
$route['site/core/micro/site/lib/controller/type/deletecircle/(:any)'] 	   = "backend/SettingController/deleteCircle/$1";
//--------CIRCLE-----------

// Fields Type Management
$route['site/core/micro/site/lib/controller/type/createdynamicform'] 		= "backend/FieldTypeController/createDynamicForm";
$route['site/core/micro/site/lib/controller/type/insertdynamicform'] 		= "backend/FieldTypeController/insertDynamicForm";
$route['site/core/micro/site/lib/controller/type/listdynamicform'] 		= "backend/FieldTypeController/listDynamicForm";
$route['site/core/micro/site/lib/controller/type/listdynamicform/(:any)'] 	= "backend/FieldTypeController/listDynamicForm/$1";
$route['site/core/micro/site/lib/controller/type/deletedynamicform/(:any)']= "backend/FieldTypeController/deleteDynamicForm/$1";
$route['site/core/micro/site/lib/controller/type/editdynamicform/(:any)'] 	= "backend/FieldTypeController/editDynamicForm/$1";
$route['site/core/micro/site/lib/controller/type/updatedynamicform/(:any)']= "backend/FieldTypeController/updateDynamicForm/$1";

// Fields Management
$route['site/core/micro/site/lib/controller/type/fieldsadd'] 				= "backend/FieldController/fieldsAdd";
$route['site/core/micro/site/lib/controller/type/fieldsinsert'] 			= "backend/FieldController/fieldsInsert";
$route['site/core/micro/site/lib/controller/type/fieldslist'] 				= "backend/FieldController/fieldsList";
$route['site/core/micro/site/lib/controller/type/fieldslist/(:any)'] 		= "backend/FieldController/fieldsList/$1";
$route['site/core/micro/site/lib/controller/type/deletefields/(:any)'] 	= "backend/FieldController/deleteFields/$1";
$route['site/core/micro/site/lib/controller/type/editfields/(:any)'] 		= "backend/FieldController/editFields/$1";
$route['site/core/micro/site/lib/controller/type/fieldsupdate/(:any)'] 	= "backend/FieldController/fieldsUpdate/$1";

// Vouchers -----------------------------------------------
$route['site/core/micro/site/lib/controller/type/voucherRegistration'] 	= "backend/VoucherController/voucherRegistration";
$route['site/core/micro/site/lib/controller/type/voucherinsert'] 			= "backend/VoucherController/voucherinsert";
$route['site/core/micro/site/lib/controller/type/voucherList'] 			= "backend/VoucherController/voucherList";
$route['site/core/micro/site/lib/controller/type/voucherList/(:any)'] 		= "backend/VoucherController/voucherList/$1";
$route['site/core/micro/site/lib/controller/type/deletevoucherList/(:any)'] = "backend/VoucherController/deletevoucherList/$1";
$route['site/core/micro/site/lib/controller/type/editvoucherList/(:any)'] 	= "backend/VoucherController/editVoucherList/$1";
$route['site/core/micro/site/lib/controller/type/updateVoucher/(:any)'] 	= "backend/VoucherController/updateVoucher/$1";
$route['site/core/micro/site/lib/controller/type/deleteVoucherList/(:any)'] = "backend/VoucherController/deleteVoucherList/$1";
$route['site/core/micro/site/lib/controller/type/searchbymobile']			= "backend/VoucherController/searchByMobile";
$route['site/core/micro/site/lib/controller/type/searchbyvoucher']			= "backend/VoucherController/searchByVoucherCode";
$route['site/core/micro/site/lib/controller/type/voucherupdatedate'] 		= "backend/VoucherController/VoucherUpdateDate";
$route['site/core/micro/site/lib/controller/type/voucherdateupdate'] 		= "backend/VoucherController/VoucherDateUpdate";
$route['site/core/micro/site/lib/controller/type/voucherlastdatetobook'] 	= "backend/VoucherController/voucherLastDateToBook";
$route['site/core/micro/site/lib/controller/type/vouchercsvupload'] 		= "backend/VoucherController/uploadCSVView";
$route['site/core/micro/site/lib/controller/type/multiplvoucheredelete']		=	"backend/VoucherController/multipleDelete";

$route['site/core/micro/site/lib/controller/type/unblockunusedvoucherview']		=	"backend/VoucherController/unblockAndUnusedView";
$route['site/core/micro/site/lib/controller/type/unblockunusedvoucherupdate']		=	"backend/VoucherController/unblockUnusedVoucherUpdate";

$route['site/core/micro/site/lib/controller/type/batchwisevoucherdateupdate'] =	"backend/VoucherController/batchWiseVoucherDateUpdate";
$route['site/core/micro/site/lib/controller/type/voucherwisedateupdate']		=	"backend/VoucherController/voucherWiseDateUpdate";
$route['site/core/micro/site/lib/controller/type/getbatchdates']		=	"backend/VoucherController/getBatchDates";

// Block/Unblock Management
$route['site/core/micro/site/lib/controller/type/blockunblockadd'] 			= "backend/BlockUnblockController/blockUnblockAdd";
$route['site/core/micro/site/lib/controller/type/blockunblockinsert'] 		= "backend/BlockUnblockController/blockUnblockInsert";
$route['site/core/micro/site/lib/controller/type/blockunblocklist'] 			= "backend/BlockUnblockController/blockUnblockList";
$route['site/core/micro/site/lib/controller/type/blockunblocklist/(:any)'] 	= "backend/BlockUnblockController/blockUnblockList/$1";
$route['site/core/micro/site/lib/controller/type/deleteblockunblock/(:any)'] 	= "backend/BlockUnblockController/deleteBlockUnblock/$1";
$route['site/core/micro/site/lib/controller/type/editblockunblock/(:any)'] 	= "backend/BlockUnblockController/editBlockUnblock/$1";
$route['site/core/micro/site/lib/controller/type/blockunblockupdate/(:any)'] 	= "backend/BlockUnblockController/updateBlockUnblock/$1";

$route['site/core/micro/site/lib/controller/type/blockunblockmsgadd'] 			= "backend/BlockUnblockController/blockUnblockMsgAdd";
$route['site/core/micro/site/lib/controller/type/blockunblockmsginsert'] 		= "backend/BlockUnblockController/blockUnblockMsgInsert";
$route['site/core/micro/site/lib/controller/type/blockunblockmsglist'] 			= "backend/BlockUnblockController/blockUnblockMsgList";
$route['site/core/micro/site/lib/controller/type/blockunblockmsglist/(:any)'] 	= "backend/BlockUnblockController/blockUnblockMsgList/$1";
$route['site/core/micro/site/lib/controller/type/deleteblockunblockmsg/(:any)'] 	= "backend/BlockUnblockController/deleteBlockUnblockMsg/$1";
$route['site/core/micro/site/lib/controller/type/editblockunblockmsg/(:any)'] 	= "backend/BlockUnblockController/editBlockUnblockMsg/$1";
$route['site/core/micro/site/lib/controller/type/blockunblockmsgupdate/(:any)'] 	= "backend/BlockUnblockController/updateBlockUnblockMsg/$1";

// Cashback Form Management
$route['site/core/micro/site/lib/controller/type/addcashbackbooking'] 		= "backend/CashbackBookingController/addCashbackBooking";
$route['site/core/micro/site/lib/controller/type/insertcashbackbooking'] = "backend/CashbackBookingController/insertCashbackBooking";
$route['site/core/micro/site/lib/controller/type/listcashbackbooking'] 	= "backend/CashbackBookingController/listCashbackBooking";
$route['site/core/micro/site/lib/controller/type/listcashbackbooking/(:any)'] = "backend/CashbackBookingController/listCashbackBooking/$1";
$route['site/core/micro/site/lib/controller/type/editcashbackbooking/(:any)'] = "backend/CashbackBookingController/editCashbackBooking/$1";
$route['site/core/micro/site/lib/controller/type/updatecashbackbooking/(:any)'] 	= "backend/CashbackBookingController/UpdateCashbackBooking/$1";
$route['site/core/micro/site/lib/controller/type/deletecashbackbooking/(:any)'] 	= "backend/CashbackBookingController/deleteCashbackBooking/$1";

// Cap Limit
$route['site/core/micro/site/lib/controller/type/caplimitadd']				= "backend/VoucherController/caplimitAdd";
$route['site/core/micro/site/lib/controller/type/caplimitInsert']			= "backend/VoucherController/caplimitInsert";
$route['site/core/micro/site/lib/controller/type/caplimitlist'] 			= "backend/VoucherController/caplimitlist";
$route['site/core/micro/site/lib/controller/type/caplimitlist/(:any)'] 	= "backend/VoucherController/caplimitlist/$1";
$route['site/core/micro/site/lib/controller/type/deletecaplimit/(:any)'] 	= "backend/VoucherController/deleteCapLimitList/$1";
$route['site/core/micro/site/lib/controller/type/editcaplimit/(:any)'] 	= "backend/VoucherController/editCapLimitList/$1";
$route['site/core/micro/site/lib/controller/type/updatecaplimit/(:any)'] 	= "backend/VoucherController/updateCapLimit/$1";

// Language Settings
$route['site/core/micro/site/lib/controller/type/languageadd'] 			= "backend/ProductController/languageAdd";
$route['site/core/micro/site/lib/controller/type/languageinsert'] 			= "backend/ProductController/languageInsert";
$route['site/core/micro/site/lib/controller/type/languagelist'] 			= "backend/ProductController/languageList";
$route['site/core/micro/site/lib/controller/type/languagelist/(:any)'] 	= "backend/ProductController/languageList/$1";
$route['site/core/micro/site/lib/controller/type/deletelanguage/(:any)']   = "backend/ProductController/deleteLanguage/$1";
$route['site/core/micro/site/lib/controller/type/editlanguagelist/(:any)'] = "backend/ProductController/editLanguageList/$1";
$route['site/core/micro/site/lib/controller/type/updatelanguage/(:any)'] 	= "backend/ProductController/updateLanguage/$1";

// Products -----------------------------------------------
$route['site/core/micro/site/lib/controller/type/productadd'] 				= "backend/ProductController/productAdd";
$route['site/core/micro/site/lib/controller/type/ProductInsert'] 			= "backend/ProductController/ProductInsert";
$route['site/core/micro/site/lib/controller/type/productList'] 			= "backend/ProductController/productList";
$route['site/core/micro/site/lib/controller/type/productList/(:any)'] 		= "backend/ProductController/productList/$1";
$route['site/core/micro/site/lib/controller/type/deleteproductList/(:any)'] = "backend/ProductController/deleteproductList/$1";
$route['site/core/micro/site/lib/controller/type/editproductList/(:any)'] 	= "backend/ProductController/editProductList/$1";
$route['site/core/micro/site/lib/controller/type/updateproduct/(:any)'] 	= "backend/ProductController/updateProduct/$1";

// Sub Product Management
$route['site/core/micro/site/lib/controller/type/subproductadd'] 			= "backend/ProductController/subProductAdd";
$route['site/core/micro/site/lib/controller/type/subproductinsert'] 			= "backend/ProductController/subProductInsert";
$route['site/core/micro/site/lib/controller/type/subproductlist'] 			= "backend/ProductController/subProductList";
$route['site/core/micro/site/lib/controller/type/subproductlist/(:any)'] = "backend/ProductController/subProductList/$1";
$route['site/core/micro/site/lib/controller/type/deletesubproduct/(:any)']   = "backend/ProductController/deleteSubProduct/$1";
$route['site/core/micro/site/lib/controller/type/editsubproduct/(:any)'] = "backend/ProductController/editSubProduct/$1";
$route['site/core/micro/site/lib/controller/type/updatesubproduct/(:any)'] 	= "backend/ProductController/updateSubProduct/$1";
$route['site/core/micro/site/lib/controller/type/uploadproduct'] 			= "backend/ProductController/uploadProduct";
$route['site/core/micro/site/lib/controller/type/uploadproductascsv'] 			= "backend/ProductController/uploadProductAsCSV";
$route['site/core/micro/site/lib/controller/type/multiplvoucheredeactivate']		=	"backend/ProductController/multiplVouchereDeactivate";

// Menu Management
$route['site/core/micro/site/lib/controller/type/menuadd'] 				= "backend/MenuController/menuadd";
$route['site/core/micro/site/lib/controller/type/menuinsert'] 				= "backend/MenuController/menuInsert";
$route['site/core/micro/site/lib/controller/type/menulist'] 				= "backend/MenuController/menulist";
$route['site/core/micro/site/lib/controller/type/menulist/(:any)'] 		= "backend/MenuController/menuList/$1";
$route['site/core/micro/site/lib/controller/type/deletemenu/(:any)'] 		= "backend/MenuController/deleteMenu/$1";
$route['site/core/micro/site/lib/controller/type/editmenu/(:any)'] 		= "backend/MenuController/editMenu/$1";
$route['site/core/micro/site/lib/controller/type/menuupdate/(:any)'] 		= "backend/MenuController/menuUpdate/$1";


// Banner Management
$route['site/core/micro/site/lib/controller/type/banneradd'] 				= "backend/BannerController/banneradd";
$route['site/core/micro/site/lib/controller/type/bannerinsert'] 			= "backend/BannerController/bannerInsert";
$route['site/core/micro/site/lib/controller/type/bannerlist'] 				= "backend/BannerController/bannerlist";
$route['site/core/micro/site/lib/controller/type/bannerlist/(:any)'] 		= "backend/BannerController/bannerList/$1";
$route['site/core/micro/site/lib/controller/type/deletebanner/(:any)'] 	= "backend/BannerController/deletebanner/$1";
$route['site/core/micro/site/lib/controller/type/editbanner/(:any)'] 		= "backend/BannerController/editbanner/$1";
$route['site/core/micro/site/lib/controller/type/bannerupdate/(:any)'] 	= "backend/BannerController/bannerupdate/$1";
$route['site/core/micro/site/lib/controller/type/logosetting'] 			= "backend/BannerController/logosetting";
$route['site/core/micro/site/lib/controller/type/logosettinginsert'] 		= "backend/BannerController/logoInsert";

$route['site/core/micro/site/lib/controller/type/banneraddforbooking']	= "backend/BannerController/bannerAddForBooking";
$route['site/core/micro/site/lib/controller/type/bannerinsertforbooking'] = "backend/BannerController/bannerInsertForBooking";
$route['site/core/micro/site/lib/controller/type/bannerlistforbooking'] 	="backend/BannerController/bannerListForBooking";
$route['site/core/micro/site/lib/controller/type/bannerlistforbooking/(:any)']= "backend/BannerController/bannerListForBooking/$1";
$route['site/core/micro/site/lib/controller/type/deletebannerforbooking/(:any)']="backend/BannerController/deletebannerForBooking/$1";
$route['site/core/micro/site/lib/controller/type/editbannerforbooking/(:any)'] = "backend/BannerController/editbannerForBooking/$1";
$route['site/core/micro/site/lib/controller/type/bannerupdateforbooking/(:any)'] 	= "backend/BannerController/bannerupdateForBooking/$1";


// Content Management
$route['site/core/micro/site/lib/controller/type/contentadd'] 				= "backend/ContentController/contentadd";
$route['site/core/micro/site/lib/controller/type/contentinsert'] 			= "backend/ContentController/contentInsert";
$route['site/core/micro/site/lib/controller/type/contentlist'] 			= "backend/ContentController/contentlist";
$route['site/core/micro/site/lib/controller/type/contentlist/(:any)'] 		= "backend/ContentController/contentList/$1";
$route['site/core/micro/site/lib/controller/type/deletecontent/(:any)'] 	= "backend/ContentController/deletecontent/$1";
$route['site/core/micro/site/lib/controller/type/editcontent/(:any)'] 		= "backend/ContentController/editcontent/$1";
$route['site/core/micro/site/lib/controller/type/contentupdate/(:any)'] 	= "backend/ContentController/contentupdate/$1";

$route['site/core/micro/site/lib/controller/type/contentaddforbooking'] 				= "backend/ContentController/contentaddForBooking";
$route['site/core/micro/site/lib/controller/type/contentinsertforbooking'] 			= "backend/ContentController/contentInsertForBooking";
$route['site/core/micro/site/lib/controller/type/contentlistforbooking'] 			= "backend/ContentController/contentListForBooking";
$route['site/core/micro/site/lib/controller/type/contentlistforbooking/(:any)'] 		= "backend/ContentController/contentListForBooking/$1";
$route['site/core/micro/site/lib/controller/type/deletecontentforbooking/(:any)'] 	= "backend/ContentController/deletecontentForBooking/$1";
$route['site/core/micro/site/lib/controller/type/editcontentforbooking/(:any)'] 		= "backend/ContentController/editcontentForBooking/$1";
$route['site/core/micro/site/lib/controller/type/contentupdateforbooking/(:any)'] 	= "backend/ContentController/contentupdateForBooking/$1";

// Inventory Management
$route['site/core/micro/site/lib/controller/type/inventoryadd'] 			= "backend/InventoryController/inventoryadd";
$route['site/core/micro/site/lib/controller/type/inventoryinsert'] 		= "backend/InventoryController/inventoryInsert";
$route['site/core/micro/site/lib/controller/type/inventorylist'] 			= "backend/InventoryController/inventorylist";
$route['site/core/micro/site/lib/controller/type/inventorylist/(:any)'] 	= "backend/InventoryController/inventoryList/$1";
$route['site/core/micro/site/lib/controller/type/deleteinventory/(:any)'] 	= "backend/InventoryController/deleteinventory/$1";
$route['site/core/micro/site/lib/controller/type/editinventory/(:any)'] 	= "backend/InventoryController/editinventory/$1";
$route['site/core/micro/site/lib/controller/type/inventoryupdate/(:any)'] 	= "backend/InventoryController/inventoryupdate/$1";
$route['site/core/micro/site/lib/controller/type/multiplinventoryedelete']		=	"backend/InventoryController/multipleDelete";

// Report Management
$route['site/core/micro/site/lib/controller/type/usedvoucher'] 			= "backend/UserController/usedVoucher";
$route['site/core/micro/site/lib/controller/type/usedvoucherlist']			= "backend/UserController/usedVoucherList";
$route['site/core/micro/site/lib/controller/type/registereduserlist']		= "backend/UserController/registeredUserList";
$route['site/core/micro/site/lib/controller/type/downloaduservoucherlist'] = "backend/UserController/downloadUserVoucherList";
$route['site/core/micro/site/lib/controller/type/downloaduserexcelbydatepicker']	= "backend/UserController/downloadUserExcelByDatePicker";
$route['site/core/micro/site/lib/controller/type/inventoryreport']			= "backend/InventoryReportController";
$route['site/core/micro/site/lib/controller/type/inventorydashboard']		= "backend/InventoryReportController/inventoryDashboard";
$route['site/core/micro/site/lib/controller/type/downloadexcelfromdate']	= "backend/InventoryReportController/downloadInventoryReportDatePicker";
$route['site/core/micro/site/lib/controller/type/downloadinventoryreportlist']	= "backend/InventoryReportController/downloadInventoryReportList";

$route['site/core/micro/site/lib/controller/type/downloadregisteredbydate'] = "backend/UserController/downloadRegisteredBYDate";

//Import CSV 
$route['site/core/micro/site/lib/controller/type/voucheruploadcsv']		= "backend/VoucherController/voucherUploadCSV";
$route['site/core/micro/site/lib/controller/type/inventoryupload']			= "backend/InventoryController/inventoryUpload";
$route['site/core/micro/site/lib/controller/type/inventoryuploadcsv']		= "backend/InventoryController/inventoryUploadCSV";

// Resend Code from site/core/micro/site/lib/controller/type
$route['site/core/micro/site/lib/controller/type/resendcode']				= "backend/ResendCodeController";
$route['site/core/micro/site/lib/controller/type/resendcodedetails']		= "backend/ResendCodeController/resendCodeDetails";
$route['site/core/micro/site/lib/controller/type/resendemailsfromsite/core/micro/site/lib/controller/type/(:any)/(:any)']	= "backend/ResendCodeController/resendEmailsFromsite/core/micro/site/lib/controller/type/$1/$2";

// Booking Mails
$route['site/core/micro/site/lib/controller/type/allbooking']				= "backend/BookingMailController";
$route['site/core/micro/site/lib/controller/type/allbooking/(:any)']		= "backend/BookingMailController/$1";
$route['site/core/micro/site/lib/controller/type/resendemailsite/core/micro/site/lib/controller/typebyid/(:any)']	= "backend/BookingMailController/resendEmailBysite/core/micro/site/lib/controller/type/$1";
$route['site/core/micro/site/lib/controller/type/resenddetails']			= "backend/BookingMailController/resendDetails";
$route['site/core/micro/site/lib/controller/type/resendmailbyvoucherid/(:any)/(:any)']= "backend/BookingMailController/resendMailByVoucherId/$1/$2";
$route['site/core/micro/site/lib/controller/type/resendemailid/(:any)/(:any)']	= "backend/BookingMailController/resendEmailId/$1/$2";

// Booking Success Mail
$route['site/core/micro/site/lib/controller/type/addbookingsuccessmailbyoffer'] 			= "backend/BookingSuccessController/addBookingSuccessMailbyoffer";
$route['site/core/micro/site/lib/controller/type/insertbookingsuccessmailbyoffer'] 		= "backend/BookingSuccessController/insertBookingSuccessMailbyoffer";
$route['site/core/micro/site/lib/controller/type/bookingsuccessmaillist'] = "backend/BookingSuccessController/bookingSuccessList";
$route['site/core/micro/site/lib/controller/type/bookingsuccessmaillist/(:any)'] 	= "backend/BookingSuccessController/bookingSuccessList/$1";
$route['site/core/micro/site/lib/controller/type/deletebookingsuccessmailbyoffer/(:any)'] 	= "backend/BookingSuccessController/deleteBookingSuccessMailbyoffer/$1";
$route['site/core/micro/site/lib/controller/type/editbookingsuccessmailbyoffer/(:any)'] 	= "backend/BookingSuccessController/editBookingSuccessMailbyoffer/$1";
$route['site/core/micro/site/lib/controller/type/updatebookingsuccessmailbyoffer/(:any)'] 	= "backend/BookingSuccessController/updateBookingSuccessMailbyoffer/$1";
$route['site/core/micro/site/lib/controller/type/bookingmailinsert'] = "backend/BookingSuccessController/bookingMailInsert";
$route['site/core/micro/site/lib/controller/type/registartionsuccessmail'] = "backend/BookingSuccessController/registrationSuccessMail";
$route['site/core/micro/site/lib/controller/type/registartionmailinsert'] = "backend/BookingSuccessController/registartionMailInsert";
$route['site/core/micro/site/lib/controller/type/uploadimgckeditor'] 		=	"backend/BookingSuccessController/uploadImgforBookingMail";

$route['site/core/micro/site/lib/controller/type/otpmessage'] = "backend/BookingSuccessController/otpMessage";
$route['site/core/micro/site/lib/controller/type/otpmessageinsert'] = "backend/BookingSuccessController/otpMessageInsert";


// User Used Resend Code
$route['site/core/micro/site/lib/controller/type/usedresendcode']			= "backend/UsedResendCodeMailController";
$route['site/core/micro/site/lib/controller/type/usedresendcode/(:any)']	= "backend/UsedResendCodeMailController/$1";
$route['site/core/micro/site/lib/controller/type/resendemail/(:any)']		= "backend/UsedResendCodeMailController/resendEmail/$1";

// Processing Inventory
$route['site/core/micro/site/lib/controller/type/processinginventory']			= "backend/ProcessingInventoryController";
$route['site/core/micro/site/lib/controller/type/processinginventory/(:any)/(:any)/(:any)']	= "backend/ProcessingInventoryController/$1/$2/$3";
$route['site/core/micro/site/lib/controller/type/resendemail/(:any)/(:any)/(:any)']		= "backend/ProcessingInventoryController/resendEmail/$1/$2/$3";

//frontend
$route[''] 								= 'MainController/index';
$route['login'] 						= 'MainController/userLogin';
$route['captcha']						= 'MainController/captch_refresh';
$route['logincheck'] 					= 'MainController/userLoginCheck';
$route['otp'] 							= 'MainController/otp';
$route['otp_check'] 					= 'MainController/otp_check';
$route['send_resend_otp/(:any)/(:any)'] = 'MainController/otp_Send_Resend_CRM/$1/$2';
$route['resendotp']						= "MainController/resendotpRequest";
$route['booking']						= 'MainController/booking';
$route['bookingdetailsformovies'] 	= 'MainController/bookingDetailsForMovies';
$route['bookingdetailsforoffer2'] 	= 'MainController/bookingDetailsForOffer2';
$route['bookingdetailsforoffer3'] 	= 'MainController/bookingDetailsForOffer3';
$route['bookingdetailsforoffer4'] 	= 'MainController/bookingDetailsForOffer4';
$route['bookingdetailsforoffer5'] 	= 'MainController/bookingDetailsForOffer5';
$route['bookingdetailsforoffer6accountnumber'] 	= 'MainController/bookingDetailsForOffer6AccountNumber';
$route['bookingdetailsforoffer6ewallets'] 	= 'MainController/bookingDetailsForOffer6Wallets';
$route['bookingdetailsforoffer6upi'] 	= 'MainController/bookingDetailsForOffer6UPI';
$route['bookingdetailsforhealthcheckup']= 'MainController/bookingDetailsForHealthCheckup';
$route['bookingdetailsformerchandise']= 'MainController/bookingDetailsForMerchandise';
$route['bookingdetailsforbogo'] 	= 'MainController/bookingDetailsForBogo';
$route['checkbogo'] 	= 'MainController/checkBookingDetailsForBogo';
$route['bookingdetailsforsalonoff'] 	= 'MainController/BookingDetailsForSalonOff';
$route['checksalonoff'] 	= 'MainController/checkBookingDetailsForSalon';
//$route['bookingdetailsforxoffsalonservices'] 	= 'MainController/bookingDetailsForXoffSalonServices';

$route['getvenue'] 						= 'MainController/getvenue';
$route['getcity'] 						= 'MainController/getcity';
$route['getmeal'] 						= 'MainController/getmeal';

// CIRCLE
$route['mobilerecharge']                = 'MainController/MobileRecharge';
// CIRCLE

$route['logout'] 						= 'MainController/logout';
//$route['terms-and-condition'] 			= "MainController/termCondition";
//$route['how-to-redeem'] 				= "MainController/howToRedeem";
//$route['privacy-policy'] 				= "MainController/privacyPolicy";
$route['thankyou'] 						= "MainController/thankyou";
$route['cancel-view'] 					= "MainController/cancelView";
$route['cancel'] 					= "MainController/cancel";
//$route['contact-us'] 					= "MainController/contact";
$route['success']						= "MainController/success";
$route['dateselect']					= "MainController/dateSelect";
$route['(:any)'] 						= "MainController/termCondition";


