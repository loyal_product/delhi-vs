<?php 
ob_start();
defined('BASEPATH') || exit('No direct script access allowed');

	class MY_Controller extends CI_Controller {
    public function __construct() {
      parent::__construct();
      $this->check();	
	  }
	  public function check()	{
	  	if ($this->uri->uri_string() !== 'login' && !$this->session->userdata('userType')) {
        redirect('site/core/micro/site/lib/controller/type/logout');
        exit();
    	}
		}
	}

?>

