<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class ResendCodeModel extends CI_Model {

  // constructor class
  function __construct() { 
      parent::__construct();
      $this->load->database();  
      $this->load->library('session'); 
  }

  public function getvoucherDetails($vouchercode,$registeredemailid,$registeredmobile){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.mobile, tbl_voucher_mng.registereddate, tbl_voucher_mng.name, tbl_voucher_mng.email, tbl_voucher_mng.vouchercode, tbl_voucher_used.reward_code, tbl_voucher_used.p_id, tbl_product.producttitle');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used','tbl_voucher_used.user_id = tbl_voucher_mng.id');
    $this->db->join('tbl_product','tbl_product.id = tbl_voucher_used.p_id');
    $this->db->where("tbl_voucher_mng.email LIKE '%$registeredemailid%' ");
    $this->db->where("tbl_voucher_mng.mobile LIKE '%$registeredmobile%' ");
    $this->db->where("tbl_voucher_mng.vouchercode LIKE '%$vouchercode%' ");
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getvoucherDetailsforSendMail($uid,$pid){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.mobile, tbl_voucher_mng.registereddate, tbl_voucher_mng.name, tbl_voucher_mng.email, tbl_voucher_mng.vouchercode, tbl_voucher_used.reward_code, tbl_product.producttitle, tbl_product.pro_mail, tbl_product.pro_mail_subject, tbl_product.productsms' );
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used','tbl_voucher_used.user_id = tbl_voucher_mng.id','inner');
    $this->db->join('tbl_product','tbl_product.id = tbl_voucher_used.p_id','inner');
    $this->db->where('tbl_voucher_used.user_id',$uid);
    $this->db->where('tbl_product.id',$pid);
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
}