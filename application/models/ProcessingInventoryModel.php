<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class ProcessingInventoryModel extends CI_Model {

  // constructor class
  function __construct() { 
      parent::__construct();
      $this->load->database();  
      $this->load->library('session'); 
  }

  public function getProcessInventoryDetails(){
    //$this->db->select('tbl_orders.id,tbl_orders.voucher_id,tbl_orders.product_id,tbl_orders.qty,tbl_orders.status,tbl_orders.created_on,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->select('COUNT("tbl_orders.id") as count');
    $this->db->from('tbl_orders');
    $this->db->join('tbl_voucher_mng', 'tbl_voucher_mng.id = tbl_orders.voucher_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_orders.product_id');
    $this->db->where('tbl_orders.status','processing');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getProcessInventoryDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_orders.id,tbl_orders.voucher_id,tbl_orders.product_id,tbl_orders.qty,tbl_orders.status,tbl_orders.created_on,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->from('tbl_orders');
    $this->db->join('tbl_voucher_mng', 'tbl_voucher_mng.id = tbl_orders.voucher_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_orders.product_id');
    $this->db->where('tbl_orders.status','processing');
    $this->db->limit($showRecordPerPage,$startFrom);
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();

    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getInventoryAvailable($pid,$qty){
    $this->db->select('*');
    $this->db->from('tbl_inventory');
    $this->db->where('tbl_inventory.prodid',$pid);
    $this->db->where('tbl_inventory.isused','0');
    $this->db->limit($qty);
    $this->db->order_by("tbl_inventory.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function getUsedResendCodeSendMail($pid,$qty){
    $this->db->select('tbl_inventory.prodid,tbl_inventory.rewardcode,tbl_product.title');
    $this->db->from('tbl_inventory');
    $this->db->where('tbl_inventory.prodid',$pid);
    $this->db->where('tbl_inventory.isused','0');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_inventory.prodid');
    $this->db->limit($qty);
    $this->db->order_by("tbl_inventory.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function updateRewardCodeById($prodId,$rewardCode){
    $data = array('isused'=>'1');
    $this->db->where('prodid',$prodId);
    $this->db->like('rewardcode',$rewardCode);
    return $this->db->update('tbl_inventory', $data);
  }

  public function updateOrderStatus($uid,$prodId) {
    $data = array('status'=>'completed');
    $this->db->where('voucher_id',$uid);
    $this->db->where('product_id',$prodId);
    $this->db->update('tbl_orders', $data);
  }

  public function getUserDetails($uid){
    $this->db->select('*');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('id',$uid);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }
  
}