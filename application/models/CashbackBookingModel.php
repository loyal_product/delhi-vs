<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class CashbackBookingModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  // All Content with where condition ----------
  public function getEditContent($id){
    $this->db->select('*');
    $this->db->from('tbl_cahbackform');
    $this->db->where('tbl_cahbackform.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  // Update function for update record in table-------    
  public function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  // Delete function for delete record in table----    
  public function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
   
  // All Content with all data join form----------
  public function getContentDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_cahbackform');
    $this->db->order_by("tbl_cahbackform.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getContentDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('*');
    $this->db->from('tbl_cahbackform');
    $this->db->order_by("tbl_cahbackform.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getProductList(){
    $this->db->select('id,title');
    $this->db->from('tbl_product');
    $this->db->where('pstatus','1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  
}