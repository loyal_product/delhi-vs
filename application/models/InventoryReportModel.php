<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class InventoryReportModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  public function registeredInventoryReportDetails(){
    //$this->db->select('tbl_voucher_used.reward_code,tbl_voucher_mng.registereddate,tbl_voucher_mng.created_on,tbl_voucher_mng.mobile,tbl_voucher_mng.email,tbl_voucher_mng.name,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->select('COUNT("tbl_voucher_mng.id") as count');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used', 'tbl_voucher_mng.id = tbl_voucher_used.user_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.p_id');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function registeredInventoryReportDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_voucher_used.reward_code,tbl_voucher_mng.registereddate,tbl_voucher_mng.created_on,tbl_voucher_mng.mobile,tbl_voucher_mng.email,tbl_voucher_mng.name,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used', 'tbl_voucher_mng.id = tbl_voucher_used.user_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.p_id');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result_array();
    }
    return NULL;
  }

  //download excel
   public function downloadUserExcel() {
    $this->db->select('tbl_voucher_used.reward_code,tbl_voucher_mng.registereddate,tbl_voucher_mng.created_on,tbl_voucher_mng.mobile,tbl_voucher_mng.email,tbl_voucher_mng.name,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used', 'tbl_voucher_mng.id = tbl_voucher_used.user_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.p_id');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  } 
  //download excel by date piker
  public function downloadUserExcelByDatePicker($from,$to) {
    $this->db->select('tbl_voucher_used.reward_code,tbl_voucher_mng.registereddate,tbl_voucher_mng.created_on,tbl_voucher_mng.mobile,tbl_voucher_mng.email,tbl_voucher_mng.name,tbl_voucher_mng.vouchercode,tbl_product.title');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used', 'tbl_voucher_mng.id = tbl_voucher_used.user_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.p_id');
    $this->db->where('tbl_voucher_mng.created_on >= ',$from);
    $this->db->where('tbl_voucher_mng.created_on <= ',$to);    
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function invntoryDashboard(){
    $this->db->select('id,sub_prod_name as producttitle,(SELECT COUNT(sub_prodid) from tbl_inventory WHERE tbl_inventory.sub_prodid = tbl_subproduct.id) as total, (SELECT COUNT(isused) from tbl_inventory WHERE tbl_inventory.isused = "0" AND tbl_inventory.sub_prodid = tbl_subproduct.id) as baltotal FROM tbl_subproduct');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

}