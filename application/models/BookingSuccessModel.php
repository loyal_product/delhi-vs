<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class BookingSuccessModel extends CI_Model {
  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$data) {
    return $this->db->insert($tbl,$data);
  }

  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  
  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }

  public function getBookingMailDetails() {
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getRegistrationMailDetails(){
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'regisrtation');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getBookingSuccesCount() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $this->db->order_by("tbl_bookingsuccessmail.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getBookingSuccesDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_bookingsuccessmail.id, tbl_bookingsuccessmail.prod_id, tbl_bookingsuccessmail.mail_subject, tbl_bookingsuccessmail.mail_body, tbl_bookingsuccessmail.sms, tbl_product.producttitle');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->join('tbl_product','tbl_product.id = tbl_bookingsuccessmail.prod_id');
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  // All banner with where condition ----------
  public function getEditBookingMailList($id){
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where('tbl_bookingsuccessmail.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getProductList(){
    $this->db->select('id,title,booking_form_id');
    $this->db->from('tbl_product');
    $this->db->where('pstatus','1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  public function getBookingFormId($bookingformid){
    $this->db->select('booking_form_id');
    $this->db->from('tbl_product');
    $this->db->where('id',$bookingformid);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }
  
  public function getOTPMessageDetails() {
    $this->db->select('*');
    $this->db->from('tbl_otpsms');
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }

}