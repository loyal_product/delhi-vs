<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class BlockUnblockModel extends CI_Model {
  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }
  
  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }    

  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }

  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }

  // All outlets with all data join form----------
  public function getBlockDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_blockunblock');
    $this->db->order_by("tbl_blockunblock.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getBlockDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('*');
    $this->db->from('tbl_blockunblock');
    $this->db->order_by("tbl_blockunblock.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result();
    }
    return NULL;
  }

  // All product with where condition all data join form----------
  public function getEditBlockData($id) {
    $this->db->select('*');
    $this->db->from('tbl_blockunblock');
    $this->db->where('tbl_blockunblock.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  // All outlets with all data join form----------
  public function getBlockMsgDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_blockunblockmsg');
    $this->db->order_by("tbl_blockunblockmsg.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getBlockMsgDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('*');
    $this->db->from('tbl_blockunblockmsg');
    $this->db->order_by("tbl_blockunblockmsg.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  // All product with where condition all data join form----------
  public function getEditBlockMsgData($id) {
    $this->db->select('*');
    $this->db->from('tbl_blockunblockmsg');
    $this->db->where('tbl_blockunblockmsg.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  
}