<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class InventoryModel extends CI_Model {
  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }
  
  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  public function insert_Encrypt($tbl,$parameter) {     
    $query = "INSERT INTO $tbl (sub_prodid,rewardcode,isused) values ('".$parameter['sub_prodid']."',AES_ENCRYPT('".$parameter['rewardcode']."','".$parameter['salt']."'),'".$parameter['isused']."')";
    return $this->db->query($query);
  }    

  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }

  public function update_Encrypt($tbl,$data,$id) {
    $query = "UPDATE $tbl SET sub_prodid='".$data['sub_prodid']."',rewardcode=AES_ENCRYPT('".$data['rewardcode']."','".$data['salt']."'),isused='".$data['isused']."' WHERE id='".$id."' ";
    return $this->db->query($query);
  }

  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }

  // All outlets with all data join form----------
  public function getInventoryDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_inventory');
    $this->db->order_by("tbl_inventory.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getInventoryDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('tbl_inventory.id,tbl_inventory.sub_prodid,AES_DECRYPT(rewardcode,"'.SALT.'") as rewardcode, tbl_inventory.isused, tbl_inventory.created_on, tbl_subproduct.sub_prod_name as producttitle');
    $this->db->from('tbl_inventory');
    $this->db->join('tbl_subproduct', 'tbl_subproduct.id = tbl_inventory.sub_prodid');
    $this->db->order_by("tbl_inventory.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result();
    }
    return NULL;
  }

  // All product with where condition all data join form----------
  public function getEditInventory($id){
    $this->db->select('id,sub_prodid,AES_DECRYPT(rewardcode,"'.SALT.'") as rewardcode ');
    $this->db->from('tbl_inventory');
    $this->db->where('tbl_inventory.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  /*
   * Insert members data into the database
   * @param $data data to be insert based on the passed parameters
   */
  public function insertCSV($vouch_tbl,$data = array()) {
    if(!empty($data)){        
      // Insert member data
      /*$insert = $this->db->insert($vouch_tbl, $data);
      
      // Return the status
      return $insert?$this->db->insert_id():false;*/
      $query = "INSERT INTO $vouch_tbl (sub_prodid,rewardcode,isused) values ('".$data['sub_prodid']."',AES_ENCRYPT('".$data['rewardcode']."','".$data['salt']."'),'".$data['isused']."')";
        return $this->db->query($query);
    }
    return false;
  }

  public function multiplDeleteByID($id,$tbl) {
    $this->db->where('id', $id);
    $this->db->delete($tbl);
  }

  public function getSubProductDetailsLimit(){
    $this->db->select('*');
    $this->db->from('tbl_subproduct');
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

}