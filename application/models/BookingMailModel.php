<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class BookingMailModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  public function getvoucherDetails($vouchercode){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.product_id, tbl_voucher_mng.name, AES_DECRYPT(tbl_voucher_mng.email,"'.SALT.'") as email, AES_DECRYPT(tbl_voucher_mng.mobile,"'.SALT.'") as  mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode,"'.SALT.'") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on,tbl_orders.voucher_id, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders','tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->where("AES_DECRYPT(tbl_voucher_mng.vouchercode, '".SALT."') LIKE '%$vouchercode%' ");
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function orderDetails($uid){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name, AES_DECRYPT(tbl_voucher_mng.email,"'.SALT.'") as email, AES_DECRYPT(tbl_voucher_mng.mobile,"'.SALT.'") as  mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode,"'.SALT.'") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on,tbl_orders.voucher_id, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders','tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->where("tbl_voucher_mng.id", $uid);
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getBookingMail($prod_id) {
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.prod_id", $prod_id);
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getRewardCode($uid){
    $this->db->select('*');
    $this->db->from('tbl_voucher_used');
    $this->db->where("user_id",$uid);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getBookingSenderEmail($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function bookingPriceQty(){
    $this->db->select('tbl_orders.voucher_id,tbl_orders.product_id,tbl_orders.qty,tbl_orders.created_on');
    $this->db->from('tbl_orders');
    $this->db->order_by("tbl_orders.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }


  public function bookingDetailss($uid,$pid){
    $this->db->select('*,tbl_orders.created_on as usedDate');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('tbl_voucher_mng.name !=', '');
    $this->db->where('tbl_voucher_mng.email !=', '');
    $this->db->join('tbl_orders','tbl_orders.voucher_id = tbl_voucher_mng.id','inner');
    $this->db->join('tbl_product','tbl_product.id = tbl_orders.product_id','inner');
    $this->db->where('tbl_product.id', $pid);
    $this->db->where('tbl_voucher_mng.id', $uid);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function bookingPriceQtys($uid){
    $this->db->select('tbl_orders.voucher_id,tbl_orders.qty,tbl_orders.created_on');
    $this->db->from('tbl_orders');
    $this->db->where('tbl_orders.voucher_id',$uid);
    $this->db->order_by("tbl_orders.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getvoucherDetailsforSendMail($uid){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.mobile, tbl_voucher_mng.registereddate, tbl_voucher_mng.name, tbl_voucher_mng.email, tbl_voucher_mng.vouchercode, tbl_voucher_mng.amount, tbl_voucher_used.reward_code');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_voucher_used','tbl_voucher_used.user_id = tbl_voucher_mng.id','inner');
    $this->db->where('tbl_voucher_used.user_id',$uid);
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
}