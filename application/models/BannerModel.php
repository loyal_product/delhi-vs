<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class BannerModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  public function logs($tbl_name,$value) {
    return $this->db->insert($tbl_name,$value);
  }
  // get function with table name --------------
  function get($tbl) {   
    $this->db->order_by('id','desc');
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  function getlist($where,$tbl) {   
    $this->db->where($where);
    $this->db->order_by('id','desc');
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }


// Edit function with table_name and where condition------------
  function edit($tbl,$where) {
   $this->db->where($where);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  function edit_whr($tbl,$whr_col,$whr_val) {
    $this->db->where_in($whr_col, $whr_val);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  } 

  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
  // Get record in where condition----------
   function get_where($tbl,$data) {
    $this->db->where($data);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  } 
   
  // All BANNER with all data join form----------
  public function getbannerDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_banner');
    $this->db->order_by("tbl_banner.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getbannerDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('*');
    $this->db->from('tbl_banner');
    $this->db->order_by("tbl_banner.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  // All banner with where condition ----------
  public function getEditBanner($id){
    $this->db->select('*');
    $this->db->from('tbl_banner');
    $this->db->where('tbl_banner.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getMetaVal($tbl) {
    $this->db->select('COUNT("meta_key") as count');
    $this->db->from($tbl);
    $this->db->where("meta_key LIKE '%logo%'");      
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getMetaUpdate($tbl, $data) {
    foreach ($data as $key => $value) {
      $this->db->set('meta_value', $value);
      $this->db->where("meta_key LIKE '%$key%'");
      $res = $this->db->update($tbl);
    }
    if ($res) {
      return true;
    } else {
      return false;
    }
  }

  public function getSiteLists($datas) {
    foreach ($datas as $key => $value) {
      $this->db->select('*');
      $this->db->from('tbl_settings');
      $this->db->where("meta_key",$value);    
      $query = $this->db->get();       
    }
    if ($query) {
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getProductList(){
    $this->db->select('id,title');
    $this->db->from('tbl_product');
    $this->db->where('pstatus','1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getbannerBookingDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_booking_banner_and_content');
    $this->db->where('type','banner');
    $this->db->order_by("id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getbannerBookingDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_booking_banner_and_content.id, tbl_booking_banner_and_content.content, tbl_product.title');
    $this->db->from('tbl_booking_banner_and_content');    
    $this->db->join('tbl_product','tbl_product.id = tbl_booking_banner_and_content.prod_id');
    $this->db->where('tbl_booking_banner_and_content.type','banner');
    $this->db->order_by("tbl_booking_banner_and_content.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  // All banner with where condition ----------
  public function getEditBannerBooking($id){
    $this->db->select('*');
    $this->db->from('tbl_booking_banner_and_content');
    $this->db->where('tbl_booking_banner_and_content.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

}