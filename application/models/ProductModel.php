<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class ProductModel extends CI_Model {

    // constructor class
    function __construct() { 
        parent::__construct();
        $this->load->database();  
        $this->load->library('session'); 
    }
    
    // Insert function with table_name , all data in array form------ 
    public function insert($tbl,$parameter) {
      return $this->db->insert($tbl,$parameter);
    }
    public function logs($tbl_name,$value) {
      return $this->db->insert($tbl_name,$value);
    }

    // get function with table name --------------
    function get($tbl) {   
        $this->db->order_by('id','desc');
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }
    function getlist($where,$tbl) {   
        $this->db->where($where);
        $this->db->order_by('id','desc');
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }


  // Edit function with table_name and where condition------------
    function edit($tbl,$where) {
       $this->db->where($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function outletsdue($tbl) {
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 


    function edit1($tbl,$where) {
        $this->db->where_in($where);
     //   $this->db->or_where_in($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 

    function edit_whr($tbl,$whr_col,$whr_val) {
        $this->db->where_in($whr_col, $whr_val);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function mobile_number($where,$tbl) {
        $this->db->where($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    // Update function for update record in table-------    
    function update($tbl,$data,$where) {
        $this->db->where($where);
        return $this->db->update($tbl, $data);
    }
    // Delete function for delete record in table----    
    function delete($tbl,$where) {
        $this->db->where($where);
        return   $this->db->delete($tbl);
    }
    // Get record in where condition----------
     function get_where($tbl,$data) {
        $this->db->where($data);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function get_outlat($tbl) {
        
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 

    // get record with where condition and order form
    function get_where_order($tbl,$data,$orderColumn,$orderValue) {
        $this->db->where($data);
        $this->db->order_by($orderColumn,$orderValue);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function get_where_lt($tbl,$data,$orderColumn,$orderValue) {
        $this->db->where($data);
        $this->db->order_by($orderColumn,$orderValue);
        $this->db->limit(1);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
   

    function getByOrder($tbl,$orderColumn,$orderValue)  {   
        $this->db->order_by($orderColumn,$orderValue);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }


    // Add record and got last insert id function-----
     public function addData($tbl,$parameter) {
        $this->db->insert($tbl,$parameter);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    public function getlastdate($tbl)
    {
        $this->db->select('MAX(createdDate) as lastupdate');
        $this->db->from($tbl);
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    // All outlets with all data join form----------
    public function getProductDetails() {
        $this->db->select('COUNT("id") as count');
         $this->db->from('tbl_product');
         $this->db->order_by("tbl_product.id",'DESC');
        $query = $this->db->get();
        if ($query) {
            return $query->row()->count;
        }
        return NULL;
    }

    public function getProductDetailsLimit($startFrom, $showRecordPerPage) {
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->order_by("tbl_product.id",'DESC');
        $this->db->limit($showRecordPerPage,$startFrom);
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    function get_othet_with_limit($tbl,$data,$orderColumn,$orderValue) {
      $this->db->where($data); 
      $this->db->order_by($orderColumn,$orderValue);
      $query = $this->db->get($tbl);
      if ($query) {
          return $query->result();
      }
      return NULL;
    } 
    // All product with where condition all data join form----------
    public function getEditProduct($id){
      $this->db->select('*');
      $this->db->from('tbl_product');
      $this->db->where('tbl_product.id',$id);
      $query = $this->db->get();
      if ($query) {
        return $query->result();
      }
      return NULL;
    }

  public function getLaguageCount() {
    $this->db->select('COUNT("id") as count');
     $this->db->from('tbl_language');
     $this->db->order_by("tbl_language.id",'DESC');
    $query = $this->db->get();
    if ($query) {
        return $query->row()->count;
    }
    return NULL;
  }

  public function getLanguageDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('*');
    $this->db->from('tbl_language');
    $this->db->order_by("tbl_language.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result();
    }
    return NULL;
  }

  public function getEditLanguage($id){
      $this->db->select('*');
      $this->db->from('tbl_language');
      $this->db->where('tbl_language.id',$id);
      $query = $this->db->get();
      if ($query) {
        return $query->result();
      }
      return NULL;
    }

  public function getSubProductCount() {
    $this->db->select('COUNT("id") as count');
     $this->db->from('tbl_subproduct');
     $this->db->order_by("tbl_subproduct.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getSubProductDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('tbl_subproduct.id, tbl_subproduct.sub_prod_name, tbl_subproduct.status, tbl_product.title');
    $this->db->from('tbl_subproduct');
    $this->db->join('tbl_product','tbl_product.id = tbl_subproduct.prod_id');
    $this->db->order_by("tbl_subproduct.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result();
    }
    return NULL;
  }

  public function getProductLists(){
    $this->db->select('id,title');
    $this->db->from('tbl_product');
    $this->db->where('tbl_product.pstatus','1');
    $this->db->order_by("tbl_product.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getLanguageLists(){
    $this->db->select('id,lang_title');
    $this->db->from('tbl_language');
    $this->db->where('tbl_language.status','1');
    $this->db->order_by("tbl_language.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getEditSubProduct($id){
    $this->db->select('*');
    $this->db->from('tbl_subproduct');
    $this->db->where('tbl_subproduct.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  public function insertCSV($tbl,$parameter = array()) {
    if(!empty($parameter)){        
      // Insert member data
      $insert = $this->db->insert($tbl, $parameter);      
      // Return the status
      return $insert?$this->db->insert_id():false;
    }
  }

  public function multiplDeactivateByID($id,$tbl) {
    $this->db->where('id', $id);
    $data = array('status' => '0', );
    return $this->db->update($tbl, $data);
  }
  
  public function getCashbackOfferLists(){
    $this->db->select('*');
    $this->db->from('tbl_cahbackform');
    $this->db->where('status','1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  public function getBookigFormId($pid){
    $this->db->select('booking_form_id');
    $this->db->from('tbl_product');
    $this->db->where('tbl_product.pstatus','1');
    $this->db->where('tbl_product.id',$pid);
    $this->db->order_by("tbl_product.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
}