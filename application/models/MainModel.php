<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class MainModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }
  
  function selectallfromId($tbl,$where) {        
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($where);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  function selectallfromIds($tbl,$where) {        
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($where);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  function get_where($tbl,$data) {        
    $this->db->where($data);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  function get_where_like($tbl,$vouchercod) {
    $this->db->select('id,product_id, name, AES_DECRYPT(email,"'.SALT.'") as email, AES_DECRYPT(mobile,"'.SALT.'") as mobile, AES_DECRYPT(vouchercode,"'.SALT.'") as vouchercode, startdate, expirydate, lastdatetobook,booking_status');
    $this->db->from($tbl);    
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$vouchercod%' ");
  
    $query = $this->db->get();
    // echo $this->db->last_query(); die;
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  function otp_insert($tbl,$data) {       
  
    return $this->db->insert($tbl,$data);
  }
  
  public function get_where_otp($tbl,$mobile,$user_otp) {        
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where("user_mobile",$mobile);
    $this->db->order_by('id','desc');
    $this->db->limit('1');
    //$this->db->where("user_otp",$user_otp);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function get_update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }

  public function update_Encrypt($tbl,$data,$id) {
    $query = "UPDATE $tbl SET name='".$data['name']."', email=AES_ENCRYPT('".$data['email']."','".$data['salt']."'), mobile=AES_ENCRYPT('".$data['mobile']."','".$data['salt']."'), registereddate='".$data['registereddate']."', ipaddress='".$data['ipaddress']."', devicetype='".$data['devicetype']."'  WHERE id='".$id."' ";
    return $this->db->query($query);
  }

  public function update_Time($u_id, $sumofseconds){
    $query = "UPDATE tbl_voucher_mng SET noofseconds='".$sumofseconds."'  WHERE id='".$u_id."' ";
    return $this->db->query($query);
  }

  function get_where_data($tbl,$where) {        
    $this->db->select('id,product_id, name, AES_DECRYPT(email,"'.SALT.'") as email, AES_DECRYPT(mobile,"'.SALT.'") as mobile, AES_DECRYPT(vouchercode,"'.SALT.'") as vouchercode, startdate, expirydate, lastdatetobook,registereddate,status');
    $this->db->from($tbl);
    $this->db->where("id",$where);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getMenuList(){
    $this->db->select('*');
    $this->db->from('tbl_menu');
    $this->db->where("status",'1');
    $this->db->order_by('sort_order','asc');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getBannerforPage($pagename){
    $this->db->select('*');
    $this->db->from('tbl_banner');
    $this->db->where("pagename",$pagename);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getBannerBookingforPage($prod_id){
    $this->db->select('*');
    $this->db->from('tbl_booking_banner_and_content');
    $this->db->where("prod_id",$prod_id);
    $this->db->where("type",'banner');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getFieldLabelwithoutpid(){
    $this->db->select('*');
    $this->db->from('tbl_fieldname');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getFieldLabel($prod_id){
    $this->db->select('*');
    $this->db->from('tbl_fieldname');
    $this->db->where("prod_id",$prod_id);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getContentforPage(){
    $this->db->select('*');
    $this->db->from('tbl_content');
    //$this->db->where("pagename",$page);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getContentBookingforPage($prod_id){
    $this->db->select('*');
    $this->db->from('tbl_booking_banner_and_content');
    $this->db->where("prod_id",$prod_id);
    $this->db->where("type",'content');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getBlockDates(){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key','chooseblockdate');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function insertMailMsg($data){
    return $this->db->insert('tbl_mail_msg_stored',$data);
  }

  public function getCaplimitTypes(){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key','setruleformobandemail');
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getCaplimitData($caplimitTypes){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key',$caplimitTypes);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getCountRegisterDatePerDay($mobileNum,$currentD) {
    $this->db->select('COUNT(registereddate) as count');
    $this->db->from('tbl_voucher_mng'); 
    $this->db->where("AES_DECRYPT(mobile, '".SALT."') LIKE '%$mobileNum%' ");
    $this->db->where("registereddate > ", $currentD.' 00:00:00');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    } else {
      return NULL;
    }
  }

  public function getCountRegisterDatePerWeek($mobile,$this_week_sd,$this_week_ed) {
    $this->db->select('COUNT(registereddate) as count');
    $this->db->from('tbl_voucher_mng'); 
    $this->db->where("AES_DECRYPT(mobile, '".SALT."') LIKE '%$mobile%' ");
    $this->db->where('registereddate >= ',$this_week_sd);
    $this->db->where('registereddate <= ',$this_week_ed);    
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    } else {
      return NULL;
    }
  }

  public function getCountRegisterDatePerMonth($mobile,$first_day_current_month,$last_day_current_month) {
    $this->db->select('COUNT(registereddate) as count');
    $this->db->from('tbl_voucher_mng'); 
    $this->db->where("AES_DECRYPT(mobile, '".SALT."') LIKE '%$mobile%' ");
    $this->db->where('registereddate >= ',$first_day_current_month);
    $this->db->where('registereddate <= ',$last_day_current_month); 
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    } else {
      return NULL;
    }
  }

  public function getCountMobileNumber($mobileNum) {
    $this->db->select('COUNT(mobile) as count');
    $this->db->from('tbl_voucher_mng'); 
    $this->db->where("AES_DECRYPT(mobile, '".SALT."') LIKE '%$mobileNum%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    } else {
      return NULL;
    }
  }

  public function getCountEmail($email){
    $this->db->select('COUNT(email) as count');
    $this->db->from('tbl_voucher_mng'); 
    $this->db->where("AES_DECRYPT(email, '".SALT."') LIKE '%$email%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    } else {
      return NULL;
    }
  }

  public function getErrorMsg($msg){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where("meta_key LIKE '%$msg%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getCityList($product_id){
    $this->db->select('*');
    $this->db->from('tbl_cities');
    $this->db->where("prod_id", $product_id);
    $this->db->where("status", '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getStateList($product_id){
    $this->db->select('*');
    $this->db->from('tbl_states');
    $this->db->where("prod_id", $product_id);
    $this->db->where("status", '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function fetch_Venue($cities_id){
    $this->db->where('cities_id', $cities_id);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('tbl_venues');
    $output = '<option value=""></option>';
    foreach($query->result() as $row)  {
     $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    return $output;
  }
  public function fetch_Meal($preferedArea1,$search){

    $this->db->select($search.'_veg_price,'.$search.'_nonveg_price,'.$search.'_male_price,'.$search.'_female_price');
    $this->db->from('tbl_venues');
    $this->db->where('id', $preferedArea1);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get();
   // echo $this->db->last_query(); die;
    $output = '<option value=""></option>';
    
   foreach($query->result() as $key=>$row)  {
      
      if($row->monday_veg_price != 0 && $row->monday_veg_price != ''){

        $output .= '<option value="'.$row->monday_veg_price.'">veg</option>';
      }
      if($row->monday_nonveg_price != 0 && $row->monday_nonveg_price != ''){
        $output .= '<option value="'.$row->monday_nonveg_price.'">Nonveg</option>';
      }
      if($row->tuesday_veg_price != 0 && $row->tuesday_veg_price != ''){

        $output .= '<option value="'.$row->tuesday_veg_price.'">veg</option>';
      }
      if($row->tuesday_nonveg_price != 0 && $row->tuesday_nonveg_price != ''){
        $output .= '<option value="'.$row->tuesday_nonveg_price.'">Nonveg</option>';
      }
      if($row->wednesday_veg_price != 0 && $row->wednesday_veg_price != ''){

        $output .= '<option value="'.$row->wednesday_veg_price.'">veg</option>';
      }
      if($row->wednesday_nonveg_price != 0 && $row->wednesday_nonveg_price != ''){
        $output .= '<option value="'.$row->wednesday_nonveg_price.'">Nonveg</option>';
      }
      if($row->thursday_veg_price != 0 && $row->thursday_veg_price != ''){

        $output .= '<option value="'.$row->thursday_veg_price.'">veg</option>';
      }
      if($row->thursday_nonveg_price != 0 && $row->thursday_nonveg_price != ''){
        $output .= '<option value="'.$row->thursday_nonveg_price.'">Nonveg</option>';
      }
      if($row->friday_veg_price != 0 && $row->friday_veg_price != ''){

        $output .= '<option value="'.$row->friday_veg_price.'">veg</option>';
      }
      if($row->friday_nonveg_price != 0 && $row->friday_nonveg_price != ''){
        $output .= '<option value="'.$row->friday_nonveg_price.'">Nonveg</option>';
      }
      if($row->saturday_veg_price != 0 && $row->saturday_veg_price != ''){

        $output .= '<option value="'.$row->saturday_veg_price.'">veg</option>';
      }
      if($row->saturday_nonveg_price != 0 && $row->saturday_nonveg_price != ''){
        $output .= '<option value="'.$row->saturday_nonveg_price.'">Nonveg</option>';
      }
      if($row->sunday_veg_price != 0 && $row->sunday_veg_price != ''){

        $output .= '<option value="'.$row->sunday_veg_price.'">veg</option>';
      }
      if($row->sunday_nonveg_price != 0 && $row->sunday_nonveg_price != ''){
        $output .= '<option value="'.$row->sunday_nonveg_price.'">Nonveg</option>';
      }
      if($row->monday_male_price != 0 && $row->monday_male_price != ''){

        $output .= '<option value="'.$row->monday_male_price.'">Male</option>';
      }
      if($row->monday_female_price != 0 && $row->monday_female_price != ''){
        $output .= '<option value="'.$row->monday_female_price.'">Female</option>';
      }
      if($row->tuesday_male_price != 0 && $row->tuesday_male_price != ''){

        $output .= '<option value="'.$row->tuesday_male_price.'">Male</option>';
      }
      if($row->tuesday_female_price != 0 && $row->tuesday_female_price != ''){
        $output .= '<option value="'.$row->tuesday_female_price.'">Female</option>';
      }
      if($row->wednesday_male_price != 0 && $row->wednesday_male_price != ''){

        $output .= '<option value="'.$row->wednesday_male_price.'">Male</option>';
      }
      if($row->wednesday_female_price != 0 && $row->wednesday_female_price != ''){
        $output .= '<option value="'.$row->wednesday_female_price.'">Female</option>';
      }
      if($row->thursday_male_price != 0 && $row->thursday_male_price != ''){

        $output .= '<option value="'.$row->thursday_male_price.'">Male</option>';
      }
      if($row->thursday_female_price != 0 && $row->thursday_female_price != ''){
        $output .= '<option value="'.$row->thursday_female_price.'">Female</option>';
      }
      if($row->friday_male_price != 0 && $row->friday_male_price != ''){

        $output .= '<option value="'.$row->friday_male_price.'">Male</option>';
      }
      if($row->friday_female_price != 0 && $row->friday_female_price != ''){
        $output .= '<option value="'.$row->friday_female_price.'">Female</option>';
      }
      if($row->saturday_male_price != 0 && $row->saturday_male_price != ''){

        $output .= '<option value="'.$row->saturday_male_price.'">Male</option>';
      }
      if($row->saturday_female_price != 0 && $row->saturday_female_price != ''){
        $output .= '<option value="'.$row->saturday_female_price.'">Female</option>';
      }
      if($row->sunday_male_price != 0 && $row->sunday_male_price != ''){

        $output .= '<option value="'.$row->sunday_male_price.'">Male</option>';
      }
      if($row->sunday_female_price != 0 && $row->sunday_female_price != ''){
        $output .= '<option value="'.$row->sunday_female_price.'">Female</option>';
      }
      
      
    }
    return $output;
  }

  public function fetch_City($state_id){
    $this->db->where('state_id', $state_id);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('tbl_cities');
    $output = '<option value=""></option>';
    foreach($query->result() as $row)  {
     $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    return $output;
  }

  public function getCityEnable($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }
  public function getPeopleEnable($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }
  public function getDiscountEnable($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getStateEnable($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getSubProductList($product_id) {
    
    $this->db->select('id, prod_id ,sub_prod_name as producttitle, status');
    $this->db->from('tbl_subproduct');

    $this->db->where('prod_id', $product_id);
    $this->db->where('status', '1');
    $this->db->order_by('producttitle','asc');

    $query = $this->db->get();
    //echo $this->db->last_query(); die;
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
  public function getMealList($product_id) {
    
    $this->db->select('id, prod_id ,sub_prod_name as producttitle, status');
    $this->db->from('tbl_subproduct');
    $this->db->where('prod_id', $product_id);
    $this->db->where('status', '1');
    $this->db->order_by('producttitle','asc');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getDaySlot() {
    $this->db->select('*');
    $this->db->from('tbl_day_timeslot');
    $this->db->where('id', '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
  
  public function insertOrder($tbl,$parameter) {

    return $this->db->insert($tbl,$parameter);
    //echo $this->db->last_query(); die;
  }

  function insertOrderByID($tbl,$parameter){
   $this->db->insert($tbl, $parameter);
   $insert_id = $this->db->insert_id();
   return  $insert_id;
  }

  public function getSingleRowFromSettings($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function orderDetails($uid){
    $this->db->select('tbl_orders.voucher_id, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime');
    $this->db->from('tbl_orders');
    //$this->db->join('tbl_orders','tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->where("tbl_orders.voucher_id", $uid);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getBookingMail($prod_id) {
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.prod_id", $prod_id);
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getRegistrationMail(){
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'regisrtation');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  /*public function getBlocklist(){
    $this->db->select('*');
    $this->db->from('tbl_blockunblock');
    $this->db->where("status", '0');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }*/
  
  public function getBlocklist($vouchercode,$email,$mobile,$current_ip){
    $query = $this->db->query('SELECT * FROM `tbl_blockunblock` WHERE `status` = "0" AND (`blockdata` = "'.$vouchercode.'" OR `blockdata` = "'.$email.'" OR `blockdata` = "'.$mobile.'" OR `blockdata` = "'.$current_ip.'")');
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getBookingSenderEmail($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getCalenderBlock($prod_id){
    $this->db->select('blocksdate');
    $this->db->from('tbl_day_timeslot');
    $this->db->where('prod_id', $prod_id);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getBlockMsg(){
    $this->db->select('*');
    $this->db->from('tbl_blockunblockmsg');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getRewardCode($preferedoffer,$voucher_id){
      // Update Query Starts here------------------------
      $where = array(
                      'sub_prodid' => $preferedoffer,
                      'isused'     => '0',
                      'voucher_id' => '0',
                      'reward_mapped'  => '0',
                    );
      $data = array('isused' => '1' ,'voucher_id' => $voucher_id);
      $this->db->where($where);
      $this->db->where('AES_DECRYPT(rewardcode,"'.SALT.'") NOT IN (SELECT reward_code from tbl_voucher_used WHERE sub_p_id = '.$preferedoffer.')', NULL, FALSE);
      $this->db->limit(1);
      $this->db->order_by("id",'DESC');
      $this->db->update('tbl_inventory', $data); 
      // Update Query Ends here------------------------

    //--------------------------------------------------------//

      // Select Query Starts here------------------------
      $this->db->select('id, voucher_id,sub_prodid, AES_DECRYPT(rewardcode,"'.SALT.'") as rewardcode, isused');
      $this->db->from('tbl_inventory');
      $this->db->where('voucher_id',$voucher_id);
      $this->db->where('reward_mapped', '0');
      $this->db->limit(1);
      $this->db->order_by("id",'ASC');
      $query = $this->db->get();
      // Select Query Ends here------------------------

      if ($query) {
        return $query->result_array();
      }else{
        return NULL;
      }
    }

  public function updateProductOrder($where,$data) {
    $this->db->where($where);
    return $this->db->update('tbl_orders', $data);
  }
  
  public function updateRewardCodeById($inventoryId){
    $data = array('reward_mapped'=>'1');
    $this->db->where('id',$inventoryId);
    return $this->db->update('tbl_inventory', $data);
  }
  public function updateVoucherCodeById($user_id){
    $data = array('booking_status'=>'1');
    $this->db->where('id',$user_id);
    return $this->db->update('tbl_voucher_mng', $data);
    //echo $this->db->last_query(); die;
  }
  

  public function getBookingFormById($prod_id){
    $this->db->select('*');
    $this->db->from('tbl_subproduct');
    $this->db->where("id",$prod_id);
    $query = $this->db->get();

    foreach($query->result_array() as $k=>$val){
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->where("id",$val['prod_id']);
        $q = $this->db->get();
    }
    if ($q) {
      return $q->row();
    } else {
      return NULL;
    }
  }

  public function fetch_Timeslot($offerID,$lowerDay){
    $this->db->select('*');
    $this->db->from('tbl_day_timeslot');
    $this->db->where("prod_id",$offerID);
    $query = $this->db->get();
    $output = '<option value=""></option>';
    foreach ($query->row() as $key => $value) {
      if ($key==$lowerDay) {
        $timeSlotData = json_decode($value, true);
        $timeSlotArr = explode(",", $timeSlotData);
        foreach($timeSlotArr as $val) {
          $output .= '<option value="'.$val.'">'.$val.'</option>';
        }
      }
    }     
    return $output;
  }

  public function getDisableDates($offerID){
    $this->db->select('setminimumnumberdisable');
    $this->db->from('tbl_day_timeslot');
    $this->db->where("prod_id",$offerID);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }

  public function getEnableDates($offerID){
    $this->db->select('setmaximumnumberofenable');
    $this->db->from('tbl_day_timeslot');
    $this->db->where("prod_id",$offerID);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }
  
  public function getMenuDetailss($last_index_page_url) {
    $this->db->select('*');
    $this->db->from('tbl_menu');
    $this->db->join('tbl_content','tbl_content.pagename = tbl_menu.menulink');
    //$this->db->join('tbl_banner','tbl_banner.pagename = tbl_menu.menulink');
    $this->db->where("tbl_menu.menulink LIKE '%$last_index_page_url%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }

  public function getBannerDetailss($last_index_page_url) {
    $this->db->select('*');
    $this->db->from('tbl_menu');
    //$this->db->join('tbl_content','tbl_content.pagename = tbl_menu.menulink');
    $this->db->join('tbl_banner','tbl_banner.pagename = tbl_menu.menulink');
    $this->db->where("tbl_menu.menulink LIKE '%$last_index_page_url%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }
  
  public function getProductName($preferedoffer){
    $this->db->select('sub_prod_name');
    $this->db->from('tbl_subproduct');
    $this->db->where("tbl_subproduct.id" ,$preferedoffer);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }
  
  public function getBookingMailOnlyId5($prod_id,$prodName) {
    $this->db->select('*');
    $this->db->from('tbl_bookingsuccessmail');
    $this->db->where("tbl_bookingsuccessmail.prod_id", $prod_id);
    $this->db->where("tbl_bookingsuccessmail.new_mail_type LIKE  '%$prodName%' ");
    $this->db->where("tbl_bookingsuccessmail.mail_type", 'booking');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  public function getAccountsCounts($accountnumber){
    $this->db->select('COUNT(accountnumber) as acccount');
    $this->db->from('tbl_orders'); 
    $this->db->where("AES_DECRYPT(accountnumber, '".SALT."') LIKE '%$accountnumber%' ");
    //$this->db->where("accountnumber LIKE '%$accountnumber%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row()->acccount;
    } else {
      return NULL;
    }
  }

  public function getAccountsLimitfromSettings($meta_val){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key',$meta_val);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getUPICounts($upivpn){
    $this->db->select('COUNT(upivpn) as upicount');
    $this->db->from('tbl_orders'); 
    $this->db->where("AES_DECRYPT(upivpn, '".SALT."') LIKE '%$upivpn%' ");
    //$this->db->where("upivpn LIKE '%$upivpn%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row()->upicount;
    } else {
      return NULL;
    }
  }

  public function getEwalletsMobileCount($walletsmobile){
    $this->db->select('COUNT(walletsmobile) as walletscount');
    $this->db->from('tbl_orders'); 
    //$this->db->where("walletsmobile LIKE '%$walletsmobile%' ");
    $this->db->where("AES_DECRYPT(walletsmobile, '".SALT."') LIKE '%$walletsmobile%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row()->walletscount;
    } else {
      return NULL;
    }
  }

  public function getCityListEnableDisable(){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key','cityEnableDisable');
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function getCityListAllForRegistration(){
    $this->db->select('cityid');
    $this->db->from('tbl_product');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
    
  public function getOTPMessage(){
    $this->db->select('*');
    $this->db->from('tbl_otpsms');
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }
  
  public function getBookingForm6Banner($selectedoffer){
    $this->db->select('cashbackid');
    $this->db->from('tbl_product');
    $this->db->where("id" ,$selectedoffer);
    $query = $this->db->get();
    $cashbachid = $query->row()->cashbackid;
    if (!empty($cashbachid)) {
      $sqlQuery = $this->db->query("SELECT * FROM tbl_cahbackform WHERE id ='".$cashbachid."' ");
      return $sqlQuery->result_array();
    } else{
      return NULL;
    }   
  }

  public function getWalletsTypesByID($preferedoffer) {
    $this->db->select('*');
    $this->db->from('tbl_subproduct');
    $this->db->where("id" ,$preferedoffer);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }   
  }

  public function getCampaignIDforCashbackAPI($campaignid){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key',$campaignid);
    $query = $this->db->get();
    if ($query) {
      return $query->row()->meta_value;
    } else{
      return NULL;
    }
  }

  public function getDynamicForm($pgname){
    $this->db->select('*');
    $this->db->from('tbl_dynamicfield');
    $this->db->where('pagename',$pgname);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }

  public function updateExtraFields($newdata,$vid){
    $where = array('id' => $vid, );
    $this->db->where($where);
    return $this->db->update('tbl_voucher_mng', $newdata);
  }
  
  public function orderDetailsForCRM($uid) {
    $this->db->select('tbl_voucher_mng.product_id,tbl_voucher_mng.name, AES_DECRYPT(tbl_voucher_mng.email,"'.SALT.'") as email, AES_DECRYPT(tbl_voucher_mng.mobile,"'.SALT.'") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode,"'.SALT.'") as vouchercode, tbl_orders.voucher_id, tbl_orders.city, (SELECT crm_city_id from tbl_cities Where tbl_cities.id = tbl_orders.city) as crm_city_id , tbl_orders.preferedArea1, tbl_orders.preferedArea2,  (SELECT crm_venue_id from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as crm_venue_id1, ( SELECT crm_venue_id from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as crm_venue_id2, (SELECT crm_prod_id from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as crm_prod_id1, (SELECT crm_prod_id from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as crm_prod_id2, tbl_orders.preferedMovie1, tbl_orders.preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime');
    $this->db->from('tbl_orders');
    $this->db->join('tbl_voucher_mng','tbl_voucher_mng.id = tbl_orders.voucher_id');
    $this->db->where("tbl_orders.voucher_id", $uid);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }

  public function getOfferIdFroCRM($product_id) {
    $this->db->select('id,crm_offer_id');
    $this->db->from('tbl_product');
    $this->db->where("id", $product_id);
    $query = $this->db->get();
    //echo $this->db->last_query(); die;
    if($query) {
      return $query->row();
    }
    return NULL;
  }
  //sonam

  public function getSalonDiscountType(){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key','setsalondiscount');
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }
  
  public function getSalonDiscountEnable($data){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', $data);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }
  //sonam
  
  public function getEmailIsRequired(){
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', 'emailrequireornotforregistration');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->meta_value;
    } else{
      return NULL;
    }
  }
  
  public function insertCashbackOrder($tbl,$parameter) {
    if($parameter['accountnumber'] != '') {
      $query = "INSERT INTO $tbl (voucher_id, prod_id, city, preferedArea1, preferedArea2, preferedMovie1, preferedMovie2, prefereddate1, prefereddate2, preferedTime1, preferedTime2, name, accountnumber, ifsccode, cashbackamount, noofseconds, api_status) values ('".$parameter['voucher_id']."', '".$parameter['prod_id']."', '".$parameter['city']."', '".$parameter['preferedArea1']."', '".$parameter['preferedArea2']."', '".$parameter['preferedMovie1']."', '".$parameter['preferedMovie2']."', '".$parameter['prefereddate1']."', '".$parameter['prefereddate2']."', '".$parameter['preferedTime1']."', '".$parameter['preferedTime2']."', AES_ENCRYPT('".$parameter['name']."','".$parameter['enc_salt']."'), AES_ENCRYPT('".$parameter['accountnumber']."','".$parameter['enc_salt']."'), '".$parameter['ifsccode']."','".$parameter['cashbackamount']."', '".$parameter['noofseconds']."', '".$parameter['api_status']."')";
    } elseif ($parameter['upivpn'] != '') {
      $query = "INSERT INTO $tbl (voucher_id, prod_id, city, preferedArea1, preferedArea2, preferedMovie1, preferedMovie2, prefereddate1, prefereddate2, preferedTime1, preferedTime2, cashbackamount, upivpn, noofseconds, api_status) values ('".$parameter['voucher_id']."', '".$parameter['prod_id']."', '".$parameter['city']."', '".$parameter['preferedArea1']."', '".$parameter['preferedArea2']."', '".$parameter['preferedMovie1']."', '".$parameter['preferedMovie2']."', '".$parameter['prefereddate1']."', '".$parameter['prefereddate2']."', '".$parameter['preferedTime1']."', '".$parameter['preferedTime2']."', '".$parameter['cashbackamount']."', AES_ENCRYPT('".$parameter['upivpn']."','".$parameter['enc_salt']."'), '".$parameter['noofseconds']."', '".$parameter['api_status']."')";
    } elseif ($parameter['walletsmobile'] != '') {
      $query = "INSERT INTO $tbl (voucher_id, prod_id, city, preferedArea1, preferedArea2, preferedMovie1, preferedMovie2, prefereddate1, prefereddate2, preferedTime1, preferedTime2, cashbackamount, walletsmobile, noofseconds, api_status) values ('".$parameter['voucher_id']."', '".$parameter['prod_id']."', '".$parameter['city']."', '".$parameter['preferedArea1']."', '".$parameter['preferedArea2']."', '".$parameter['preferedMovie1']."', '".$parameter['preferedMovie2']."', '".$parameter['prefereddate1']."', '".$parameter['prefereddate2']."', '".$parameter['preferedTime1']."', '".$parameter['preferedTime2']."', '".$parameter['cashbackamount']."', AES_ENCRYPT('".$parameter['walletsmobile']."','".$parameter['enc_salt']."'), '".$parameter['noofseconds']."', '".$parameter['api_status']."')";
    }
    return $this->db->query($query);
  }
  //Added By Vipin
  
  public function getIDs($meta_key) {
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key',$meta_key);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else{
      return NULL;
    }
  }
  
  // CIRCLE
    public function getCircleList($product_id){
      $this->db->select('*');
      $this->db->from('tbl_circle');
      // $this->db->where("prod_id", $product_id);
      $this->db->where("status", '1');
      $query = $this->db->get();
      if ($query) {
        return $query->result_array();
      } else{
        return NULL;
      }
    }
  // CIRCLE
  
  public function getUniqueVoucherId($u_id) {
    $this->db->select('COUNT(voucher_id) as count');
    $this->db->from('tbl_orders');
    $this->db->where("voucher_id", $u_id);
    $query = $this->db->get();
    if($query) {
      return $query->row()->count;
    }
    return NULL;
  }
  
  // Delete function for delete record in table----    
  function deletebyVoucherID($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
  
  public function updateBookingStatusByVid($vid) {
    $data = array('booking_status'=>'0');
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$vid%' ");
    //return $this->db->update('tbl_voucher_mng', $data);
    $this->db->update('tbl_voucher_mng', $data);

    $this->db->select('id');
    $this->db->from('tbl_voucher_mng');
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$vid%' ");
    $query = $this->db->get();

    if($query) {
      $vid = $query->row()->id;
      $where = array('voucher_id' => $vid);
      $this->db->where($where);
      return $this->db->delete('tbl_orders');
    }
    return NULL;
  }

  public function getOTPBlockedTimeByMobile($mobile) {
    $this->db->select('*');
    $this->db->from('tbl_loginattempts');
    $this->db->where("mobile", $mobile);
    $this->db->order_by('id','desc');
    $this->db->limit('1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
  
  public function update_login_attempts($data,$where) {
    $this->db->where($where);
    $this->db->order_by('id','desc');
    $this->db->limit('1');
    $this->db->update('tbl_loginattempts', $data);
  }

}