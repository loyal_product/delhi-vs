<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class MyModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  public function getAdminLogo() {
    $this->db->select('meta_value');
    $this->db->from('tbl_settings');
    $this->db->where("meta_key",'logoimg');      
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }
  
}