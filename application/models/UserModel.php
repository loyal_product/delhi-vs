<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class UserModel extends CI_Model {

    // constructor class
    function __construct() { 
        parent::__construct();
        $this->load->database();  
        $this->load->library('session'); 
    }
  
    // Insert function with table_name , all data in array form------ 
    public function insert($tbl,$parameter) {
        return $this->db->insert($tbl,$parameter);
    }
    public function logs($tbl_name,$value) {
        return $this->db->insert($tbl_name,$value);
    }
    // get function with table name --------------
    function get($tbl) {   
        $this->db->order_by('id','desc');
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }
    function getlist($where,$tbl) {   
        $this->db->where($where);
        $this->db->order_by('id','desc');
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }


 // Edit function with table_name and where condition------------
    function edit($tbl,$where) 
    {
       $this->db->where($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function outletsdue($tbl) 
    {
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 


function edit1($tbl,$where) 
    {
        $this->db->where_in($where);
     //   $this->db->or_where_in($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 

    function edit_whr($tbl,$whr_col,$whr_val)
    {
        $this->db->where_in($whr_col, $whr_val);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function mobile_number($where,$tbl) {
        $this->db->where($where);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    // Update function for update record in table-------    
    function update($tbl,$data,$where) {
        $this->db->where($where);
        return $this->db->update($tbl, $data);
    }
    // Delete function for delete record in table----    
    function delete($tbl,$where) {
        $this->db->where($where);
        return   $this->db->delete($tbl);
    }
    // Get record in where condition----------
     function get_where($tbl,$data) {
        $this->db->where($data);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function get_outlat($tbl) {
        
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 

    // get record with where condition and order form
    function get_where_order($tbl,$data,$orderColumn,$orderValue) {
        $this->db->where($data);
        $this->db->order_by($orderColumn,$orderValue);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    function get_where_lt($tbl,$data,$orderColumn,$orderValue) {
        $this->db->where($data);
        $this->db->order_by($orderColumn,$orderValue);
        $this->db->limit(1);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
   

    function getByOrder($tbl,$orderColumn,$orderValue)  {   
        $this->db->order_by($orderColumn,$orderValue);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    }


    // Add record and got last insert id function-----
     public function addData($tbl,$parameter)
    {
        $this->db->insert($tbl,$parameter);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }
    public function getlastdate($tbl)
    {
        $this->db->select('MAX(createdDate) as lastupdate');
        $this->db->from($tbl);
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    // All outlets with all data join form----------
    public function getUserDetails() {
        $this->db->select('tbl_user.id, tbl_user.name, tbl_user.emailAddress, tbl_user.mobile, tbl_user.status');
         $this->db->from('tbl_user');
         //$this->db->join('tbl_outlets', 'tbl_user.id = tbl_outlets.userId');
         //$this->db->join('tbl_userType', 'tbl_user.userType= tbl_userType.id');
         $this->db->order_by("tbl_user.id",'DESC');
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    public function getUserDetailsLimit($startFrom, $showRecordPerPage){
        $this->db->select('tbl_user.id, tbl_user.name, tbl_user.emailAddress, tbl_user.mobile, tbl_user.status');
         $this->db->from('tbl_user');
         $this->db->order_by("tbl_user.id",'DESC');
         $this->db->where("tbl_user.userType","2");
         $this->db->limit($showRecordPerPage,$startFrom);
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }
        return NULL;
    }

    function get_othet_with_limit($tbl,$data,$orderColumn,$orderValue) {
        $this->db->where($data); 
        $this->db->order_by($orderColumn,$orderValue);
        $query = $this->db->get($tbl);
        if ($query) {
            return $query->result();
        }
        return NULL;
    } 
    // All user edit with where condition all data join form----------
    public function getEditUser($id){
      $this->db->select('*');
      $this->db->from('tbl_user');
      $this->db->where('tbl_user.id',$id);
      $query = $this->db->get();
      if ($query) {
          return $query->result();
      }
      return NULL;
    }
    
    public function registeredUserCount(){
      $query = $this->db->query('SELECT * FROM tbl_voucher_mng WHERE name !="" AND email != "" AND mobile !="" ');
      if ($query) {
        return $query->result_array();
      }
      return NULL;
    }


    public function usedVoucherDetailsShow10Records() {
      $this->db->select('tbl_voucher_mng.id,tbl_voucher_mng.name,AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email,AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode,tbl_voucher_mng.amount,tbl_voucher_mng.expirydate, tbl_orders.voucher_id, tbl_orders.product_id,tbl_orders.qty,tbl_orders.price,tbl_orders.created_on');
      $this->db->from('tbl_voucher_mng');
      $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
      $this->db->limit(20);
      $query = $this->db->get();
      if ($query) {
          return $query->result_array();
      }
      return NULL;
    }

    public function usedVoucherDetails() {
      $this->db->select('tbl_voucher_mng.id,tbl_voucher_mng.name,AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email,AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode,tbl_voucher_mng.amount,tbl_voucher_mng.expirydate,tbl_voucher_mng.registereddate,tbl_voucher_mng.created_on, tbl_voucher_used.user_id, tbl_voucher_used.sub_p_id,tbl_voucher_used.reward_code,tbl_voucher_used.reward_code,tbl_product.id,tbl_product.title,tbl_product.price');
      $this->db->from('tbl_voucher_mng');
      $this->db->join('tbl_voucher_used', 'tbl_voucher_used.user_id = tbl_voucher_mng.id');
      $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.sub_p_id');
      //$this->db->where('tbl_voucher_mng.id','60');
      $this->db->order_by("tbl_voucher_mng.id",'ASC');
      $query = $this->db->get();
      if ($query) {
        return $query->result_array();
      }
      return NULL;
    }

    public function usedVoucherDetailsCount() {
      $this->db->select('COUNT("id") as count');
      $this->db->from('tbl_voucher_mng');
      $this->db->join('tbl_voucher_used', 'tbl_voucher_used.user_id = tbl_voucher_mng.id');
      $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.sub_p_id');
      //$this->db->where('tbl_voucher_mng.id','60');
      $this->db->order_by("tbl_voucher_mng.id",'ASC');
      $query = $this->db->get();
      if ($query) {
        return $query->row()->count;
      }
      return NULL;
    }
    
    public function usedVoucherDetailsLimit($startFrom, $showRecordPerPage){
      $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name as cname, AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email, AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime, tbl_orders.noofseconds, (SELECT title from tbl_product WHERE tbl_product.id = tbl_voucher_mng.product_id) as title, AES_DECRYPT(tbl_orders.name, "ds234dfe3234de") as name, AES_DECRYPT(tbl_orders.accountnumber, "ds234dfe3234de") as accountnumber, tbl_orders.ifsccode, tbl_orders.cashbackamount, AES_DECRYPT(tbl_orders.upivpn, "ds234dfe3234de") as upivpn, AES_DECRYPT(tbl_orders.walletsmobile, "ds234dfe3234de") as walletsmobile, tbl_orders.api_status,(SELECT name from tbl_states Where tbl_states.id = tbl_orders.state) as state,tbl_orders.address1,tbl_orders.address2,tbl_orders.address3,tbl_orders.landmark,tbl_orders.pin_code,tbl_orders.alternate_phone_no');
      $this->db->from('tbl_voucher_mng');
      $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
      
      $this->db->order_by("tbl_orders.id",'ASC');      
      $this->db->limit($showRecordPerPage,$startFrom);
      $query = $this->db->get();
      //echo $this->db->last_query();
      //die;
      if ($query) {
        return $query->result_array();
      }
      return NULL;
    }


  public function registeredUserListDetails(){
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('tbl_voucher_mng.registereddate !=', " ");
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }
  public function registeredUserListDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name as cname, AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email, AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, tbl_voucher_mng.noofseconds, tbl_product.title');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_mng.product_id');
    $this->db->where('tbl_voucher_mng.registereddate !=', " ");
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  //download excel
   public function downloadUserExcel() {
    //$this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name, tbl_voucher_mng.email, tbl_voucher_mng.mobile, tbl_voucher_mng.vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime, tbl_orders.noofseconds, tbl_orders.name, tbl_orders.accountnumber, tbl_orders.ifsccode, tbl_orders.cashbackamount, tbl_orders.upivpn, tbl_orders.walletsmobile, tbl_orders.api_status,(SELECT name from tbl_states Where tbl_states.id = tbl_orders.state) as state,tbl_orders.address1,tbl_orders.address2,tbl_orders.address3,tbl_orders.landmark,tbl_orders.pin_code,tbl_orders.alternate_phone_no');
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name as cname, AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email, AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime, tbl_orders.noofseconds, (SELECT title from tbl_product WHERE tbl_product.id = tbl_voucher_mng.product_id) as title, AES_DECRYPT(tbl_orders.name, "ds234dfe3234de") as name, AES_DECRYPT(tbl_orders.accountnumber, "ds234dfe3234de") as accountnumber, tbl_orders.ifsccode, tbl_orders.cashbackamount, AES_DECRYPT(tbl_orders.upivpn, "ds234dfe3234de") as upivpn, AES_DECRYPT(tbl_orders.walletsmobile, "ds234dfe3234de") as walletsmobile, tbl_orders.api_status,(SELECT name from tbl_states Where tbl_states.id = tbl_orders.state) as state,tbl_orders.address1,tbl_orders.address2,tbl_orders.address3,tbl_orders.landmark,tbl_orders.pin_code,tbl_orders.alternate_phone_no');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->order_by("tbl_orders.id",'ASC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  } 
  //download excel by date piker
  public function downloadUserExcelByDatePicker($from,$to) {
    //$this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name, tbl_voucher_mng.email, tbl_voucher_mng.mobile, tbl_voucher_mng.vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime, tbl_orders.noofseconds, tbl_orders.name, tbl_orders.accountnumber, tbl_orders.ifsccode, tbl_orders.cashbackamount, tbl_orders.upivpn, tbl_orders.walletsmobile, tbl_orders.api_status');
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name as cname, AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email, AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, (SELECT name from tbl_cities Where tbl_cities.id = tbl_orders.city) as city , (SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea1) as preferedArea1, ( SELECT name from tbl_venues Where tbl_venues.id = tbl_orders.preferedArea2) as preferedArea2, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie1) as preferedMovie1, (SELECT sub_prod_name from tbl_subproduct Where tbl_subproduct.id = tbl_orders.preferedMovie2) as preferedMovie2, tbl_orders.prefereddate1, tbl_orders.prefereddate2, tbl_orders.preferedTime1, tbl_orders.preferedTime2, tbl_orders.created_on as orderdatetime, tbl_orders.noofseconds, (SELECT title from tbl_product WHERE tbl_product.id = tbl_voucher_mng.product_id) as title, AES_DECRYPT(tbl_orders.name, "ds234dfe3234de") as name, AES_DECRYPT(tbl_orders.accountnumber, "ds234dfe3234de") as accountnumber, tbl_orders.ifsccode, tbl_orders.cashbackamount, AES_DECRYPT(tbl_orders.upivpn, "ds234dfe3234de") as upivpn, AES_DECRYPT(tbl_orders.walletsmobile, "ds234dfe3234de") as walletsmobile, tbl_orders.api_status,(SELECT name from tbl_states Where tbl_states.id = tbl_orders.state) as state,tbl_orders.address1,tbl_orders.address2,tbl_orders.address3,tbl_orders.landmark,tbl_orders.pin_code,tbl_orders.alternate_phone_no');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->where('tbl_orders.created_on >= ',$from.' 00:00:00');
    $this->db->where('tbl_orders.created_on <= ',$to.' 23:59:59');
    $this->db->order_by("tbl_orders.id",'ASC');
    $query = $this->db->get();
    if ($query) {
        return $query->result_array();
    }
    return NULL;
  }

  public function downloadUserRegisterUser($from,$to) {
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.name, AES_DECRYPT(tbl_voucher_mng.email, "ds234dfe3234de") as email, AES_DECRYPT(tbl_voucher_mng.mobile, "ds234dfe3234de") as mobile, AES_DECRYPT(tbl_voucher_mng.vouchercode, "ds234dfe3234de") as vouchercode, tbl_voucher_mng.expirydate, tbl_voucher_mng.registereddate, tbl_voucher_mng.created_on, tbl_voucher_mng.ipaddress, tbl_voucher_mng.devicetype, tbl_voucher_mng.noofseconds');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('tbl_voucher_mng.registereddate >= ',$from.' 00:00:00');
    $this->db->where('tbl_voucher_mng.registereddate <= ',$to.' 23:59:59');
    $this->db->order_by("tbl_voucher_mng.id",'ASC');
    $query = $this->db->get();
    if ($query) {
        return $query->result_array();
    }
    return NULL;
  }

  public function morethan5orequalRegistrationSameIPCounts(){
    $this->db->select('ipaddress,COUNT(ipaddress) as ipadd');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("ipaddress");
    $this->db->having('ipadd >= 5');
    $query = $this->db->get();
    if ($query) {
      return $query->num_rows();
    }
    return NULL;
  }

  public function morethan5orequalBookingnSameIPCounts(){
    $this->db->select('tbl_voucher_mng.ipaddress , COUNT(tbl_voucher_mng.ipaddress) as ipadd');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.ipaddress");
    $this->db->having('ipadd >= 5');
    $query = $this->db->get();    
    if ($query) {
      return $query->num_rows();
    }
    return NULL;
  }

  public function morethan3orequalRegistrationSameEmailCounts(){
    //$this->db->select('AES_DECRYPT(email,"'.SALT.'") as email,COUNT(email) as emailid');
    $this->db->select('email,COUNT(email) as emailid');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("email");
    $this->db->having('emailid >= 3');
    $query = $this->db->get();
    if ($query) {
      return $query->num_rows();
    }
    return NULL;
  }

  public function morethan3orequalBookingnSameEmailCounts(){
    //$this->db->select('AES_DECRYPT(email,"'.SALT.'") as email, COUNT(tbl_voucher_mng.email) as emailid');
    $this->db->select('email, COUNT(tbl_voucher_mng.email) as emailid');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.email");
    $this->db->having('emailid >= 3');
    $query = $this->db->get();
    if ($query) {
      return $query->num_rows();
    }
    return NULL;
  }

  public function morethan5orequalRegistrationSameIPDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('ipaddress,COUNT(ipaddress) as ipadd');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("ipaddress");
    $this->db->having('ipadd >= 5');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function morethan5orequalBookingnSameIPDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_voucher_mng.ipaddress, COUNT(tbl_voucher_mng.ipaddress) as ipadd');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.ipaddress");
    $this->db->having('ipadd >= 5');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function morethan3orequalBookingnSameEmailDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('email, COUNT(tbl_voucher_mng.email) as emailid');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.email");
    $this->db->having('emailid >= 3');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function morethan3orequalRegistrationSameEmailDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('email,COUNT(email) as emailid');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("email");
    $this->db->having('emailid >= 3');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();    
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  public function downloadRegisterMoreThan5SameIP(){
    $this->db->select('ipaddress,COUNT(ipaddress) as ipadd');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("ipaddress");
    $this->db->having('ipadd >= 5');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function downloadBookingMoreThan5SameIP(){
    $this->db->select('tbl_voucher_mng.ipaddress, COUNT(tbl_voucher_mng.ipaddress) as ipadd');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.ipaddress");
    $this->db->having('ipadd >= 5');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function downloadRegisterMoreThan3SameEmail(){
    $this->db->select('email,COUNT(email) as emailid');
    $this->db->from('tbl_voucher_mng');
    $this->db->group_by("email");
    $this->db->having('emailid >= 3');
    $query = $this->db->get();    
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function downloadBookingMoreThan3SameEmail(){
    $this->db->select('email, COUNT(tbl_voucher_mng.email) as emailid');    
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_orders', 'tbl_orders.voucher_id = tbl_voucher_mng.id');
    $this->db->group_by("tbl_voucher_mng.email");
    $this->db->having('emailid >= 3');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function usedVoucherListCount() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('registereddate is NOT NULL', NULL, FALSE);
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

}