<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class UsedResendCodeMailModel extends CI_Model {

  // constructor class
  function __construct() { 
      parent::__construct();
      $this->load->database();  
      $this->load->library('session'); 
  }

  public function getUserUsedResendDetails(){
    //$this->db->select('DISTINCT(tbl_resendcode.id),tbl_voucher_mng.vouchercode,tbl_resendcode.created_on,tbl_voucher_mng.email,tbl_voucher_mng.mobile');
    $this->db->select('COUNT("tbl_resendcode.id") as count');
    $this->db->from('tbl_resendcode');
    $this->db->join('tbl_voucher_mng', 'tbl_voucher_mng.id = tbl_resendcode.user_id');
    $this->db->group_by('tbl_resendcode.id');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getUserUsedResendDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('DISTINCT(tbl_resendcode.id),tbl_voucher_mng.vouchercode,tbl_resendcode.created_on,tbl_voucher_mng.email,tbl_voucher_mng.mobile');
    $this->db->from('tbl_resendcode');
    $this->db->join('tbl_voucher_mng', 'tbl_voucher_mng.id = tbl_resendcode.user_id');
    $this->db->group_by('tbl_resendcode.id');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();

    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }


  public function getUsedResendCodeSendMail($uid){
    $this->db->select('tbl_voucher_used.user_id,tbl_voucher_used.p_id,tbl_voucher_used.reward_code,tbl_product.title,tbl_voucher_mng.name,tbl_voucher_mng.email');
    $this->db->from('tbl_voucher_used');
    $this->db->where('tbl_voucher_used.user_id',$uid);
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_used.p_id');
    $this->db->join('tbl_voucher_mng', 'tbl_voucher_mng.id = tbl_voucher_used.user_id');
    $this->db->order_by("tbl_voucher_used.user_id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
  }
  
}