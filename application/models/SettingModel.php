<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class SettingModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }
  
  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$data) {
    return $this->db->insert($tbl,$data);
  }
  
  public function getStateListCounts() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_states');
    $this->db->where("status", '1');    
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getStateListDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('tbl_states.id, tbl_states.name, tbl_states.status,tbl_product.producttitle as title,tbl_states.prod_id,');
    $this->db->from('tbl_states');
    
    $this->db->join('tbl_product', 'tbl_product.id = tbl_states.prod_id');
    $this->db->order_by("id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }
  // get function with table name --------------
  public function getSiteLists($datas) {
    foreach ($datas as $key => $value) {
      $this->db->select('*');
      $this->db->from('tbl_settings');
      $this->db->where("meta_key",$value);    
      $query = $this->db->get();       
    }
    if ($query) {
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getSettingData() {
    $this->db->select('*');
    $this->db->from('tbl_settings');
    $query = $this->db->get(); 
    if ($query) {
      return $query->result_array();
    } else {
      return false;
    }
  }


  // Edit function with table_name and where condition------------
  function edit($tbl,$where) {
    $this->db->where($where);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return $this->db->delete($tbl);
  }
  
  public function fetch_cities($query) {
    $this->db->like('name', $query);
    return $this->db->get('tbl_cities')->result_array();
  }

  public function fetch_states($query) {
    $this->db->like('name', $query);
    return $this->db->get('tbl_states')->result_array();
  }

  public function insertCSV($tbl,$data = array()) {
    if(!empty($data)){        
      // Insert data
      $insert = $this->db->insert($tbl, $data);        
      // Return the status
      return $insert?$this->db->insert_id():false;
    }
    return false;
  }

  public function getMetaVal($tbl) {
    $this->db->select('COUNT("meta_key") as count');
    $this->db->from($tbl);
    $this->db->where("meta_key", 'logoimg');      
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getMetaUpdate($tbl, $data,$userid) {
    foreach ($data as $key => $value) {
      $this->db->set('meta_value', $value);
      $this->db->where("userid", $userid);
      $this->db->where("meta_key LIKE '%$key%'");
      $res = $this->db->update($tbl);
    }
    if ($res) {
      return true;
    } else {
      return false;
    }
  }

  public function getDomainCounts() {
    $this->db->select('COUNT("meta_key") as count');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', 'subdomain');      
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getDomainDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('theme_id,meta_key,meta_value');
    $this->db->from('tbl_settings');
    $this->db->where('meta_key', 'subdomain');
    $this->db->order_by("tbl_settings.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function getEditDomain($id) {
    $this->db->select('*');
    $this->db->from('tbl_settings');
    $this->db->where("theme_id LIKE '%$id%'");
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function insertTimeSlot($data){
    return $this->db->insert('tbl_day_timeslot',$data);
  }

  public function updateTimeSlot($data) {
    $this->db->where('id',1);
    return $this->db->update('tbl_day_timeslot', $data);
  }

  public function getTimeSlotDatas(){
    $this->db->select('*');
    $this->db->from('tbl_day_timeslot');
    $this->db->where("id", 1);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    } else {
      return NULL;
    }
  }

  public function getCityList(){
    $this->db->select('*');
    $this->db->from('tbl_cities');
    $this->db->where("status", '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getCityListsss($pid){
    $this->db->select('*');
    $this->db->from('tbl_cities');
    $this->db->where("prod_id", $pid);
    $this->db->where("status", '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }

  public function getStateListsss($pid){
    $this->db->select('*');
    $this->db->from('tbl_states');
    $this->db->where("prod_id", $pid);
    $this->db->where("status", '1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
  public function getCityListCounts() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_cities');
    $this->db->where("status", '1');    
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getCityListDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('tbl_cities.id, tbl_cities.name, tbl_cities.status, tbl_product.producttitle,tbl_cities.prod_id');
    $this->db->from('tbl_cities');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_cities.prod_id');
    $this->db->order_by("id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function getVenueListCounts() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_venues');
    $this->db->where("status", '1');    
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getVenueListDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('tbl_venues.id, tbl_venues.name, tbl_venues.status, tbl_cities.name as cityname, tbl_cities.prod_id, tbl_product.producttitle as title ');
    $this->db->from('tbl_venues');
    $this->db->join('tbl_cities', 'tbl_cities.id = tbl_venues.cities_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_venues.prod_id');
    $this->db->order_by("id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function getEditCities($id){
    $this->db->select('*');
    $this->db->from('tbl_cities');
    $this->db->where('tbl_cities.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getEditStates($id){
    $this->db->select('*');
    $this->db->from('tbl_states');
    $this->db->where('tbl_states.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getEditVenues($id){
    $this->db->select('*');
    $this->db->from('tbl_venues');
    $this->db->where('tbl_venues.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function downloadCityExcel(){
    $this->db->select('*');
    $this->db->from('tbl_cities');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function downloadStateExcel(){
    $this->db->select('*');
    $this->db->from('tbl_states');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  public function getCityName($cityname){
    $this->db->select('tbl_cities.id, tbl_cities.name, tbl_cities.status, tbl_product.producttitle');
    $this->db->from('tbl_cities');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_cities.prod_id');
    $this->db->where("tbl_cities.name LIKE '%$cityname%'");
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }
  
  public function multiplDeleteByID($id,$tbl) {
    $this->db->where('id', $id);
    $this->db->delete($tbl);
  }

  public function fetch_CityList($state_id){
    $this->db->where('state_id', $state_id);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('tbl_cities');
    $output = '<option value="">Selec City</option>';
    foreach($query->result() as $row)  {
      $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    return $output;
  }

  public function fetch_StateList($pid){
    $this->db->where('prod_id', $pid);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('tbl_states');
    $output = '<option value="">Select State</option>';
    foreach($query->result() as $row)  {
      $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    return $output;
  }

  public function getSettingsForOfferDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_day_timeslot');     
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getSettingsForOfferDetailsLimit($startFrom, $showRecordPerPage) {
    $this->db->select('id,(SELECT title from tbl_product WHERE tbl_product.id=tbl_day_timeslot.prod_id) as title');
    $this->db->from('tbl_day_timeslot');
    $this->db->order_by("id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function getEditSettingDetails($id){
    $this->db->select('*');
    $this->db->from('tbl_day_timeslot');
    $this->db->where('id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  public function getOfferLists(){
    $this->db->select('*');
    $this->db->from('tbl_product');
    $this->db->where('pstatus','1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function fetchCitiesLists($offername){
    $this->db->where('prod_id', $offername);
    $this->db->where('status', '1');
    $this->db->order_by('name', 'ASC');
    $query = $this->db->get('tbl_cities');
    $output = '<option value=""></option>';
    foreach($query->result() as $row)  {
     $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
    }
    return $output;
  }

  public function getVenueListCountsForSearch($offerid,$cityid) {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_venues'); 
    $this->db->where("prod_id", $offerid);
    $this->db->where("cities_id", $cityid);
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getVenueListDetailsLimitForSearch($startFrom, $showRecordPerPage, $offerid, $cityid) {
    $this->db->select('tbl_venues.id, tbl_venues.name, tbl_venues.status, tbl_cities.name as cityname, tbl_cities.prod_id, tbl_product.producttitle as title ');
    $this->db->from('tbl_venues');
    $this->db->join('tbl_cities', 'tbl_cities.id = tbl_venues.cities_id');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_venues.prod_id');
    $this->db->where("tbl_venues.prod_id", $offerid);
    $this->db->where("tbl_venues.cities_id", $cityid);
    $this->db->order_by("id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function multiplDeactivateByID($id,$tbl) {
    $this->db->where('id', $id);
    $data = array('status' => '0');
    return $this->db->update($tbl, $data);
  }

  public function multiplActivateByID($id,$tbl) {
    $this->db->where('id', $id);
    $data = array('status' => '1');
    return $this->db->update($tbl, $data);
  }
  
  //----------------------------------------------------//
  // CIRCLE

    public function getCircleListCounts() {
      $this->db->select('COUNT("id") as count');
      $this->db->from('tbl_circle');
      $this->db->where("status", '1');    
      $query = $this->db->get();
      if ($query) {
        return $query->row()->count;
      }
      return NULL;
    }

    public function getCircleListDetailsLimit($startFrom, $showRecordPerPage) {
      $this->db->select('tbl_circle.id, tbl_circle.prod_id, tbl_circle.name, tbl_circle.status, tbl_product.producttitle');
      $this->db->from('tbl_circle');
      $this->db->join('tbl_product', 'tbl_product.id = tbl_circle.prod_id');
      $this->db->order_by("id",'DESC');
      $this->db->limit($showRecordPerPage,$startFrom);
      $query = $this->db->get();
      if ($query) {
        return $query->result_array();
      } else {
        return NULL;
      }
    }

    public function getEditCircle($id){
    $this->db->select('*');
    $this->db->from('tbl_circle');
    $this->db->where('tbl_circle.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getCircleName($circlename){
    $this->db->select('tbl_circle.id, tbl_circle.prod_id, tbl_circle.name, tbl_circle.status, tbl_product.producttitle');
    $this->db->from('tbl_circle');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_circle.prod_id');
    $this->db->where("tbl_circle.name LIKE '%$circlename%'");
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }
  //----------------------------------------------------//

}