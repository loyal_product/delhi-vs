<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class FieldTypeModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  // All Content with where condition ----------
  public function getEditContent($id){
    $this->db->select('*');
    $this->db->from('tbl_dynamicfield');
    $this->db->where('tbl_dynamicfield.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }

  // Update function for update record in table-------    
  public function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  // Delete function for delete record in table----    
  public function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
   
  // All Content with all data join form----------
  public function getContentDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_dynamicfield');
    $this->db->order_by("tbl_dynamicfield.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getContentDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('*');
    $this->db->from('tbl_dynamicfield');
    $this->db->order_by("tbl_dynamicfield.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getFieldsList(){
    $this->db->select('id,fieldtype');
    $this->db->from('tbl_fieldtype');
    $this->db->order_by("tbl_fieldtype.id",'ASC');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }
  
  
}
