<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class VoucherModel extends CI_Model {

  // constructor class
  function __construct() { 
      parent::__construct();
      $this->load->database();  
      $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  public function insert_Encrypt($tbl,$parameter) {     
    $query = "INSERT INTO $tbl (product_id, vouchercode, startdate, expirydate, lastdatetobook, status, batchname) values ('".$parameter['product_id']."', AES_ENCRYPT('".$parameter['vouchercode']."','".$parameter['salt']."'),'".$parameter['startdate']."','".$parameter['expirydate']."','".$parameter['lastdatetobook']."','".$parameter['status']."','".$parameter['batchname']."')";
    return $this->db->query($query);
  }
  
  // get function with table name --------------
  function get($tbl) {   
    $this->db->order_by('id','desc');
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    } else {
      return NULL;
    }
  }
  
  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  public function update_Encrypt($tbl,$data,$id) {
    $query = "UPDATE $tbl SET product_id='".$data['product_id']."', vouchercode=AES_ENCRYPT('".$data['vouchercode']."','".$data['salt']."'), startdate='".$data['startdate']."', expirydate='".$data['expirydate']."', lastdatetobook='".$data['lastdatetobook']."', batchname='".$data['batchname']."' WHERE id='".$id."' ";
    return $this->db->query($query);
  }
  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
  // Get record in where condition----------
  function get_where($tbl,$data) {
    $this->db->where($data);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  // All outlets with all data join form----------
  public function getvoucherDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_voucher_mng');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getvoucherDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('tbl_voucher_mng.id, tbl_voucher_mng.vouchercode, tbl_voucher_mng.startdate, tbl_voucher_mng.expirydate, tbl_voucher_mng.lastdatetobook, tbl_product.title');
    $this->db->from('tbl_voucher_mng');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_voucher_mng.product_id');
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
        return $query->result();
    }
    return NULL;
  }

   
  // All outlets with where condition all data join form----------
  public function getEditVoucher($id) {
    $this->db->select('id, product_id, vouchercode, startdate, expirydate, lastdatetobook, batchname');
    $this->db->from('tbl_voucher_mng');
    $this->db->where('tbl_voucher_mng.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getvoucherDetailsByMobile($voucher_mob){
    $this->db->select('id,name, email, mobile, vouchercode ,amount,expirydate,status,registereddate');
    $this->db->from('tbl_voucher_mng');
    $this->db->where("AES_DECRYPT(mobile, '".SALT."') LIKE '%$voucher_mob%' ");
    $this->db->order_by("id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getvoucherDetailsByVoucher($voucher_code){
    //$this->db->select('id,name,AES_DECRYPT(email,"'.SALT.'") as email,AES_DECRYPT(mobile,"'.SALT.'") as  mobile,AES_DECRYPT(vouchercode,"'.SALT.'") as vouchercode ,amount,expirydate,status,registereddate');
    $this->db->select('id,name, email, mobile, vouchercode ,amount,expirydate,status,registereddate');
    $this->db->from('tbl_voucher_mng');
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$voucher_code%' ");
    $this->db->order_by("tbl_voucher_mng.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  /*
   * Insert members data into the database
   * @param $data data to be insert based on the passed parameters
   */
  public function insertCSV($vouch_tbl,$parameter = array()) {
    if(!empty($parameter)){        
      // Insert member data
      /*$insert = $this->db->insert($vouch_tbl, $data);
      
      // Return the status
      return $insert?$this->db->insert_id():false;*/
      $query = "INSERT INTO $vouch_tbl (product_id, vouchercode, startdate, expirydate, lastdatetobook, status, batchname) values ('".$parameter['product_id']."', AES_ENCRYPT('".$parameter['vouchercode']."','".$parameter['salt']."'),'".$parameter['startdate']."','".$parameter['expirydate']."','".$parameter['lastdatetobook']."','".$parameter['status']."','".$parameter['batchname']."')";
      return $this->db->query($query);
    }
    return false;
  }

  //Get Cap Limit Data
  public function getCapLimitData(){
    $this->db->select('*');
    $this->db->from('tbl_caplimit');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function getEditCapLimit($id){
    $this->db->select('*');
    $this->db->from('tbl_caplimit');
    $this->db->where('tbl_caplimit.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function truncateCaplimit($tbl){
    $this->db->truncate($tbl);
  }

  //Upadte Start and Exipry Date
  public function updateStartExpiryDate($tbl,$startdate,$expirydate){
    $this->db->set('startdate', $startdate);
    $this->db->set('expirydate', $expirydate);
    return $this->db->update($tbl);
  }

  //Upadte Last Date To Book Date
  public function updateLastDateToBook($tbl,$lastdatetobook){
    $this->db->set('lastdatetobook', $lastdatetobook);
    return $this->db->update($tbl);
  }

  public function getProductListing(){
    //$this->db->select('*');
    $this->db->select('tbl_subproduct.id, tbl_subproduct.sub_prod_name, tbl_product.title');
    $this->db->from('tbl_subproduct');
    $this->db->join('tbl_product', 'tbl_product.id = tbl_subproduct.prod_id');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else {
      return NULL;
    }
  }

  public function multiplDeleteByID($id,$tbl) {
    $this->db->where('id', $id);
    $this->db->delete($tbl);
  }

  public function largeCSVUpload($target_path,$startdate,$expirydate,$lastDateBook) {
    $query_1 = 'LOAD DATA LOCAL INFILE "'.$target_path.'"
        INTO TABLE tbl_voucher_mng1
        FIELDS TERMINATED by \',\'
        LINES TERMINATED BY \'\n\' ';
    return $this->db->query($query_1);
  }

  public function getAllUploadedData(){
    $this->db->select('*');
    $this->db->from('tbl_voucher_mng1');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function truncateTable($tblname){
    $this->db->truncate($tblname);
  }

  public function getBatchType(){
    $this->db->select('batchname');
    $this->db->distinct();
    $query = $this->db->get('tbl_voucher_mng');
    if ($query) {
      return $query->result_array();
    }
    return NULL;
  }

  public function updateBatchWiseLastDateToBook($tbl,$batchtype,$startdate,$expirydate){
    $this->db->set('startdate', $startdate);
    $this->db->set('expirydate', $expirydate);
    $this->db->where("batchname LIKE '%$batchtype%' ");
    return $this->db->update($tbl);
  }

  public function getVoucherById($tbl,$value) {
    $this->db->select('id');
    $this->db->from($tbl);
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$value%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }

  public function updateVoucherCodeDateUPDATE($tbl,$id,$startdate,$expirydate){
    $this->db->set('startdate', $startdate);
    $this->db->set('expirydate', $expirydate);
    $this->db->where("id" ,$id);
    return $this->db->update($tbl);
  }
  
  public function fetchDatesForBatches($batchname){
    $this->db->where('batchname', $batchname);
    $query = $this->db->get('tbl_voucher_mng');
    if ($query) {
      return $query->row();
    }
    return NULL;
  }
    
  public function getVoucherDetailsByVoucherCode($tbl,$vouchercod) {
    $this->db->select('id,product_id, name, email, mobile, AES_DECRYPT(vouchercode,"'.SALT.'") as vouchercode, startdate, expirydate, lastdatetobook,status,batchname');
    $this->db->from($tbl);    
    $this->db->where("AES_DECRYPT(vouchercode, '".SALT."') LIKE '%$vouchercod%' ");
    $query = $this->db->get();
    if ($query) {
      return $query->row();
    }
    return NULL;
  }
  
}