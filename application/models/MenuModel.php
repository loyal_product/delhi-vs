<?php
 
defined('BASEPATH') || exit('No direct script access allowed');
class MenuModel extends CI_Model {

  // constructor class
  function __construct() { 
    parent::__construct();
    $this->load->database();  
    $this->load->library('session'); 
  }

  // Insert function with table_name , all data in array form------ 
  public function insert($tbl,$parameter) {
    return $this->db->insert($tbl,$parameter);
  }
  public function logs($tbl_name,$value) {
    return $this->db->insert($tbl_name,$value);
  }
  // get function with table name --------------
  function get($tbl) {   
    $this->db->order_by('id','desc');
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  function getlist($where,$tbl) {   
    $this->db->where($where);
    $this->db->order_by('id','desc');
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }


// Edit function with table_name and where condition------------
  function edit($tbl,$where) {
   $this->db->where($where);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  function edit_whr($tbl,$whr_col,$whr_val) {
    $this->db->where_in($whr_col, $whr_val);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  } 

  // Update function for update record in table-------    
  function update($tbl,$data,$where) {
    $this->db->where($where);
    return $this->db->update($tbl, $data);
  }
  // Delete function for delete record in table----    
  function delete($tbl,$where) {
    $this->db->where($where);
    return   $this->db->delete($tbl);
  }
  // Get record in where condition----------
   function get_where($tbl,$data) {
    $this->db->where($data);
    $query = $this->db->get($tbl);
    if ($query) {
      return $query->result();
    }
    return NULL;
  } 
   
  // All BANNER with all data join form----------
  public function getMenuDetails() {
    $this->db->select('COUNT("id") as count');
    $this->db->from('tbl_menu');
    $this->db->order_by("tbl_menu.id",'DESC');
    $query = $this->db->get();
    if ($query) {
      return $query->row()->count;
    }
    return NULL;
  }

  public function getMenuDetailsLimit($startFrom, $showRecordPerPage){
    $this->db->select('*');
    $this->db->from('tbl_menu');
    $this->db->order_by("tbl_menu.id",'DESC');
    $this->db->limit($showRecordPerPage,$startFrom);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }
  
  // All banner with where condition ----------
  public function getEditMenu($id){
    $this->db->select('*');
    $this->db->from('tbl_menu');
    $this->db->where('tbl_menu.id',$id);
    $query = $this->db->get();
    if ($query) {
      return $query->result();
    }
    return NULL;
  }

  public function getMenuList(){
    $this->db->select('*');
    $this->db->from('tbl_menu');
    $this->db->where("status",'1');
    $this->db->order_by('sort_order','asc');
    $query = $this->db->get();
    if ($query) {
      return $query->result_array();
    } else{
      return NULL;
    }
  }
  

}