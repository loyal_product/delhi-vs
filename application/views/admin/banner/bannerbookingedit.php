<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Banner for Booking Page</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Edit Banner for Booking Page</a>
            </li>
          </ol>
        </div>
      </div>
      <?php
          if($this->session->flashdata('banner')) {
          $message = $this->session->flashdata('banner');
        ?>
         <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
        <?php   }    ?>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Banner for Booking Page</strong></h4>
            <div class="row">
             <?php 
              if(isset($editbannerList)){
                foreach($editbannerList as $key=>$row){
              ?>
              <form  id="bannerbookingEditFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/bannerupdateforbooking/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
                
                <div class="col-md-12">   
                <input type="hidden" name="oldimg" value="<?php echo $row->content; ?>">
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Product Name</label>
                    <select class="form-control" name="pid" id="pid">
                      <option value="">-- Please Select --</option>
                      <?php if (isset($productlist)) {
                        foreach ($productlist as $key => $val) { ?>
                        <option value="<?php echo $val['id']; ?>" <?php if($row->prod_id==$val['id']){echo 'selected="selected"';}?>><?php echo $val['title']; ?></option>
                      <?php } } ?>
                    </select>
                  </div>                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Banner</label>
                      <input type="file" class="form-control" name="image" id="image" accept="image/*" placeholder="Image" >
                      <img id="bannerid" src="<?php echo base_url().'assets/uploads/'.$row->content; ?>" width="150" height="80" alt="Banner" />
                    </div> 
                  </div>            
                </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form> 
              <?php } } ?>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $('#bannerbookingEditFrom').on('submit', function (e) {
    e.preventDefault();              
    var noError       = true;
    var pid     = $("#pid").val();

    if(pid == ""){
      $('#pid').css('border','1px solid red');
      noError = false;               
    } else {
      $('#pid').css('border','1px solid #eee');
      noError = true; 
    }
    if(noError){
      $('#bannerbookingEditFrom')[0].submit();   
    }
  });
</script>