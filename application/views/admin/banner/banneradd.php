<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Banner</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active"><a>Create Banner</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <?php
          if($this->session->flashdata('banner')) {
          $message = $this->session->flashdata('banner');
        ?>
         <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
        <?php   }    ?>
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Banner</strong></h4>
            <div class="row">
              <form  id="bannerFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/bannerinsert" method="post" enctype="multipart/form-data">
                
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Banner</label>
                      <input type="file" class="form-control" name="image" id="image" accept="image/x-png,image/gif,image/jpeg" placeholder="Image" >
                      <img id="bannerid" src="#" width="150" height="80" alt="Banner" />
                    </div> 
                    </div>                    
                    <div class="form-group col-md-6">
                      <label>Page Name</label>
                      <select class="form-control" name="pagename" id="pagename">
                        <option value="">-- Please Select --</option>
                        <?php foreach ($menulist as $key => $val) { ?>
                          <option value="<?php echo $val['menulink']; ?>"><?php echo $val['menuname']; ?></option>
                        <?php } ?>
                        <!-- <option value="index">Landing Page</option>
                        <option value="cart">Cart</option>
                        <option value="rewards">Rewards</option>
                        <option value="login">Login</option>
                        <option value="success">Success</option>
                        <option value="term-condition">Terms & Condition</option> -->
                      </select>
                    </div>                   
                  </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
