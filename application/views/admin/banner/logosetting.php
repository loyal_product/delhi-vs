<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create New Website For General Settings</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Create New Website For General Settings</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="row">
              <form id="logosettingForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/logosettinginsert" method="POST" enctype="multipart/form-data">
              <?php
                if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow');
              ?>
                <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Logo</label>
                        <p id="errmsg"></p>
                        <input type='file' name="logoimg" id="logoimg" onchange="readURL(this);" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <img id="imgView" src="<?php if(isset($logoData[0]['meta_key']) && $logoData[0]['meta_key']=='logo'){ echo base_url().'assets/uploads/'.$logoData[0]['meta_value'];}?>" alt="logo" style="height: 100px;width: 200px;" />
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  (function($) {
    $.fn.checkFileType = function(options) {
      var defaults = {
          allowedExtensions: [],
          success: function() {},
          error: function() {}
      };
      options = $.extend(defaults, options);

      return this.each(function() {
        $(this).on('change', function() {
          var value = $(this).val(),
            file = value.toLowerCase(),
            extension = file.substring(file.lastIndexOf('.') + 1);

          if ($.inArray(extension, options.allowedExtensions) == -1) {
            options.error();
            $(this).focus();
          } else {
            options.success();
          }
        });
      });
    };
  })(jQuery);

  $(function() {
    $('#logoimg').checkFileType({
        allowedExtensions: ['jpg', 'jpeg', 'png', 'svg', 'gif'],
        success: function() {
          //alert('Success');
          //$('#logosettingForm')[0].submit(); 
        },
        error: function() {
          alert('Error');
          $('#errmsg').html("<p style='color:red'>Please select Only jpg,jpeg,png and svg</p>");
        }
    });
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#imgView')
            .attr('src', e.target.result)
            .width(100)
            .height(80);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $(document).ready(function() {
    $('#logosettingForm').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var logoimg     = $("#logoimg").val();

      if(logoimg == "") {
        $('#logoimg').css('border','1px solid red');
        noError = false;               
      } else {
        $('#logoimg').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#logosettingForm')[0].submit();   
      }
    });

  });
</script>