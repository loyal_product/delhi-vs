<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Banner List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Banner List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Banner List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/banneradd" style="position: absolute;right: 31px;" class="btn btn-default">Add Banner</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/bannerlistforbooking" style="position: absolute;right: 150px;" class="btn btn-default">Banner Listing For Booking Page</a>
            </h4>
            <div class="row">
              <div class="col-md-12">                
              <?php
                if($this->session->flashdata('banner')) {
                $message = $this->session->flashdata('banner');
              ?>
               <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
              <?php   }    ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Banner Listing">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Banner</th>
                          <th scope="col" class='collapsable'>Page Name</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($bannerlist) && !empty($bannerlist) ){
                        $i= $startFrom;
                      foreach($bannerlist as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><img src="<?php echo base_url().'assets/uploads/'.$row->banner; ?>" id="pic" alt="banner" width="80" height="80"></td>
                      <td class='collapsable'><?php if($row->pagename=='index'){ echo "Landing";}else{ echo ucwords($row->pagename); } ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editbanner/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletebanner/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>


<!-- Modal -->
    <div class="modal fade" id="outlet_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->

<style type="text/css">

</style>