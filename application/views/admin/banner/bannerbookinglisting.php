<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Banner List for Booking Page</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Banner List for Booking Page</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Banner List for Booking Page</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/banneraddforbooking" style="position: absolute;right: 31px;" class="btn btn-default">Add Banner for Booking </a>
            </h4>
            <div class="row">
              <div class="col-md-12">                
              <?php
                if($this->session->flashdata('banner')) {
                $message = $this->session->flashdata('banner');
              ?>
               <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
              <?php   }    ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Banner List For Booking">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Banner</th>
                          <th scope="col" class='collapsable'>Product Booking Page</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($bannerlist) && !empty($bannerlist) ){
                      foreach($bannerlist as $key=>$row){
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><img src="<?php echo base_url().'assets/uploads/'.$row->content; ?>" id="pic" alt="banner" width="80" height="80"></td>
                      <td class='collapsable'><?php echo $row->title; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editbannerforbooking/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletebannerforbooking/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
