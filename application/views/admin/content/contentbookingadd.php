<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Content For Booking</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Create Content For Booking</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Content For Booking</strong></h4>
            <div class="row">
              <form  id="contentFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/contentinsertforbooking" method="post" enctype="multipart/form-data">
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Content</label>                      
                      <textarea class="ckeditor" name='content' id="description1"></textarea>
                    </div> 
                    </div>                    
                    <div class="form-group col-md-6">
                      <label>Product Name</label>
                      <select class="form-control" name="pid" id="pid">
                        <option value="">-- Please Select --</option>
                        <?php if (isset($productlist)) {
                          foreach ($productlist as $key => $val) { ?>
                          <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                        <?php } } ?>
                      </select>
                    </div>                   
                  </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->
<script type="text/javascript">
  $('#contentFrom').on('submit', function (e) {
    e.preventDefault();              
    var noError         = true;
    var pid             = $("#pid").val();
    var description1    = $("#description1").val();

    if(pid == ""){
      $('#pid').css('border','1px solid red');
      noError = false;               
    } else {
      $('#pid').css('border','1px solid #eee');
      noError = true; 
    }
    if(description1 == ""){
      $('#description1').css('border','1px solid red');
      noError = false;               
    } else {
      $('#description1').css('border','1px solid #eee');
      noError = true; 
    }
    if(noError){
      $('#contentFrom')[0].submit();   
    }
  });
</script>