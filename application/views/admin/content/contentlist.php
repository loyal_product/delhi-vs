<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Content List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Content List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Content List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/contentadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Content</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/contentlistforbooking" style="position: absolute;right: 150px;" class="btn btn-default">Content List For Booking</a>
            </h4>
            <div class="row">
              <div class="col-md-12">
                <?php
                    if($this->session->flashdata('user')) {
                    $message = $this->session->flashdata('user');
                  ?>
                    <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php
                    }
                  ?>
                <div class="table-responsive">
                  <table width="100%" class="table table-striped table-bordered" summary="Content List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" >Page Name</th>
                          <th scope="col" class='collapsable'>Created Date</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($contentlist) && !empty($contentlist) ){
                      $i= $startFrom;
                      foreach($contentlist as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td><?php echo ucwords($row->pagename); ?></td>
                      <td class='collapsable'><?php echo $row->created_on; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editcontent/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletecontent/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else {  ?>
                      <tr><td  colspan="4">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
