<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Field Name</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a href="#">Edit Field Name</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php if($this->session->flashdata('msgshow')) {
            $message = $this->session->flashdata('msgshow'); ?>
            <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Field Name</strong></h4>
            <div class="row">
            <?php if(isset($getFieldsList)){
              foreach ($getFieldsList as $key => $row) { ?>     
              <form  id="FieldEditFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/fieldsupdate/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Product Name</label>
                    <select class="form-control" name="pid" id="pid">
                      <option value="">-- Please Select --</option>
                      <?php if (isset($productlist)) {
                        foreach ($productlist as $key => $val) { ?>
                        <option value="<?php echo $val['id']; ?>" <?php if($row->prod_id==$val['id']){echo 'selected="selected"';}?>><?php echo $val['title']; ?></option>
                      <?php } } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Field Name Type</label>
                    <select class="form-control" name="fieldtype" id="fieldtype">
                      <option value="">-- Please Select --</option>
                      <option value="vouchercode" <?php if($row->fieldtype=='vouchercode'){echo 'selected="selected"';}?>>Voucher Code</option>
                      <option value="cname" <?php if($row->fieldtype=='cname'){echo 'selected="selected"';}?>>Customer Name</option>
                      <option value="mobile" <?php if($row->fieldtype=='mobile'){echo 'selected="selected"';}?>>Mobile</option>
                      <option value="emailid" <?php if($row->fieldtype=='emailid'){echo 'selected="selected"';}?>>Email Id</option>
                      <option value="otp" <?php if($row->fieldtype=='otp'){echo 'selected="selected"';}?>>OTP</option>
                      <option value="prodname1" <?php if($row->fieldtype=='prodname1'){echo 'selected="selected"';}?>>Product Name 1</option>
                      <option value="prodname2" <?php if($row->fieldtype=='prodname2'){echo 'selected="selected"';}?>>Product Name 2</option>
                      <option value="prefarea1" <?php if($row->fieldtype=='prefarea1'){echo 'selected="selected"';}?>>Prefered Area 1</option>
                      <option value="prefarea2" <?php if($row->fieldtype=='prefarea2'){echo 'selected="selected"';}?>>Prefered Area 2</option>
                    </select>
                  </div>                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Field Name Title</label>                      
                      <input type="text" class="form-control" name="fieldname" id="fieldname" value="<?php echo $row->fieldname;?>" placeholder="Field Name Title" >
                    </div> 
                  </div>             
                </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
                  </div>
              </form> 
              <?php } } ?>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#FieldEditFrom').on('submit', function (e) {
      e.preventDefault();              
      var noError    = true;
      var fieldtype   = $("#fieldtype").val();
      var fieldname     = $("#fieldname").val();

      if(fieldtype == "") {
        $('#fieldtype').css('border','1px solid red');
        noError = false;               
      } else {
        $('#fieldtype').css('border','1px solid #eee');
        noError = true;
      }
      if(fieldname == "") {
        $('#fieldname').css('border','1px solid red');
        noError = false;               
      } else {
        $('#fieldname').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#FieldEditFrom')[0].submit();   
      }
    });
  });
</script>