<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Field Name</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a href="#">Create Field Name</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php if($this->session->flashdata('msgshow')) {
            $message = $this->session->flashdata('msgshow'); ?>
            <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Field Name</strong></h4>
            <div class="row">
              <form  id="FieldAddFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/fieldsinsert" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Product Name</label>
                    <select class="form-control" name="pid" id="pid">
                      <option value="">-- Please Select --</option>
                      <?php if (isset($productlist)) {
                        foreach ($productlist as $key => $val) { ?>
                        <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                      <?php } } ?>
                    </select>
                  </div> 
                </div>
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Field Name Type</label>
                    <select class="form-control" name="fieldtype" id="fieldtype">
                      <option value="">-- Please Select --</option>
                      <option value="vouchercode">Voucher Code</option>
                      <option value="cname">Customer Name</option>
                      <option value="mobile">Mobile</option>
                      <option value="emailid">Email Id</option>
                      <option value="otp">OTP</option>
                      <option value="prodname1">Product Name 1</option>
                      <option value="prodname2">Product Name 2</option>
                      <option value="prefarea1">Prefered Area 1</option>
                      <option value="prefarea2">Prefered Area 2</option>
                      <option value="nominate_name">Booking form Nominated Name</option>
                      <option value="mobile_circle">Booking Form Circle</option>
                      <option value="mobile_povider">Booking Form Service Provider</option>
                      <option value="mobile_connection_type">Booking Form Connection Type</option>
                    </select>
                  </div>                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Field Name Title</label>                      
                      <input type="text" class="form-control" name="fieldname" id="fieldname" placeholder="Field Name Title" >
                    </div> 
                  </div>             
                </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#FieldAddFrom').on('submit', function (e) {
      e.preventDefault();              
      var noError     = true;
      var fieldtype   = $("#fieldtype").val();
      var fieldname   = $("#fieldname").val();
      var pid         = $('#pid').val();

      if(pid == "") {
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true;
      }
      if(fieldtype == "") {
        $('#fieldtype').css('border','1px solid red');
        noError = false;               
      } else {
        $('#fieldtype').css('border','1px solid #eee');
        noError = true;
      }
      if(fieldname == "") {
        $('#fieldname').css('border','1px solid red');
        noError = false;               
      } else {
        $('#fieldname').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#FieldAddFrom')[0].submit();   
      }
    });
  });
</script>