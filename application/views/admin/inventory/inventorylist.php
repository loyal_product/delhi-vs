<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Inventory List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Inventory List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Inventory List</strong>
              <a href="<?php echo base_url().'assets/download/inventory.csv';?>" target="_blank" style="position: absolute;right: 345px;" class="btn btn-default" rel="noopener">Sample file download for upload</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventoryupload" style="position: absolute;right: 175px;" class="btn btn-default">Upload Inventory</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventoryadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Inventory</a>
            </h4>

            <div class="row">
              <div class="col-md-12">
                <?php
                    if($this->session->flashdata('inventory')) {
                    $message = $this->session->flashdata('inventory');
                  ?>
                    <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php
                    }
                  ?>
                  <?php if(isset($inventoryList) && count($inventoryList)>0){ ?>
                    <div style="height:30px;"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete All</button></div>
                  <?php } ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Inventory List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Product Name</th>
                          <th scope="col" class='collapsable'>Reward Code</th>
                          <th scope="col" class='collapsable'>Created On</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    
                    if(isset($inventoryList) && !empty($inventoryList) ){
                        $i= $startFrom;
                      foreach($inventoryList as $key=>$row){
                        $i++;
                        $res = explode(" ", $row->created_on);
                    ?>

                    <tr>
                      <td><input type="checkbox" class="delete_checkbox" value="<?php echo $row->id; ?>" /></td>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row->producttitle; ?></td>
                      <td class='collapsable'><?php echo $row->rewardcode; ?></td>
                      <td class='collapsable'><?php echo $res[0]; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editinventory/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deleteinventory/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                     </td>

                    </tr> 
                    <?php  } } else {  ?>
                      <tr><td  colspan="10">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                  <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script>
  $(document).ready(function(){
    $('.selectall').change(function () {
      if ($(this).prop('checked')) {
        $('input').prop('checked', true);
      } else {
        $('input').prop('checked', false);
      }
    });
    $('.selectall').trigger('change');

   $('#delete_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplinventoryedelete",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          success:function() {
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple delete has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
     alert('Please select atleast one records');
    }
   });

  });
</script>