<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Inventory</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>         
            <li class="active"><a>Create Inventory</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Product</strong></h4>
            <div class="row">
              <form id="inventoryAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventoryinsert" method="POST" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('inventory')) {
                      $message = $this->session->flashdata('inventory');
                   ?>
                 <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php   }    ?>
                   
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Sub Product Name</label>
                        <select name="prodId" class="form-control" id="prodId">
                          <option value=""> --Please Select-- </option>
                          <?php 
                            foreach ($allProducts as $key => $value) { ?>
                              <option value="<?php echo $value->id;?>"> <?php echo $value->sub_prod_name;?> </option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                     <div class="col-md-4">
                      <div class="form-group">
                        <label>Reward Code</label>
                        <div class="input_fields_wrap">
                          <input type="text" class="form-control rewardcode" name="rewardcode[]" placeholder="Reward Code" >  
                        </div>                      
                      </div>
                    </div>
                    <div class="col-md-2"></br>
                      <button class="btn btn-info add_Field" style="margin-top: 5px;" type="button">Add</button>
                    </div>
                 </div>


                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() { 
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_Field"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append('<div><input style="margin-top:3px;" type="text" class="form-control" name="rewardcode[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    });
    $('#inventoryAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var prodId          = $("#prodId").val();
      var rewardcode      = $(".rewardcode").val(); 

      if(prodId == ""){
        $('#prodId').css('border','1px solid red');
        noError = false;               
      } else {
        $('#prodId').css('border','1px solid #eee');
        noError = true; 
      }
      if(rewardcode == ""){
        $('.rewardcode').css('border','1px solid red');
        noError = false;               
      } else {
        $('.rewardcode').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#inventoryAddForm')[0].submit();   
      }

    });
  }); 
</script>