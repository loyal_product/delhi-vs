<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Upload Inventory</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a>Upload Inventory</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Upload Inventory</strong></h4>
            <div class="row">
              <form id="inventoryUploadForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventoryuploadcsv" method="POST" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('inventory')) {
                      $message = $this->session->flashdata('inventory');
                   ?>
                 <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php } ?>
                   
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Product Name</label>
                        <select name="prodId" class="form-control" id="prodId">
                          <option value=""> --Please Select-- </option>
                          <?php 
                            foreach ($allProducts as $key => $value) { ?>
                              <option value="<?php echo $value->id;?>"> <?php echo $value->sub_prod_name;?> </option>
                            <?php } ?>
                        </select>
                      </div>
                    </div>
                     <div class="col-md-4">
                      <div class="form-group">
                        <label>Reward Code</label>
                        <input type="file" name="file" id="csvfile" />                      
                      </div>
                    </div>
                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="submit" name="importSubmit" class="btn btn-default" value="Submit" role="button" />
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#inventoryUploadForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var prodId          = $("#prodId").val();
      var csvfile         = $("#csvfile").val(); 

      if(prodId == ""){
        $('#prodId').css('border','1px solid red');
        noError = false;               
      } else {
        $('#prodId').css('border','1px solid #eee');
        noError = true; 
      }
      if(csvfile == ""){
        $('#csvfile').css('border','1px solid red');
        noError = false;               
      } else {
        $('#csvfile').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#inventoryUploadForm')[0].submit();   
      }

    });
  }); 
</script>