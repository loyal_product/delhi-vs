<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Cashback Booking Form</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a href="#">Edit Cashback Booking Form</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php if($this->session->flashdata('msgshow')) {
            $message = $this->session->flashdata('msgshow'); ?>
            <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Cashback Booking Form</strong></h4>
            <div class="row">
              <?php foreach ($getCashbackList as $key => $row) { ?>
                <form  id="CashbackAddFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updatecashbackbooking/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
                  <div class="col-md-12">
                    <div class="form-group col-md-6">
                      <div class="form-group">
                        <div id="msg"></div>
                        <label>&nbsp;</label></br>
                        <label for="file-upload" class="custom-file-upload">
                        <em class="fa fa-cloud-upload"></em> Choose Banner </label> 
                        <style type="text/css">
                          input[type="file"] {
                            display: none;
                          }
                          .custom-file-upload {
                            border: 1px solid #ccc;
                            display: inline-block;
                            padding: 8px 6px;
                            cursor: pointer;
                          }
                        </style>       
                        <input type="file" id="file-upload" name="image" accept="image/*"  />
                      </div>
                      <?php if(isset($row->banner_img) && $row->banner_img!=""){ ?>
                      <div class="form-group">
                        <img src="<?php echo base_url().'assets/uploads/'.$row->banner_img; ?>" width="90" height="80" alt="">
                      </div>
                      <?php } ?>
                    </div>                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>                      
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $row->title; ?>">
                      </div> 
                    </div>             
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Price</label>                      
                        <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="<?php echo $row->price; ?>">
                      </div> 
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>                      
                        <select class="form-control" name="status" id="status">
                          <option value="">--Please Select--</option>
                          <option value="1" <?php if($row->status=='1'){echo 'selected="selected"';} ?>>Active</option>
                          <option value="0" <?php if($row->status=='0'){echo 'selected="selected"';} ?>>Inactive</option>
                        </select>
                      </div> 
                    </div>             
                  </div>                    
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                          <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                        </div>
                      </div>
                    </div>
                </form>  
              <?php } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#CashbackAddFrom').on('submit', function (e) {
      e.preventDefault();              
      var noError     = true;
      var fileupload  = $("#file-upload").val();
      var price       = $("#price").val();
      var status      = $('#status').val();
      var title       = $('#title').val();      

      if(fileupload == ""){
        $('.custom-file-upload').css('border','1px solid red');
        noError = false;               
      } else {
        $('.custom-file-upload').css('border','1px solid #eee');
        noError = true; 
      }
      if(title == "") {
        $('#title').css('border','1px solid red');
        noError = false;               
      } else {
        $('#title').css('border','1px solid #eee');
        noError = true;
      }
      if(price == "") {
        $('#price').css('border','1px solid red');
        noError = false;               
      } else {
        $('#price').css('border','1px solid #eee');
        noError = true;
      }
      if(status == "") {
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#CashbackAddFrom')[0].submit();   
      }
    });
  });
</script>