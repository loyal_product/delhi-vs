<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Cashback Booking Form List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a href="#">Cashback Booking Form List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Cashback Booking Form List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/addcashbackbooking" style="position: absolute;right: 31px;" class="btn btn-default">Add Cashback Booking Form</a>
            </h4>
            <div class="row">
              <div class="col-md-12">
                <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <div class="table-responsive">
                  <table width="100%" class="table table-striped table-bordered" summary="Cashback Booking List">
                    <thead>
                      <tr>
                        <th scope="col" class='collapsable'>S.No</th>
                        <th scope="col" >title</th>
                        <th scope="col" >Banner</th>
                        <th scope="col" >Price</th>
                        <th scope="col" class='collapsable'>Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                  <tbody>
                   <?php if(isset($cashbackbookingformlists) && !empty($cashbackbookingformlists) ){
                      $i= $startFrom;
                      foreach($cashbackbookingformlists as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row->title; ?></td>
                      <td><img src="<?php echo base_url().'assets/uploads/'.$row->banner_img;?>" height="80" width="200" alt="banner"></td>
                      <td><?php echo $row->price; ?></td>
                      <td class='collapsable'><?php if($row->status=='1'){echo "Active";}else{ echo "Inactive";} ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editcashbackbooking/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletecashbackbooking/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else {  ?>
                      <tr><td  colspan="6">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

