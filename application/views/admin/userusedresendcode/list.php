<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">User Used Resend Code</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>User Used Resend Code</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>User Used Resend Code</strong>
            </h4>
            <div class="row">
              <div class="col-md-12">                
              <?php
                if($this->session->flashdata('usedresendcode')) {
                $message = $this->session->flashdata('usedresendcode');
              ?>
               <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
              <?php   }    ?>
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="User Used Resend Code">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Voucher Code Used</th>
                          <th scope="col" class='collapsable'>Date and Time of Resend Code Request</th>
                          <th scope="col" width="200" class='collapsable'>Email ID</th>
                          <th scope="col" class='collapsable'>Mobile</th>
                          <th scope="col" width="200" class='collapsable'>Booking Email</th>
                          <th scope="col" class='collapsable'>Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if((count($usedResendCode)>0) && !empty($usedResendCode)) {
                      foreach($usedResendCode as $key=>$row) {
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['vouchercode'];?></td>
                      <td class='collapsable'><?php echo $row['created_on'];?></td>
                      <td width="200" class='collapsable'><?php echo $row['email'];?></td>
                      <td class='collapsable'><?php echo $row['mobile'];?></td>
                      <td width="200" class='collapsable'><?php echo $row['email'];?></td>
                      <td class="custom_width">
                        <a title="Resend Email" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/resendemail/<?php echo $row['user_id']; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon">Resend Email</a>
                      </td>                      
                    </tr>
                    <?php } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } 
                      ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div> 

      

    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
    $(document).ready(function(){   
      $("#registeredmobile").keyup(function () {
        var val = $(this).val();
          if(isNaN(val)){
            val = val.replace(/[^0-9\.]/g,'');
            if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
          }
        $(this).val(val);       
      }); 
      $('#resendcode').on('submit', function (e) {
        e.preventDefault();              
        var noError               = true;
        var vouchercode           = $("#vouchercode").val();
        var registeredemailid     = $("#registeredemailid").val(); 
        var registeredmobile      = $("#registeredmobile").val();

        if(vouchercode == ""){
          $('#vouchercode').css('border','1px solid red');
          noError = false;               
        } else {
          $('#vouchercode').css('border','1px solid #eee');
          noError = true; 
        }
        if(registeredemailid == ""){
          $('#registeredemailid').css('border','1px solid red');
          noError = false;               
        } else {
          $('#registeredemailid').css('border','1px solid #eee');
          noError = true; 
        }
        if(registeredmobile == ""){
          $('#registeredmobile').css('border','1px solid red');
          noError = false;               
        } else {
          $('#registeredmobile').css('border','1px solid #eee');
          noError = true; 
        }

        if(noError){  
          var form = $(this);
          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'site/core/micro/site/lib/controller/type/resendcodedetails'?>",
            data: form.serialize(), 
            dataType: "html",
            beforeSend: function () {              
              $('.loader1').removeClass('hide');
            },
            success: function(data){
              $('.loader1').addClass('hide');
              $('#code-container').html("");
              $('#code-container').prepend(data);
            },
            error: function() { 
              $('.loader1').addClass('hide');
              $('#msg').html("<div class='alert alert-danger'>Something went wrong..!!</div>");
            }
          });
        }
      });

      $(document).on('click','#resendSubmit',function(e) {
        e.preventDefault(); 
        var url = $(this).attr('data-id');
        $.ajax({
          type: "POST",
          url: url,
          dataType: "html",
          beforeSend: function () {
            $('.loader1').removeClass('hide');
          },
          success: function(data){
            $('.loader1').addClass('hide');
            $('#msg').html("<div class='alert alert-success'>Mail has been sent successfully..!!</div>");
          },
          error: function() { 
            $('.loader1').addClass('hide');
            $('#msg').html("<div class='alert alert-danger'>Mail has not been sent successfully..!!</div>"); 
          }
        });        
      });
    });
  </script>