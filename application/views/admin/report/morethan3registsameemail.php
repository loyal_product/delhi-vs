<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">3 or more registration from same Email</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>3 or more registration from same Email</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom:40px !important;">
              <a href="<?php echo base_url().'site/core/micro/site/lib/controller/type/registrationdownloadmorethan3sameemail';?>" style="position: absolute;right:31px;" class="btn btn-default">Download All As Excel</a>
            </h4>
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="3 or more registration same email">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Email</th>
                        </tr>
                      </thead>
                  <tbody>
                  <?php 
                    if(isset($lists) && !empty($lists) ){
                        $i= $startFrom;
                      foreach($lists as $key=>$row){
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['email'];?></td>
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                   <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
