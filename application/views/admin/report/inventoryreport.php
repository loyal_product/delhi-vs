<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Inventory Report</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Inventory Report</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="col-md-12" style="margin-bottom: 5px;border: 1px solid #eee;padding: 4px;">
              <label>Filter By Issued Date</label>
            <form method="post" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/downloadexcelfromdate'?>">
            <div class="col-md-3">
              <input type="text" style="line-height: 15px;" class="form-control" name="filterDateTo" id="txtFrom" autocomplete="off" placeholder="dd/mm/yyyy">
            </div>
            <div class="col-md-3">
              <input type="text" style="line-height: 15px;" class="form-control" name="filterDateFrom" id="txtTo" autocomplete="off" placeholder="dd/mm/yyyy">
            </div>
              <div class="col-md-2">
               <input type='submit' name='but_search' class="filer btn btn-default" value='Download as Excel'>
            </div>
            <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/downloadinventoryreportlist" style="position: absolute;right: 31px;" class="btn btn-info">Download All as Excel</a>
            </form>
          </div>
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Inventry Report">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Product Type</th>
                          <th scope="col" class='collapsable'>Gift Voucher No</th>
                          <th scope="col" class='collapsable'>Date & Time Added</th>
                          <th scope="col" class='collapsable'>Issued date & time</th>
                          <th scope="col" class='collapsable'>Customer Mob No</th>
                          <th scope="col" class='collapsable'>Email ID</th>
                          <th scope="col" class='collapsable'>Name</th>
                          <th scope="col" class='collapsable'>Voucher Code</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($InventoryReportList) && !empty($InventoryReportList) ){
                        $i= $startFrom;
                      foreach($InventoryReportList as $key=>$row){
                        //$res = explode(' ',$row['created_on']);
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row['title'];?></td>
                      <td class='collapsable'><?php echo $row['reward_code'];?></td>
                      <td class='collapsable'><?php echo $row['registereddate'];?></td>
                      <td class='collapsable'><?php echo $row['created_on'];?></td>
                      <td class='collapsable'><?php echo $row['mobile'];?></td>
                      <td class='collapsable'><?php echo $row['email'];?></td>
                      <td class='collapsable'><?php echo $row['name'];?></td>
                      <td class='collapsable'><?php echo $row['vouchercode'];?></td>
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="9">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
