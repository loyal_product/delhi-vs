<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Customer Registration & Booking Report</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Customer Registration & Booking Report</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="col-md-12" id="importFrm" style="margin-bottom: 5px;border: 1px solid #eee;padding: 4px;">
              <label>Filter by Used voucher</label>
            <form method="post" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/downloaduserexcelbydatepicker'?>">
            <div class="col-md-3">
              <input type="text" style="line-height: 15px;" class="form-control" name="filterDateTo" id="txtFrom" autocomplete="off" placeholder="dd/mm/yyyy">
            </div>
            <div class="col-md-3">
              <input type="text" style="line-height: 15px;" class="form-control" name="filterDateFrom" id="txtTo" autocomplete="off" placeholder="dd/mm/yyyy">
            </div>
              <div class="col-md-2">
               <input type='submit' name='but_search' class="filer btn btn-default" value='Download as Excel'>
            </div>
            <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/downloaduservoucherlist" style="position: absolute;right: 31px;" class="btn btn-info">Download All as Excel</a>
            </form>
          </div>
            <div class="row">
              <div class="col-md-12">                
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Customer Registration Booking Report">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'> S.No</th>
                          <th scope="col" class='collapsable'> Voucher Code</th>
                          <th scope="col" class='collapsable'> Voucher Opening Balance</th>
                          <th scope="col" class='collapsable'> Mobile number</th>
                          <th scope="col" class='collapsable'> Email id</th>
                          <th scope="col" class='collapsable'> Name</th>
                          <th scope="col" class='collapsable'> Date & Time of Registration</th>
                          <th scope="col" class='collapsable'> Gift Voucher Choose</th>
                          <th scope="col" class='collapsable'> Qty</th>
                          <th scope="col" class='collapsable'> Gift Voucher Code Issued</th>
                          <th scope="col" class='collapsable'> Closing Balance</th>
                        </tr>
                      </thead>
                  <tbody>
                  <?php                    
                    if((count($usedVoucherList)>0) && !empty($usedVoucherList) ){
                      $closeBalance = 0;                    
                      $i = 0;
                      foreach($usedVoucherList as $key=>$row){
                        $mobile = $row['user_id'];
                        if($i==0){
                          $$mobile = 0;
                          $close = $row['amount']-$row['price'];
                        }else{
                          $close = $row['amount']-($$mobile+$row['price']);
                        }                     
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['vouchercode'];?></td>
                      <td class='collapsable'><?php echo $row['amount']-$$mobile;?></td>
                      <td class='collapsable'><?php echo $row['mobile'];?></td>
                      <td class='collapsable'><?php echo $row['email'];?></td>
                      <td class='collapsable'><?php echo $row['name'];?></td>
                      <td class='collapsable'><?php echo $row['registereddate'];?></td>
                      <td class='collapsable'><?php echo $row['title'];?></td>
                      <td class='collapsable'>1</td>
                      <td class='collapsable'><?php echo $row['created_on'];?></td>
                      <td class='collapsable'><?php echo $close; ?></td>
                    </tr>
                    <?php  
                      $i++;
                      $$mobile = $$mobile+$row['price'];                      
                    } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> </tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>


<!-- Modal -->
    <div class="modal fade" id="outlet_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
               
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->

<style type="text/css">

</style>