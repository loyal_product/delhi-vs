<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Inventory Dashboard</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Inventory Dashboard</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Inventory Dashboard">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Product Name</th>
                          <th scope="col" class='collapsable'>Total Inventory Added</th>
                          <th scope="col" class='collapsable'>Balance Qty</th>
                        </tr>
                      </thead>
                  <tbody>
                  <?php 
                    if(isset($resData) && !empty($resData) ){
                        $i= $startFrom;
                      foreach($resData as $key=>$row){
                        //$res = explode(' ',$row['created_on']);
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row['producttitle'];?></td>
                      <td class='collapsable'><?php echo $row['total'];?></td>
                      <td class='collapsable'><?php echo $row['baltotal'];?></td>
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                   <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
