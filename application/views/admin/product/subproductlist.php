<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Product List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Product List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Product List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/subproductadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Product</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/uploadproduct" style="position: absolute;right: 155px;" class="btn btn-default">Upload Product</a>
            </h4>

            <div class="row">
              <div class="col-md-12">
                <?php if($this->session->flashdata('product')) {
                  $message = $this->session->flashdata('product'); ?>
                  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>

                <?php if(isset($subproductlists) && count($subproductlists)>0){ ?>
                  <div style="height:30px;"><button type="button" name="deactivate_all" id="deactivate_all" class="btn btn-danger btn-xs">Deactivate All</button></div>
                <?php } ?>
                <div id="msgshow"></div> 
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Sub Product List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col">Product Name</th>
                          <th scope="col" class='collapsable'>Sub Product Name</th>
                          <th scope="col" class='collapsable'>Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php            
                    if(isset($subproductlists) && !empty($subproductlists) ){
                        $i= $startFrom;
                      foreach($subproductlists as $key=>$row){
                    ?>
                    <tr>
                      <td><input type="checkbox" class="deactivate_checkbox" value="<?php echo $row->id; ?>" /></td>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row->title; ?></td>
                      <td class='collapsable'><?php echo $row->sub_prod_name; ?></td>
                      <td class='collapsable'><?php if($row->status=='1'){echo "<p style='color:green;'><strong>Active</strong></p>";} else { echo "<p style='color:red;'><strong>Inactive</strong></p>";} ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editsubproduct/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>
                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletesubproduct/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>
                    </tr>
                    <?php } } else {  ?>
                      <tr><td colspan="5">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                  <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  $('#deactivate_all').click(function(){
    var checkbox = $('.deactivate_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplvoucheredeactivate",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          success:function() {
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple deactivate has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
     alert('Please select atleast one records');
    }
   });
</script>