<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Product</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a>Create Product</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Product</strong></h4>
            <div class="row">
              <form  id="subproductAdd" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/subproductinsert" method="post" enctype="multipart/form-data">
                  <?php if($this->session->flashdata('product')) {
                      $message = $this->session->flashdata('product'); ?>
                    <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>

                  <div class="col-md-12">
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Product Name</label>
                        <select class="form-control" name="pid" id="pid">
                          <option value="">--Please Select--</option>
                          <?php if(isset($productLists)){ 
                            foreach ($productLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" id="title"  placeholder="Title">
                      </div>
                    </div>                    
                  </div>
                  
                  <div class="col-md-12">
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Language</label>
                        <select class="form-control" name="langid" id="langid">
                          <option value="">--Please Select--</option>
                          <?php if(isset($languageLists)){ 
                            foreach ($languageLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>"><?php echo $val['lang_title'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="subpstatus" id="subpstatus">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>CRM Product ID<span style="font-weight: normal;font-size: 10px;"> (Only enter numeric value)</span></label>
                        <input type="text" class="form-control" name="crm_prod_id" id="crm_prod_id" placeholder="CRM Product ID" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
$("#crm_prod_id").keyup(function () {
    var val = $(this).val();
      if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2) 
        val =val.replace(/\.+$/,"");
      }
    $(this).val(val);       
  });
  $(document).ready(function() {
    $('#subproductAdd').on('submit', function (e) {
      e.preventDefault();              
      var noError     = true;
      var pid         = $("#pid").val();
      var title       = $("#title").val();
      var subpstatus  = $("#subpstatus").val();
      var crm_prod_id = $("#crm_prod_id").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(title == ""){
        $('#title').css('border','1px solid red');
        noError = false;               
      } else {
        $('#title').css('border','1px solid #eee');
        noError = true; 
      }
      if(subpstatus == ""){
        $('#subpstatus').css('border','1px solid red');
        noError = false;               
      } else {
        $('#subpstatus').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_prod_id == ""){
        $('#crm_prod_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_prod_id').css('border','1px solid #eee');
        noError = true; 
      }

      if(noError){
        $('#subproductAdd')[0].submit();   
      }
    });
  });
</script>