<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Upload Product</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a>Upload Product</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Upload Product</strong>
              <a href="<?php echo base_url().'assets/download/sample-of-product.csv';?>" style="position: absolute;right: 30px;width: 180px; height: 50px;" class="btn btn-default">Sample file download </br> for upload</a>
            </h4>
            <div class="row">
              <form  id="productcsvupload" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/uploadproductascsv" method="post" enctype="multipart/form-data">
                  <?php if($this->session->flashdata('product')) {
                      $message = $this->session->flashdata('product'); ?>
                    <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>

                  <div class="col-md-12">
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Product Name</label>
                        <select class="form-control" name="pid" id="pid">
                          <option value="">--Please Select--</option>
                          <?php if(isset($productLists)){ 
                            foreach ($productLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <div id="msg"></div>
                        <label>&nbsp;</label></br>
                        <label for="file-upload" class="custom-file-upload">
                        <em class="fa fa-cloud-upload"></em> Choose File </label> 
                        <style type="text/css">
                          input[type="file"] {
                            display: none;
                          }
                          .custom-file-upload {
                            border: 1px solid #ccc;
                            display: inline-block;
                            padding: 8px 6px;
                            cursor: pointer;
                          }
                        </style>       
                        <input type="file" id="file-upload" name="file" accept=".csv" onchange="triggerValidation(this)" /><br>
                        <label style="font-size: 10px;text-transform: initial;color: red;">* Only choose csv file</label>
                      </div>
                    </div>                     
                  </div>
                  
                  <div class="col-md-12">
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Language</label>
                        <select class="form-control" name="langid" id="langid">
                          <option value="">--Please Select--</option>
                          <?php if(isset($languageLists)){ 
                            foreach ($languageLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>"><?php echo $val['lang_title'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                    <div  class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="subpstatus" id="subpstatus">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    var regex = new RegExp("(.*?)\.(csv)$");
    function triggerValidation(el) {
      if (!(regex.test(el.value.toLowerCase()))) {
        el.value = '';
        $('#msg').html('<p style="color:red">Please select only csv file.</p>');
      }
    }

    $('#productcsvupload').on('submit', function (e) {
      e.preventDefault();              
      var noError     = true;
      var pid         = $("#pid").val();
      var fileupload    = $("#file-upload").val();
      var subpstatus  = $("#subpstatus").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(fileupload == ""){
        $('.custom-file-upload').css('border','1px solid red');
        noError = false;               
      } else {
        $('.custom-file-upload').css('border','1px solid #eee');
        noError = true; 
      }
      if(subpstatus == ""){
        $('#subpstatus').css('border','1px solid red');
        noError = false;               
      } else {
        $('#subpstatus').css('border','1px solid #eee');
        noError = true; 
      }

      if(noError){
        $('#productcsvupload')[0].submit();   
      }
    });
  });
</script>