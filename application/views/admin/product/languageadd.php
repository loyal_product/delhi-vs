<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Language</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Create Language</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Language</strong></h4>
            <div class="row">
              <form  id="languaeAdd" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/languageinsert" method="post" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('languagemsg')) {
                      $message = $this->session->flashdata('languagemsg');
                   ?>
                 <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php } ?>
                   
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="lang_title" id="lang_title"  placeholder="Title">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select name="status" id="status" class="form-control">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                 </div>                
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#languaeAdd').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var lang_title    = $("#lang_title").val();
      var status        = $("#status").val();

      if(lang_title == ""){
        $('#lang_title').css('border','1px solid red');
        noError = false;               
      } else {
        $('#lang_title').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#languaeAdd')[0].submit();   
      }
    });
  });
</script>