<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Offer</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a>Create Offer</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Offer</strong></h4>
            <div class="row">
              <form  id="productAdd" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/ProductInsert" method="post" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('product')) {
                      $message = $this->session->flashdata('product');
                   ?>
                 <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php } ?>
                   
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" id="title"  placeholder="Title">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Image</label><br>
                          <style type="text/css">
                            input[type="file"] {
                              display: none;
                            }
                            .custom-file-upload {
                              border: 1px solid #ccc;
                              display: inline-block;
                              padding: 8px 6px;
                              cursor: pointer;
                            }
                          </style>
                        <label for="image" class="custom-file-upload">
                          <em class="fa fa-cloud-upload"></em> Choose Image </label>
                        <input type="file" class="form-control" name="image" id="image"  autocomplete="off"  placeholder="Image" >
                                               
                      </div> 
                    </div>
                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Terms and Condition</label>
                        <textarea class='ckeditor' name='tandc' id="description1"></textarea>
                      </div> 
                    </div>   
                    <div class="col-md-6">
                      <div class="form-group">
                        <img id="preview" width="200" height="200" src="#" alt="image" />
                      </div> 
                      <div class="form-group">
                        <label>Booking Form</label>
                        <select class="form-control" name="booking_form_id" id="booking_form_id">
                          <option value="">--Please Select--</option>
                          <option value="1">Movies For 2</option>
                          <option value="2">Lunch for 2</option>
                          <option value="3">Salon Services</option>
                          <option value="4">Amazon Pay Voucher</option>
                          <option value="5">Uber Voucher / Swiggy Voucher / Free Coffee</option>
                          <option value="6">Bigcity Cashback</option>
                          <option value="7">Health Checkup</option>
                          <option value="8">Merchandise</option>
                          <option value="9">Bogo Dining</option>
                          <option value="10">Rs. X Off on Salon Services</option>
                          <option value="11">Mobile Recharge</option>
                        </select>
                      </div>
                      <?php if(isset($cashbackofferlist)){ ?>
                      <div class="form-group" id="cashbackidshow" style="display: none;">
                        <label>Select Cashback</label>
                        <select class="form-control" name="cashbackid" id="cashbackid">
                          <option value="">--Please Select--</option>
                          <?php foreach ($cashbackofferlist as $key => $val) { ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo $val['title']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea class='ckeditor' name='pro_description' id="description2"></textarea>
                      </div> 
                    </div>   
                    <div class="col-md-6">  
                      <div class="col-md-12">                    
                        <div class="form-group">
                          <label>Offer Till Date</label>
                          <input type="text" class="form-control" name="offertilldate" id="offertilldate" placeholder="dd-mm-yyyy">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Product Title</label>
                          <input type="text" class="form-control" name="producttitle" id="producttitle" placeholder="Product Title">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="form-control" name="pstatus" id="pstatus">
                            <option value="">--Please Select--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>CRM Offer ID<span style="font-weight: normal;font-size: 10px;"> (Only enter numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_offer_id" id="crm_offer_id" placeholder="CRM Offer ID">
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="col-md-12">
                    <div class="form-group">
                      <label>SMS</label>
                      <textarea class="form-control" name="productsms" rows="6" id="productsms"></textarea>
                    </div>
                  </div> -->
                  
                  

                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
    $("#crm_offer_id").keyup(function () {
    var val = $(this).val();
      if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2) 
        val =val.replace(/\.+$/,"");
      }
    $(this).val(val);       
  });
  
  $(document).ready(function(){
    $('#cashbackidshow').hide();
    $('#booking_form_id').change(function() {
      var cval = $(this).val();
      if (cval == '6') {
        $("#cashbackid").attr("required","required");
        $('#cashbackidshow').show();
      } else {
        $("#cashbackid").removeAttr("required","required");
        $('#cashbackidshow').hide();
      }
    });
  });
  function readIMG(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#image").change(function(){
    readIMG(this);
  });

  $(document).ready(function() {
    $('#offertilldate').datepicker({ 
        format: 'dd-mm-yyyy'
    });
    $('#productAdd').on('submit', function (e) {
      e.preventDefault();              
      var noError   = true;
      var title     = $("#title").val(); 
      var booking_form_id = $("#booking_form_id").val();
      var crm_offer_id = $("#crm_offer_id").val();
      var pstatus = $("#pstatus").val();

      if(booking_form_id == ""){
        $('#booking_form_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#booking_form_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(title == ""){
        $('#title').css('border','1px solid red');
        noError = false;               
      } else {
        $('#title').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_offer_id == ""){
        $('#crm_offer_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_offer_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(pstatus == ""){
        $('#pstatus').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pstatus').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#productAdd')[0].submit();   
      }
    });
  });
</script>