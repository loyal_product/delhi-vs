<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Language List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Language List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Language List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/languageadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Language</a>
            </h4>

            <div class="row">
              <div class="col-md-12">
                <?php
                    if($this->session->flashdata('languagemsg')) {
                    $message = $this->session->flashdata('languagemsg');
                  ?>
                    <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Language List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col">Title</th>
                          <th scope="col">Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php          
                    if(isset($languagelist) && !empty($languagelist) ){
                      $i= $startFrom;
                      foreach($languagelist as $key=>$row){
                        $i++;
                    ?>

                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row->lang_title; ?></td>
                      <td class='collapsable'><?php if($row->status==1){ echo "<p style='color:green'><strong>Active</strong></p>";}else{echo "<p style='color:red'><strong>Inactive</strong></p>";} ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editlanguagelist/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletelanguage/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>
                    </tr>
                    <?php  } } else {  ?>
                      <tr><td  colspan="10">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                  <ul class="tsc_pagination">
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
