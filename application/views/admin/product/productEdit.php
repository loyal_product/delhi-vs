<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Offer</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active">
              <a>Edit Offer</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Offer</strong></h4>
            <div class="row">
               <?php
                  if($this->session->flashdata('product')) {
                  $message = $this->session->flashdata('product');
                ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php   }    ?>
                 <?php 
                    if(isset($productList)){
                      foreach($productList as $key=>$row){
              ?>
              <form id="productEdit" method="POST" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updateproduct/<?php echo $row->id; ?>" enctype="multipart/form-data">
                
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" value="<?php echo $row->title; ?>" name="title" id="title"  placeholder="Title">
                      </div>                      
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image" id="image"  autocomplete="off"  placeholder="File" >
                      </div>
                    </div>

                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Term & Condition</label>
                        <textarea class='ckeditor' name='tandc' id="description1"><?php echo $row->tandc; ?></textarea>
                      </div> 
                    </div> 
                    <div class="col-md-6">
                      <?php if(isset($row->image) && $row->image!=""){ ?>
                      <div class="form-group">
                        <img src="<?php echo base_url().'assets/uploads/'.$row->image; ?>" width="90" height="80" alt="banner">
                      </div>
                      <?php } ?>
                      <div class="form-group">
                        <label>Booking Form</label>
                        <select class="form-control" name="booking_form_id" id="booking_form_id">
                          <option value="">--Please Select--</option>
                          <option value="1" <?php if($row->booking_form_id==1){echo 'selected="selected"';} ?>>Movies For 2</option>
                          <option value="2" <?php if($row->booking_form_id==2){echo 'selected="selected"';} ?>>Lunch for 2</option>
                          <option value="3" <?php if($row->booking_form_id==3){echo 'selected="selected"';} ?>>Salon Services</option>
                          <option value="4" <?php if($row->booking_form_id==4){echo 'selected="selected"';} ?>>Amazon Pay Voucher</option>
                          <option value="5" <?php if($row->booking_form_id==5){echo 'selected="selected"';} ?>>Uber Voucher / Swiggy Voucher / Free Coffee</option>
                          <option value="6" <?php if($row->booking_form_id==6){echo 'selected="selected"';} ?>>Bigcity Cashback</option>
                          <option value="7" <?php if($row->booking_form_id==7){echo 'selected="selected"';} ?>>Health Checkup</option>
                          <option value="8" <?php if($row->booking_form_id==8){echo 'selected="selected"';} ?>>Merchandise</option>
                          <option value="9" <?php if($row->booking_form_id==9){echo 'selected="selected"';} ?>>Bogo Dining</option>
                          <option value="10" <?php if($row->booking_form_id==10){echo 'selected="selected"';} ?>>Rs. X Off on Salon Services</option>
                          <option value="11" <?php if($row->booking_form_id==11){echo 'selected="selected"';} ?>>Mobile Recharge</option>
                        </select>
                      </div>
                      
                      <?php if ($row->cashbackid != '') {
                       if(isset($cashbackofferlist)){ ?>
                      <div class="form-group" id="cashbackidshow">
                        <label>Select Cashback</label>
                        <select class="form-control" name="cashbackid" id="cashbackid">
                          <option value="">--Please Select--</option>
                          <?php foreach ($cashbackofferlist as $key => $val) { ?>
                            <option value="<?php echo $val['id']; ?>" <?php if($row->cashbackid==$val['id']){echo 'selected="selected"';} ?>><?php echo $val['title']; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    <?php } ?>
                  <?php } else { ?>
                    <div class="form-group" id="cashbackidshow" style="display: none;">
                      <label>Select Cashback</label>
                      <select class="form-control" name="cashbackid" id="cashbackid">
                        <option value="">--Please Select--</option>
                        <?php foreach ($cashbackofferlist as $key => $val) { ?>
                          <option value="<?php echo $val['id']; ?>" <?php if($row->cashbackid==$val['id']){echo 'selected="selected"';} ?>><?php echo $val['title']; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <?php } ?>
                    </div>                   
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea class='ckeditor' name='pro_description' id="description2"><?php echo $row->pro_description; ?></textarea>
                      </div> 
                    </div>   
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Offer Till Date</label>
                          <input type="text" name="offertilldate" id="offertilldate" placeholder="dd-mm-yyyy" value="<?php echo $row->offertilldate; ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Product Title</label>
                          <input type="text" class="form-control" name="producttitle" id="producttitle" placeholder="Product Title" value="<?php echo $row->producttitle; ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="form-control" name="pstatus" id="pstatus">
                            <option value="">--Please Select--</option>
                            <option value="1" <?php if($row->pstatus=='1'){echo 'selected="selected"';}?>>Active</option>
                            <option value="0" <?php if($row->pstatus=='0'){echo 'selected="selected"';}?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>CRM Offer ID<span style="font-weight: normal;font-size: 10px;"> (Only enter numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_offer_id" id="crm_offer_id" placeholder="CRM Offer ID" value="<?php echo $row->crm_offer_id; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Offer Till Date</label>
                        <input type="text" name="offertilldate" id="offertilldate" placeholder="dd-mm-yyyy" value="<?php echo $row->offertilldate; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Product Title</label>
                        <input type="text" class="form-control" name="producttitle" id="producttitle" placeholder="Product Title" value="<?php echo $row->producttitle; ?>">
                      </div>
                    </div>
                  </div> -->

                 <!--  <div class="col-md-12">
                    <div class="form-group">
                      <label>SMS</label>
                      <textarea class="form-control" name="productsms" rows="6" id="productsms"><?php echo $row->productsms; ?></textarea>
                    </div>
                  </div> -->

                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Update</button>
                      </div>
                    </div>
                  </div>
              </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
    $("#crm_offer_id").keyup(function () {
    var val = $(this).val();
      if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2) 
        val =val.replace(/\.+$/,"");
      }
    $(this).val(val);       
  });
  $(document).ready(function(){
    //$("#cashbackidshow").hide();
    $('#booking_form_id').change(function() {      
      var cval = $(this).val();
      if (cval == '6') {
        $("#cashbackid").attr("required","required");
        $('#cashbackidshow').show();
      } else {
        $("#cashbackid").removeAttr("required","required");
        $('#cashbackidshow').hide();
      }
    });
  });
  $(document).ready(function() {
    $('#offertilldate').datepicker({ 
        format: 'dd-mm-yyyy'
    });
    $('#productAdd').on('submit', function (e) {
      e.preventDefault();              
      var noError   = true;
      var title     = $("#title").val();
      var booking_form_id = $("#booking_form_id").val();
      var crm_offer_id = $("#crm_offer_id").val();
      var pstatus = $("#pstatus").val();

      if(booking_form_id == ""){
        $('#booking_form_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#booking_form_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(title == ""){
        $('#title').css('border','1px solid red');
        noError = false;               
      } else {
        $('#title').css('border','1px solid #eee');
        noError = true; 
      }
      if(pstatus == ""){
        $('#pstatus').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pstatus').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(crm_offer_id == ""){
        $('#crm_offer_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_offer_id').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#productAdd')[0].submit();   
      }
    });
  });
</script>