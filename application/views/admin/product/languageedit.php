<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Product</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active"><a>Edit Product</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Product</strong></h4>
            <div class="row">
               <?php
                  if($this->session->flashdata('languagemsg')) {
                  $message = $this->session->flashdata('languagemsg');
                ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php   }    ?>
                <?php 
                  if(isset($languageList)){
                    foreach($languageList as $key=>$row){
                ?>
              <form id="productEdit" method="POST" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updatelanguage/<?php echo $row->id; ?>" enctype="multipart/form-data">
                
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" value="<?php echo $row->lang_title; ?>" name="lang_title" id="lang_title" placeholder="Title">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select name="status" id="status" class="form-control">
                          <option value="">--Please Select--</option>
                          <option value="1" <?php if($row->status==1){echo 'selected="selected"';}?>>Active</option>
                          <option value="0" <?php if($row->status==0){echo 'selected="selected"';}?>>Inactive</option>
                        </select>
                      </div>
                    </div>
                 </div>  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Update</button>
                      </div>
                    </div>
                  </div>
              </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
