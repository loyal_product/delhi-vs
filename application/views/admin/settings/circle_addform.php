<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Circle List</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/circlelisting"><a>Circle List</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom: 25px !important;"><strong>Circle List</strong></h4>
            <div id="msgshow"></div>
            <div class="row">
            <div>
              <h4>Add Circle</h4>
            </div>
            <form id="circleAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/circleinsert" method="POST" enctype="multipart/form-data"> 
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Product Name</label>
                    <select class="form-control" name="pid" id="pid">
                      <option value="">--Please Select--</option>
                      <?php 
                        if(isset($productLists)){ 
                          foreach ($productLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>">
                              <?php echo $val['title'];?>
                            </option>
                            <?php 
                          } 
                        } 
                      ?>
                    </select>                                              
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>State</label>
                    <select class="form-control" name="state_id" id="state_id">
                      <option value="">--Please Select--</option>
                    </select>                      
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>CRM City ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                    <input type="text" class="form-control" name="crm_city_id" id="crm_city_id" placeholder="CRM City ID" >
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Circle Name</label>
                    <div class="input_fields_wraps">
                      <input type="text" class="form-control circlename" name="circlename[]" placeholder="Circle Name" >  
                    </div>                      
                  </div>
                </div>
                <div class="col-md-1"><br>
                  <button class="btn btn-info add_Btn" style="margin-top: 5px;" type="button">Add</button>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status">
                      <option value="">--Please Select--</option>
                      <option value="1">Active</option>
                      <option value="0">Inactive</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <button type="submit" class="btn btn-default">Submit</button>
                  </div>
                </div>
              </div>
            </form> 
          </div>
        </div>
      </div>
    </div>  
  </div> <!-- container -->                         
</div> <!-- content -->
</div>

<script type="text/javascript">
    $("#crm_city_id").keyup(function () {
      var val = $(this).val();
      if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2) 
        val =val.replace(/\.+$/,"");
      }
      $(this).val(val);       
    });

//-----------------------------------------------------------------------------//

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wraps"); //Fields wrapper
    var add_button      = $(".add_Btn"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append('<div><input style="margin-top:3px;" type="text" class="form-control circlename" name="circlename[]" placeholder="Circle Name" /><a href="#" class="remove_field">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    //---------------------------------------------------------------------------//

    $('#cicircleAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var pid             = $("#pid").val();
      var status          = $("#status").val();
      var circlename        = $(".circlename").val();
      var crm_city_id     = $("#crm_city_id").val();
      var state_id     = $("#state_id").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(state_id == ""){
        $('#state_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#state_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_city_id == ""){
        $('#crm_city_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_city_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(circlename == ""){
        $('.circlename').css('border','1px solid red');
        noError = false;               
      } else {
        $('.circlename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#circleAddForm')[0].submit();   
      }

    });


    $('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getstatelist",
          beforeSend: function(){
                    $('.loader1').removeClass('hide');

                },
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('.loader1').addClass('hide');
            $('#state_id').html(data);
          }
        });
      } else {
        $('#state_id').html('<option value=""></option>');
      }
    });
</script>