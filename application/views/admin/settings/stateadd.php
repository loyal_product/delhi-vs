<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Add State</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Add State</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Add State</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <form id="stateAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/stateinsert" method="POST" enctype="multipart/form-data">
                    <div class="col-md-12">
                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label>State Name</label>
                            <div class="input_fields_wrap_venue">
                              <input type="text" class="form-control statename" name="statename[]" placeholder="State Name" >  
                            </div>                      
                          </div>
                        </div>
                      <div class="col-md-2"></br>
                        <button class="btn btn-info add_Btn_Venue" style="margin-top: 5px;" type="button">Add</button>
                      </div>
                    </div>
                      
                      
                    </div>
                    <div class="col-md-12">
                      
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>CRM State ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_state_id" id="crm_state_id" placeholder="CRM State ID" >
                        </div>
                      </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status" id="status">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    var max_fieldss      = 20; //maximum input boxes allowed
    var wrappers         = $(".input_fields_wrap_venue"); //Fields wrapper
    var add_button_venue = $(".add_Btn_Venue"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button_venue).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fieldss) { //max input box allowed
        x++; //text box increment
        $(wrappers).append('<div><input style="margin-top:3px;" type="text" class="form-control statename" name="statename[]" placeholder="State Name" /><a href="#" class="remove_field_venue">Remove</a></div>'); //add input box
      }
    });    
    $(wrappers).on("click",".remove_field_venue", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    $("#crm_state_id").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    
    $('#stateAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError        = true;
      var pid       = $("#pid").val();
      
      var statename      = $(".statename").val();
      var status         = $("#status").val();
      var crm_state_id = $("#crm_state_id").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_state_id == ""){
        $('#crm_state_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_state_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(statename == ""){
        $('.statename').css('border','1px solid red');
        noError = false;               
      } else {
        $('.statename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#stateAddForm')[0].submit();
      }
    });

    /*$('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getcitylist",
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('#city_id').html(data);
          }
        });
      } else {
        $('#city_id').html('<option value=""></option>');
      }
    });*/

  });
     
</script>