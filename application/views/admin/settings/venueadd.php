<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Add Venue</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Add Venue</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Add Venue</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <form id="venueAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venueinsert" method="POST" enctype="multipart/form-data">
                    <div class="col-md-12">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>State</label>
                          <select class="form-control" name="state_id" id="state_id">
                            <option value="">--Please Select--</option>
                          </select>                      
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>City</label>
                          <select class="form-control" name="city_id" id="city_id">
                            <option value="">--Please Select--</option>
                          </select>                      
                        </div>
                      </div>                      
                    </div>

                    <div class="col-md-12" style="border: 1px solid #eee;margin: 5px;">
                      <div class="input_fields_wrap_venue">
                        <div class="col-md-12">
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>CRM Venue ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                              <input type="text" class="form-control" name="crm_venue_id[]" id="crm_venue_id" placeholder="CRM Venue ID" >
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Venue Name</label><br>
                                <label>&nbsp;</label>                        
                                  <input type="text" class="form-control venuename" name="venuename[]" placeholder="Venue Name" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_veg_price" name="monday_veg_price[]" placeholder="Veg Monday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_veg_price" name="tuesday_veg_price[]" placeholder="Veg Tuesday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_veg_price" name="wednesday_veg_price[]" placeholder="Veg Wednesday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_veg_price" name="thursday_veg_price[]" placeholder="Veg Thursday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_veg_price" name="friday_veg_price[]" placeholder="Veg Friday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_veg_price" name="saturday_veg_price[]" placeholder="Veg Saturday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_veg_price" name="sunday_veg_price[]" placeholder="Veg Sunday Price" > 
                              </div>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_nonveg_price" name="monday_nonveg_price[]" placeholder="Non Veg Monday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_nonveg_price" name="tuesday_nonveg_price[]" placeholder="Non Veg Tuesday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_nonveg_price" name="wednesday_nonveg_price[]" placeholder="Non Veg Wednesday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_nonveg_price" name="thursday_nonveg_price[]" placeholder="Non Veg Thursday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_nonveg_price" name="friday_nonveg_price[]" placeholder="Non Veg Friday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_nonveg_price" name="saturday_nonveg_price[]" placeholder="Non Veg Saturday Price" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_nonveg_price" name="sunday_nonveg_price[]" placeholder="Non Veg Sunday Price" > 
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-2"></br>
                        <button class="btn btn-info add_Btn_Venue" style="margin-top: 5px;" type="button">Add</button>
                      </div>
                    </div>



                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status" id="status">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                    <!-- <div class="col-md-4">
                      <div class="form-group">
                        <label>Meal</label>
                        <select class="form-control" name="meal" id="meal">
                          <option value="">--Please Select--</option>
                          <option value="1">Veg</option>
                          <option value="2">Non Veg</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                          <div class="form-group">
                            <label>Price Per Person</label>
                            <div class="">
                              <input type="text" class="form-control" name="price_per_person" id="price_per_person" placeholder="Price Per Person" >  
                            </div>                      
                          </div>
                        </div> -->
                  <!-- </div> -->
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    var max_fieldss      = 20; //maximum input boxes allowed
    var wrappers         = $(".input_fields_wrap_venue"); //Fields wrapper
    var add_button_venue = $(".add_Btn_Venue"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button_venue).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fieldss) { //max input box allowed
        x++; //text box increment
        //$(wrappers).append('<div><input style="margin-top:3px;" type="text" class="form-control venuename" name="venuename[]" placeholder="Venue Name" /><a href="#" class="remove_field_venue">Remove</a></div>'); //add input box

        $(wrappers).append('<div><div class="col-md-12" style="margin-top: 10px;"><div class="col-md-2"><div class="form-group"><label>CRM Venue ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label><input type="text" class="form-control" name="crm_venue_id[]" id="crm_venue_id" placeholder="CRM Venue ID" ></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Venue Name</label><br><label>&nbsp;</label> <input type="text" class="form-control venuename" name="venuename[]" placeholder="Venue Name" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Monday Price</label><input type="text" class="form-control" id="monday_veg_price" name="monday_veg_price[]" placeholder="Veg Monday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Tuesday Price</label><input type="text" class="form-control" id="tuesday_veg_price" name="tuesday_veg_price[]" placeholder="Veg Tuesday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Wednesday Price</label><input type="text" class="form-control" id="wednesday_veg_price" name="wednesday_veg_price[]" placeholder="Veg Wednesday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Thursday Price</label> <input type="text" class="form-control" id="thursday_veg_price" name="thursday_veg_price[]" placeholder="Veg Thursday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Friday Price</label><input type="text" class="form-control" id="friday_veg_price" name="friday_veg_price[]" placeholder="Veg Friday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Saturday Price</label><input type="text" class="form-control" id="saturday_veg_price" name="saturday_veg_price[]" placeholder="Veg Saturday Price" > </div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Veg</label><br><label>Sunday Price</label><input type="text" class="form-control" id="sunday_veg_price" name="sunday_veg_price[]" placeholder="Veg Sunday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Monday Price</label> <input type="text" class="form-control" id="monday_nonveg_price" name="monday_nonveg_price[]" placeholder="Non Veg Monday Price" > </div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Tuesday Price</label><input type="text" class="form-control" id="tuesday_nonveg_price" name="tuesday_nonveg_price[]" placeholder="Non Veg Tuesday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Wednesday Price</label><input type="text" class="form-control" id="wednesday_nonveg_price" name="wednesday_nonveg_price[]" placeholder="Non Veg Wednesday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Thursday Price</label><input type="text" class="form-control" id="thursday_nonveg_price" name="thursday_nonveg_price[]" placeholder="Non Veg Thursday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Friday Price</label> <input type="text" class="form-control" id="friday_nonveg_price" name="friday_nonveg_price[]" placeholder="Non Veg Friday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Saturday Price</label> <input type="text" class="form-control" id="saturday_nonveg_price" name="saturday_nonveg_price[]" placeholder="Non Veg Saturday Price" ></div></div></div><div class="col-md-2"><div class="col-md-10"><div class="form-group"><label>Non Veg</label><br><label>Sunday Price</label> <input type="text" class="form-control" id="sunday_nonveg_price" name="sunday_nonveg_price[]" placeholder="Non Veg Sunday Price" ></div></div></div></div><div class="col-md-2"><a href="#" class="remove_field_venue" role="button" style="background-color: red;padding: 8px;color: #fff;">Remove</a></div></div>');
      }
    });    
    $(wrappers).on("click",".remove_field_venue", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    });

    $("#crm_venue_id").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $("#monday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#tuesday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#wednesday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#thursday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#friday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#saturday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#sunday_veg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });


    $("#monday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#tuesday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#wednesday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#thursday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#friday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#saturday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    $("#sunday_nonveg_price").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
    
    $('#venueAddForm').on('submit', function (e) {
      var reg = "/^[1-9]\d*(\.\d+)?$/"; //reg pattern<br/>
      if(reg.test("1.23")){//validate
      }else{
        alert('no');
      }
      e.preventDefault();              
      var noError        = true;
      var pid            = $("#pid").val();
      var city_id        = $("#city_id").val();
      var state_id       = $("#state_id").val();
      var venuename      = $("#venuename").val();
      var status         = $("#status").val();
      var crm_venue_id   = $("#crm_venue_id").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(state_id == ""){
        $('#state_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#state_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_venue_id == ""){
        $('#crm_venue_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_venue_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(city_id == ""){
        $('#city_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#city_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(venuename == ""){
        $('#venuename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#venuename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#venueAddForm')[0].submit();
      }
    });

    $('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getstatelist",
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('#state_id').html(data);
          }
        });
      } else {
        $('#state_id').html('<option value=""></option>');
      }
    });

    $('#state_id').change(function(){
      var state_id = $('#state_id').val();
      if(state_id != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getcitylist",
          method:"POST",
          data:{state_id:state_id},
          success:function(data) {
            $('#city_id').html(data);
          }
        });
      } else {
        $('#city_id').html('<option value=""></option>');
      }
    });

  });
     
</script>