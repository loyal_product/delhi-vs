<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">State List</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active">
              <a>Venue List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom: 25px !important;"><strong>State List</strong></h4>
            <div class="row"> 
              <div class="col-md-12" style="border: 1px solid #ccc;margin-bottom: 5px;">
                <form method="GET" id="searchbyvenue" class="search_order_input" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/searchbyvenuelists'?>">                    
                  <div class="row" style="padding-bottom: 5px;background-color: #fff;">
                    <div class="col-md-4 col-xs-12">
                      <label>Select Offer Name</label>
                      <select name="offername" class="form-control" id="offername">
                        <option value="">--Please Select--</option>
                        <?php if(isset($offerlists)) {
                          foreach ($offerlists as $key => $value) { ?>
                            <option value="<?php echo $value['id'];?>"><?php echo $value['title'];?></option>
                         <?php } } ?>
                      </select>
                    </div>
                    <div class="col-md-4 col-xs-12">
                      <label>Select City Name</label>
                      <select name="cityid" class="form-control" id="cityid">
                        <option value="">--Please Select--</option>
                      </select>
                    </div>
                    <div class="col-md-4 col-xs-12" style="margin-top: 25px;">
                      <input type="submit" name="search" class="btn btn-info" id="search" value="Get State">
                    </div>
                  </div> 
                </form>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">  
              </div>              
              <?php if($this->session->flashdata('msgshow')) {
                $message = $this->session->flashdata('msgshow'); ?>
                <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <div class="row">
                <div class="col-md-2">
                <?php if(isset($venueListsss) && count($venueListsss)>0){ ?>
                  <div style="height:30px;"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete All</button></div>
                <?php } ?>
                </div> 
                <div class="col-md-2"> 
                <?php if(isset($venueListsss) && count($venueListsss)>0){ ?>
                  <div style="height:30px;"><button type="button" name="deactivate_all" id="deactivate_all" class="btn btn-info btn-xs">Deactivate All</button></div>
                <?php } ?>
                </div> 
                <div class="col-md-2">
                <?php if(isset($venueListsss) && count($venueListsss)>0){ ?>
                  <div style="height:30px;"><button type="button" name="activate_all" id="activate_all" class="btn btn-success btn-xs">Activate All</button></div>
                <?php } ?>
                </div> 
                </div>
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="State list">
                  <thead>
                    <tr>
                      <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                      <th scope="col" class='collapsable'>S.No</th>
                      <th scope="col" class='collapsable'>Offer Name</th>
                      
                      <th scope="col" class='collapsable'>State Name</th>
                      <th scope="col" class='collapsable'>Status</th>
                      <th scope="col" style="text-align:center;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php  //print "<pre>"; print_r($venueListsss) ; die;
                    if(isset($stateListsss) && !empty($stateListsss) ){
                        $i= $startFrom;
                      foreach($stateListsss as $key=>$row) {
                    ?>
                    <tr>
                      <td><input type="checkbox" class="delete_checkbox" value="<?php echo $row['id']; ?>" /></td>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['title']; ?></td>
                      <td class='collapsable'><?php echo $row['name']; ?></td>
                      <td class='collapsable'><?php if ($row['status']=='1') { echo '<p style="color:green;"><strong>Active</strong></p>';} else { echo '<p style="color:red;"><strong>Inactive</strong></p>'; } ?></td>
                      <td class="custom_width" align="center">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editstate/<?php echo $row['id']; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletestate/<?php echo $row['id']; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> </tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links!=''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script>
  $(document).ready(function(){
    $('.selectall').change(function () {
      if ($(this).prop('checked')) {
        $('input').prop('checked', true);
      } else {
        $('input').prop('checked', false);
      }
    });
    $('.selectall').trigger('change');

  $('#delete_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplevenuedelete",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          beforeSend: function () {
            $('.loader1').removeClass('hide');
          },
          success:function() {
            $('.loader1').addClass('hide');
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple delete has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
      $('.loader1').addClass('hide');
     alert('Please select atleast one records');
    }
  });

  $('#deactivate_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure want to deactivate?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplevenuedeactivate",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          beforeSend: function () {
            $('.loader1').removeClass('hide');
          },
          success:function() {
            $('.loader1').addClass('hide');
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple deactivate has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
      $('.loader1').addClass('hide');
     alert('Please select atleast one records');
    }
  });

  $('#activate_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure want to activate?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplevenueactivate",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          beforeSend: function () {
            $('.loader1').removeClass('hide');
          },
          success:function() {
            $('.loader1').addClass('hide');
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple activate has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
      $('.loader1').addClass('hide');
     alert('Please select atleast one records');
    }
  });

   
  $('#searchbyvenue').on('submit', function (e) {
    e.preventDefault();              
    var noError        = true;
    var offername      = $("#offername").val();
    var cityid         = $("#cityid").val();

    if(offername == ""){
      $('#offername').css('border','1px solid red');
      noError = false;               
    } else {
      $('#offername').css('border','1px solid #eee');
      noError = true;
    }
    if(cityid == ""){
      $('#cityid').css('border','1px solid red');
      noError = false;               
    } else {
      $('#cityid').css('border','1px solid #eee');
      noError = true;
    }
      
    if(noError) {
      $('#searchbyvenue')[0].submit();        
    }
  });

  $('#offername').change(function(){
    var offername = $('#offername').val();
    if(offername != '') {
      $.ajax({
        url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getcitylists",
        method:"POST",
        data:{offername:offername},
        beforeSend: function () {
          $('.loader1').removeClass('hide');
        },
        success:function(data) {
          $('.loader1').addClass('hide');
          $('#cityid').html(data);
        }
      });
    } else {
      $('.loader').addClass('hide');
      $('#cityid').html('<option value=""></option>');
    }
  });

  });
</script>