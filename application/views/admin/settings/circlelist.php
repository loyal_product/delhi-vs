<!-- ============================================================== -->
<?php if (isset($searchcirclelist)) { ?>
    <div class="content-page">
   <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Circle List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/circlelist"><a>Circle List</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom: 25px !important;"><strong>Circle List</strong></h4>
            <div id="msgshow"></div>
            <div class="row">
              <div class="col-md-12"> 
                <div style="border: 1px solid #ccc;margin-bottom:5px;">
                  <form method="post" id="searchbycircle" class="search_order_input" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/searchbycirclename'?>">
                   <div class="row" style="padding-bottom: 5px;background-color: #fff;">
                    <div class="col-md-4 col-xs-8">
                      <label>Search By Circle Name</label>
                      <input type="text" name="circlename" class="form-control" id="circlename" value="<?php if(isset($name)){echo $name;} ?>" placeholder="Circle Name">
                    </div>
                     <div class="col-md-2 col-xs-4" style="margin-top: 25px;">
                      <input type="submit" name="search" class="btn btn-info" id="search" value="Search">
                    </div>
                  </div> 
                </form>
              </div>               
              <?php if($this->session->flashdata('msgshow')) {
                $message = $this->session->flashdata('msgshow'); ?>
                <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php if(isset($searchcirclelist) && count($searchcirclelist)>0){ ?>
                  <div style="height:30px;"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete All</button></div>
                <?php } ?> 
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Circle List">
                  <thead>
                    <tr>
                      <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                      <th scope="col" class='collapsable'>S.No</th>
                      <th scope="col" class='collapsable'>Product Name</th>
                      <th scope="col" class='collapsable'>Circle Name</th>
                      <th scope="col" class='collapsable'>Status</th>
                      <th scope="col" style="text-align:center;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php //print "<pre>"; print_r($circleListsss); die;
                    if(isset($searchcirclelist) && !empty($searchcirclelist) ){
                      foreach($searchcirclelist as $key=>$row){
                    ?>
                    <tr>
                      <td><input type="checkbox" class="delete_checkbox" value="<?php echo $row['id']; ?>" /></td>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['producttitle']; ?></td>
                      <td class='collapsable'><?php echo $row['name']; ?></td>
                      <td class='collapsable'><?php if ($row['status']=='1') { echo '<p style="color:green;"><strong>Active</strong></p>';} else { echo '<p style="color:red;"><strong>Inactive</strong></p>'; } ?></td>
                      <td class="custom_width" align="center">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editcircle/<?php echo $row['id']; ?>/<?php echo $row['prod_id']?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletecircle/<?php echo $row['id']; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<?php } else { ?>
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Circle List</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/circlelisting"><a>Circle List</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom: 25px !important;"><strong>Circle List</strong></h4>
            <div id="msgshow"></div>
            <div class="row">
              <div class="col-md-12"> 
                <div style="border: 1px solid #ccc;margin-bottom:5px;">
                  <div class="row">
                    <div class="col-md-10 col-xs-10">
                    <form method="post" id="searchbycircle" class="search_order_input" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/searchbycirclename'?>">
                      <div class="row" style="padding-bottom: 5px;background-color: #fff;">
                        <div class="col-md-4 col-xs-8">
                          <label>Search By circle Name</label>
                          <input type="text" name="circlename" class="form-control" id="circlename" value="<?php if(isset($name)){echo $name;} ?>" placeholder="Circle Name">
                        </div>
                        <div class="col-md-2 col-xs-4" style="margin-top: 25px;">
                          <input type="submit" name="search" class="btn btn-info" id="search" value="Search">
                        </div>
                      </div> 
                    </form>
                  </div>
                    <div class="col-md-1 col-xs-1" style="margin-top: 25px;">
                    <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/circleupload">
                      <button class="btn btn-info">Upload</button>
                    </a>
                  </div>
                  <div class="col-md-1 col-xs-1" style="margin-top: 25px;">
                    <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/circleadd">
                      <button class="btn btn-info">Add New</button>
                    </a>
                  </div>
                  </div>
                </div>               
              <?php if($this->session->flashdata('msgshow')) {
                $message = $this->session->flashdata('msgshow'); ?>
                <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php if(isset($circleList) && count($circleList)>0){ ?>
                <div style="height:30px;"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete All</button></div>
                <?php } ?>           
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Circle List">
                  <thead>
                    <tr>                      
                      <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                      <th scope="col" class='collapsable'>S.No</th>
                      <th scope="col" class='collapsable'>Product Name</th>
                      <th scope="col" class='collapsable'>Circle Name</th>
                      <th scope="col" class='collapsable'>Status</th>
                      <th scope="col" style="text-align:center;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
                      if(isset($circleList) && !empty($circleList) ){
                        $i= $startFrom;
                          foreach($circleList as $key=>$row){ ?>
                    <tr>
                      <td><input type="checkbox" class="delete_checkbox" value="<?php echo $row['id']; ?>" /></td>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['producttitle']; ?></td>
                      <td class='collapsable'><?php echo $row['name']; ?></td>
                      <td class='collapsable'><?php if ($row['status']=='1') { echo '<p style="color:green;"><strong>Active</strong></p>';} else { echo '<p style="color:red;"><strong>Inactive</strong></p>'; } ?></td>
                      <td class="custom_width" align="center">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editcircle/<?php echo $row['id']; ?>/<?php echo $row['prod_id']?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletecircle/<?php echo $row['id']; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="6" class="text-center text-danger">No Record Found!!</td> </tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links!=''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>
<?php } ?>
<!-- <style>.removeRow {background-color: #FF0000;color:#FFFFFF;}</style> -->

<script type="text/javascript">  
  $(document).ready(function() {
    $('#searchbycircle').on('submit', function (e) {
      e.preventDefault();              
      var noError    = true;
      var circlename   = $('#circlename').val();
      if(circlename == ""){
        $('#circlename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#circlename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#searchbycircle')[0].submit();
      }
    });
  });
</script>

<script>
  $(document).ready(function(){ 
    $('.selectall').change(function () {
      if ($(this).prop('checked')) {
        $('input').prop('checked', true);
      } else {
        $('input').prop('checked', false);
      }
    });
    $('.selectall').trigger('change');

   $('#delete_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multipledeletecircle",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          success:function() {
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple delete has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
     alert('Please select atleast one records');
    }
   });

  });
</script>