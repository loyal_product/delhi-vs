<style type="text/css">
  /*select#city_id {
    display: none !important;
  }*/
  .dropdown-select {
    background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#40FFFFFF', endColorstr='#00FFFFFF', GradientType=0);
    background-color: #fff;
    border-radius: 6px;
    border: solid 1px #eee;
    box-shadow: 0px 2px 5px 0px rgba(155, 155, 155, 0.5);
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    float: left;
    font-size: 14px;
    font-weight: normal;
    height: 42px;
    line-height: 23px;
    outline: none;
    padding-left: 18px;
    padding-right: 30px;
    position: relative;
    text-align: left !important;
    transition: all 0.2s ease-in-out;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    white-space: nowrap;
    width: auto;
  }
  .dropdown-select:focus {
    background-color: #fff;
  }
  .dropdown-select:hover {
    background-color: #fff;
  }
  .dropdown-select:active,
  .dropdown-select.open {
    background-color: #fff !important;
    border-color: #bbb;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.05) inset;
  }
  .dropdown-select:after {
    height: 0;
    width: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-top: 4px solid #777;
    -webkit-transform: origin(50% 20%);
    transform: origin(50% 20%);
    transition: all 0.125s ease-in-out;
    content: '';
    display: block;
    margin-top: -2px;
    pointer-events: none;
    position: absolute;
    right: 10px;
    top: 50%;
  }
  .dropdown-select.open:after {
    -webkit-transform: rotate(-180deg);
    transform: rotate(-180deg);
  }
  .dropdown-select.open .list {
    -webkit-transform: scale(1);
    transform: scale(1);
    opacity: 1;
    pointer-events: auto;
  }
  .dropdown-select.open .option {
    cursor: pointer;
  }
  .dropdown-select.wide {
    width: 100%;
  }
  .dropdown-select.wide .list {
    left: 0 !important;
    right: 0 !important;
  }
  .dropdown-select .list {
    box-sizing: border-box;
    transition: all 0.15s cubic-bezier(0.25, 0, 0.25, 1.75), opacity 0.1s linear;
    -webkit-transform: scale(0.75);
    transform: scale(0.75);
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.09);
    background-color: #fff;
    border-radius: 6px;
    margin-top: 4px;
    padding: 3px 0;
    opacity: 0;
    overflow: hidden;
    pointer-events: none;
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 999;
    max-height: 250px;
    border: 1px solid #ddd;
  }
  .dropdown-select .list:hover .option:not(:hover) {
    background-color: transparent !important;
  }
  .dropdown-select .dd-search{
    overflow:hidden;
    display:flex;
    align-items:center;
    justify-content:center;
    margin:0.5rem;
  }
  .dropdown-select .dd-searchbox{
    width:90%;
    padding:0.5rem;
    border:1px solid #999;
    border-color:#999;
    border-radius:4px;
    outline:none;
  }
  .dropdown-select .dd-searchbox:focus{
    border-color:#12CBC4;
  }
  .dropdown-select .list ul {
    padding: 0;
  }
  .dropdown-select .option {
    cursor: default;
    font-weight: 400;
    line-height: 40px;
    outline: none;
    padding-left: 18px;
    padding-right: 29px;
    text-align: left;
    transition: all 0.2s;
    list-style: none;
  }
  .dropdown-select .option:hover,
  .dropdown-select .option:focus {
    background-color: #f6f6f6 !important;
  }
  .dropdown-select .option.selected {
    font-weight: 600;
    color: #12cbc4;
  }
  .dropdown-select .option.selected:focus {
    background: #f6f6f6;
  }
  .dropdown-select a {
    color: #aaa;
    text-decoration: none;
    transition: all 0.2s ease-in-out;
  }
  .dropdown-select a:hover {
    color: #666;
  }
</style>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">City & Venue Upload</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>City & Venue Upload</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php if($this->session->flashdata('citymsg')) {
              $message = $this->session->flashdata('citymsg'); ?>
              <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="msgShow"></div>
          <div class="card-box">
            <div class="row">
              <h4 class="m-t-0 header-title" style="min-height: 85px;border-bottom: 1px solid #eee;"><strong>City & Venue</strong><br><br>  
                <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/salonvenueadd" style="position: absolute;right: 1255px;" class="btn btn-default">Salon Venue Add</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/statelisting" style="position: absolute;right: 1002px;" class="btn btn-default">State Listing</a>
                
                <a style="position: absolute;right: 31px;" class="btn btn-default addcity">City Add</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/cityuploadcsv" style="position: absolute;right: 120px;" class="btn btn-default">City Upload</a>
                <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/venueadd" style="position: absolute;right: 235px;" class="btn btn-default">Venue Add</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venueuploadcsv" style="position: absolute;right: 338px;" class="btn btn-default">Venue Upload</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/citylisting" style="position: absolute;right: 467px;" class="btn btn-default">City Listing</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venuelisting" style="position: absolute;right: 580px;" class="btn btn-default">Venue Listing</a>

                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venueuploadingbulk" style="position: absolute;right: 811px;" class="btn btn-default">Venue Bulk Uploading</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/stateadd" style="position: absolute;right: 710px;" class="btn btn-default">State Add</a>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/stateuploadcsv" style="position: absolute;right: 1128px;" class="btn btn-default">State Upload</a>

              </h4>
            </div>
            <div class="row">            
              <div class="cityadd" style="display: none;">
                <div>
                  <h4>Add City</h4>
                </div>
                <form id="cityAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/cityinsert" method="POST" enctype="multipart/form-data"> 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Product Name</label>
                        <select class="form-control" name="pid" id="pid">
                          <option value="">--Please Select--</option>
                          <?php if(isset($productLists)){ 
                            foreach ($productLists as $k => $val) { ?>
                            <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                          <?php } } ?>
                        </select>                                              
                      </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>State</label>
                          <select class="form-control" name="state_id" id="state_id">
                            <option value="">--Please Select--</option>
                          </select>                      
                        </div>
                      </div>
                    
                   </div>
                    <div class="col-md-12">
                      <div class="col-md-4">
                      <div class="form-group">
                        <label>CRM City ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                        <input type="text" class="form-control" name="crm_city_id" id="crm_city_id" placeholder="CRM City ID" >
                      </div>
                    </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>City Name</label>
                          <div class="input_fields_wraps">
                            <input type="text" class="form-control cityname" name="cityname[]" placeholder="City Name" >  
                          </div>                      
                        </div>
                      </div>
                      <div class="col-md-2"></br>
                        <button class="btn btn-info add_Btn" style="margin-top: 5px;" type="button">Add</button>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="form-control" name="status" id="status">
                            <option value="">--Please Select--</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form> 
              </div>

              <!-- <div class="venueadd" style="display: none;">
                <div>
                  <h4>Add Venue</h4>
                </div>
                <form id="venueAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venueinsert" method="POST" enctype="multipart/form-data">
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City</label>
                          <select class="form-control" name="city_id" id="city_id">
                            <option value="">--Please Select--</option>
                          </select>                      
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label>Venue Name</label>
                            <div class="input_fields_wrap_venue">
                              <input type="text" class="form-control venuename" name="venuename[]" placeholder="Venue Name" >  
                            </div>                      
                          </div>
                        </div>
                      <div class="col-md-2"></br>
                        <button class="btn btn-info add_Btn_Venue" style="margin-top: 5px;" type="button">Add</button>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status" id="status">
                          <option value="">--Please Select--</option>
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form> 
              </div> -->
               
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>


<script type="text/javascript">
  $("#crm_city_id").keyup(function () {
    var val = $(this).val();
      if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2) 
        val =val.replace(/\.+$/,"");
      }
    $(this).val(val);       
  });
  $(document).ready(function(){
    //create_custom_dropdowns();
    $('.cityupload').click(function(){
      $('.cityuploads').show();
      $('.venueuploads').hide();
    });
    $('.venueupload').click(function(){
      $('.cityuploads').hide();
      $('.venueuploads').show();
    });
    $('.addcity').click(function(){
      $('.cityadd').show();
      $('.venueadd').hide();
    });
    $('.addvenue').click(function(){
      $('.cityadd').hide();
      $('.venueadd').show();
    });

    var max_fields      = 20; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wraps"); //Fields wrapper
    var add_button      = $(".add_Btn"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append('<div><input style="margin-top:3px;" type="text" class="form-control cityname" name="cityname[]" placeholder="City Name" /><a href="#" class="remove_field">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); $(this).parent('div').remove(); x--;
    });
    $('#cityAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var pid             = $("#pid").val();
      var status          = $("#status").val();
      var cityname        = $(".cityname").val();
      var crm_city_id     = $("#crm_city_id").val();
      var state_id     = $("#state_id").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(state_id == ""){
        $('#state_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#state_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(crm_city_id == ""){
        $('#crm_city_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#crm_city_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(cityname == ""){
        $('.cityname').css('border','1px solid red');
        noError = false;               
      } else {
        $('.cityname').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#cityAddForm')[0].submit();   
      }

    });

    
    $('#venueAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var city_id       = $("#city_id").val();
      var venuename        = $(".venuename").val();
      var status1        = $("#status1").val();
      //var vpid           = $("#vpid").val();

      /*if(vpid == ""){
        $('#vpid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#vpid').css('border','1px solid #eee');
        noError = true; 
      }*/
      if(status1 == ""){
        $('#status1').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status1').css('border','1px solid #eee');
        noError = true; 
      }
      if(city_id == ""){
        $('#city_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#city_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(venuename == ""){
        $('.venuename').css('border','1px solid red');
        noError = false;               
      } else {
        $('.venuename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#venueAddForm')[0].submit();   
      }
    });

    //set initial state.
    $('#enabledisable').val(this.checked);
    $('#enabledisable').change(function() {
      if(this.checked) {
        var returnVal = confirm("Are you sure?");
        $(this).prop("checked", returnVal);
        if(returnVal){
          $.ajax({ 
            url: "<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/enabledisablecity",
            type:'POST',
            data: {cityED:returnVal}, // <--- THIS IS THE CHANGE
              beforeSend: function () {
                $('.loader1').removeClass('hide');
              }, 
            success: function(result){
              $('.loader1').addClass('hide');
              $('.msgShow').html('<div class="alert alert-success">Added Successfully..</div>');
            }
          });
        }
      } else {
        $('#enabledisable').val(this.checked);
        $.ajax({ 
          url: "<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/enabledisablecity",
          type:'POST',
          data: {cityED:returnVal}, // <--- THIS IS THE CHANGE
            beforeSend: function () {
              $('.loader1').removeClass('hide');
            }, 
          success: function(result){
            $('.loader1').addClass('hide');
            $('.msgShow').html('<div class="alert alert-success">Updated Successfully..</div>');
          }
        }); 
      }
    });

    $('#enabledisablevenue').val(this.checked);
    $('#enabledisablevenue').change(function() {
      if(this.checked) {
        var returnVal = confirm("Are you sure?");
        $(this).prop("checked", returnVal);
        if(returnVal){
          $.ajax({ 
            url: "<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/enabledisablevenues",
            type:'POST',
            data: {venueED:returnVal}, // <--- THIS IS THE CHANGE
              beforeSend: function () {
                $('.loader1').removeClass('hide');
              }, 
            success: function(result){
              $('.loader1').addClass('hide');
              $('.msgShow').html('<div class="alert alert-success">Added Successfully..</div>');
            }
          });
        }
      } else {
        $('#enabledisablevenue').val(this.checked);
        $.ajax({ 
          url: "<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/enabledisablevenues",
          type:'POST',
          data: {venueED:returnVal}, // <--- THIS IS THE CHANGE
            beforeSend: function () {
              $('.loader1').removeClass('hide');
            }, 
          success: function(result){
            $('.loader1').addClass('hide');
            $('.msgShow').html('<div class="alert alert-success">Updated Successfully..</div>');
          }
        }); 
      }
    });


  });
</script>

<script type="text/javascript">
  function create_custom_dropdowns() {
    $('#city_id').each(function (i, select) {
      if (!$(this).next().hasClass('dropdown-select')) {
        $(this).after('<div class="dropdown-select wide ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
        var dropdown = $(this).next();
        var options = $(select).find('option');
        var selected = $(this).find('option:selected');
        dropdown.find('.current').html(selected.data('display-text') || selected.text());
        options.each(function (j, o) {
          var display = $(o).data('display-text') || '';
          dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
        });
      }
    });

    $('.dropdown-select ul').before('<div class="dd-search"><input id="txtSearchValue" autocomplete="off" onkeyup="filter()" class="dd-searchbox" type="text"></div>');
  }

  // Event listeners

  // Open/close
  $(document).on('click', '.dropdown-select', function (event) {
    if($(event.target).hasClass('dd-searchbox')){
        return;
    }
    $('.dropdown-select').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
      $(this).find('.option').attr('tabindex', 0);
      $(this).find('.selected').focus();
    } else {
      $(this).find('.option').removeAttr('tabindex');
      $(this).focus();
    }
  });

  // Close when clicking outside
  $(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown-select').length === 0) {
      $('.dropdown-select').removeClass('open');
      $('.dropdown-select .option').removeAttr('tabindex');
    }
    event.stopPropagation();
  });

  function filter(){
    var valThis = $('#txtSearchValue').val();
    $('.dropdown-select ul > li').each(function(){
     var text = $(this).text();
        (text.toLowerCase().indexOf(valThis.toLowerCase()) > -1) ? $(this).show() : $(this).hide();         
   });
  };
  // Search

  // Option click
  $(document).on('click', '.dropdown-select .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown-select').find('.current').text(text);
    $(this).closest('.dropdown-select').prev('select').val($(this).data('value')).trigger('change');
  });

  // Keyboard events
  $(document).on('keydown', '.dropdown-select', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    //if (event.keyCode == 32 || event.keyCode == 13) {
    if (event.keyCode == 13) {
      if ($(this).hasClass('open')) {
        focused_option.trigger('click');
      } else {
        $(this).trigger('click');
      }
      return false;
      // Down
    } else if (event.keyCode == 40) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        focused_option.next().focus();
      }
      return false;
      // Up
    } else if (event.keyCode == 38) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
        focused_option.prev().focus();
      }
      return false;
      // Esc
    } else if (event.keyCode == 27) {
      if ($(this).hasClass('open')) {
        $(this).trigger('click');
      }
      return false;
    }
  });
  $('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getstatelist",
          beforeSend: function(){
                    $('.loader1').removeClass('hide');

                },
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('.loader1').addClass('hide');
            $('#state_id').html(data);
          }
        });
      } else {
        $('#state_id').html('<option value=""></option>');
      }
    });
</script>