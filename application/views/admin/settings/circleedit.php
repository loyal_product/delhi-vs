<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Circle</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type2/dashboard">Dashboard</a></li>
            <li class="active"><a>Edit Circle</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Circle</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php if(isset($editcircleList)){
                  foreach($editcircleList as $key=>$row) { ?>
                  <form id="circleEditForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type2/circleupdate/<?php echo $row->id;?>" method="POST" enctype="multipart/form-data"> 
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>" <?php if ($val['id']==$row->prod_id) {echo 'selected="selected"';}?>><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Circle</label>
                          <style type="text/css">
                            .dropdown-menu{
                              min-width: 422px !important;
                            }
                          </style>
                          <select class="form-control" name="state_id" id="state_id">
                            <option value=""></option>
                            <?php if(isset($statesList)) { 
                              foreach ($statesList as $key => $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php if ($row->state_id==$value['id']) {echo 'selected="selected"';}?>><?php echo $value['name'];?></option>
                            <?php } } ?>
                          </select>
                          
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>CRM City ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_city_id" id="crm_city_id" placeholder="CRM City ID" value="<?php echo $row->crm_city_id; ?>">
                        </div>
                      </div>
                   </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Circle Name</label>
                          <div class="input_fields_wraps">
                            <input type="text" class="form-control" id="circlename" name="circlename" placeholder="Circle Name" value="<?php echo $row->name; ?>">  
                          </div>                      
                        </div>
                      </div>                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="form-control" name="status" id="status">
                            <option value="">--Please Select--</option>
                            <option value="1" <?php if ($row->status=='1') {echo 'selected="selected"';}?>>Active</option>
                            <option value="0" <?php if ($row->status=='0') {echo 'selected="selected"';}?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                   </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#circleEditForm').on('submit', function (e) {
      e.preventDefault();              
      var noError        = true;
      var circlename       = $("#circlename").val(); 
      var status         = $("#status").val();

      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(circlename == ""){
        $('#circlename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#circlename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#circleEditForm')[0].submit();
      }
    });
     
     $('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type2/getstatelist",
          beforeSend: function(){
            $('.loader1').removeClass('hide');
          },
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('.loader1').addClass('hide');
            $('#state_id').html(data);
          }
        });
      } else {
        $('#state_id').html('<option value=""></option>');
      }
    });
  });
</script>