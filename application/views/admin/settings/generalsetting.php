<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create New Website For General Settings</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Create New Website For General Settings</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="row">
              <form id="generalsettingForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/generalsettinginsert" method="POST" enctype="multipart/form-data">
              <?php
                if($this->session->flashdata('generalsettingmsg')) {
                  $message = $this->session->flashdata('generalsettingmsg');
              ?>
                <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php //print "<pre>"; print_r($generalsettingdata); die; ?>
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Logo</label>
                        <input type='file' name="logoimg" onchange="readURL(this);" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <img id="imgView" style="width: 100px;height: 80px;" src="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)){ if(isset($generalsettingdata[0]) && $generalsettingdata[0]['meta_value']!=''){ echo base_url().'assets/uploads/'.$generalsettingdata[0]['meta_value'];} } ?>" alt="logo" />
                      <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if($generalsettingdata[0]['meta_key']=='logoimg') { ?>
                        <input type="hidden" name="postImg" value="<?php echo $generalsettingdata[0]['meta_value'];?>">
                      <?php } } ?>
                    </div>
                  </div>
                  <div class="col-md-12" style="margin-bottom: 20px;border-bottom: 3px solid #eee;">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Site Title</label>
                        <input type="text" class="form-control" name="sitetitle" id="sitetitle" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[1]) && $generalsettingdata[1]['meta_value']!=''){ echo $generalsettingdata[1]['meta_value'];} }?>" />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Site Description</label>
                        <textarea class="form-control" name="sitedesc" id="sitedesc"><?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[2]) && $generalsettingdata[2]['meta_value']!=''){ echo $generalsettingdata[2]['meta_value'];} } ?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12" style="margin-bottom: 20px;border-bottom: 3px solid #eee;">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Set Mobile Number & Email ID Usage Rule</label><br><br>
                          <input type="radio" id="voucherperday" name="setruleformobandemail" value="voucherperday" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[3]['meta_value']=='voucherperday'){ echo 'checked="checked"'; } } ?>>
                          <label for="voucherperday" style="font-weight: normal;"><input type="text" class="voucherperday" name="voucherperday" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[4]['meta_value'])) && $generalsettingdata[4]['meta_value']!=''){echo $generalsettingdata[4]['meta_value'];} } ?>"> Voucher Per Day</label><br><br>

                          <input type="radio" id="voucherperweek" name="setruleformobandemail" value="voucherperweek" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[3]['meta_value']=='voucherperweek'){ echo 'checked="checked"'; } }?>>
                          <label for="voucherperweek" style="font-weight: normal;"><input type="text" class="voucherperweek" name="voucherperweek" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[5]['meta_value'])) && $generalsettingdata[5]['meta_value']!=''){echo $generalsettingdata[5]['meta_value'];} }?>"> Voucher Per Week</label><br><br>

                          <input type="radio" id="voucherpermonth" name="setruleformobandemail" value="voucherpermonth" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[3]['meta_value']=='voucherpermonth'){ echo 'checked="checked"'; } } ?>>
                          <label for="voucherpermonth" style="font-weight: normal;"><input type="text" class="voucherpermonth" name="voucherpermonth" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[6]['meta_value'])) && $generalsettingdata[6]['meta_value']!=''){echo $generalsettingdata[6]['meta_value'];} }?>"> Voucher Per Month</label><br><br>

                          <input type="radio" id="voucherduringpromotion" name="setruleformobandemail" value="voucherduringpromotion" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[3]['meta_value']=='voucherduringpromotion'){ echo 'checked="checked"'; } }?>>
                          <label for="voucherduringpromotion" style="font-weight: normal;"><input type="text" class="voucherduringpromotion" name="voucherduringpromotion" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[7]['meta_value'])) && $generalsettingdata[7]['meta_value']!=''){echo $generalsettingdata[7]['meta_value'];} }?>"> Voucher During Promotion</label>

                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Error Message For 2 Voucher Per Day</label>
                            <input type='text' name="errormessage2voucherperday" id="errormessage2voucherperday" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[8]) && $generalsettingdata[8]['meta_value']!=''){ echo $generalsettingdata[8]['meta_value'];} }?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Error Message For 5 Voucher Per Week</label>
                            <input type='text' name="errormessage5voucherperweek" id="errormessage5voucherperweek" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[9]) && $generalsettingdata[9]['meta_value']!=''){ echo $generalsettingdata[9]['meta_value'];} }?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Error Message For 10 Voucher Per Month</label>
                            <input type='text' name="errormessage10voucherpermonth" id="errormessage10voucherpermonth" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[10]) && $generalsettingdata[10]['meta_value']!=''){ echo $generalsettingdata[10]['meta_value'];} }?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Error Message For 15 Voucher During Promotion</label>
                            <input type='text' name="errormessage15voucherduringpromotion" id="errormessage15voucherduringpromotion" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[11]) && $generalsettingdata[11]['meta_value']!=''){ echo $generalsettingdata[11]['meta_value'];} }?>">
                        </div>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="checkbox checkbox-primary">
                          <input id="enabledisablecity" type="checkbox" name="enabledisablecity" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[12]) && $generalsettingdata[12]['meta_value']=='on'){ echo 'checked="checked"'; } }?> >
                          <label for="enabledisablecity">City Enable/Disable</label>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="checkbox checkbox-primary">
                          <input id="enabledisablevenue" type="checkbox" name="enabledisablevenue" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[13]) && $generalsettingdata[13]['meta_value']=='on'){ echo 'checked="checked"'; } }?>>
                          <label for="enabledisablevenue">Venue Enable/Disable</label>
                        </div>                        
                      </div>
                    </div>
                  </div>
                  <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[14]) && $generalsettingdata[14]['meta_value']!='') {
                    $dateArr = json_decode($generalsettingdata[14]['meta_value']);
                  } }?>
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label>Sites Blocked Dates</label>
                          <div class="date_wrap">
                            <?php if(!empty($dateArr)){ 
                              foreach($dateArr as $key=>$val){  ?>
                                <div>
                                  <input type='text' name="chooseblockdate[]" style="margin-top:7px;" id="chooseblockdate" class="form-control" readonly="readonly" value="<?php echo $val;?>">
                                  <?php if($key!=0){?>
                                  <a href="#" style="color:red;" class="remove_field">Remove</a>
                                </div>
                            <?php } } } else { ?>
                            <input type='text' name="chooseblockdate[]" id="chooseblockdate" class="form-control" readonly="readonly" value="">
                          <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>&nbsp;</label><br>
                          <button class="btn btn-info add_Date" type="button">Add Date</button>
                        </div>
                      </div>
                    </div>
                    <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { echo "</div>"; }?>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Set & Edit Registration Screen Refresh Time (in seconds)</label>
                        <input type="text" name="screenrefresh" id="screenrefresh" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[15]) && $generalsettingdata[15]['meta_value']!=''){ echo $generalsettingdata[15]['meta_value'];} } ?>"/>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">                
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Booking Success Sender Mail</label><br>
                        <input id="bookingsuccesssendermail" class="form-control" type="text" name="bookingsuccesssendermail" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[16]) && $generalsettingdata[16]['meta_value']!=''){ echo $generalsettingdata[16]['meta_value'];} }?>" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Registartion Sender Mail</label><br>
                        <input id="registrationsendermail" class="form-control" type="text" name="registrationsendermail" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[17]) && $generalsettingdata[17]['meta_value']!=''){ echo $generalsettingdata[17]['meta_value'];} }?>" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12"> 
                    <div class="col-md-6" style="border: 1px solid #eee;padding: 5px;">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Rule For Account Number</label><br>
                          <label for="accountnumberrule" style="font-weight: normal;"><input type="text" class="accountnumberrule" name="accountnumberrule" style="width: 50px;" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[18]) && $generalsettingdata[18]['meta_value']!='') { echo $generalsettingdata[18]['meta_value'];} }?>"> Per Account Number</label><br>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Rule For UPA</label><br>
                          <label for="uparule" style="font-weight: normal;"><input type="text" class="uparule" name="uparule" style="width: 50px;" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[19]) && $generalsettingdata[19]['meta_value']!='') { echo $generalsettingdata[19]['meta_value'];} }?>"> Per UPA</label><br>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Rule For E-wallets</label><br>
                          <label for="ewalletsrule" style="font-weight: normal;"><input type="text" class="ewalletsrule" name="ewalletsrule" style="width: 50px;" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[20]) && $generalsettingdata[20]['meta_value']!='') { echo $generalsettingdata[20]['meta_value'];} }?>"> Per E-wallets For Mobile</label><br>
                        </div>
                      </div>
                    </div>
                      <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Campaign ID for Cashback API</label>
                          <input type="text" name="campaginidforcashbackapi" id="campaginidforcashbackapi" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[21]) && $generalsettingdata[21]['meta_value']!=''){ echo $generalsettingdata[21]['meta_value'];} } ?>"/>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Campaign ID for CRM</label>
                          <input type="text" name="campaginidforcrm" id="campaginidforcrm" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[22]) && $generalsettingdata[22]['meta_value']!=''){ echo $generalsettingdata[22]['meta_value'];} } ?>"/>
                        </div>
                      </div>                      
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Enter Google Analytics Code</label>
                        <textarea name="googleanalyticscode" id="googleanalyticscode" class="form-control"><?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[23]) && $generalsettingdata[23]['meta_value']!=''){ echo $generalsettingdata[23]['meta_value'];} } ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Enter Chat Code</label>
                        <textarea name="chatcode" id="chatcode" class="form-control"><?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[24]) && $generalsettingdata[24]['meta_value']!=''){ echo $generalsettingdata[24]['meta_value'];} } ?></textarea>
                      </div>
                    </div>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="checkbox checkbox-primary">
                          <input id="emailrequireornotforregistration" type="checkbox" name="emailrequireornotforregistration" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[30]) && $generalsettingdata[30]['meta_value']=='on'){ echo 'checked="checked"'; } }?> >
                          <label for="emailrequireornotforregistration">Email Require For Registration</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="col-md-4">
                        <div class="form-group">
                          <label>Select No. of People</label>
                          <select class="form-control" name="no_of_people" id="no_of_people">
                            <option value="">--Please Select--</option>
                            
                              <?php foreach (range(0, 10) as $number) { ?>
                              <option value="<?php echo $number;?>"><?php echo $number;?></option>
                            <?php } ?>
                          </select>                                              
                        </div>
                      </div> -->
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>No. of People</label>
                      <input type='number' name="no_of_people" id="no_of_people" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[25]) && $generalsettingdata[25]['meta_value']!=''){ echo $generalsettingdata[25]['meta_value'];} } ?>">
                    </div>
                  </div> 
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Veg & Noveg Discount Applied %</label>
                      <input type='text' name="discount_applied" id="discount_applied" class="form-control" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[26]) && $generalsettingdata[26]['meta_value']!=''){ echo $generalsettingdata[26]['meta_value'];} } ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Set Salon Discount Applied %</label><br><br>
                        <input type="radio" id="salonflatdiscount" name="setsalondiscount" value="salonflatdiscount" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[27]['meta_value']=='salonflatdiscount'){ echo 'checked="checked"'; } } ?>>
                        <label for="salonflatdiscount" style="font-weight: normal;"><input type="text" class="salonflatdiscount" name="salonflatdiscount" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[28]['meta_value'])) && $generalsettingdata[28]['meta_value']!=''){echo $generalsettingdata[28]['meta_value'];} } ?>"> Flat Discount</label><br><br>

                        <input type="radio" id="salonnormaldiscount" name="setsalondiscount" value="salonnormaldiscount" <?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata) && $generalsettingdata[27]['meta_value']=='salonnormaldiscount'){ echo 'checked="checked"'; } }?>>
                        <label for="salonnormaldiscount" style="font-weight: normal;"><input type="text" class="salonnormaldiscount" name="salonnormaldiscount" style="width: 50px;" readonly="readonly" value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if((isset($generalsettingdata[29]['meta_value'])) && $generalsettingdata[29]['meta_value']!=''){echo $generalsettingdata[29]['meta_value'];} }?>">Discount</label><br><br>
                        </div>
                    </div>
                    
                    <!---------------------------------------------------------------------------------------------------->
                    <div class="row">
                      <div class="col-md-6">
                        <label>Entity ID</label>
                        <input type="text" class="form-control" name="entity_id" placeholder="Enter Entity ID" required value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[31]) && $generalsettingdata[31]['meta_value']!=''){ echo $generalsettingdata[31]['meta_value'];} } ?>">
                      </div>
                      <div class="col-md-6">
                        <label>OTP Template ID</label>
                        <input type="text" class="form-control" name="otp_template_id" placeholder="Enter OTP Template ID" required value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[32]) && $generalsettingdata[32]['meta_value']!=''){ echo $generalsettingdata[32]['meta_value'];} } ?>">
                      </div>
                    </div>
                    <br>
                    <div class="row">  
                      <div class="col-md-6">
                        <label>Registration Template ID</label>
                        <input type="text" class="form-control" name="reg_template_id" placeholder="Enter Registration Template ID" required value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[33]) && $generalsettingdata[33]['meta_value']!=''){ echo $generalsettingdata[33]['meta_value'];} } ?>">
                      </div>
                      <div class="col-md-6" style="display:none;">
                        <label>Booking Template ID</label>
                        <input type="text" class="form-control" name="booking_template_id" placeholder="Enter Booking Template ID" required value="<?php if(isset($generalsettingdata) && !empty($generalsettingdata)) { if(isset($generalsettingdata[34]) && $generalsettingdata[34]['meta_value']!=''){ echo $generalsettingdata[34]['meta_value'];} } ?>">
                      </div>
                    </div>
                    <br>  
                    <!------------------------------------------------------------------------------------------------->
                    
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#imgView')
            .attr('src', e.target.result)
            .width(100)
            .height(80);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).ready(function(){
    $("#chooseblockdate").datepicker();
    
    
    $(".calenderblockdate").datepicker();

    $("#minimumnoofdays").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $("#maximumnoofdays").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $("#screenrefresh").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });

    var max_fields      = 30; //maximum input boxes allowed
    var wrapper         = $(".date_wrap"); //Fields wrapper
    var add_button      = $(".add_Date"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append('<div style="margin-top:7px;"><input type="text" class="form-control" id="date_'+x+'" name="chooseblockdate[]" readonly="readonly"/><a href="#" style="color:red;" class="remove_field">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); 
      $(this).parent('div').remove(); 
      x--;
    });

    $(wrapper).on("click","input[type='text']", function(){      
      var dateId = $(this).attr('id');
      console.log(dateId);    
      $("#"+dateId).datepicker();
      $("#"+dateId).datepicker('show');
    });


    $("input[name='setruleformobandemail']").click(function(){
      var idVal = $(this).attr("id");
      $('input[name="voucherperday"]').attr('readonly','readonly');
      $('input[name="voucherperweek"]').attr('readonly','readonly');
      $('input[name="voucherpermonth"]').attr('readonly','readonly');
      $('input[name="voucherduringpromotion"]').attr('readonly','readonly');
      $('input[name="voucherperday"]').val('');
      $('input[name="voucherperweek"]').val('');
      $('input[name="voucherpermonth"]').val('');
      $('input[name="voucherduringpromotion"]').val('');      
      var inputname = $('input[name="'+idVal+'"]').attr('class');
      if(inputname==idVal) {
        $('input[name="'+idVal+'"]').removeAttr('readonly');
      }
    });

    $('input[name="voucherperday"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $('input[name="voucherperweek"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $('input[name="voucherpermonth"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $('input[name="voucherduringpromotion"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });

    $('input[name="accountnumberrule"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $('input[name="uparule"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $('input[name="ewalletsrule"]').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });

    /*$('#generalsettingForm').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var themename     = $("#themename").val();
      var subdomain     = $("#subdomain").val();

      if(themename == "") {
        $('#themename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#themename').css('border','1px solid #eee');
        noError = true;
      }
      if(subdomain == "") {
        $('#subdomain').css('border','1px solid red');
        noError = false;               
      } else {
        $('#subdomain').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#generalsettingForm')[0].submit();   
      }
    });*/

  });
</script>