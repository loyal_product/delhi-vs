<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Domain List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Domain List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title" style="margin-bottom: 25px !important;"><strong>Domain List</strong>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/generalsetting" style="position: absolute;right: 30px;" class="btn btn-default"><strong>Create New Website And General Setting</strong></a>
            </h4>
            <div class="row">
              <div class="col-md-12">                
              <?php
                if($this->session->flashdata('domainlisting')) {
                $message = $this->session->flashdata('domainlisting');
              ?>
               <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
              <?php   }    ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Domain Listing">
                  <thead>
                    <tr>
                      <th scope="col" class='collapsable'>S.No</th>
                      <th scope="col" class='collapsable'>Domain Name</th>
                      <th scope="col" style="text-align:center;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php                    
                    if(isset($domainList) && !empty($domainList) ){
                        $i= $startFrom;
                      foreach($domainList as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><a href="<?php echo $row['meta_value']; ?>" target="_blank" rel="noopener"><?php echo $row['meta_value']; ?></a></td>
                      <td class="custom_width" align="center">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/domainedit?url=<?php echo $row['theme_id']; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <!-- <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletedomain?url=<?php echo $row['theme_id']; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a> -->
                      </td>                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links!=''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>