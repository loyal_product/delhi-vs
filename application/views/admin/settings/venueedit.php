<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Venue</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Edit Venue</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Venue</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php //print "<pre>"; print_r($editvenuesList); die; ?>
                <?php if(isset($editvenuesList)){
                  foreach($editvenuesList as $key=>$row) { ?>
                  <form id="venueEditForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/venueupdate/<?php echo $row->id;?>" method="POST" enctype="multipart/form-data"> 
                    <div class="row col-md-12">
                      <div class="col-md-4">
                        <div class="form-group">

                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>" <?php if ($row->prod_id==$val['id']) {echo 'selected="selected"';}?>><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>State</label>
                          <style type="text/css">
                            .dropdown-menu{
                              min-width: 422px !important;
                            }
                          </style>
                          <select class="form-control" name="state_id" id="state_id">
                            <option value=""></option>
                            <?php if(isset($statesList)) { 
                              foreach ($statesList as $key => $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php if ($row->state_id==$value['id']) {echo 'selected="selected"';}?>><?php echo $value['name'];?></option>
                            <?php } } ?>
                          </select>
                          
                        </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>City</label>
                          <style type="text/css">
                            .dropdown-menu{
                              min-width: 422px !important;
                            }
                          </style>
                          <select class="form-control" name="city_id" id="city_id">
                            <option value=""></option>
                            <?php if(isset($citiesList)) { 
                              foreach ($citiesList as $key => $value) { ?>
                                <option value="<?php echo $value['id'];?>" <?php if ($row->cities_id==$value['id']) {echo 'selected="selected"';}?>><?php echo $value['name'];?></option>
                            <?php } } ?>
                          </select>
                          
                        </div>
                      </div>
                      
                    </div>
                    <div class="col-md-12">   
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>CRM Venue ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_venue_id" id="crm_venue_id" placeholder="CRM Venue ID" value="<?php echo $row->crm_venue_id;?>">
                        </div>
                      </div>                   
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Venue Name</label>
                          <div class="input_fields_wrap_venue">
                            <input type="text" class="form-control" id="venuename" name="venuename" placeholder="Venue Name" value="<?php echo $row->name;?>" >  
                          </div>                      
                        </div>
                      </div> 
                      <div class="col-md-4">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="status" id="status">
                          <option value="">--Please Select--</option>
                            <option value="1" <?php if ($row->status=='1') {echo 'selected="selected"';}?>>Active</option>
                            <option value="0" <?php if ($row->status=='0') {echo 'selected="selected"';}?>>Inactive</option>
                        </select>
                      </div>
                    </div>                     
                    </div>
                    <div class="col-md-12" style="border: 1px solid #eee;margin: 5px;">
                      <div class="input_fields_wrap_venue">
                        <div class="col-md-12">
                          <!-- <div class="col-md-2">
                            <div class="form-group">
                              <label>CRM Venue ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                              <input type="text" class="form-control" name="crm_venue_id[]" id="crm_venue_id" placeholder="CRM Venue ID" >
                            </div>
                          </div> -->
                          <!-- <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Venue Name</label><br>
                                <label>&nbsp;</label>                        
                                  <input type="text" class="form-control venuename" name="venuename[]" placeholder="Venue Name" > 
                              </div>
                            </div>
                          </div> -->
                          <?php if($bookingFormId[0]['booking_form_id'] == 10) { ?>

                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_male_price" name="monday_male_price" placeholder="Male Monday Price" value="<?php echo $row->monday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_male_price" name="tuesday_male_price" placeholder="Male Tuesday Price" value="<?php echo $row->tuesday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_male_price" name="wednesday_male_price" placeholder="Male Wednesday Price" value="<?php echo $row->wednesday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_male_price" name="thursday_male_price" placeholder="Male Thursday Price" value="<?php echo $row->thursday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_male_price" name="friday_male_price" placeholder="Male Friday Price" value="<?php echo $row->friday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_male_price" name="saturday_male_price" placeholder="Male Saturday Price" value="<?php echo $row->saturday_male_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Male</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_male_price" name="sunday_male_price" placeholder="Male Sunday Price" value="<?php echo $row->sunday_male_price;?>"> 
                              </div>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_female_price" name="monday_female_price" placeholder="Female Monday Price" value="<?php echo $row->monday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_female_price" name="tuesday_female_price" placeholder="Female Tuesday Price" value="<?php echo $row->tuesday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_female_price" name="wednesday_female_price" placeholder="Female Wednesday Price" value="<?php echo $row->tuesday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_female_price" name="thursday_female_price" placeholder="Female Thursday Price" value="<?php echo $row->thursday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_female_price" name="friday_female_price" placeholder="Female Friday Price" value="<?php echo $row->friday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_female_price" name="saturday_female_price" placeholder="Female Saturday Price" value="<?php echo $row->saturday_female_price;?>" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Female</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_female_price" name="sunday_female_price" placeholder="Female Sunday Price" value="<?php echo $row->sunday_female_price;?>"> 
                              </div>
                            </div>
                          </div>
                        <?php } else { ?>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_veg_price" name="monday_veg_price" placeholder="Veg Monday Price" value="<?php echo $row->monday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_veg_price" name="tuesday_veg_price" placeholder="Veg Tuesday Price" value="<?php echo $row->tuesday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_veg_price" name="wednesday_veg_price" placeholder="Veg Wednesday Price" value="<?php echo $row->wednesday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_veg_price" name="thursday_veg_price" placeholder="Veg Thursday Price" value="<?php echo $row->thursday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_veg_price" name="friday_veg_price" placeholder="Veg Friday Price" value="<?php echo $row->friday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_veg_price" name="saturday_veg_price" placeholder="Veg Saturday Price" value="<?php echo $row->saturday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Veg</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_veg_price" name="sunday_veg_price" placeholder="Veg Sunday Price" value="<?php echo $row->sunday_veg_price;?>"> 
                              </div>
                            </div>
                          </div>

                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Monday Price</label>                        
                                  <input type="text" class="form-control" id="monday_nonveg_price" name="monday_nonveg_price" placeholder="Non Veg Monday Price" value="<?php echo $row->monday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Tuesday Price</label>                        
                                  <input type="text" class="form-control" id="tuesday_nonveg_price" name="tuesday_nonveg_price" placeholder="Non Veg Tuesday Price" value="<?php echo $row->tuesday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Wednesday Price</label>                        
                                  <input type="text" class="form-control" id="wednesday_nonveg_price" name="wednesday_nonveg_price" placeholder="Non Veg Wednesday Price" value="<?php echo $row->tuesday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Thursday Price</label>                        
                                  <input type="text" class="form-control" id="thursday_nonveg_price" name="thursday_nonveg_price" placeholder="Non Veg Thursday Price" value="<?php echo $row->thursday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Friday Price</label>                        
                                  <input type="text" class="form-control" id="friday_nonveg_price" name="friday_nonveg_price" placeholder="Non Veg Friday Price" value="<?php echo $row->friday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Saturday Price</label>                        
                                  <input type="text" class="form-control" id="saturday_nonveg_price" name="saturday_nonveg_price" placeholder="Non Veg Saturday Price" value="<?php echo $row->saturday_nonveg_price;?>" > 
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="col-md-10">
                              <div class="form-group">
                                <label>Non Veg</label><br>
                                <label>Sunday Price</label>                        
                                  <input type="text" class="form-control" id="sunday_nonveg_price" name="sunday_nonveg_price" placeholder="Non Veg Sunday Price" value="<?php echo $row->sunday_nonveg_price;?>"> 
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                      </div>

                      <!-- <div class="col-md-2"></br>
                        <button class="btn btn-info add_Btn_Venue" style="margin-top: 5px;" type="button">Add</button>
                      </div> -->
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $("#price_per_person").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){

          val = val.replace(/[^0-9\.]/g,'');
          
          /*if(val.split('.').length>2)
          
          val = val.replace(/\.+$/,"");*/
        }
      $(this).val(val);       
    });
  $(document).ready(function(){
    $('#venueEditForm').on('submit', function (e) {
      e.preventDefault();              
      var noError        = true;
      var pid       = $("#pid").val(); 
      var city_id       = $("#city_id").val(); 
      var venuename      = $("#venuename").val();
      var status         = $("#status").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(city_id == ""){
        $('#city_id').css('border','1px solid red');
        noError = false;               
      } else {
        $('#city_id').css('border','1px solid #eee');
        noError = true; 
      }
      if(venuename == ""){
        $('#venuename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#venuename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#venueEditForm')[0].submit();
      }
    });

    $('#pid').change(function(){
      var pid = $('#pid').val();
      if(pid != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getstatelist",
          method:"POST",
          data:{pid:pid},
          success:function(data) {
            $('#state_id').html(data);
          }
        });
      } else {
        $('#state_id').html('<option value=""></option>');
      }
    });

    $('#state_id').change(function(){
      var state_id = $('#state_id').val();
      if(state_id != '') {
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getcitylist",
          method:"POST",
          data:{state_id:state_id},
          success:function(data) {
            $('#city_id').html(data);
          }
        });
      } else {
        $('#city_id').html('<option value=""></option>');
      }
    });
     
  });
</script>