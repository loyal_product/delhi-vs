<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Circle Upload</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Circle Upload</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <?php if($this->session->flashdata('circlemsg')) {
              $message = $this->session->flashdata('circlemsg'); ?>
              <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
            <h4 class="m-t-0 m-b-20 header-title"><strong>Circle Upload</strong>
              <a href="<?php echo base_url(); ?>assets/download/sample-of-circle.csv" style="position: absolute;right: 31px;" class="btn btn-default">Download Sample </a>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/allstatelist" style="position: absolute;right: 200px;" class="btn btn-default">Download State List </a>
            </h4>
            <div class="row">
              <form id="circleAddCSVForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/circleuploadadd" method="POST" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div  class="col-md-6">
                    <div class="form-group">
                      <label>Product Name</label>
                      <select class="form-control" name="pid" id="pid">
                        <option value="">--Please Select--</option>
                        <?php if(isset($productLists)){ 
                          foreach ($productLists as $k => $val) { ?>
                          <option value="<?php echo $val['id'];?>"><?php echo $val['title'];?></option>
                        <?php } } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Status</label>
                      <select class="form-control" name="status" id="status">
                        <option value="">--Please Select--</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Circle List</label><br>
                      <div id="msg"></div>
                      <label for="file-upload-city" class="custom-file-upload">
                        <em class="fa fa-cloud-upload"></em> Choose File </label> 
                        <style type="text/css">
                          input[type="file"] {
                            display: none;
                          }
                          .custom-file-upload {
                            border: 1px solid #ccc;
                            display: inline-block;
                            padding: 8px 6px;
                            cursor: pointer;
                          }
                        </style>       
                        <input type="file" id="file-upload-city" name="file_city" accept=".csv" onchange="triggerValidation(this)" /><br>
                      <p style="font-size: 10px;text-transform: initial;color: red;">* Only choose csv file</p>                     
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="submit" class="btn btn-default" value="Submit" />
                    </div>
                  </div>
                </div>
              </form> 
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>
<script type="text/javascript">
  var regex = new RegExp("(.*?)\.(csv)$");
  function triggerValidation(el) {
    if (!(regex.test(el.value.toLowerCase()))) {
      el.value = '';
      $('#msg').html('<p style="color:red">Please select only csv file.</p>');
    }
  }
  $(document).ready(function(){
    $('#circleAddCSVForm').on('submit', function (e) {
      e.preventDefault();              
      var noError        = true;
      var circlename     = $("#file-upload-city").val(); 
      var status         = $("#status").val();
      var pid            = $("#pid").val();

      if(pid == ""){
        $('#pid').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pid').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(circlename == ""){
        $('.custom-file-upload').css('border','1px solid red');
        noError = false;               
      } else {
        $('.custom-file-upload').css('border','1px solid #eee');
          noError = true; 
      }
      if(noError){
        $('#circleAddCSVForm')[0].submit();   
      }
    });
     
  });
</script>