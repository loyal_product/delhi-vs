<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Add Setting Offer for Booking</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Add Setting Offer for Booking</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Add Setting Offer for Booking</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <form id="settigOfferAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/settingsforofferinsert" method="POST" enctype="multipart/form-data">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-check">
                        <label>Choose Your Offer</label>
                        <select class="form-control" name="productname" id="productname">
                          <option value="">--Please Select--</option>
                          <?php if(isset($productList)) { 
                            foreach ($productList as $key => $value) { ?>
                              <option value="<?php echo $value['id'];?>"><?php echo $value['title'];?></option>
                          <?php } } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                    <style type="text/css">
                      input[type="checkbox"]{
                        margin: 4px 6px 0px;
                        transform: scale(1.5);
                      }
                    </style>
                    <br><label>Enter Days Available and Time Slot</label>
                      <div class="form-group" id="dayschecked">  
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;">            
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input type="checkbox" class="form-check-input" id="timeslot_1" name="daysavailibility_monday" value="monday">Monday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_1" class="form-control txtslot" name="timeslot_monday" readonly="readonly">
                              </textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                              <input id="timeslot_2" type="checkbox" class="form-check-input" name="daysavailibility_tuesday" value="tuesday">Tuesday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_2" class="form-control txtslot" name="timeslot_tuesday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input id="timeslot_3" type="checkbox" class="form-check-input" name="daysavailibility_wednesday" value="wednesday">Wednesday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_3" class="form-control txtslot" name="timeslot_wednesday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input id="timeslot_4" type="checkbox" class="form-check-input" name="daysavailibility_thrusday" value="thrusday">Thrusday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_4" class="form-control txtslot" name="timeslot_thrusday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input id="timeslot_5" type="checkbox" class="form-check-input" name="daysavailibility_friday" value="friday">Friday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_5" class="form-control txtslot" name="timeslot_friday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input id="timeslot_6" type="checkbox" class="form-check-input" name="daysavailibility_saturday" value="saturday">Saturday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_6" class="form-control txtslot" name="timeslot_saturday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="border: 1px solid #eee;padding: 5px;margin: 5px;"> 
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">
                                <input id="timeslot_7" type="checkbox" class="form-check-input" name="daysavailibility_sunday" value="sunday">Sunday
                              </label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-check">
                              <label class="form-check-label" style="font-weight: normal;">Enter Time Slot (Comma Separted)</label>
                              <textarea id="timeslottxt_7" class="form-control txtslot" name="timeslot_sunday" readonly="readonly"></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">                      
                      <div class="col-md-6">                        
                        <div class="col-md-8">
                          <div class="form-group">
                            <label>Calender Blocked Dates</label>
                            <div class="date_wrap1">
                              <input type='text' name="calenderblockdate[]" class="calenderblockdate form-control" readonly="readonly" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>&nbsp;</label><br>
                            <button class="btn btn-info add_calDate" type="button">Add Date</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-check">
                          <label>Set Minimum Number Calendar Disable</label>
                          <input type="text" name="setminimumnumberdaydisable" id="setminimumnumberdaydisable" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-check">
                          <label>Set Maximum Number Calendar Enable</label>
                          <input type="text" name="setmaximumnumberdayenable" id="setmaximumnumberdayenable" class="form-control">
                        </div>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $(".calenderblockdate").datepicker();

    $('#setminimumnumberdaydisable').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });

    $('#setmaximumnumberdayenable').keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    var max_fields1      = 30; //maximum input boxes allowed
    var wrapper1         = $(".date_wrap1"); //Fields wrapper
    var add_button1      = $(".add_calDate"); //Add button ID    
    var z = 1; //initlal text box count
    $(add_button1).click(function(e) { //on add input button click
      e.preventDefault();
      if(z < max_fields1) { //max input box allowed
        z++; //text box increment
        $(wrapper1).append('<div style="margin-top:7px;"><input type="text" class="form-control" id="date_'+z+'" name="calenderblockdate[]" readonly="readonly"/><a href="#" style="color:red;" class="remove_field1">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper1).on("click",".remove_field1", function(e){ //user click on remove text
      e.preventDefault(); 
      $(this).parent('div').remove(); 
      z--;
    });

    $(wrapper1).on("click","input[type='text']", function(){      
      var dateId1 = $(this).attr('id');
      console.log(dateId1);    
      $("#"+dateId1).datepicker();
      $("#"+dateId1).datepicker('show');
    });

    var val = [];
    $('#dayschecked input[type=checkbox]:checked').each(function(i){
      val[i] = $(this).val();
      //alert(val[i])
      if(val[i] == 'monday'){
        $('#timeslottxt_1').removeAttr('readonly');
      } else if (val[i] == 'tuesday') {
        $('#timeslottxt_2').removeAttr('readonly');
      } else if (val[i] == 'wednesday') {
        $('#timeslottxt_3').removeAttr('readonly');
      } else if (val[i] == 'thrusday') {
        $('#timeslottxt_4').removeAttr('readonly');
      } else if (val[i] == 'friday') {
        $('#timeslottxt_5').removeAttr('readonly');
      } else if (val[i] == 'saturday') {
        $('#timeslottxt_6').removeAttr('readonly');
      } else if (val[i] == 'sunday') {
        $('#timeslottxt_7').removeAttr('readonly');
      }
    });

    $(".form-check-input").click(function() {
      var idVal = $(this).attr("id");
      var res = idVal.split("_");
      if($(this).prop("checked") == true){
        $('#timeslottxt_'+res[1]).removeAttr('readonly');
      } else if($(this).prop("checked") == false){
        $('#timeslottxt_'+res[1]).attr('readonly','readonly');
        $('#timeslottxt_'+res[1]).val(' ');
      }      
    });

  });
     
</script>