<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Domain</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Edit Domain</a>
            </li>
          </ol>
        </div>
      </div>
      <?php
        if($this->session->flashdata('domainupdatemsg')) {
        $message = $this->session->flashdata('domainupdatemsg');
      ?>
      <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
      <?php } ?>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Domain</strong></h4>
            <div class="row">
              <?php
                if(isset($editDomainList)){
                  $row = $editDomainList;
              ?>
              <?php
              $url = $_GET['url'];
              $safeUrl = htmlspecialchars($url);
              ?>
              <form id="domainUpdateForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/domaindataupdate?url=<?php echo $safeUrl;?>" method="post" enctype="multipart/form-data">
                         
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Enter Campaign Name</label>
                        <input class="form-control" type="text" name="campaignname" value="<?php if($row[0]['meta_key']=='campaignname'){echo $row[0]['meta_value'];}?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Select Theme</label>
                        <select class="form-control" name="themename">
                          <option value=""> --Please Select-- </option>
                          <option value="1" <?php if($row[1]['meta_key']=='themename' && $row[1]['meta_value']=='1'){echo 'selected="selected"';}?>> Theme 1 </option>
                          <option value="2" <?php if($row[1]['meta_key']=='themename' && $row[1]['meta_value']=='2'){echo 'selected="selected"';}?>> Theme 2 </option>
                          <option value="3" <?php if($row[1]['meta_key']=='themename' && $row[1]['meta_value']=='3'){echo 'selected="selected"';}?>> Theme 3 </option>
                        </select>
                    </div>
                  </div>                    
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Enter Domain</label>
                        <input class="form-control" type="text" name="subdomain" value="<?php if($row[2]['meta_key']=='subdomain'){echo $row[2]['meta_value'];}?>" readonly="readonly">
                    </div>
                  </div>
                                      
                </div>
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Logo</label>
                      <input type='file' name="logoimg" onchange="readURL(this);" />
                    </div>
                  </div>

                  <div class="col-md-6">
                    <img id="imgView" style="height: 80px;" src="<?php if($row[3]['meta_key']=='logoimg'){echo base_url().'assets/uploads/'.$row[3]['meta_value'];}?>" alt="logo" />
                    <?php if($row[3]['meta_key']=='logoimg') { ?>
                      <input type="hidden" name="postImg" value="<?php echo $row[3]['meta_value'];?>">
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 20px;border-bottom: 3px solid #eee;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Site Title</label>
                      <input type="text" class="form-control" name="sitetitle" id="sitetitle" value="<?php if($row[4]['meta_key']=='sitetitle'){echo $row[4]['meta_value'];}?>" />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Site Description</label>
                      <textarea class="form-control" name="sitedesc" id="sitedesc"><?php if($row[5]['meta_key']=='sitedesc'){echo $row[5]['meta_value'];}?></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-12" style="margin-bottom: 20px;border-bottom: 3px solid #eee;">                    
                  <div class="col-md-6">
                    <!-- <div class="form-group">
                      <label>Set Mobile Number & Email ID Usage Rule</label><br><br>
                        <input type="radio" id="2voucherperday" name="setruleformobandemail" value="2voucherperday" <?php if($row[6]['meta_key']=='setruleformobandemail' && $row[6]['meta_value']=='2voucherperday'){echo 'checked="checked"';}?>>
                        <label for="2voucherperday" style="font-weight: normal;">2 Voucher Per Day</label><br><br>
                        <input type="radio" id="5voucherperday" name="setruleformobandemail" value="5voucherperday" <?php if($row[6]['meta_key']=='setruleformobandemail' && $row[6]['meta_value']=='5voucherperday'){echo 'checked="checked"';}?>>
                        <label for="5voucherperday" style="font-weight: normal;">5 Voucher Per Week</label><br><br>
                        <input type="radio" id="10voucherpermonth" name="setruleformobandemail" value="10voucherpermonth" <?php if($row[6]['meta_key']=='setruleformobandemail' && $row[6]['meta_value']=='10voucherpermonth'){echo 'checked="checked"';}?>>
                        <label for="10voucherpermonth" style="font-weight: normal;">10 Voucher Per Month</label><br><br>
                        <input type="radio" id="15voucherduringpromotion" name="setruleformobandemail" value="15voucherduringpromotion" <?php if($row[6]['meta_key']=='setruleformobandemail' && $row[6]['meta_value']=='15voucherduringpromotion'){echo 'checked="checked"';}?>>
                        <label for="15voucherduringpromotion" style="font-weight: normal;">15 Voucher During Promotion</label>
                    </div> -->

                    <div class="form-group">
                      <label>Set Mobile Number & Email ID Usage Rule</label><br><br>
                        <input type="radio" id="voucherperday" name="setruleformobandemail" value="voucherperday" <?php if((isset($row[7]['meta_value'])) && $row[7]['meta_value']!=''){echo 'checked="checked"';}?>>
                        <label for="voucherperday" style="font-weight: normal;"><input type="text" class="voucherperday" name="voucherperday" style="width: 50px;" readonly="readonly" value="<?php if((isset($row[7]['meta_value'])) && $row[7]['meta_value']!=''){echo $row[7]['meta_value'];}?>"> Voucher Per Day</label><br><br>

                        <input type="radio" id="voucherperweek" name="setruleformobandemail" value="voucherperweek" <?php if((isset($row[8]['meta_value'])) && $row[8]['meta_value']!=''){echo 'checked="checked"';}?>>
                        <label for="voucherperweek" style="font-weight: normal;"><input type="text" class="voucherperweek" name="voucherperweek" style="width: 50px;" readonly="readonly" value="<?php if((isset($row[8]['meta_value'])) && $row[8]['meta_value']!=''){echo $row[8]['meta_value'];}?>"> Voucher Per Week</label><br><br>

                        <input type="radio" id="voucherpermonth" name="setruleformobandemail" value="voucherpermonth" <?php if((isset($row[9]['meta_value'])) && $row[9]['meta_value']!=''){echo 'checked="checked"';}?>>
                        <label for="voucherpermonth" style="font-weight: normal;"><input type="text" class="voucherpermonth" name="voucherpermonth" style="width: 50px;" readonly="readonly" value="<?php if((isset($row[9]['meta_value'])) && $row[9]['meta_value']!=''){echo $row[9]['meta_value'];}?>"> Voucher Per Month</label><br><br>

                        <input type="radio" id="voucherduringpromotion" name="setruleformobandemail" value="voucherduringpromotion" <?php if((isset($row[10]['meta_value'])) && $row[10]['meta_value']!=''){echo 'checked="checked"';}?>>
                        <label for="voucherduringpromotion" style="font-weight: normal;"><input type="text" class="voucherduringpromotion" name="voucherduringpromotion" style="width: 50px;" readonly="readonly" value="<?php if((isset($row[10]['meta_value'])) && $row[10]['meta_value']!=''){echo $row[10]['meta_value'];}?>"> Voucher During Promotion</label>

                    </div>

                  </div>
                  <div class="col-md-6">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Error Message For 2 Voucher Per Day</label>
                          <input type='text' name="errormessage2voucherperday" id="errormessage2voucherperday" class="form-control" value="<?php if($row[11]['meta_key']=='errormessage2voucherperday'){echo $row[11]['meta_value'];}?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Error Message For 5 Voucher Per Week</label>
                          <input type='text' name="errormessage5voucherperweek" id="errormessage5voucherperweek" class="form-control"  value="<?php if($row[12]['meta_key']=='errormessage5voucherperweek'){echo $row[12]['meta_value'];}?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Error Message For 10 Voucher Per Month</label>
                          <input type='text' name="errormessage10voucherpermonth" id="errormessage10voucherpermonth" class="form-control"  value="<?php if($row[13]['meta_key']=='errormessage10voucherpermonth'){echo $row[13]['meta_value'];}?>">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Error Message For 15 Voucher During Promotion</label>
                          <input type='text' name="errormessage15voucherduringpromotion" id="errormessage15voucherduringpromotion" class="form-control"  value="<?php if($row[14]['meta_key']=='errormessage15voucherduringpromotion'){echo $row[14]['meta_value'];}?>">
                      </div>
                    </div>
                  </div>
                </div> 
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="checkbox checkbox-primary">
                        <input id="enabledisablecity" type="checkbox" name="enabledisablecity" <?php if(isset($row[15]) && $row[15]['meta_key']=='enabledisablecity' && $row[15]['meta_value']=='on'){echo 'checked="checked"';}?>>
                        <label for="enabledisablecity">City Enable/Disable</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="checkbox checkbox-primary">
                        <input id="enabledisablevenue" type="checkbox"  name="enabledisablevenue" <?php if(isset($row[16]) && $row[16]['meta_key']=='enabledisablevenue' && $row[16]['meta_value']=='on'){echo 'checked="checked"';}?>>
                        <label for="enabledisablevenue">Venue Enable/Disable</label>
                      </div>                        
                    </div>
                  </div>
                </div>
                <?php 
                  if(isset($row[17]['meta_key'])) {
                    $dateJson = $row[17]['meta_value'];
                    $dateArr = json_decode($dateJson);
                  }                  
                ?>
                <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label>Choose Blocked Dates</label>
                          <div class="date_wrap">
                            <?php if(!empty($dateArr)){ 
                              foreach($dateArr as $key=>$val){  ?>
                                <div>
                                  <input type='text' name="chooseblockdate[]" style="margin-top:7px;" id="chooseblockdate" class="form-control" readonly="readonly" value="<?php echo $val;?>">
                                  <?php if($key!=0){?>
                                  <a href="#" style="color:red;" class="remove_field">Remove</a>
                                </div>
                            <?php } } } else { ?>
                            <input type='text' name="chooseblockdate[]" id="chooseblockdate" class="form-control" readonly="readonly" value="">
                          <?php } ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>&nbsp;</label><br>
                          <button class="btn btn-info add_Date" type="button">Add Date</button>
                        </div>
                      </div>
                    </div>
                  </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Set Minimum No of Days Required (numeric only)</label>
                        <input type='text' name="minimumnoofdays" id="minimumnoofdays" class="form-control" value="<?php if($row[18]['meta_key']=='minimumnoofdays'){echo $row[18]['meta_value'];}?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set Maximum No of Days Required (numeric only)</label>
                          <input type='text' name="maximumnoofdays" id="maximumnoofdays" class="form-control" value="<?php if($row[19]['meta_key']=='maximumnoofdays'){echo $row[19]['meta_value'];}?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Enter Time Slots Available</label>
                          <select name="timeslot" id="timeslot" class="form-control">
                            
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Set & Edit Registration Screen Refresh Time (in seconds)</label>
                          <input type="text" name="screenrefresh" id="screenrefresh" class="form-control" value="<?php if($row[21]['meta_key']=='screenrefresh'){echo $row[21]['meta_value'];}?>">
                        </div>
                      </div>
                    </div>
                    <style type="text/css">
                      input[type="checkbox"]{
                        margin: 4px 6px 0px;
                        transform: scale(1.5);
                      }
                    </style>
                    <?php 
                      if(isset($row[22]['meta_key'])){ 
                        $dayJson = $row[22]['meta_value'];
                        $dayArr = json_decode($dayJson);
                      }
                    ?>                
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Enter Days Available</label>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="monday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='monday'){echo 'checked="checked"';} } }?>>Monday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="tuesday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='tuesday'){echo 'checked="checked"';} } }?>>Tuesday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="wednesday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='wednesday'){echo 'checked="checked"';} } }?>>Wednesday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="thrusday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='thrusday'){echo 'checked="checked"';} } }?>>Thrusday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="friday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='friday'){echo 'checked="checked"';} } }?>>Friday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="saturday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='saturday'){echo 'checked="checked"';} } }?>>Saturday
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label" style="font-weight: normal;">
                            <input type="checkbox" class="form-check-input" name="daysavailibility[]" value="sunday" <?php if(isset($dayArr)){foreach ($dayArr as $key => $value) { if ($value=='sunday'){echo 'checked="checked"';} } }?>>Sunday
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>

                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                  </div>
                </div>
              </form> 
            <?php } ?>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
      $('#imgView')
          .attr('src', e.target.result)
          .width(100)
          .height(80);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  var selectedVal = '<?php if($row[20]['meta_key']=='timeslot'){echo $row[20]['meta_value'];}?>';
  if (selectedVal!=undefined) {
    setTimeout(function(){
      $("#timeslot option[value='"+selectedVal+"']").attr('selected', 'selected'); 
    }, 2000);
  }

  var $select = $("#timeslot");
  $select.append('<option value="">--Please Select --</option>');
  for (var hr = 0; hr < 24; hr++) {
    var hrStr = hr.toString().padStart(2, "0") + ":";
    var val = hrStr + "00";
    $select.append('<option value="' + val + '" >' + val + '</option>');
    val = hrStr + "30";
    $select.append('<option value="' + val + '" >' + val + '</option>');
  }
  $(document).ready(function() {
    $("#chooseblockdate").datepicker();

    $("#minimumnoofdays").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $("#maximumnoofdays").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });
    $("#screenrefresh").keyup(function () {
      var val = $(this).val();
        if(isNaN(val)){
          val = val.replace(/[^0-9\.]/g,'');
          if(val.split('.').length>2) 
          val =val.replace(/\.+$/,"");
        }
      $(this).val(val);       
    });

    var max_fields      = 30; //maximum input boxes allowed
    var wrapper         = $(".date_wrap"); //Fields wrapper
    var add_button      = $(".add_Date"); //Add button ID    
    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
      e.preventDefault();
      if(x < max_fields) { //max input box allowed
        x++; //text box increment
        $(wrapper).append('<div style="margin-top:7px;"><input type="text" class="form-control" id="date_'+x+'" name="chooseblockdate[]" readonly="readonly"/><a href="#" style="color:red;" class="remove_field">Remove</a></div>'); //add input box
      }
    });    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      e.preventDefault(); 
      $(this).parent('div').remove(); 
      x--;
    });

    $(wrapper).on("click","input[type='text']", function(){      
      var dateId = $(this).attr('id');
      console.log(dateId);    
      $("#"+dateId).datepicker();
      $("#"+dateId).datepicker('show');
    });

    $("input[name='setruleformobandemail']").click(function(){
      var idVal = $(this).attr("id");
      $('input[name="voucherperday"]').attr('readonly','readonly');
      $('input[name="voucherperweek"]').attr('readonly','readonly');
      $('input[name="voucherpermonth"]').attr('readonly','readonly');
      $('input[name="voucherduringpromotion"]').attr('readonly','readonly');
      $('input[name="voucherperday"]').val('');
      $('input[name="voucherperweek"]').val('');
      $('input[name="voucherpermonth"]').val('');
      $('input[name="voucherduringpromotion"]').val('');      
      var inputname = $('input[name="'+idVal+'"]').attr('class');
      if(inputname==idVal) {
        $('input[name="'+idVal+'"]').removeAttr('readonly');
      }
    });

    $('#domainUpdateForm').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var themename     = $("#themename").val();
      var subdomain     = $("#subdomain").val();

      if(themename == "") {
        $('#themename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#themename').css('border','1px solid #eee');
        noError = true;
      }
      if(subdomain == "") {
        $('#subdomain').css('border','1px solid red');
        noError = false;               
      } else {
        $('#subdomain').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#domainUpdateForm')[0].submit();   
      }
    });
  });
</script>



