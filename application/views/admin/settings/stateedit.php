<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->  
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit State</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Edit State</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit State</strong></h4>
            <div class="row">
               <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <?php if(isset($editstatesList)){
                  foreach($editstatesList as $key=>$row) { ?>
                  <form id="stateEditForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/stateupdate/<?php echo $row->id;?>" method="POST" enctype="multipart/form-data"> 
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Product Name</label>
                          <select class="form-control" name="pid" id="pid">
                            <option value="">--Please Select--</option>
                            <?php if(isset($productLists)){ 
                              foreach ($productLists as $k => $val) { ?>
                              <option value="<?php echo $val['id'];?>" <?php if ($val['id']==$row->prod_id) {echo 'selected="selected"';}?>><?php echo $val['title'];?></option>
                            <?php } } ?>
                          </select>                                              
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>CRM State ID<span style="font-weight: normal;font-size: 10px;"> (Only enter Numeric value)</span></label>
                          <input type="text" class="form-control" name="crm_state_id" id="crm_state_id" placeholder="CRM State ID" value="<?php echo $row->crm_state_id; ?>">
                        </div>
                      </div>
                   </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City Name</label>
                          <div class="input_fields_wraps">
                            <input type="text" class="form-control" id="statename" name="statename" placeholder="State Name" value="<?php echo $row->name; ?>">  
                          </div>                      
                        </div>
                      </div>                      
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Status</label>
                          <select class="form-control" name="status" id="status">
                            <option value="">--Please Select--</option>
                            <option value="1" <?php if ($row->status=='1') {echo 'selected="selected"';}?>>Active</option>
                            <option value="0" <?php if ($row->status=='0') {echo 'selected="selected"';}?>>Inactive</option>
                          </select>
                        </div>
                      </div>
                   </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>
                </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#stateEditForm').on('submit', function (e) {
      e.preventDefault();              
      var noError        = true;
      var statename       = $("#statename").val(); 
      var status         = $("#status").val();

      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(statename == ""){
        $('#statename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#statename').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#stateEditForm')[0].submit();
      }
    });
     
  });
</script>