<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit User</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active"><a>Edit User</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit User</strong></h4>
            <div class="row">
               <?php
                  if($this->session->flashdata('registration')) {
                  $message = $this->session->flashdata('registration');
                ?>
                 <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php   }    ?>
                 <?php 
                    if(isset($editUserList)){
                        foreach($editUserList as $key=>$row){
              ?>
              <form id="registrationEdit" method="POST" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updateUser/<?php echo $row->id; ?>" enctype="multipart/form-data">
               
                 <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" value="<?php echo $row->name; ?>" name="name" id="name"  placeholder="Name">
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" value="<?php echo $row->emailaddress; ?>" name="emailaddress" id="emailaddress"  placeholder="Email Address">
                      </div>
                    </div>

                 </div>
                 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group" id="stateError">
                        <label>State</label>
                        <select name="state" id="state" class="select2 select2-multiple"  data-placeholder="Choose state..." required>
                          <option value=""> -- Select-- </option>
                          <?php foreach($stateList as $k=>$r){
                            ?>
                             <option  <?php if($row->state == $r->id ){ ?> selected <?php } ?>  value="<?php echo $r->id; ?>"><?php echo $r->stateName; ?></option>
                            <?php
                          } ?>
                         </select>                     
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id="cityError">
                        <label>City</label>
                        <select name="city" id="city" class="select2 select2-multiple"  data-placeholder="Choose City..." required>
                          <option value=""> -- Select-- </option>
                          <?php foreach($citiesList as $k=>$r){
                            ?>
                             <option  <?php if($row->city == $r->id ){ ?> selected <?php } ?>  value="<?php echo $r->id; ?>"><?php echo $r->cityName; ?></option>
                            <?php
                          } ?>
                         </select>                     
                      </div>
                    </div>
                   
                  </div>

                  <div class="col-md-12">                     
                    <div class="form-group col-md-6">
                      <label>Pincode</label>
                        <input type="number" class="form-control" name="pincode" id="pincode"  placeholder="Pincode" value="<?php echo $row->pincode; ?>">
                    </div>
                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Mobile No</label>
                        <input type="number" class="form-control" name="mobile" id="mobile"  autocomplete="off"  placeholder="Mobile" value="<?php echo $row->mobile; ?>" >
                      </div> 
                  </div>
                  </div>

                   <div class="col-md-12">
                     <!-- <div class="col-md-6">
                      <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" id="password" autocomplete="off" value="<?php echo $row->password; ?>" placeholder="Password" >
                      </div> 
                  </div> -->
                    <div class="col-md-6">
                      <div class="form-group">
                         <label>Status</label>
                        <select class="form-control"   name="status" id="status" required>
                          <?php foreach($status as $k=>$r){
                            ?>
                             <option  <?php if($row->status == $r->id ){ ?> selected <?php } ?>  value="<?php echo $r->id; ?>"><?php echo $r->status; ?></option>
                            <?php
                          } ?>
                        </select>
                      </div>  
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="button" onclick="editUserValid()" class="btn btn-default">Update</button>
                      </div>
                    </div>
                  </div>
              </form>  
                   <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->
<script>
    var loadFile = function(event) {
        var banner = document.getElementById('loadimage');
        banner.src = URL.createObjectURL(event.target.files[0]);
    };
</script> 


