<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create User</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
          
            <li class="active"><a>Create User</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create User</strong></h4>
            <div class="row">
              <form  id="userRegistration" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/insert" method="post" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('registration')) {
                      $message = $this->session->flashdata('registration');
                   ?>
                 <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php   }    ?>
                   
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" id="name"  placeholder="Name">
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                         <label>Email Address</label>
                        <input type="email" class="form-control" name="emailaddress" id="emailaddress"  placeholder="Email Address" >
                      </div>
                    </div>
                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                       <label>State</label>
                      <div class="form-group" id="stateError">
                        <select name="state" id="state"  class="select2 select2-multiple"  data-placeholder="Choose state..." required>
                          <option value=""> -- Select-- </option>
                        <?php foreach($stateList as $key=>$row){ ?>
                          <option value="<?php echo $row->id; ?>"><?php echo $row->stateName; ?></option>
                        <?php } ?>
                         </select>                     
                      </div>
                    </div>
                     <div class="col-md-6">
                       <label>City</label>
                      <div class="form-group" id="cityError">                       
                        <select name="city" id="city" class="select2 select2-multiple"  data-placeholder="Choose City..." required>
                          <option value=""> -- Select-- </option>
                          
                         </select>                     
                      </div>
                     </div>                     
                  </div>
                   <div class="col-md-12">                     
                    <div class="form-group col-md-6">
                      <label>Pincode</label>
                        <input type="number" class="form-control" name="pincode" id="pincode"  placeholder="Pincode" >
                    </div>
                   <div class="col-md-6">
                      <div class="form-group">
                        <label>Mobile No</label>
                        <input type="number" class="form-control" name="mobile" id="mobile"  autocomplete="off"  placeholder="Mobile" >
                      </div> 
                  </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" class="form-control" name="password" id="password"  autocomplete="off"  placeholder="Password" >
                        </div> 
                    </div>                    
                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Confirm Password</label>
                          <input type="password" class="form-control" name="confirmPassword" id="confirmPassword"  autocomplete="off"  placeholder="Confirm Password" >
                        </div> 
                    </div>
                    </div>
                    <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" data-placeholder="Choose Status..."   name="status" id="status" required>
                          <option value="">--Select Status--</option>
                          <?php if(isset($status)){
                            foreach($status as $key=>$row){ ?>
                               <option value="<?php echo $row->id; ?>"><?php echo $row->status; ?></option>
                         <?php   }
                          } ?>                      
                        </select>
                      </div> 
                    </div>
                    </div>
                 
               
                 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="button" onclick="registration()" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->
<script>
    var loadFile = function(event) {
        var banner = document.getElementById('loadimage');
        banner.src = URL.createObjectURL(event.target.files[0]);
    };
</script> 



