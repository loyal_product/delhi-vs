<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <?php if(isset($registeredlists)){ ?>
          <div class="col-sm-4 col-lg-3">
            <div class="card" style="min-height: 180px;">
              <div class="card-block p-3 clearfix">
                <em class="fa fa-user bg-primary p-3 font-2xl mr-3 float-left"></em>
                <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">Registered Users</div>
                <div class="h5" style="text-align: center;position: absolute;width: 100%;left: 0px;bottom: 45px;"><?php echo $registeredlists;?></div>
              </div>
              <div class="card-footer px-3 py-2" style="position: absolute;bottom: 0px;width: 100%;"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/registereduserlist">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
          </div>
        <?php } ?>
        <?php if(isset($morethan5regsameip)){ ?>
          <div class="col-sm-4 col-lg-3">
            <div class="card" style="min-height: 180px;">
              <div class="card-block p-3 clearfix">
                <em class="fa fa-user bg-primary p-3 font-2xl mr-3 float-left"></em>
                <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">IP Address <span style="font-size: 10px;display: flex;">(Registration Report)</span><p style="font-size: 8px;font-weight: normal;">5 or more registrations from same IP address</p></div>
                <div class="h5" style="text-align: center;position: absolute;width: 100%;left: 0px;bottom: 45px;"><?php echo $morethan5regsameip;?></div>
              </div>
              <div class="card-footer px-3 py-2" style="position: absolute;bottom: 0px;width: 100%;"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/registrationmorethan5sameip">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
          </div>
        <?php } ?>
        <?php if(isset($morethan5booksameip)){ ?>
          <div class="col-sm-4 col-lg-3">
            <div class="card" style="min-height: 180px;">
              <div class="card-block p-3 clearfix">
                <em class="fa fa-user bg-primary p-3 font-2xl mr-3 float-left"></em>
                <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">IP Address <span style="font-size: 10px;display: flex;">(Booking Report)</span><p style="font-size: 8px;font-weight: normal;">5 or more bookings from same IP address</p></div>
                <div class="h5" style="text-align: center;position: absolute;width: 100%;left: 0px;bottom: 45px;"><?php echo $morethan5booksameip;?></div>
              </div>
              <div class="card-footer px-3 py-2" style="position: absolute;bottom: 0px;width: 100%;"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/bookingmorethan5sameip">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
          </div>
        <?php } ?>
        <?php if(isset($morethan3regsameemail)){ ?>
          <div class="col-sm-4 col-lg-3">
            <div class="card" style="min-height: 180px;">
              <div class="card-block p-3 clearfix">
                <em class="fa fa-user bg-primary p-3 font-2xl mr-3 float-left"></em>
                <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">Email Id <span style="font-size: 10px;display: flex;">(Registration Report)</span><p style="font-size: 8px;font-weight: normal;">3 or more registrations done similar email ids</p></div>
                <div class="h5" style="text-align: center;position: absolute;width: 100%;left: 0px;bottom: 45px;"><?php echo $morethan3regsameemail;?></div>
              </div>
              <div class="card-footer px-3 py-2" style="position: absolute;bottom: 0px;width: 100%;"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/registrationmorethan3sameemail">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
          </div>
        <?php } ?>
        <?php if(isset($morethan3booksameemail)){ ?>
          <div class="col-sm-4 col-lg-3">
            <div class="card" style="min-height: 180px;">
              <div class="card-block p-3 clearfix">
                <em class="fa fa-user bg-primary p-3 font-2xl mr-3 float-left"></em>
                <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">Email Id <span style="font-size: 10px;display: flex;">(Booking Report)</span><p style="font-size: 8px;font-weight: normal;">3 or more bookings done similar email ids</p></div>
                <div class="h5" style="text-align: center;position: absolute;width: 100%;left: 0px;bottom: 45px;"><?php echo $morethan3booksameemail;?></div>
              </div>
              <div class="card-footer px-3 py-2" style="position: absolute;bottom: 0px;width: 100%;"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/bookingmorethan3sameemail">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
          </div>
        <?php } ?>
        <!-- <div class="col-sm-6 col-lg-3">
            <div class="card">
               <div class="card-block p-3 clearfix">
                  <em class="fa fa-trophy bg-info p-3 font-2xl mr-3 float-left"></em>
                  <div class="text-uppercase text-muted font-weight-bold font-xs mb-0 mt-2">Used Voucher</div>
                  <div class="h5"><?php echo $usedVouchers;?></div>
               </div>
               <div class="card-footer px-3 py-2"><a class="font-weight-bold font-xs btn-block text-muted" href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/usedvoucherlist">View More <em class="fa fa-angle-right float-right font-lg"></em></a></div>
            </div>
         </div> -->
         
      </div>
        
      <div id=resultprint></div>    

             
             





    </div> <!-- content -->
  </div>
</div>
 
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->
<style type="text/css">
  .card{
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    position: relative;
    display: flex;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid #c2cfd6;
    margin-bottom: 1.5rem;
  }
  .p-3 {
    padding: 1rem !important;
  }
  .font-2xl {
    font-size: 1.5rem !important;
  }
  .p-3 {
    padding: 1rem !important;
  }
  .mr-3 {
    margin-right: 1rem !important;
  }
  .mt-2 {
    margin-top: 0.5rem !important;
  }
  .text-uppercase {
    text-transform: uppercase !important;
  }
  .font-weight-bold {
    font-weight: bold;
  }
  .float-left {
    float: left !important;
  }
  .text-muted {
    color: 
    #536c79 !important;
  }
  .bg-primary, .bg-success, .bg-info, .bg-warning, .bg-danger, .bg-inverse {
    color: 
    #fff;
  }
  .card {
    word-wrap: break-word;
 }
  .font-2xl {
    font-size: 3.5rem !important;
  }
  .card-footer {

    padding: 0.75rem 1.25rem !important;
    background-color: #f0f3f5 !important;
    border-top: 1px solid #c2cfd6 !important;
  }
  .px-3 {
    padding-right: 1rem !important;
    padding-left: 1rem !important;
  }
  .py-2 {
    padding-top: 0.5rem !important;
    padding-bottom: 0.5rem !important;
  }
  .font-2xl {
    font-size: 3.5rem !important;
  }
  .h5 {
    font-size: 2rem !important;
    margin-bottom: 0.5rem;
    font-family: inherit;
    font-weight: 500;
    line-height: 1.1;
    color: rgb(21, 27, 30) !important;
  }
  
</style>
</body>

</html>

