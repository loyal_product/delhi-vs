<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Change Password</h4>
          <ol class="breadcrumb">
            <li class="active">
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/changePassword">Change Password</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Change Password</strong></h4>
            <div class="row">
              <form  id="userChangePass" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/changePasswordUpdate" method="post" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('user')) {
                    $message = $this->session->flashdata('user');
                  ?>
                 <div id="user" class="<?php echo $message['class'];?>" ><?php echo $message['message']; ?></div>
                  <?php } ?>
                 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password"  placeholder="New Password">
                      </div>
                    </div>
                  </div>
                   <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" name="cpassword" id="cpassword"  placeholder="Confirm Password" >
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="button" onclick="changePassword()"  class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ==============================================================  -->
<script>
    var loadFile = function(event) {
        var banner = document.getElementById('loadimage');
        banner.src = URL.createObjectURL(event.target.files[0]);
    };
</script> 



