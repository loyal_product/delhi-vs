<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $title;?></title>
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>
    </head>
    <body>
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
                <div class="panel-heading">
                    <h3 class="text-center"> Sign In to <strong  class="text-custom">Admin</strong> </h3>
                </div> 
                <div class="panel-body" >
                    <?php
                        if($this->session->flashdata('user')) {
                            $message = $this->session->flashdata('user');
                    ?>
                     <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                    <?php   }  ?>
                   <div id="forgot">
                    <form class="form-horizontal m-t-20"  id="userLoginForm" method="POST" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/login">
                        
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" id="emailaddress" type="text" placeholder="Please Enter Email address" name="emailaddress">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" id="password" type="password" placeholder="Please enter password" name="password">
                            </div>
                        </div>                
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button  type="button" onclick="loginvalid()" class="btn btn-primary btn-block text-uppercase waves-effect waves-light">Log In</button>
                            </div>
                        </div>
                      </form>
                   <!--    <a  onclick="forgot_password()">forgot password</a>-->
                    </div>
                </div>
            </div> 
        </div>
        </div>
        
    	
        <!-- jQuery  -->
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/detect.js"></script>
        <script src="<?php echo base_url();?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url();?>assets/js/waves.js"></script>
        <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.app.js"></script>
        <!-- validatio scriipts -->
        <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/validation.script.js"></script>
        <script>
            function forgot_password(){
                $("#forgot").html('<form class="form-horizontal m-t-20"  id="forgotForm" method="POST" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/forgot_password"><div class="form-group "><div class="col-xs-12">  <input class="form-control" id="code" type="code" placeholder="Please Enter valid code" name="code"></div></div><div class="form-group text-center m-t-20"> <div class="col-xs-12"> <button  type="button" onclick="forget_valid()" class="btn btn-primary btn-block text-uppercase waves-effect waves-light">Submit</button></div> </div></form>');
            }

            function loginvalid(){
               // alert("in");
                var isError = 0;
                var emailaddress =$('#emailaddress').val();
                var password =$('#password').val();
                if(emailaddress =='')  {
                    $('#emailaddress').css({'border':'red solid 1px'});
                    isError++;
                }
                else {
                    $('#emailaddress').css({'border':'1px solid #E3E3E3'});
                    $('span.error-keyup-2').remove();
                }
                if(password =='')  {
                    $('#password').css({'border':'red solid 1px'});
                    isError++;
                }
                else {
                    $('#password').css({'border':'1px solid #E3E3E3'});
                    $('span.error-keyup-2').remove();
                }
                                   
                if(isError == 0){
                   $("#userLoginForm").submit();
                }else{
                    return false;
                }
            }

        function forget_valid(){
           var isError = 0;
           var code =  $("#code").val();
           if(code == ""){
                $('#code').css({'border':'red solid 1px'});
                isError++;
           }
           else{
             $('#code').css({'border':'1px solid #E3E3E3'});
             $('span.error-keyup-2').remove();
           }
           if(isError == 0){
                $("#forgotForm").submit();
              
            }else{
                return false;
            }
        }
        </script>
	</body>
</html>