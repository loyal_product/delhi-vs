<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">OTP Message</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>OTP Message</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <label>OTP shortcode: {{otp}}</label>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>OTP Message</strong></h4>
            <div class="row">
              <form  id="otpMessageForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/otpmessageinsert" method="post" enctype="multipart/form-data">
                <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                  <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                 <?php } ?>

                  <div class="col-md-12">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>OTP SMS</label>
                        <textarea class="form-control" name="otpsms" rows="6" id="otpsms"><?php if(isset($otpmessagelist)) {echo $otpmessagelist->message;}?></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $('#otpMessageForm').on('submit', function (e) {
      e.preventDefault();              
      var noError = true;
      var otpsms  = $("#otpsms").val();

      if(otpsms == ""){
        $('#otpsms').css('border','1px solid red');
        noError = false;               
      } else {
        $('#otpsms').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#otpMessageForm')[0].submit();
      }
    });
  });
</script>