<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Booking Success Mail</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Edit Booking Success Mail</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
            <label>Customer Name: {{ConsumerName}}</label>
          <label>Booking Details: {{BookingDetails}}</label>
          <label>Reward Code: {{RewardCode}}</label>
          <label>Email Id: {{emailId}}</label>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Booking Success Mail</strong></h4>
            <div class="row">
              <?php 
              if(isset($editbookingmaillist)){
                foreach($editbookingmaillist as $key=>$row){
                  $formails = $row->new_mail_type ? $row->new_mail_type : '';
              ?>
              <form  id="bookingSuccessForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updatebookingsuccessmailbyoffer/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
                <?php if($this->session->flashdata('msgshow')) {
                  $message = $this->session->flashdata('msgshow'); ?>
                  <div id="registration" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Product Name</label>
                        <select class="form-control" name="pid" id="pid">
                          <option value="">-- Please Select --</option>
                          <?php if (isset($productlist)) {
                            foreach ($productlist as $key => $val) { ?>
                            <option value="<?php echo $val['id']; ?>" data-offset="<?php echo $val['title']; ?>" <?php if($row->prod_id==$val['id']){echo 'selected="selected"';}?>><?php echo $val['title']; ?></option>
                          <?php } } ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Mail Subject</label>
                      <input type="text" class="form-control" name="mail_subject" id="mail_subject" value="<?php echo $row->mail_subject;?>" placeholder="Mail Subject">
                    </div>
                  </div>
                

                  <div class="col-md-12" id="formailingenable">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>For Mail</label>
                          <select class="form-control" name="formail" id="formail">
                          </select>
                      </div>
                    </div>
                  </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Mail Message</label>
                        <textarea class='ckeditor' name='mail_body' id="mail_body"><?php echo $row->mail_body;?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>SMS</label>
                        <textarea class="form-control" name="sms" rows="6" id="sms"><?php echo $row->sms;?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>SMS Template ID</label>
                        <input type="text" class="form-control" name="smstmpid" id="smstmpid" value="<?php echo $row->smstmpid;?>" placeholder="SMS Template ID" required="required">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>
              <?php } } ?>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<?php 
if(isset($bookingid5) && !empty($bookingid5)) { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#formailingenable').show();
      var element1 = $('#pid').find('option:selected');
      var myTag1 = element1.attr("data-offset");
      setTimeout(function(){
        var newdrpdwn = '<?php echo $formails; ?>';
        console.log(element1);
        var res1 = myTag1.split("/");
        $('#formail').html('');
        $.each(res1, function( index, vals ) {
          if(vals==newdrpdwn){
            var sel = 'selected="selected"';
          } else {
            var sel = '';
          }
          $('#formail').append('<option value="'+vals+'" '+ sel+'>'+vals+'</option>');
        });
      }, 1000);
    });
  </script>
<?php } else { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#formailingenable').hide();
    });
  </script>
<?php } ?>
<script type="text/javascript">
  $(document).ready(function() {    
    $('#pid').change(function() {
      var element = $(this).find('option:selected'); 
      var myTag = element.attr("data-offset"); 

      if (myTag.indexOf('/') == -1) {
        $('#formailingenable').hide();
      } else {
        $('#formailingenable').show();
        var res = myTag.split("/");
        $('#formail').html('');
        $.each(res, function( index, val ) {
          $('#formail').append('<option value="'+val+'">'+val+'</option>');
        });
      }
    });    

    CKEDITOR.replace( 'mail_body', {
      height: 300,
      filebrowserUploadUrl: "<?= base_url()?>site/core/micro/site/lib/controller/type/uploadimgckeditor",
    });
    
    $('#bookingSuccessForm').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var mail_subject  = $("#mail_subject").val();
      var mail_body     = $("#cke_mail_body").val();
      var sms           = $("#sms").val();

      if(mail_subject == ""){
        $('#mail_subject').css('border','1px solid red');
        noError = false;               
      } else {
        $('#mail_subject').css('border','1px solid #eee');
        noError = true; 
      }
      if(mail_body == ""){
        $('#cke_mail_body').css('border','1px solid red');
        noError = false;               
      } else {
        $('#cke_mail_body').css('border','1px solid #eee');
        noError = true; 
      }
      if(sms == ""){
        $('#sms').css('border','1px solid red');
        noError = false;               
      } else {
        $('#sms').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#bookingSuccessForm')[0].submit();   
      }
    });
  });
</script>