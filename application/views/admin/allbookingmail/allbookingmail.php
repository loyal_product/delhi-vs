<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Booking Mail</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>admin/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Booking Mail</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php
            if($this->session->flashdata('allbookings')) {
            $message = $this->session->flashdata('allbookings');
          ?>
           <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Booking Mail</strong>
            </h4>
            <div class="col-md-12" style="background-color: #fff;border: 1px solid #eee;padding: 3px;">
            <form method="post" id="resendmail" class="search_order_input" style="padding: 5px;">
              <label>Search for Resend mail</label>
               <div class="row">
                <div class="col-md-3 col-xs-12">
                  <label>Enter Voucher Code</label>
                  <input type="text" name="vouchercode" class="form-control" id="vouchercode" value="<?php if(isset($vouchercode)){echo $vouchercode;} ?>" placeholder="Enter Voucher Code">
                </div>
                <!-- <div class="col-md-3 col-xs-12">
                  <label>Registered Email Id</label>
                  <input type="text" name="registeredemailid" class="form-control" id="registeredemailid" value="<?php if(isset($registeredemailid)){echo $registeredemailid;} ?>" placeholder="Registered Email Id">
                </div>
                <div class="col-md-3 col-xs-12">
                  <label>Registered Mobile</label>
                  <input type="text" name="registeredmobile" class="form-control" id="registeredmobile" value="<?php if(isset($registeredmobile)){echo $registeredmobile;} ?>" placeholder="Registered Mobile">
                </div> -->
                 <div class="col-md-3 col-xs-12" style="margin-top: 25px;">
                  <input type="submit" name="search" class="btn btn-info" id="search" value="Search">
                </div>
              </div> 
            </form>
          </div>

            <div class="row">
              <div class="col-md-12">
                <div id="tbl-data-bind"></div>

                <!-- <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th scope="col" class='collapsable'>S.No</th>
                        <th scope="col" class='collapsable'>Voucher Code Used</th>
                        <th scope="col" class='collapsable'>Date & Time</th>
                        <th scope="col" class='collapsable'>Mobile</th>
                        <th scope="col" width="200" class='collapsable'>Email ID</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php 
                    if((count($allbookings)>0) && !empty($allbookings)) {
                      foreach($allbookings as $key=>$row) {
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['vouchercode'];?></td>
                      <td class='collapsable'><?php echo $row['orderdatetime'];?></td>
                      <td class='collapsable'><?php echo $row['mobile'];?></td>
                      <td width="200" class='collapsable'><?php echo $row['email'];?></td>                     
                    </tr>
                    <?php } } else{  ?>
                      <tr><td  colspan="7">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               
                </div> -->
              </div>    
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
    $(document).ready(function(){   
      /*$("#registeredmobile").keyup(function () {
        var val = $(this).val();
          if(isNaN(val)){
            val = val.replace(/[^0-9\.]/g,'');
            if(val.split('.').length>2) 
            val =val.replace(/\.+$/,"");
          }
        $(this).val(val);       
      }); */
      $('#resendmail').on('submit', function (e) {
        e.preventDefault();              
        var noError               = true;
        var vouchercode           = $("#vouchercode").val();
        /*var registeredemailid     = $("#registeredemailid").val(); 
        var registeredmobile      = $("#registeredmobile").val();*/

        if(vouchercode == ""){
          $('#vouchercode').css('border','1px solid red');
          noError = false;               
        } else {
          $('#vouchercode').css('border','1px solid #eee');
          noError = true; 
        }
        /*if(registeredemailid == ""){
          $('#registeredemailid').css('border','1px solid red');
          noError = false;               
        } else {
          $('#registeredemailid').css('border','1px solid #eee');
          noError = true; 
        }
        if(registeredmobile == ""){
          $('#registeredmobile').css('border','1px solid red');
          noError = false;               
        } else {
          $('#registeredmobile').css('border','1px solid #eee');
          noError = true; 
        }*/

        if(noError){  
          var form = $(this);
          $.ajax({
            type: "POST",
            url: "<?php echo base_url().'site/core/micro/site/lib/controller/type/resenddetails'?>",
            data: form.serialize(), 
            dataType: "html",
            beforeSend: function () {              
              $('.loader1').removeClass('hide');
            },
            success: function(data){
              $('.loader1').addClass('hide');
              $('#tbl-data-bind').html("");
              $('#tbl-data-bind').prepend(data);
            },
            error: function() { 
              $('.loader1').addClass('hide');
              $('#msg').html("<div class='alert alert-danger'>Something went wrong..!!</div>");
            }
          });
        }
      });

      $(document).on('click','#resendSubmit',function(e) {
        e.preventDefault(); 
        var url = $(this).attr('data-id');
        $.ajax({
          type: "POST",
          url: url,
          dataType: "html",
          beforeSend: function () {
            $('.loader1').removeClass('hide');
          },
          success: function(data){
            $('.loader1').addClass('hide');
            $('#msg').html("<div class='alert alert-success'>Mail has been sent successfully..!!</div>");
          },
          error: function() { 
            $('.loader1').addClass('hide');
            $('#msg').html("<div class='alert alert-danger'>Mail has not been sent successfully..!!</div>"); 
          }
        });        
      });
    });
  </script>