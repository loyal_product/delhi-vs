    <div class="loader1 hide">
        <div class="middle">
            <img src="<?php echo base_url();?>assets/img/loader2.gif" alt="loader"/><span>Please Wait</span>
        </div>
    </div>
    <style type="text/css">
    .loader1{position:fixed;left:0%;top:0%;bottom:0%;border:1px solid #000;background-color:#000;color:#fff;opacity:0.8;-webkit-border-radius:5px;-moz-border-radius:5px;width:100%;height:auto;text-align:center;z-index:100000;border-radius:0px;}
    .middle{background:transparent;width:100%;height:100%;}
    .loader1 .middle img{top:50%;position:absolute;left:50%;width:35px;margin-top:-50px;margin-left:-50px;}
    .loader1 .middle span{top:50%;position:absolute;left:50%;font-weight:600;color:#fff;margin-top:-50px;}
    </style>
      <footer class="footer text-right">
        © <?php echo date("Y"); ?>. All rights reserved.
      </footer>

      <!-- ============================================================== -->
      <!-- End Right content here -->
      <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    
    <!-- jQuery  --> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/fastclick.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>

  	<script type="text/javascript">
      
      var resizefunc = [];
      CKEDITOR.replace('description1', {
        height: 300,
        filebrowserUploadUrl: "<?= base_url()?>site/core/micro/site/lib/controller/type/uploadimgckeditor",
        allowedContent: true,
      });
      CKEDITOR.replace('description2', {
        height: 300,
        filebrowserUploadUrl: "<?= base_url()?>site/core/micro/site/lib/controller/type/uploadimgckeditor",
        allowedContent: true,
      });
      CKEDITOR.replace('description3', {
        height: 300,
        filebrowserUploadUrl: "<?= base_url()?>site/core/micro/site/lib/controller/type/uploadimgckeditor",
        allowedContent: true,
      });

      CKEDITOR.editorConfig = function( config ) {
        config.extraPlugins = 'autogrow,texttransform';
      };
    </script>   
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>

<script type="text/javascript">
  function resetPassword(){
    //var oldPassword=$('#oldPassword').val();
    var password=$('#password').val();
    var cpassword= $('#cpassword').val();
    //  var preUrl= $('#preUrl').val();
    
    var id=$('#id').val();    
     if(password=='') {
      $('#password').css({'border':'red solid 1px'});
      return false;
     } else{
      $('#password').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
    }    
    if(cpassword=='')  {
      $('#cpassword').css({'border':'red solid 1px'});
      return false;
    } else{
      $('#cpassword').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
     }
    if(password != cpassword ){
      $('#cpassword').css({'border':'red solid 1px'});
      return false;
    }
    else{
      $('#cpassword').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
    }
     
    $.ajax({ 
      url: "<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/resetPassword",
      type:'POST',
      data:{password,id}, 
      success: function(result){
          var referrer =  document.referrer;
          window.location.replace(referrer)   
      }
    });
  }


  // User Reset Password of outlets BY Admin/HO ---------------------
  function changePassword(){
    var isError = 0;
    var password=$('#password').val();
    var cpassword= $('#cpassword').val();    
    if(password=='') {
      $('#password').css({'border':'red solid 1px'});
      isError++;
    } else {
      $('#password').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
    }    
    if(cpassword=='') {
      $('#cpassword').css({'border':'red solid 1px'});
      isError++;
    } else {
      $('#cpassword').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
    }
    if(password != cpassword ){
      $('#cpassword').css({'border':'red solid 1px'});
      isError++;
    } else {
      $('#cpassword').css({'border':'1px solid #E3E3E3'});
      $('span.error-keyup-2').remove();
    }     
    if(isError == 0){
      $("#userChangePass").submit();
    } else {
      return false;
    }
  }
</script>
  <!-- End Field --->
  <!-- ============================================================== -->
  <!-- End Right content here -->
  <!-- ==============================================================	 -->

  <style type="text/css">
    .modal-footer {
      border-top:none;
    }   
  </style>
  <script type="text/javascript">
    $(window).load(function(){
      $('.loader').fadeOut();
    });    
  </script>

    
  </body>
</html>