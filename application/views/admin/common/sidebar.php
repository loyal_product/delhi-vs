<!-- ========== Left Sidebar Start ========== -->  
  <?php
    $UserType =$this->session->userdata('userType'); 
    $id =$this->session->userdata('id');   
  ?>
  <div class="left side-menu" style="background:#5c5e5f; overflow: scroll;">
    <div class="sidebar-inner slimscrollleft">
  	  <!--- Divider -->
      <div id="sidebar-menu">
				<ul>
				  <?php if($UserType == 1) { ?>	
					<li class="has_sub">
						<a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard" class="waves-effect"><em class="ti-home"></em> <span> Dashboard</span> </a>
					</li>
					<li class="has_sub">
      			<a href="javascript:void(0);" class="waves-effect"><em class="ti-settings"></em><span>Settings </span> <span class="menu-arrow"></span></a>
     				<ul class="list-unstyled">
			       	<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/generalsetting">General Setting</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/settingsforofferlist">Setting for Offers</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/cityvenue">State,City & Venue Upload</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/circlelist">Circle List</a></li>
						</ul>
					</li>
					<li>
			      <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/listdynamicform"><em class="ti-gift"></em><span>Create Dynamic Form</span> </a>		        
					</li>
					<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/fieldslist"><em class="ti-notepad"></em> Field Label Management</a></li>
					<!-- <li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-gift"></em><span>Voucher Management </span> <span class="menu-arrow"></span></a>
		        <ul class="list-unstyled"> -->
							<li><a class="waves-effect" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherList"><em class="ti-gift"></em> Voucher Management</a></li>
							<!-- <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/caplimitlist">Cap Limit Setting</a></li> -->
						<!-- </ul>
					</li> -->
					<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblocklist" class="waves-effect"><em class="ti-bar-chart"></em> <span> Block / Unblock</span> </a></li>
					<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/listcashbackbooking" class="waves-effect"><em class="ti-bar-chart"></em> <span> Booking Page For Cashback</span> </a></li>
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-layout"></em><span>Product Management </span> <span class="menu-arrow"></span></a>
		        <ul class="list-unstyled">
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/productList">Offer List</a></li>
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/subproductlist">Product List</a></li>
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/languagelist">Language List</a></li>
						</ul>
					</li>
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-notepad"></em><span>Registration/Booking Mail </span> <span class="menu-arrow"></span></a>
		        <ul class="list-unstyled">
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/bookingsuccessmaillist"> Booking Success Mail</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/registartionsuccessmail"> Registartion Success Mail</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/otpmessage"> OTP SMS</a></li>
						</ul>
					</li>
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-files"></em><span>Inventory Management </span> <span class="menu-arrow"></span></a>
		        <ul class="list-unstyled">
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventorylist">Inventory Management</a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventorydashboard">Inventory Dashboard</a></li>
						</ul>
					</li>
					<!-- <li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-gift"></em><span>Menu Management </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled"> -->
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/menulist"><em class="ti-gift"></em> Menu Management</a></li>
						<!-- </ul>
					</li> -->
					<!-- <li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-image"></em><span>Banner Management </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled"> -->
		        	<!-- <li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/logosetting">Logo Setting</a></li> -->
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/bannerlist"><em class="ti-image"></em> Banner Management</a></li>
						<!-- </ul>
					</li> -->
					<!-- <li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-notepad"></em><span>Content Management </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled"> -->
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/contentlist"><em class="ti-notepad"></em> Content Management</a></li>
						<!-- </ul>
					</li> -->
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-files"></em><span>Report Management </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled">
		        	<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/registereduserlist">Customer Registration</a></li>
		        	<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/usedvoucherlist">Customer Booking</a></li>
						</ul>
					</li>
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-email"></em><span>Resend Mail </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled">
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/allbooking" class="waves-effect"><em class="ti-bar-chart"></em> <span> Resend Booking Confirmation SMS & Email</span> </a></li>
							<!-- <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/usedresendcode" class="waves-effect"><em class="ti-agenda"></em> <span> Users who have used Resend Codes</span> </a></li>
							<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/processinginventory" class="waves-effect"><em class="ti-time"></em> <span> Processing Inventory</span> </a></li> -->
						</ul>
					</li>

					<!--	
					<li class="has_sub">
			      <a href="javascript:void(0);" class="waves-effect"><em class="ti-files"></em><span>Report Management </span> <span class="menu-arrow"></span></a>			    
		        <ul class="list-unstyled">
		        	<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/usedvoucherlist">Customer Registration & Booking Report</a></li>
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventoryreport">Inventory Report</a></li>
							<li ><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/inventorydashboard">Inventory Dashboard</a></li>
						</ul>
					</li>
					<li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/resendcode" class="waves-effect"><em class="ti-write"></em><span> Resend Code</span></a></li>	 -->				
					<!--  -->
				<?php } ?>
				</ul>	
       	<div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
      
    </div>
  </div>

      
<!-- Left Sidebar End --> 