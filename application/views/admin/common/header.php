<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500&display=swap" rel="stylesheet">
    <title><?php echo $title;?></title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css"/>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css">
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap3-typeahead.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
  </head>
  <body class="fixed-left">
    <style type="text/css">
      .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo base_url(); ?>assets/img/loader.gif') 50% 50% no-repeat rgb(249,249,249);
      }
    </style>
    <!-- Begin page -->
    <div id="wrapper">
      <!-- Top Bar Start -->
      <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
          <div class="text-center">
              <a style="font-size:15px;" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard" class="logo"><em class="icon-magnet icon-c-logo"></em><?php if($this->session->userdata('name')){echo "SA";} ?></a>
            </div>
          </div>    
        
          <div class="navbar navbar-default" style="background-color:#5c5e5f" role="navigation" >
            <div class="container">
              <div class="">
                
                <ul class="nav navbar-nav navbar-right pull-right">
                  <li class="hidden-xs">
                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><em class="icon-size-fullscreen"></em></a>
                  </li>
                  <li class="dropdown top-menu-item-xs">
                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="<?php echo base_url();?>assets/img/businessman_318-72886.jpg" alt="user-img" class="img-circle"> <?php echo $this->session->userdata('name'); ?></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/changePassword"><em style="padding-right: 9px;" class="fa fa-lock"></em>Change Password</a></li>
                      <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/logout"><em class="ti-power-off m-r-10 text-danger"></em> Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <!--/.nav-collapse -->
            </div>
          </div>
        </div>
        <!-- Top Bar End -->
      <div class="loader"></div>
      <style>
        .dt-buttons.btn-group {
          display: none;
        }
      </style> 