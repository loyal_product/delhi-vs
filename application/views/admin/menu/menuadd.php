<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Menu</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Create Menu</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <?php
          if($this->session->flashdata('msgshow')) {
          $message = $this->session->flashdata('msgshow');
        ?>
         <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
        <?php   }    ?>
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Menu</strong></h4>
            <div class="row">
              <form  id="MenuFrom" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/menuinsert" method="post" enctype="multipart/form-data">
                
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Menu</label>
                      <input type="text" class="form-control" name="menuname" id="menuname"  placeholder="Menu Name" >
                    </div> 
                  </div>                    
                  <div class="form-group col-md-6">
                    <label>&nbsp;</label></br>
                    <input type="text" class="form-control" name="menulink" id="menulink"  placeholder="Menu Link" readonly="readonly">
                  </div>                   
                </div>
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Status</label>
                      <select name="status" id="status" class="form-control">
                        <option value="">--Please Select--</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                      </select>
                    </div> 
                  </div>
                  <div class="form-group col-md-6">
                    <label>Sort Order</label></br>
                    <input type="text" class="form-control" name="sort_order" id="sort_order"  placeholder="Sort Order" >
                  </div>                  
                </div>
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#menuname").keyup(function() {
      var str = $(this).val();
      str = str.replace(/\s+/g, '-').toLowerCase();
      $('#menulink').val(str);
      //console.log(str);
    });

    $('#MenuFrom').on('submit', function (e) {
      e.preventDefault();              
      var noError    = true;
      var menuname   = $("#menuname").val();
      var status     = $("#status").val();

      if(menuname == "") {
        $('#menuname').css('border','1px solid red');
        noError = false;               
      } else {
        $('#menuname').css('border','1px solid #eee');
        noError = true;
      }
      if(status == "") {
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true;
      }
      if(noError == true) {
        $('#MenuFrom')[0].submit();   
      }
    });
  });
</script>