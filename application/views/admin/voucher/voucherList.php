<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
  select {
    display: none !important;
  }
  .dropdown-select {
    background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#40FFFFFF', endColorstr='#00FFFFFF', GradientType=0);
    background-color: #fff;
    border-radius: 6px;
    border: solid 1px #eee;
    box-shadow: 0px 2px 5px 0px rgba(155, 155, 155, 0.5);
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    float: left;
    font-size: 14px;
    font-weight: normal;
    height: 42px;
    line-height: 23px;
    outline: none;
    padding-left: 18px;
    padding-right: 30px;
    position: relative;
    text-align: left !important;
    transition: all 0.2s ease-in-out;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    white-space: nowrap;
    width: auto;
  }
  .dropdown-select:focus {
    background-color: #fff;
  }
  .dropdown-select:hover {
    background-color: #fff;
  }
  .dropdown-select:active,
  .dropdown-select.open {
    background-color: #fff !important;
    border-color: #bbb;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.05) inset;
  }
  .dropdown-select:after {
    height: 0;
    width: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-top: 4px solid #777;
    -webkit-transform: origin(50% 20%);
    transform: origin(50% 20%);
    transition: all 0.125s ease-in-out;
    content: '';
    display: block;
    margin-top: -2px;
    pointer-events: none;
    position: absolute;
    right: 10px;
    top: 50%;
  }
  .dropdown-select.open:after {
    -webkit-transform: rotate(-180deg);
    transform: rotate(-180deg);
  }
  .dropdown-select.open .list {
    -webkit-transform: scale(1);
    transform: scale(1);
    opacity: 1;
    pointer-events: auto;
  }
  .dropdown-select.open .option {
    cursor: pointer;
  }
  .dropdown-select.wide {
    width: 100%;
  }
  .dropdown-select.wide .list {
    left: 0 !important;
    right: 0 !important;
  }
  .dropdown-select .list {
    box-sizing: border-box;
    transition: all 0.15s cubic-bezier(0.25, 0, 0.25, 1.75), opacity 0.1s linear;
    -webkit-transform: scale(0.75);
    transform: scale(0.75);
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.09);
    background-color: #fff;
    border-radius: 6px;
    margin-top: 4px;
    padding: 3px 0;
    opacity: 0;
    overflow: hidden;
    pointer-events: none;
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 999;
    max-height: 250px;
    border: 1px solid #ddd;
  }
  .dropdown-select .list:hover .option:not(:hover) {
    background-color: transparent !important;
  }
  .dropdown-select .dd-search{
    overflow:hidden;
    display:flex;
    align-items:center;
    justify-content:center;
    margin:0.5rem;
  }
  .dropdown-select .dd-searchbox{
    width:90%;
    padding:0.5rem;
    border:1px solid #999;
    border-color:#999;
    border-radius:4px;
    outline:none;
  }
  .dropdown-select .dd-searchbox:focus{
    border-color:#12CBC4;
  }
  .dropdown-select .list ul {
    padding: 0;
  }
  .dropdown-select .option {
    cursor: default;
    font-weight: 400;
    line-height: 40px;
    outline: none;
    padding-left: 18px;
    padding-right: 29px;
    text-align: left;
    transition: all 0.2s;
    list-style: none;
  }
  .dropdown-select .option:hover,
  .dropdown-select .option:focus {
    background-color: #f6f6f6 !important;
  }
  .dropdown-select .option.selected {
    font-weight: 600;
    color: #12cbc4;
  }
  .dropdown-select .option.selected:focus {
    background: #f6f6f6;
  }
  .dropdown-select a {
    color: #aaa;
    text-decoration: none;
    transition: all 0.2s ease-in-out;
  }
  .dropdown-select a:hover {
    color: #666;
  }
</style>
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Voucher List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Voucher List</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12"> 
        <?php if($this->session->flashdata('voucher')) {
          $message = $this->session->flashdata('voucher'); ?>
          <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
        <?php } ?>
        <div id="msgshow"></div>
          <div class="col-md-6" style="border: 1px solid #ccc;margin-bottom: 5px;">
              <form method="post" id="searchbymobile" class="search_order_input" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/searchbymobile'?>">
                
               <div class="row" style="padding-bottom: 5px;background-color: #fff;">
                <div class="col-md-8 col-xs-12">
                  <label>Search By Mobile</label>
                  <input type="text" name="voucher_mob" class="form-control" id="voucher_mob" value="<?php if(isset($voucher_mob)){echo $voucher_mob;} ?>" placeholder="Mobile">
                </div>
                 <div class="col-md-4 col-xs-12" style="margin-top: 25px;">
                  <input type="submit" name="search" class="btn btn-info" id="search" value="Search">
                </div>
              </div> 
            </form>
          </div>
          <div class="col-md-6" style="border: 1px solid #ccc;margin-bottom: 5px;border-left:0px;">
            <form method="post" id="searchbyvouchercode" class="search_order_input"  action="<?php echo base_url().'site/core/micro/site/lib/controller/type/searchbyvoucher'?>">
              
               <div class="row" style="padding-bottom: 5px;background-color: #fff;">
                <div class="col-md-8 col-xs-12">
                  <label>Search By Voucher Code</label>
                  <input type="text" name="voucher_code" class="form-control" id="voucher_code" value="<?php if(isset($voucher_code)){echo $voucher_code;} ?>" placeholder="Voucher Code">
                </div>
                 <div class="col-md-4 col-xs-12" style="margin-top: 25px;">
                  <input type="submit" name="search" class="btn btn-info" value="Search">
                </div>
              </div> 
            </form>
          </div>          
        </div>        
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Voucher List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherRegistration" style="position: absolute;right: 31px;" class="btn btn-default">Add Voucher</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherupdatedate" style="position: absolute;right: 160px;min-width: 119px;" class="btn btn-primary">Update Date</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/vouchercsvupload" style="position: absolute;right: 285px;" class="btn btn-default">Upload Voucher as CSV</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/unblockunusedvoucherview" style="position: absolute;right: 483px;" class="btn btn-default">Unblock/Unused Voucher</a>
            </h4>
            <?php if(isset($voucherList) && count($voucherList)>0){ ?>
              <div style="height:30px;"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete All</button></div>
            <?php } ?>
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Voucher List">
                    <thead>
                      <tr>
                        <th scope="col" class='collapsable'><input type="checkbox" class="selectall"/></th>
                        <th scope="col" class='collapsable'>S.No</th>
                        <th scope="col" class='collapsable'>Product Name</th>
                        <th scope="col" class='collapsable'>Voucher Code</th>
                        <th scope="col" class='collapsable'>Start Date</th>
                        <th scope="col" class='collapsable'>Expiry Date</th>
                        <th scope="col" class='collapsable'>Last Date to Book</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                  <tbody>
                   <?php 
                    if(isset($voucherList) && !empty($voucherList) ){
                      $i= $startFrom;
                      foreach($voucherList as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td><input type="checkbox" class="delete_checkbox" value="<?php echo $row->id; ?>" /></td>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row->title; ?></td>
                      <td class='collapsable'><?php echo $row->vouchercode; ?></td>
                      <td class='collapsable'><?php echo $row->startdate; ?></td>
                      <td class='collapsable'><?php echo $row->expirydate; ?></td>
                      <td class='collapsable'><?php echo $row->lastdatetobook; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editvoucherList/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deleteVoucherList/<?php echo $row->id; ?>" onclick="return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>
                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="7">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>


<script type="text/javascript">
  function create_custom_dropdowns() {
    $('#domainname').each(function (i, select) {
      if (!$(this).next().hasClass('dropdown-select')) {
        $(this).after('<div class="dropdown-select wide ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
        var dropdown = $(this).next();
        var options = $(select).find('option');
        var selected = $(this).find('option:selected');
        dropdown.find('.current').html(selected.data('display-text') || selected.text());
        options.each(function (j, o) {
          var display = $(o).data('display-text') || '';
          dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
        });
      }
    });

    $('.dropdown-select ul').before('<div class="dd-search"><input id="txtSearchValue" autocomplete="off" onkeyup="filter()" class="dd-searchbox" type="text"></div>');
  }

  // Event listeners

  // Open/close
  $(document).on('click', '.dropdown-select', function (event) {
    if($(event.target).hasClass('dd-searchbox')){
        return;
    }
    $('.dropdown-select').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
      $(this).find('.option').attr('tabindex', 0);
      $(this).find('.selected').focus();
    } else {
      $(this).find('.option').removeAttr('tabindex');
      $(this).focus();
    }
  });

  // Close when clicking outside
  $(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown-select').length === 0) {
        $('.dropdown-select').removeClass('open');
        $('.dropdown-select .option').removeAttr('tabindex');
    }
    event.stopPropagation();
  });

  function filter(){
    var valThis = $('#txtSearchValue').val();
    $('.dropdown-select ul > li').each(function(){
     var text = $(this).text();
        (text.toLowerCase().indexOf(valThis.toLowerCase()) > -1) ? $(this).show() : $(this).hide();         
   });
  };
  // Search

  // Option click
  $(document).on('click', '.dropdown-select .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown-select').find('.current').text(text);
    $(this).closest('.dropdown-select').prev('select').val($(this).data('value')).trigger('change');
  });

  // Keyboard events
  $(document).on('keydown', '.dropdown-select', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    //if (event.keyCode == 32 || event.keyCode == 13) {
    if (event.keyCode == 13) {
      if ($(this).hasClass('open')) {
        focused_option.trigger('click');
      } else {
        $(this).trigger('click');
      }
      return false;
      // Down
    } else if (event.keyCode == 40) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        focused_option.next().focus();
      }
      return false;
      // Up
    } else if (event.keyCode == 38) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
        focused_option.prev().focus();
      }
      return false;
      // Esc
    } else if (event.keyCode == 27) {
      if ($(this).hasClass('open')) {
        $(this).trigger('click');
      }
      return false;
    }
  });

  $(document).ready(function(){
    create_custom_dropdowns();
    $( "#domainname" ).change(function() {
      var domainname = $("#domainname").val();
      $.ajax({
        type: "GET",
        url: '<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherList?domainname='+domainname,
        data: {domainname:domainname},
        processData: false,
        contentType: false,
        dataType: 'json',
        beforeSend: function () {
          $('.loader1').removeClass('hide');
        },
        success: function (data) {          
          $('.loader1').addClass('hide');
          $('.datashow').html('');
          $('.datashow').html(data);
        }
      });
    });

    $('#searchbymobile').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var voucher_mob   = $("#voucher_mob").val();
      if(voucher_mob == ""){
        $('#voucher_mob').css('border','1px solid red');
        noError = false;               
      } else {
        $('#voucher_mob').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#searchbymobile')[0].submit();   
      }
    });

    $('#searchbyvouchercode').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var voucher_code  = $("#voucher_code").val();
      if(voucher_code == ""){
        $('#voucher_code').css('border','1px solid red');
        noError = false;               
      } else {
        $('#voucher_code').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#searchbyvouchercode')[0].submit();   
      }
    });

  });
</script>
<script>
  $(document).ready(function(){
    $('.selectall').change(function () {
      if ($(this).prop('checked')) {
        $('input').prop('checked', true);
      } else {
        $('input').prop('checked', false);
      }
    });
    $('.selectall').trigger('change');

   $('#delete_all').click(function(){
    var checkbox = $('.delete_checkbox:checked');
    if(checkbox.length > 0) {
      if (confirm('Are you sure?')) {
        var checkbox_value = [];
        $(checkbox).each(function(){
          checkbox_value.push($(this).val());
        });
        $.ajax({
          url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/multiplvoucheredelete",
          method:"POST",
          data:{checkbox_value:checkbox_value},
          success:function() {
            $('#msgshow').css('height','40px');
            $('#msgshow').html('<span class="alert alert-success">Multiple delete has been successfully.</span>');
            $("#datatable-buttons1").load(" #datatable-buttons1");
          }
        });
      }
    } else {
     alert('Please select atleast one records');
    }
   });

  });
</script>