<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Voucher Update Date</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Voucher Update Date</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Whole Voucher Update Date</strong></h4>
            <div class="row">
              <form id="voucherupdatedate" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherdateupdate" method="post" enctype="multipart/form-data">
                <?php
                  if($this->session->flashdata('voucherdateUp')) {
                    $message = $this->session->flashdata('voucherdateUp');
                 ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>               
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control" name="startdate" id="startdate"  placeholder="Start Date" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Expiry Date</label>
                        <input type="text" class="form-control" name="expirydate" id="expirydate" autocomplete="off"  placeholder="Expiry Date" readonly="readonly">
                      </div>
                    </div> 
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>                 
                  </div>                   
              </form>         
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Batch Wise Voucher Update Date</strong></h4>
            <div class="row">
              <form id="batchwisevoucherupdatedate" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/batchwisevoucherdateupdate" method="post" enctype="multipart/form-data">
                <?php
                  if($this->session->flashdata('voucherdateUp2')) {
                    $message = $this->session->flashdata('voucherdateUp2');
                 ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>               
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Choose Batch Type</label>
                        <select name="batchtype" id="batchtype" class="form-control">
                          <option value="">--Please Select--</option>
                          <?php if(isset($batchType)){
                            foreach ($batchType as $key => $value) { ?>
                            <option value="<?php echo $value['batchname'];?>"><?php echo $value['batchname'];?></option>
                            <?php } } ?>
                        </select>
                      </div>
                    </div>               
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control" name="strtdate" id="strtdate"  placeholder="Start Date" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Expiry Date</label>
                        <input type="text" class="form-control" name="expdate" id="expdate" autocomplete="off"  placeholder="Expiry Date" readonly="readonly">
                      </div>
                    </div> 
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </div>                 
                  </div>                   
              </form>         
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Voucher Code Update date</strong></h4>
            <div class="row">
              <form id="voucherwiseupdatedate" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherwisedateupdate" method="post" enctype="multipart/form-data">
                <?php
                  if($this->session->flashdata('voucherdateUp1')) {
                    $message = $this->session->flashdata('voucherdateUp1');
                 ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                <?php } ?>
                  <style type="text/css">
                    textarea.form-control {
                      min-height: 150px !important;
                    }
                  </style>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Enter Voucher Code<span >(Comma seprated without space)</span></label>
                      <textarea class="form-control" name="vouchercode" id="vouchercode"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" class="form-control" name="strtdate1" id="strtdate1"  placeholder="Start Date" readonly="readonly">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Expiry Date</label>
                        <input type="text" class="form-control" name="expdate1" id="expdate1" autocomplete="off"  placeholder="Expiry Date" readonly="readonly" >
                      </div>
                    </div>
                  </div>                
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>                  
              </form>         
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Last Date to Book</strong></h4>
            <div class="row">
              <form id="lastdatetobookform" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/voucherlastdatetobook" method="post" enctype="multipart/form-data">
                <?php
                  if($this->session->flashdata('lastdatetobook')) {
                    $message = $this->session->flashdata('lastdatetobook');
                 ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php } ?>
                  
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Last Date to Book</label>
                        <input type="text" class="form-control" name="lastdatetobook" id="lastdatetobook" autocomplete="off"  placeholder="Last Date to Book" readonly="readonly">
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>                  
              </form>         
            </div>
          </div>
        </div>
      </div>

    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
    $('#batchtype').change(function() {
    var batchtype = $('#batchtype').val();
    if(batchtype != '') {
      $.ajax({
        url:"<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/getbatchdates",
        method:"POST",
        data:{batchtype:batchtype},
        dataType: "json",
        beforeSend: function () {
          $('.loader1').removeClass('hide');
        },
        success:function(data) {
            $('.loader1').addClass('hide');
          console.log(data);
          var stdt = JSON.stringify(data.strtdate);
          var nstrtdt = stdt.replace(/\"/g,'');
          var exdt = JSON.stringify(data.expdate);
          var nexpdt = exdt.replace(/\"/g,'');
          $('#strtdate').val(nstrtdt);
          $('#expdate').val(nexpdt);
        }
      });
    } else {
      $('#strtdate').val(' ');
      $('#expdate').val(' ');
    }
  });
  
  $(function () {
    $("#startdate").datepicker({            
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() + 1);
          $("#expirydate").datepicker("option", "minDate", dt);
      }
    });
    $("#expirydate").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() - 1);
          $("#startdate").datepicker("option", "maxDate", dt);
      }
    });
    $("#strtdate").datepicker({            
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() + 1);
          $("#expdate").datepicker("option", "minDate", dt);
      }
    });
    $("#expdate").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() - 1);
          $("#strtdate").datepicker("option", "maxDate", dt);
      }
    });
    $("#strtdate1").datepicker({            
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() + 1);
          $("#expdate1").datepicker("option", "minDate", dt);
      }
    });
    $("#expdate1").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() - 1);
          $("#strtdate1").datepicker("option", "maxDate", dt);
      }
    });
  });  
  $(function() {
    $("#lastdatetobook").datepicker({ minDate: 0});
  });

  $(document).ready(function(){
    $('#voucherupdatedate').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var startdate     = $("#startdate").val();
      var expirydate    = $("#expirydate").val();

      if(startdate == ""){
        $('#startdate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#startdate').css('border','1px solid #eee');
        noError = true; 
      }
      if(expirydate == ""){
        $('#expirydate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#expirydate').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#voucherupdatedate')[0].submit();   
      }
    });

    $('#batchwisevoucherupdatedate').on('submit', function (e) {
      e.preventDefault();              
      var noError    = true;
      var batchtype  = $("#batchtype").val(); 
      var strtdate   = $("#strtdate").val();
      var expdate    = $("#expdate").val();

      if(batchtype == ""){
        $('#batchtype').css('border','1px solid red');
        noError = false;               
      } else {
        $('#batchtype').css('border','1px solid #eee');
        noError = true; 
      }
      if(strtdate == ""){
        $('#strtdate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#strtdate').css('border','1px solid #eee');
        noError = true; 
      }
      if(expdate == ""){
        $('#expdate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#expdate').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#batchwisevoucherupdatedate')[0].submit();   
      }
    });

    $('#lastdatetobookform').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var lastdatetobook   = $("#lastdatetobook").val(); 

      if(lastdatetobook == ""){
        $('#lastdatetobook').css('border','1px solid red');
        noError = false;               
      } else {
        $('#lastdatetobook').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#lastdatetobookform')[0].submit();   
      }
    });
  });
</script>