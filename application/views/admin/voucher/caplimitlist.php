<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Cap Limit</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Cap Limit</a></li>
          </ol>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Cap Limit</strong>
              <?php 
              if(empty($caplimitdata)) { ?>
                <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/caplimitadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Cap Limit</a>
              <?php } ?>
            </h4>           

            <div class="row">
              <div class="col-md-12">
                <?php
                    if($this->session->flashdata('caplimitmsg')) {
                    $message = $this->session->flashdata('caplimitmsg');
                  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                <div class="table-responsive">
                  <p>If cap limit is 0 then same mobile and email-id registered unlimited.</p>
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Cap Limit">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Cap Limit</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($caplimitdata) && !empty($caplimitdata) ){
                      foreach($caplimitdata as $key=>$row){
                    ?>

                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['caplimit']; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editcaplimit/<?php echo $row['id']; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>
                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deletecaplimit/<?php echo $row['id']; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                      </td>
                      
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>