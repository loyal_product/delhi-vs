<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Cap Limit</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Cap Limit</a></li>
          </ol>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box"> 
            <div class="row">
              <form id="caplimit" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/caplimitInsert" method="post" enctype="multipart/form-data">
                  <?php
                    if($this->session->flashdata('setlimit')) {
                      $message = $this->session->flashdata('setlimit');
                   ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                   <?php } ?>
                  
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Cap Limit</label>
                        <input type="text" class="form-control" name="setcaplimit" id="setcaplimit" placeholder="Cap Limit">
                      </div>
                    </div>
                  </div>                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form> 
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  $("#setcaplimit").keyup(function () {
    var val = $(this).val();
    if(isNaN(val)) {
      val = val.replace(/[^0-9\.]/g,'');
      if(val.split('.').length>2) 
      val =val.replace(/\.+$/,"");
    }
    $(this).val(val);       
  });
</script>