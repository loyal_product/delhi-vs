<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
  select {
    display: none !important;
  }
  .dropdown-select {
    background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.25) 0%, rgba(255, 255, 255, 0) 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#40FFFFFF', endColorstr='#00FFFFFF', GradientType=0);
    background-color: #fff;
    border-radius: 6px;
    border: solid 1px #eee;
    box-shadow: 0px 2px 5px 0px rgba(155, 155, 155, 0.5);
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    float: left;
    font-size: 14px;
    font-weight: normal;
    height: 42px;
    line-height: 23px;
    outline: none;
    padding-left: 18px;
    padding-right: 30px;
    position: relative;
    text-align: left !important;
    transition: all 0.2s ease-in-out;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    white-space: nowrap;
    width: auto;
  }
  .dropdown-select:focus {
    background-color: #fff;
  }
  .dropdown-select:hover {
    background-color: #fff;
  }
  .dropdown-select:active,
  .dropdown-select.open {
    background-color: #fff !important;
    border-color: #bbb;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.05) inset;
  }
  .dropdown-select:after {
    height: 0;
    width: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-top: 4px solid #777;
    -webkit-transform: origin(50% 20%);
    transform: origin(50% 20%);
    transition: all 0.125s ease-in-out;
    content: '';
    display: block;
    margin-top: -2px;
    pointer-events: none;
    position: absolute;
    right: 10px;
    top: 50%;
  }
  .dropdown-select.open:after {
    -webkit-transform: rotate(-180deg);
    transform: rotate(-180deg);
  }
  .dropdown-select.open .list {
    -webkit-transform: scale(1);
    transform: scale(1);
    opacity: 1;
    pointer-events: auto;
  }
  .dropdown-select.open .option {
    cursor: pointer;
  }
  .dropdown-select.wide {
    width: 100%;
  }
  .dropdown-select.wide .list {
    left: 0 !important;
    right: 0 !important;
  }
  .dropdown-select .list {
    box-sizing: border-box;
    transition: all 0.15s cubic-bezier(0.25, 0, 0.25, 1.75), opacity 0.1s linear;
    -webkit-transform: scale(0.75);
    transform: scale(0.75);
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.09);
    background-color: #fff;
    border-radius: 6px;
    margin-top: 4px;
    padding: 3px 0;
    opacity: 0;
    pointer-events: none;
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 999;
    max-height: 250px;
    overflow: hidden;
    border: 1px solid #ddd;
  }
  .dropdown-select .list:hover .option:not(:hover) {
    background-color: transparent !important;
  }
  .dropdown-select .dd-search{
    overflow:hidden;
    display:flex;
    align-items:center;
    justify-content:center;
    margin:0.5rem;
  }
  .dropdown-select .dd-searchbox{
    width:90%;
    padding:0.5rem;
    border:1px solid #999;
    border-color:#999;
    border-radius:4px;
    outline:none;
  }
  .dropdown-select .dd-searchbox:focus{
    border-color:#12CBC4;
  }
  .dropdown-select .list ul {
    padding: 0;
  }
  .dropdown-select .option {
    cursor: default;
    font-weight: 400;
    line-height: 40px;
    outline: none;
    padding-left: 18px;
    padding-right: 29px;
    text-align: left;
    transition: all 0.2s;
    list-style: none;
  }
  .dropdown-select .option:hover,
  .dropdown-select .option:focus {
    background-color: #f6f6f6 !important;
  }
  .dropdown-select .option.selected {
    font-weight: 600;
    color: #12cbc4;
  }
  .dropdown-select .option.selected:focus {
    background: #f6f6f6;
  }
  .dropdown-select a {
    color: #aaa;
    text-decoration: none;
    transition: all 0.2s ease-in-out;
  }
  .dropdown-select a:hover {
    color: #666;
  }
</style>
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Voucher</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Edit Voucher</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
          <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Voucher</strong></h4>
          <div class="row">
            <?php
              if($this->session->flashdata('voucher')) {
              $message = $this->session->flashdata('voucher');
            ?>
            <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
            <?php } ?>
            <?php 
              if(isset($editVoucherList)){
                foreach($editVoucherList as $key=>$row) {
                $expirydate = date("Y-m-d", strtotime($row->expirydate));
                $startdate = date("Y-m-d", strtotime($row->startdate));
                $lastdatetobook = date("Y-m-d", strtotime($row->lastdatetobook));
            ?>
              <form id="voucheredit" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/updateVoucher/<?php echo $row->id; ?>" method="post" enctype="multipart/form-data">
                
                <div class="col-md-12">                    
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Select Domain</label>
                        <select class="form-control" name="productname" id="productname">
                          <option value="">--Please Select--</option>
                          <?php if(isset($productList)) { 
                            foreach ($productList as $key => $value) { ?>
                              <option value="<?php echo $value['id'];?>" <?php if ($value['id']==$row->product_id) { echo 'selected="selected"';}?>><?php echo $value['title'].' - '. $value['sub_prod_name'];?></option>
                          <?php } } ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Voucher Code</label>
                      <input type="text" class="form-control" value="<?php echo $row->vouchercode; ?>" name="vouchercode" id="vouchercode"  placeholder="Voucher Code" style="text-transform: uppercase;">
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Start Date</label>
                      <input type="text" class="form-control" value="<?php echo $row->startdate; ?>" name="startdate" id="startdate"  placeholder="Start Date" readonly="readonly" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Expiry Date</label>
                      <input type="text" class="form-control" value="<?php echo $row->expirydate; ?>" name="expirydate" id="expirydate"  autocomplete="off"  placeholder="Expiry Date" readonly="readonly" >
                    </div>
                  </div>
                </div>
                <div class="col-md-12">                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Last Date to Book</label>
                      <input type="text" class="form-control" value="<?php echo $row->lastdatetobook; ?>" name="lastdatetobook" id="lastdatetobook" autocomplete="off"  placeholder="Last Date to Book" readonly="readonly" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Enter Batch Name</label>
                      <input type="text" class="form-control" name="batchname" id="batchname" value="<?php echo $row->batchname; ?>" autocomplete="off"  placeholder="Enter Batch Name" >
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <button type="submit" class="btn btn-default">Submit</button>
                    </div>
                  </div>
                </div>
              </form>  

          <?php } } ?>       
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>
<script type="text/javascript">
  $(function () {
    $("#startdate").datepicker({            
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() + 1);
          $("#expirydate").datepicker("option", "minDate", dt);
      }
    });
    $("#expirydate").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
          var dt = new Date(selected);
          dt.setDate(dt.getDate() - 1);
          $("#startdate").datepicker("option", "maxDate", dt);
      }
    });
  });
  $(function() {
    $("#lastdatetobook").datepicker({ minDate: 0});
  });

  function create_custom_dropdowns() {
    $('#productname').each(function (i, select) {
      if (!$(this).next().hasClass('dropdown-select')) {
        $(this).after('<div class="dropdown-select wide ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current"></span><div class="list"><ul></ul></div></div>');
        var dropdown = $(this).next();
        var options = $(select).find('option');
        var selected = $(this).find('option:selected');
        dropdown.find('.current').html(selected.data('display-text') || selected.text());
        options.each(function (j, o) {
          var display = $(o).data('display-text') || '';
          dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
        });
      }
    });

    $('.dropdown-select ul').before('<div class="dd-search"><input id="txtSearchValue" autocomplete="off" onkeyup="filter()" class="dd-searchbox" type="text"></div>');
  }

  // Event listeners

  // Open/close
  $(document).on('click', '.dropdown-select', function (event) {
    if($(event.target).hasClass('dd-searchbox')){
        return;
    }
    $('.dropdown-select').not($(this)).removeClass('open');
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
      $(this).find('.option').attr('tabindex', 0);
      $(this).find('.selected').focus();
    } else {
      $(this).find('.option').removeAttr('tabindex');
      $(this).focus();
    }
  });

  // Close when clicking outside
  $(document).on('click', function (event) {
    if ($(event.target).closest('.dropdown-select').length === 0) {
        $('.dropdown-select').removeClass('open');
        $('.dropdown-select .option').removeAttr('tabindex');
    }
    event.stopPropagation();
  });

  function filter(){
    var valThis = $('#txtSearchValue').val();
    $('.dropdown-select ul > li').each(function(){
     var text = $(this).text();
        (text.toLowerCase().indexOf(valThis.toLowerCase()) > -1) ? $(this).show() : $(this).hide();         
   });
  };
  // Search

  // Option click
  $(document).on('click', '.dropdown-select .option', function (event) {
    $(this).closest('.list').find('.selected').removeClass('selected');
    $(this).addClass('selected');
    var text = $(this).data('display-text') || $(this).text();
    $(this).closest('.dropdown-select').find('.current').text(text);
    $(this).closest('.dropdown-select').prev('select').val($(this).data('value')).trigger('change');
  });

  // Keyboard events
  $(document).on('keydown', '.dropdown-select', function (event) {
    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
    // Space or Enter
    //if (event.keyCode == 32 || event.keyCode == 13) {
    if (event.keyCode == 13) {
      if ($(this).hasClass('open')) {
        focused_option.trigger('click');
      } else {
        $(this).trigger('click');
      }
      return false;
      // Down
    } else if (event.keyCode == 40) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        focused_option.next().focus();
      }
      return false;
      // Up
    } else if (event.keyCode == 38) {
      if (!$(this).hasClass('open')) {
        $(this).trigger('click');
      } else {
        var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
        focused_option.prev().focus();
      }
      return false;
      // Esc
    } else if (event.keyCode == 27) {
      if ($(this).hasClass('open')) {
        $(this).trigger('click');
      }
      return false;
    }
  });

  $(document).ready(function() {
    create_custom_dropdowns();

    $('#voucheredit').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var productname   = $('#productname').val();
      var vouchercode   = $("#vouchercode").val(); 
      var startdate     = $("#startdate").val();
      var expirydate    = $("#expirydate").val();
      var lastdatetobook = $("#lastdatetobook").val();

      if(productname == ""){
        $('#productname').css('border','1px solid red');
        noError = false;               
      } else {
        $('#productname').css('border','1px solid #eee');
        noError = true; 
      }
      if(vouchercode == ""){
        $('#vouchercode').css('border','1px solid red');
        noError = false;               
      } else {
        $('#vouchercode').css('border','1px solid #eee');
        noError = true; 
      }
      if(startdate == ""){
        $('#startdate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#startdate').css('border','1px solid #eee');
        noError = true; 
      }
      if(expirydate == ""){
        $('#expirydate').css('border','1px solid red');
        noError = false;               
      } else {
        $('#expirydate').css('border','1px solid #eee');
        noError = true; 
      }
      if(lastdatetobook == ""){
        $('#lastdatetobook').css('border','1px solid red');
        noError = false;               
      } else {
        $('#lastdatetobook').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#voucheredit')[0].submit();
      }
    });
  });
</script>