<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Unblock And Unused Voucher</h4>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a></li>
            <li class="active"><a>Unblock And Unused Voucher</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Unblock And Unused Voucher</strong></h4>
            <div class="row">
              <form id="unblockunusedvoucher" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/unblockunusedvoucherupdate" method="post" enctype="multipart/form-data">
                <?php
                  if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');
                 ?>
                 <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Enter Voucher Code</label>
                        <input type="text" class="form-control" name="vcode" id="vcode"  placeholder="Enter Voucher Code">
                      </div>
                    </div> 
                  </div> 
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>                   
              </form>         
            </div>
          </div>
        </div>
      </div>

    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#unblockunusedvoucher').on('submit', function (e) {
      e.preventDefault();              
      var noError       = true;
      var vcode     = $("#vcode").val();

      if(vcode == ""){
        $('#vcode').css('border','1px solid red');
        noError = false;               
      } else {
        $('#vcode').css('border','1px solid #eee');
        noError = true; 
      }
      
      if(noError){
        $('#unblockunusedvoucher')[0].submit();   
      }
    });    
  });
</script>