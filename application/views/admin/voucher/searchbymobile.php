<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Search By Mobile</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Search By Mobile</a>
            </li>
          </ol>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Search By Mobile">
                    <thead>
                      <tr>
                        <th scope="col" class='collapsable'>S.No</th>
                        <th scope="col" class='collapsable'>Mobile Number</th>
                        <th scope="col" class='collapsable'>Date & Time of Registration</th>
                        <th scope="col" class='collapsable'>Name</th>
                        <th scope="col" class='collapsable'>Email ID</th>
                        <th scope="col" class='collapsable'>Voucher Code used</th>
                      </tr>
                    </thead>
                  <tbody>
                  <?php 
                    if((count($voucherList)>0) && !empty($voucherList) ){
                      foreach($voucherList as $key=>$row){
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row->mobile; ?></td>
                      <td class='collapsable'><?php echo $row->registereddate; ?></td>
                      <td class='collapsable'><?php echo $row->name; ?></td>
                      <td class='collapsable'><?php echo $row->email; ?></td>
                      <td class='collapsable'><?php echo $row->vouchercode; ?></td>
                    </tr>
                    <?php  } } else { ?>
                      <tr><td  colspan="6">No Record Found!!</td></tr>
                    <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
