<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo $title;?></title>
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url();?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        
	<!-- <script>
		window.location.hash="no-back-button";
		window.location.hash="Again-No-back-button";//again because google chrome don't insert first hash into history
		window.onhashchange=function(){window.location.hash="no-back-button";}
	</script> -->
</head>
<body onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">

<header>
	<div class="container clearfix">
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-3">&nbsp;</div>
				<div class="col-lg-9" style="margin-top:60px;">
					<span style="font-size:18px;color:#00AEEF; font-weight:bold;">You have successfully <span style="font-size:16px;">Logged out of Control Panel.</span>
					<a href="<?php echo base_url();?>admin" style="font-size:22px; color:#ec971f !important; font-weight:bold;">Login Again</a></span>
				</div>
			</div>
		</div>
	</div>
	</header>
	<script type="text/javascript">
		$(document).ready(function(){
			setTimeout(function(){ 
				<?php header('Location: '.base_url().'admin'); ?>
			}, 1000);
		});
	</script>
  </body>
</html>
