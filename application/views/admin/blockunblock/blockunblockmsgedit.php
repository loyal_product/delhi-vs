<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Mobile or Email or Voucher or IP Address for Block</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Edit Mobile or Email or Voucher or IP Address for Block</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Mobile or Email or Voucher or IP Address for Block</strong></h4>
            <div class="row">
              <?php if(isset($editBlockList)){
                foreach($editBlockList as $key=>$row){ ?>
              <form id="blockMsgEditForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockmsgupdate/<?php echo $row->id; ?>" method="POST" enctype="multipart/form-data">
                <?php if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Message Type</label>
                        <select class="form-control" id="mtype" name="mtype">
                          <option value="">--Please Select--</option>
                          <option value="mobile" <?php if ($row->type=='mobile') { echo 'selected="selected"';}?>>Mobile</option>
                          <option value="vouchercode" <?php if ($row->type=='vouchercode') { echo 'selected="selected"';}?>>Voucher Code</option>
                          <option value="ipaddress" <?php if ($row->type=='ipaddress') { echo 'selected="selected"';}?>>IP Address</option>
                          <option value="emailid" <?php if ($row->type=='emailid') { echo 'selected="selected"';}?>>Email Id</option>
                        </select>           
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Message</label>                        
                        <input type="text" class="form-control" id="blockfieldmsg" name="blockfieldmsg" placeholder="message" value="<?php echo $row->message; ?>" >                    
                      </div>
                    </div>
                  </div>               
                  
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form> 
              <?php } } ?>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#blockMsgEditForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var blockfieldmsg      = $("#blockfieldmsg").val(); 
      var mtype          = $("#mtype").val();

      if(blockfieldmsg == ""){
        $('#blockfieldmsg').css('border','1px solid red');
        noError = false;               
      } else {
        $('#blockfieldmsg').css('border','1px solid #eee');
        noError = true; 
      }
      if(mtype == ""){
        $('#mtype').css('border','1px solid red');
        noError = false;               
      } else {
        $('#mtype').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#blockMsgEditForm')[0].submit();   
      }
    });
  }); 
</script>