<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Block Message</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Create Block Message</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Block Message</strong></h4>
            <div class="row">
              <form id="blockMsgAddForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockmsginsert" method="POST" enctype="multipart/form-data">
                <?php if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                   
                  <div class="col-md-12">                    
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Message Type</label>
                        <select class="form-control" id="mtype" name="mtype">
                          <option value="">--Please Select--</option>
                          <option value="mobile">Mobile</option>
                          <option value="vouchercode">Voucher Code</option>
                          <option value="ipaddress">IP Address</option>
                          <option value="emailid">Email Id</option>
                        </select>           
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Message</label>                        
                        <input type="text" class="form-control" id="blockfieldmsg" name="blockfieldmsg" placeholder="message" >                    
                      </div>
                    </div>
                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#blockMsgAddForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var blockfieldmsg      = $("#blockfieldmsg").val(); 
      var mtype          = $("#mtype").val();

      if(blockfieldmsg == ""){
        $('#blockfieldmsg').css('border','1px solid red');
        noError = false;               
      } else {
        $('#blockfieldmsg').css('border','1px solid #eee');
        noError = true; 
      }
      if(mtype == ""){
        $('#mtype').css('border','1px solid red');
        noError = false;               
      } else {
        $('#mtype').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#blockMsgAddForm')[0].submit();   
      }

    });
  }); 
</script>