<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Block/Unblock Message List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Block/Unblock Message List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Block/Unblock Message List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockmsgadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Block/Unblock Message</a>
            </h4>
            <div class="row">
              <div class="col-md-12">
                <?php if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Block Unblock Message List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Message Type</th>
                          <th scope="col" class='collapsable'>Message</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php                     
                    if(isset($blockunblockmsgList) && !empty($blockunblockmsgList) ){
                      $i= $startFrom;
                      foreach($blockunblockmsgList as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo ucfirst($row->type); ?></td>
                      <td class='collapsable'><?php echo $row->message; ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editblockunblockmsg/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deleteblockunblockmsg/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                     </td>
                    </tr> 
                    <?php } } else { ?>
                      <tr><td  colspan="3">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                  <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>