<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->   
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Edit Mobile or Email or Voucher or IP Address for Block</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a>Edit Mobile or Email or Voucher or IP Address for Block</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Edit Mobile or Email or Voucher or IP Address for Block</strong></h4>
            <div class="row">
              <?php if(isset($editBlockList)){
                foreach($editBlockList as $key=>$row){ ?>
              <form id="blockEditForm" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockupdate/<?php echo $row->id; ?>" method="POST" enctype="multipart/form-data">
                <?php if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>                   
                   <div class="col-md-12">                    
                     <div class="col-md-6">
                      <div class="form-group">
                        <label>Mobile or Email or Voucher or IP Address for Block</label>
                        <input type="text" class="form-control" id="blockfield" name="blockfield" placeholder="Mobile or Email or Voucher or IP Address for Block" value="<?php echo $row->blockdata; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" id="status" name="status">
                          <option value="">--Please Select--</option>
                          <option value="0" <?php if ($row->status=='0') { echo 'selected="selected"';}?>>Block</option>
                          <option value="1" <?php if ($row->status=='1') { echo 'selected="selected"';}?>>Unblock</option>
                        </select>                   
                      </div>
                    </div>
                 </div>
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                      </div>
                    </div>
                  </div>
              </form> 
              <?php } } ?>        
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
  </div> <!-- content -->
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#blockEditForm').on('submit', function (e) {
      e.preventDefault();              
      var noError         = true;
      var blockfield      = $("#blockfield").val(); 
      var status          = $("#status").val();

      if(blockfield == ""){
        $('#blockfield').css('border','1px solid red');
        noError = false;               
      } else {
        $('#blockfield').css('border','1px solid #eee');
        noError = true; 
      }
      if(status == ""){
        $('#status').css('border','1px solid red');
        noError = false;               
      } else {
        $('#status').css('border','1px solid #eee');
        noError = true; 
      }
      if(noError){
        $('#blockEditForm')[0].submit();   
      }

    });
  }); 
</script>