<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Block/Unblock List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Block/Unblock List</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Block/Unblock List</strong>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockadd" style="position: absolute;right: 31px;" class="btn btn-default">Add Block/Unblock</a>
            <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/blockunblockmsglist" style="position: absolute;right: 205px;" class="btn btn-default">Block/Unblock Error Message</a>
            </h4>
            <div class="row">
              <div class="col-md-12">
                <?php if($this->session->flashdata('msgshow')) {
                    $message = $this->session->flashdata('msgshow');  ?>
                    <div class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
                  <?php } ?>
                <div class="table-responsive">
                  <table class="table table-striped table-bordered" summary="Block Unblock List">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Block/Unblock</th>
                          <th scope="col" class='collapsable'>Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php                     
                    if(isset($blockunblockList) && !empty($blockunblockList) ){
                      $i= $startFrom;
                      foreach($blockunblockList as $key=>$row){
                        $i++;
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $i; ?></td>
                      <td class='collapsable'><?php echo $row->blockdata; ?></td>
                      <td class='collapsable'><?php if($row->status=='0'){echo '<p style="color:red;"><strong>Blocked</strong></p>';} else { echo '<p style="color:green;"><strong>Unblocked</strong></p>';} ?></td>
                      <td class="custom_width">
                        <a title="Edit" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/editblockunblock/<?php echo $row->id; ?>" class="demo-edit-row btn btn-success btn-xs btn-icon"><em class="fa fa-pencil"></em></a>

                        <a title="Delete" href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/deleteblockunblock/<?php echo $row->id; ?>" onClick = "return confirm('Are you sure you want to delete?');" class="demo-delete-row btn btn-danger btn-xs btn-icon"><em class="fa fa-trash-o"></em></a>
                     </td>
                    </tr> 
                    <?php } } else { ?>
                      <tr><td  colspan="3">No Record Found!!</td></tr>
                  <?php } ?>
                  </tbody>
                </table>
               <div id="pagination">
                  <ul class="tsc_pagination">
                    <!-- Show pagination links -->
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>