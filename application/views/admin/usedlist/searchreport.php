<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Search Report</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active">
              <a>Search Report</a>
            </li>
          </ol>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Search User">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Name</th>
                          <th scope="col" class='collapsable'>Email</th>
                          <th scope="col" class='collapsable'>Mobile</th>
                          <th scope="col" class='collapsable'>Voucher Code</th>
                          <th scope="col" class='collapsable'>Amount</th>
                          <th scope="col" class='collapsable'>Voucher Expiry Date</th>
                        </tr>
                      </thead>
                  <tbody>
                   
                  </tbody>
                </table>
                
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>
