<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Registered User List</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url();?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>
            <li class="active"><a>Registered User List</a></li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php
            if($this->session->flashdata('user')) {
            $message = $this->session->flashdata('user');
          ?>
           <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <div class="col-md-12" id="importFrm" style="margin-bottom: 5px;border: 1px solid #eee;padding: 4px;">
              <label>Download By Registered Date</label>
              <form method="post" id="registeredUser" action="<?php echo base_url().'site/core/micro/site/lib/controller/type/downloadregisteredbydate'?>">
                <div class="col-md-3">
                  <input type="text" style="line-height: 15px;" class="form-control" name="filterDateTo" id="txtFrom" autocomplete="off" placeholder="dd/mm/yyyy">
                </div>
                <div class="col-md-3">
                  <input type="text" style="line-height: 15px;" class="form-control" name="filterDateFrom" id="txtTo" autocomplete="off" placeholder="dd/mm/yyyy">
                </div>
                  <div class="col-md-2">
                   <input type='submit' name='but_search' class="filer btn btn-default" value='Download as Excel'>
                </div>
              </form>
          </div>
            <div class="row">
              <div class="col-md-12"> 
                <div class="table-responsive">
                  <table id="datatable-buttons1" class="table table-striped table-bordered" summary="Recnt User">
                      <thead>
                        <tr>
                          <th scope="col" class='collapsable'>S.No</th>
                          <th scope="col" class='collapsable'>Offer Name</th>
                          <th scope="col" class='collapsable'>Name</th>
                          <th scope="col" class='collapsable'>Email</th>
                          <th scope="col" class='collapsable'>Mobile</th>
                          <th scope="col" class='collapsable'>Voucher Code</th>
                          <th scope="col" class='collapsable'>IP Address</th>
                          <th scope="col" class='collapsable'>Device Type</th>
                          <th scope="col" class='collapsable'>No. of Seconds</th>
                          <th scope="col" class='collapsable'>Date of Registration</th>
                          <th scope="col" class='collapsable'>Time of Registration</th>
                          <th scope="col" class='collapsable'>Voucher Expiry Date</th>
                        </tr>
                      </thead>
                  <tbody>
                   <?php 
                    if(isset($registeredUserList) && !empty($registeredUserList) ){
                        $i= $startFrom;
                      foreach($registeredUserList as $key=>$row){
                        $res = explode(' ',$row['registereddate']);
                    ?>
                    <tr>
                      <td class='collapsable'><?php echo $key+1; ?></td>
                      <td class='collapsable'><?php echo $row['title'];?></td>
                      <td class='collapsable'><?php echo $row['cname'];?></td>
                      <td class='collapsable'><?php echo $row['email'];?></td>
                      <td class='collapsable'><?php echo $row['mobile'];?></td>
                      <td class='collapsable'><?php echo $row['vouchercode'];?></td>
                      <td class='collapsable'><?php echo $row['ipaddress'];?></td>
                      <td class='collapsable'><?php echo $row['devicetype'];?></td>
                      <td class='collapsable'><?php echo $row['noofseconds'];?></td>
                      <td class='collapsable'><?php echo $res[0];?></td>
                      <td class='collapsable'><?php echo $res[1];?></td>
                      <td class='collapsable'><?php echo $row['expirydate'];?></td>
                                          
                    </tr>
                    <?php  } } else{  ?>
                      <tr><td  colspan="4">No Record Found!!</td> 
                        </tr>
                  <?php  }
                     ?>
                  </tbody>
                </table>
               <div id="pagination">
                    <ul class="tsc_pagination">
                    <?php 
                     if($links != ''){
                      foreach ($links as $link) {
                      echo "<li>". $link."</li>";
                      } } ?>
                    </ul>
                </div>
              </div>
              </div>    
            </div>
          </div>
        </div>
      </div>  
    </div> <!-- container -->                         
    </div> <!-- content -->
  </div>
  <script type="text/javascript">
    $(function () {
      $("#txtFrom").datepicker({            
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#txtTo").datepicker("option", "minDate", dt);
        }
      });
      $("#txtTo").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#txtFrom").datepicker("option", "maxDate", dt);
        }
      });
    });
    $(document).ready(function(){
      $('#registeredUser').on('submit', function (e) {
        e.preventDefault();              
        var noError       = true;
        var txtFrom       = $("#txtFrom").val();
        var txtTo         = $("#txtTo").val();

        if(txtFrom == ""){
          $('#txtFrom').css('border','1px solid red');
          noError = false;               
        } else {
          $('#txtFrom').css('border','1px solid #eee');
          noError = true; 
        }
        if(txtTo == ""){
          $('#txtTo').css('border','1px solid red');
          noError = false;               
        } else {
          $('#txtTo').css('border','1px solid #eee');
          noError = true; 
        }
        if(noError){
          $('#registeredUser')[0].submit();   
        }
      });
    });
  </script>
