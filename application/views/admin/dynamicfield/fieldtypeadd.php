<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Create Dynamic Form</h4>
          <ol class="breadcrumb">
            <li>
              <a href="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/dashboard">Dashboard</a>
            </li>          
            <li class="active">
              <a href="#">Create Dynamic Form</a>
            </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php if($this->session->flashdata('msgshow')) {
            $message = $this->session->flashdata('msgshow'); ?>
            <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
          <?php } ?>
          <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><strong>Create Dynamic Form</strong></h4>
            <div class="row">
              <form  id="FieldAddFroms" action="<?php echo base_url(); ?>site/core/micro/site/lib/controller/type/insertdynamicform" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                  <div class="form-group col-md-6">
                    <label>Page Name</label>
                    <select class="form-control" name="pagename" id="pagename">
                      <option value="home">Registration</option>
                      <!-- <option value="otp">OTP</option>
                      <option value="booking">Booking</option> -->
                    </select>
                  </div>                  
                </div>
                <style type="text/css">
                  div.cloningform:nth-child(even) {background: #ECECEC;}
                  div.cloningform:nth-child(odd) { background: #FFF;}
                </style>
                <div class="wrapper">
                  <div class="row cloningform" style="border-bottom: 1px solid #000;">
                    <div class="col-md-12">                  
                      <div class="form-group col-md-2" id="fieldtype">
                        <label>Field <br>Type</label>
                        <select class="form-control fieldid" name="fieldid[]" id="fieldid">
                          <option value="">Please Select</option>
                          <?php if (isset($fieldlist)) {
                            foreach ($fieldlist as $key => $val) { ?>
                            <option value="<?php echo $val['id']; ?>"><?php echo $val['fieldtype']; ?></option>
                          <?php } } ?>
                        </select>
                      </div>

                      <!-- Modal Start For Dropdown -->
                      <div class="modal fade drpdwnpopup" role="dialog" id="drpdwnpopup">
                        <div class="modal-dialog">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">For Dropdown</h4>
                            </div>
                            <div class="modal-body">
                              <label>Enter Option Value <span style="font-weight:300;font-size:12px;">(Enter comma seprated value)</span></label>
                              <textarea class="form-control optionboxval" id="optionboxval" name="optionboxval[]"></textarea>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>                      
                        </div>
                      </div>
                      <!-- Modal End -->
                      <!-- Modal Start For Checkbox -->
                      <div class="modal fade chkboxpopup" role="dialog" id="chkboxpopup">
                        <div class="modal-dialog">                    
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">For Checkbox Value</h4>
                            </div>
                            <div class="modal-body">
                              <label>Enter Checkbox Value <span style="font-weight:300;font-size:12px;">(Enter comma seprated value)</span></label>
                              <textarea class="form-control checkboxval" id="checkboxval" name="checkboxval[]"></textarea>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>                      
                        </div>
                      </div>
                      <!-- Modal End -->
                      <!-- Modal Start For Radiobox -->
                      <div class="modal fade radioboxpopup" role="dialog" id="radioboxpopup">
                        <div class="modal-dialog">                    
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">For Radio</h4>
                            </div>
                            <div class="modal-body">
                              <label>Enter Radio Value <span style="font-weight:300;font-size:12px;">(Enter comma seprated value)</span></label>
                              <textarea class="form-control radioboxval" id="radioboxval" name="radioboxval[]"></textarea>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>                      
                        </div>
                      </div>
                      <!-- Modal End -->
                      
                      <div class="form-group col-md-3">
                        <label>Placeholder OR <br>Label</label>
                        <input class="form-control" type="text" id="fieldname" name="fieldname[]">
                      </div> 
                      <div class="form-group col-md-3">
                        <label>Styling <span style="font-weight:300;font-size:11px;">(Ex: class="xxxx" OR style="xxxx:xxxx")<a href="<?php echo base_url(); ?>assets/download/style.docx">Click Here</a></span></label>
                        <!-- <textarea class="form-control" id="styling" name="styling[]" style="min-height: auto;"></textarea> -->
                        <input class="form-control" type="text" id="styling" name="styling[]">
                      </div> 
                      <div class="form-group col-md-1">
                        <label>Sort Order</label>
                        <input class="form-control" type="text" id="sortorder" name="sortorder[]">
                      </div>
                      <div class="form-group col-md-2">
                        <label>Require</label><br><br>
                        <select class="form-control" name="frequire[]" id="frequire">
                          <option value="">Please Select</option>
                          <option value="yes">YES</option>
                          <option value="no">NO</option>
                        </select>
                      </div>
                      <div class="form-group col-md-1 addbtn" style="margin-left: -12px;">
                        <label>&nbsp;</label><br><br>
                        <a class="btn btn-default add_item">Add</a>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                                  
                  <div class="col-md-12" style="margin-top: 10px;">
                    <div class="col-md-6">
                      <div class="form-group">
                        <button type="submit" class="btn btn-default">Submit</button>
                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-default">Cancel</button>
                      </div>
                    </div>
                  </div>
              </form>         
            </div>
          </div>
        </div>
      </div>
    </div> <!-- container -->                         
    </div> <!-- content -->
</div>

<script type="text/javascript">
  var uniqueId = 1;
  $(function() {
    $('.add_item').click(function() { 
      var copy = $(".cloningform").first().clone(true).appendTo(".wrapper");
      copy.find("input").val("").end();
      copy.find("textarea").val("").end();
      copy.find("select").val("").end();
      copy.find("a").remove();

      var cosponsorDivId = 'cosponsors_' + uniqueId;
      copy.attr('id', cosponsorDivId );

      $(copy).find(".addbtn").html('<label>&nbsp;</label><br><br><a class="btn btn-danger remove">Remove</a>');  
      $(".cloningform").last().after(copy);
       
      copy.find('input').each(function(){
        $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
      });
      copy.find('select').each(function(){
        $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
      });
      copy.find('textarea').each(function(){
        $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
      });
      copy.find('div').each(function(){
        $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
      });

      uniqueId++;  
    });
  });
  $("body").on("click",".remove",function(){ 
    $(this).parents(".cloningform").remove();
  });

  var array = []
  var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
  for (var i = 0; i < checkboxes.length; i++) {
    array.push(checkboxes[i].value)
  }

  $(document).ready(function() {
    $(document).on("change",".fieldid",function(event){ 
      var fieldidval = $(this).val();
      var field_id = $(this).attr("id");      
      var $res = field_id.split("_");

      if(fieldidval == 1 || fieldidval == 2 || fieldidval == 3 || fieldidval == 4 || fieldidval == 8) {
        $('.fieldid').next('a').hide();
      }

      if($res[1]==undefined || $res[1]=='undefined') {
        if(fieldidval == 5 || fieldidval == '5') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#chkboxpopup">View Value</a>').insertAfter('#fieldid');
          $("#chkboxpopup").modal();
        }
        if(fieldidval == 6 || fieldidval == '6') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#radioboxpopup">View Value</a>').insertAfter('#fieldid');
          $("#radioboxpopup").modal();
        }
        if(fieldidval == 7 || fieldidval == '7') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#drpdwnpopup">View Value</a>').insertAfter('#fieldid');
          $("#drpdwnpopup").modal();
        }
      } else {
        if(fieldidval == 5 || fieldidval == '5') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#chkboxpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
          $("#checkboxval_"+$res[1]).val("");
          $("#chkboxpopup_"+$res[1]).modal();
        }
        if(fieldidval == 6 || fieldidval == '6') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#radioboxpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
          $("#radioboxval_"+$res[1]).val("");
          $("#radioboxpopup_"+$res[1]).modal();
        }
        if(fieldidval == 7 || fieldidval == '7') {
          $('.fieldid').next('a').hide();
          $('<a style="cursor: pointer;" data-toggle="modal" data-target="#drpdwnpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
          $("#optionboxval_"+$res[1]).val("");
          $("#drpdwnpopup_"+$res[1]).modal();
        }
      }
    });

  });


  $(document).ready(function() {
    $('#FieldAddFroms').on('submit', function (e) {
      e.preventDefault();              
      var noError     = true;
      var pagename   = $("#pagename").val();

      if(pagename == "") {
        $('#pagename').css('border','1px solid red');
        noError = false;               
      } else {
        $('#pagename').css('border','1px solid #eee');
        noError = true;
      }
      
      if(noError == true) {
        $('#FieldAddFroms')[0].submit();   
      }
    });
  });
</script>