			
			<?php 
				$bookingform6banner 	= $this->session->userdata('bookingform6banner');
				$blockdates 					= $this->session->userdata('blockdates');
				$commonbanner 				= $this->session->userdata('commonbanner');
				$banner 							= $this->session->userdata('banner');
				$stateEnable 					= $this->session->userdata('stateEnable');
				$venueEnable 					= $this->session->userdata('venueEnable');
				$cityEnable 					= $this->session->userdata('cityEnable');
                $peopleEnable 					= $this->session->userdata('peopleEnable');
				$citylist 						= $this->session->userdata('citylist');
				$circlelist				= $this->session->userdata('circlelist');
				$statelist 						= $this->session->userdata('statelist');
				$productList 					= $this->session->userdata('productList');
				$contentData 					= $this->session->userdata('contentData');
				$fieldLabelData 			= $this->session->userdata('fieldLabelData');
				$daySlot 							= $this->session->userdata('daySlot');
				$calBlocksDates 			= $this->session->userdata('calBlocksDates');
				$calEnablesDates 			= $this->session->userdata('calEnablesDates');
				$discountEnable             = $this->session->userdata('discountEnable');
				$discountSalonEnable    = $this->session->userdata('discountSalonEnable');

                if(isset($banner) && !empty($banner[0]['content'])) { ?>
						<div class="custom-banner">
							<img src="<?php echo base_url().'assets/uploads/'.$banner[0]['content'];?>" class="img-fluid w-100" alt="banner">
						</div>
					<?php } else { ?>
						<?php if(isset($commonbanner)) {
							if(count($commonbanner)==1){
						  foreach ($commonbanner as $key => $value) { 
						?>
						<div class="custom-banner">
							<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
						</div>
					<?php } } else { 
						$j = 1; $k = 1;?>
							<div id="demo" class="carousel slide" data-ride="carousel">
			        <!-- Indicators -->
			        <ul class="carousel-indicators">
			        	<?php for($i=0;$i<count($commonbanner);$i++){ ?>
			          	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php if($j == 1){ echo "active";} ?>"></li>
			        	<?php $j = $j+1; } ?>
			        </ul>        
			        <!-- The slideshow -->
			        <div class="carousel-inner">
			        	<?php foreach ($commonbanner as $key => $value) { ?>
				          <div class="carousel-item <?php if($k == 1){ echo "active";} ?>">
				            <img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>"  alt="banner">
				          </div>
				        <?php $k = $k+1; } ?>
			        </div>        
			        <!-- Left and right controls -->
			        <a class="carousel-control-prev" href="#demo" data-slide="prev">
			          <span class="carousel-control-prev-icon"></span>
			        </a>
			        <a class="carousel-control-next" href="#demo" data-slide="next">
			          <span class="carousel-control-next-icon"></span>
			        </a>
			      </div>
					<?php  } } ?>
					<?php } ?>
					</div>

				<?php
				$prodID 	= $this->session->userdata('bookingformId');

				$offerID 	= $this->session->userdata('product_id');

			    if($prodID==1){ ?>
							
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container">
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>						
						        <form id="bookingForm" method="POST" action="<?php echo base_url().'bookingdetailsformovies';?>">
						          <div class="booking-step">
						          	<h4>Booking Detail </h4>
						          	<div class="row flex-wrap">
						          		<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
						          		<div class="col-md-12">
						          			<div class="input-group">  
						                  <select class="input-box" name="city" id="city" required>
				                        <option value=""></option>
				                        <?php if(isset($citylist)) { 
				                          foreach ($citylist as $key => $value) { ?>
				                            <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
				                        <?php } } ?>
				                      </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">City</label>
						                </div>
						          		</div>
						          		<?php } ?>
						          		<?php if(isset($venueEnable) && $venueEnable->meta_value == 'on') { ?>
							          		<div class="col-lg-6 col-md-12">
							          			<label><h5>Selected Slot</h5></label>
							              	<div class="input-group">
							                  <select class="input-box" id="preferedArea1" name="preferedArea1" required>
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							              <div class="col-lg-6 col-md-12">
							              	<label><h5>Backup Slot</h5></label>
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedArea2" name="preferedArea2" required>
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea2') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
						              <?php } ?>		              
						              <?php if(isset($productList)) { ?>                    
							              <div class="col-lg-6 col-md-12">
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedMovie1" name="preferedMovie1" required>
							                  	<option value=""></option>
							                  	<?php foreach ($productList as $key => $value) { ?>
								                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
								                  <?php } ?>		                  	
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prodname1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							            <?php } ?>
							            <?php if(isset($productList)) { ?>
							              <div class="col-lg-6 col-md-12">
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedMovie2" name="preferedMovie2" required>
							                  	<option value=""></option>
							                  	<?php foreach ($productList as $key => $value) { ?>
								                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
								                  <?php } ?>
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prodname2') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							            <?php } ?>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate1" name="prefereddate1" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate2" name="prefereddate2" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						              </div>
						              <?php
														$dt = date("d-m-Y");
										        $dt1 = strtotime($dt);
										        $dt2 = date("l", $dt1);
										        $dt3 = strtolower($dt2);						        
													?>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime1" name="preferedTime1" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {              			
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime2" name="preferedTime2" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>
						            	<div class="col-md-12">
						              	<div class="verify-code">
						              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              	</div>
						              </div>
						            </div>
						            <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            <input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
						            <div class="form-group">
						          		<button type="submit" class="btn btn-primary booking-details" id="booking-details" name="contact-button" disabled="disabled">Submit</button>
						          	</div>
						          </div>
						      	</form>
									</div>
									<div class="col-lg-5 col-md-6 mt-4 mb-4">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingForm').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  = true;
							    var city     		  = $("#city").val();
							    var preferedMovie1    = $("#preferedMovie1").val();
							    var preferedMovie2    = $("#preferedMovie2").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var prefereddate2     = $("#prefereddate2").val();
							    var preferedTime1     = $("#preferedTime1").val();
							    var preferedTime2     = $("#preferedTime2").val();

							    if(city == ""){
							      $('#city').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedMovie1 == ""){
							      $('#preferedMovie1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedMovie1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedMovie2 == ""){
							      $('#preferedMovie2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedMovie2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate2 == ""){
							      $('#prefereddate2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime2 == ""){
							      $('#preferedTime2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }	    
							    if(noError) {
							    	$('#bookingForm')[0].submit();	      
							    }
								});
							});
						</script>						
            <?php } else if($prodID ==9){?>
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container" >
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php
											if($this->session->flashdata('msg_error')){
												echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg_error')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
											}
											elseif($this->session->flashdata('msg_success')){
												echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('msg_success')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
											}
										?>
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>						
		                <form  name="payuForm" method="POST" enctype="multipart/form-data" action="<?php echo base_url().'checkbogo' ?>">
		                <div class="booking-step">

					          	<h4>Booking Detail </h4>
					          	<div class="row flex-wrap">
				          		
				          		<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
				          		<div class="col-md-6">
				          			<div class="input-group">  
			                    <select class="input-box" name="city" id="city" required>
	                          <option value=""></option>
	                          <?php if(isset($citylist)) { 
			                        foreach ($citylist as $key => $value) { ?>
			                      	  <option value="<?php echo $value['id'];?>" <?php echo (!isset($posted['city'])) ? '' : $posted['city'] ?>><?php echo $value['name'];?></option>
			                      <?php } } ?>
	                        </select>
			                    <span class="highlight"></span>
			                    <span class="bar"></span>
			                    <label class="label-text">City</label>
				                </div>
						          </div>
						          <?php } ?>
						          <?php if(isset($venueEnable) && $venueEnable->meta_value == 'on') { ?>
							        <div class="col-lg-6 col-md-6">
				              	<div class="input-group">
				                  <select class="input-box" id="preferedArea1" name="preferedArea1" required>
				                  </select>
				                  <span class="highlight"></span>
				                  <span class="bar"></span>
				                  <label class="label-text">Venue</label>
				                  <?php if (isset($fieldLabelData)) {
				                  	foreach ($fieldLabelData as $key => $val) {
					                  	if ($val['fieldtype']=='prefarea1') { ?>
					                  <label class="label-text"><?php echo $val['fieldname'];?></label>
				                	<?php } } } ?>
				                </div>
				              </div>
				              <div class="col-lg-6 col-md-12">
				              	<div class="input-group">							                  
			                    <select class="input-box" id="meal" name="meal" required></select>
			                    <span class="highlight"></span>							                    
					                <label class="label-text">Meal</label>
				                </div>
				            	</div> 
						          <?php } ?>

						          <?php if(isset($productList)) { ?>    
					              <div class="col-lg-6 col-md-12">
					              	<div class="input-group">      
					                  <select class="input-box" id="preferedMovie1" name="offer_id" required>
					                  	<?php if(count($productList)==1) { ?>
								                <?php foreach ($productList as $key => $value) { ?>
								                  <option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
							                  <?php } ?>
						                  <?php } else { ?>
						                    <option value="">Select Your offer</option>
						                      <?php foreach ($productList as $key => $value) { ?>
							                      <option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
					                    <?php } } ?>                    	
					                  </select>
					                  <span class="highlight"></span>
					                  <span class="bar"></span>
					                  <?php if (isset($fieldLabelData)) {
					                  	foreach ($fieldLabelData as $key => $val) {
						                  	if ($val['fieldtype']=='prodname1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
						               	<?php } } } ?>
						              </div>
						            </div>
						          <?php } ?>
				              <div class="col-lg-6 col-md-12">
				              	<div class="input-group">      
				                  <input class="input-box" type="text" id="prefereddate1" name="prefereddate1" required autocomplete="off" readonly="readonly">
				                  <span class="highlight"></span>
				                  <span class="bar"></span>
				                  <label class="label-text preferdate">Preferred Date</label>
				                </div>
				              </div>
						              
				              <?php
												$dt = date("d-m-Y");
								        $dt1 = strtotime($dt);
								        $dt2 = date("l", $dt1);
								        $dt3 = strtolower($dt2);						        
											?>
				              <div class="col-lg-6 col-md-12">
				              	<div class="input-group">      
				                  <select class="input-box" id="preferedTime1" name="preferedTime1" required>
				                  	<option value=""></option>
				                  	<?php if(isset($daySlot)) {
				                  		foreach($daySlot[0] as $key => $value) {              			
				                  			if ($key==$dt3) {
					                  			$timeSlotData = json_decode($value, true);
					                  			$timeSlotArr = explode(",", $timeSlotData);
					                  			foreach ($timeSlotArr as $k => $val) {
				                  		?>
					                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
					                  <?php } } } } ?>
				                  </select>
				                  <span class="highlight"></span>
				                  <span class="bar"></span>
				                  <label class="label-text">Prefered Time</label>
				                </div>
				              </div>
				              <?php if(isset($peopleEnable)) { ?>
				          		<div class="col-md-6">
				          			<div class="input-group">  
				                  <select class="input-box" name="no_of_person" id="no_of_person" required>
		                        <option value=""></option>
		                           
				                    <?php foreach (range(1, $peopleEnable->meta_value) as $key => $value) { ?>
				                      <option value="<?php echo $value;?>"><?php echo $value;?></option>
				                    <?php  } ?>
		                      </select>
				                  <span class="highlight"></span>
				                  <span class="bar"></span>
				                  <label class="label-text">Select No. Of Person</label>
				                </div>
						          </div>
						        <?php } ?>
						        <div class="col-25 checkout" style="-ms-flex: 25%;flex: 25%;display: none;">
										    <div class="container">
										    	<p><a href="#">Total Amount Payable</a> <span class="price" id="total" style="float: right;color: grey;">$</span></p>
										      <p><?php if(isset($discountEnable)) { ?><a href="#">Discount(<?php echo $discountEnable->meta_value;?>)%</a><?php } ?> <span class="price" id="total_dist" style="float: right;color: grey;"></span></p>
										      
										     
										      <p><a href="#">Net Payable</a> <span id="net_amt" class="price" style="float: right;color: grey;"></span></p>
										      <hr>
										      <p>Total <span class="price" id="tol_amt" style="color:black;float: right;"><strong></strong></span></p>
										    </div>
									    </div>
									    <input type="hidden" id="amount_val" name="amount_val" value="">
									    <input type="hidden" id="total_amount_val" name="total_amount_val" value="">
									    <input type="hidden" id="total_discount_val" name="total_discount_val" value="">
									    
						          <input class="input-box" type="hidden" id="discount" name="discount" placeholder=" " autocomplete="off" value="<?php echo $discountEnable->meta_value;?>" readonly="readonly">
						          		
						            	<div class="col-md-12">
						              	<div class="verify-code">
						              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              	</div>
						              </div>
						            </div>
						            <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            <input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
						            <div class="form-group">
						          		<button type="submit" class="btn btn-primary booking-details" id="booking-details"  name="contact-button" disabled="disabled">Submit</button>
						          	</div>
						          </div>
						          <div class="col-25">
    
						      	</form>
									</div>
									<!-- <div class="col-lg-5 col-md-6 mt-4 mb-4">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
										</div>
									</div> -->
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingForm').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  = true;
							    var city     		  = $("#city").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var preferedTime1     = $("#preferedTime1").val();							    

							    if(city == ""){
							      $('#city').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							       
							    if(noError) {
							    	$('#bookingForm')[0].submit();	      
							    }
								});
							});
						</script>
						<script type="text/javascript">						  
							$(document).ready(function(){				
							//create_custom_dropdowns();
    					    $('#preferedArea1').change(function(){
    						  var preferedArea1 = $('#preferedArea1').val();
    						  if(preferedArea1 != '') {
    						  	$.ajaxSetup({
    					      	data: {
    					        	'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
    					      	}
    					  		});
    								$.ajax({
    									url:"<?php echo base_url(); ?>getmeal",
    									method:"POST",
    									data:{preferedArea1:preferedArea1},
    									beforeSend: function () {
    	                  $('.loader').removeClass('hide');
    	                },
    									success:function(data) {
    									  $('.loader').addClass('hide');
    									  $('#meal').html(data);    									 
    									}
    								});
    						  } else {
    						    $('.loader').addClass('hide');
    						    $('#meal').html('<option value=""></option>');    						   
    						  }
    					    });
    					    
    					    $('#no_of_person').change(function(){  
    					       $(".checkout").show();
    				           var no_of_person = $(this).val();  
    				           var per_price = $("#meal").val();  
    				           var discount = $("#discount").val();  
    
                       var total     =parseInt(no_of_person)*parseFloat(per_price);
                       if(isNaN(total)){total=0;}else{total=total;}
                       var total_dist     =(discount / 100) * total;
                       if(isNaN(total_dist)){total_dist=0;}else{total_dist=total_dist;}
                       var net_amt      = parseFloat(total)-parseFloat(total_dist);
                       
                       if(isNaN(net_amt)){net_amt=0;}else{net_amt=net_amt;}


    				           $("#total").text(total);
    				           $("#total_dist").text(total_dist);
    				           $("#amount").val(net_amt);
    				           $("#net_amt").text(net_amt);
    				           $("#tol_amt").text(net_amt);
    				           $("#total_amount").val(total);
    				           $("#total_discount").val(total_dist);

    				           $('#amount_val').val(net_amt);
    				           $('#total_amount_val').val(total);
    				           $('#total_discount_val').val(total_dist);
    				         
    				      });  
    				      $('#meal').change(function(){  
    					       $(".checkout").hide();
    					       $("#no_of_person").val("");
    				           
    				           var total = $("#meal").val();  
    				           var discount = $("#discount").val();  
    				           if(isNaN(total)){total=0;}else{total=total;}
    				           if(isNaN(discount)){discount=0;}else{discount=discount;}
                       var total_dist     =(discount / 100) * total;
                       if(isNaN(total_dist)){total_dist=0;}else{total_dist=total_dist;}
                       var net_amt      = parseInt(total)-parseInt(total_dist);

                       if(isNaN(net_amt)){net_amt=0;}else{net_amt=net_amt;}

    				           $("#total").text(total);
    				           $("#total_dist").text(total_dist);
    				           $("#amount").val(net_amt);
    				           $("#net_amt").text(net_amt);
    				           $("#tol_amt").text(net_amt);
    				           $("#total_amount").val(total);
    				           $("#total_discount").val(total_dist);

    				           $('#amount_val').val(net_amt);
    				           $('#total_amount_val').val(total);
    				           $('#total_discount_val').val(total_dist);

    				      });  
    					});
						</script>
						<?php } else if($prodID ==10){?>
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container" >
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php
											if($this->session->flashdata('msg_error')){
												echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg_error')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
											}
											elseif($this->session->flashdata('msg_success')){
												echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('msg_success')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
											}
										?>
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>						
						        <form  name="payuForm" method="POST" action="<?php echo base_url().'checksalonoff' ?>">
						          <div class="booking-step">
						          	<h4>Booking Detail </h4>
						          	<div class="row flex-wrap">
					          		
					          		<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
						          		<div class="col-md-6">
							          		<div class="input-group">  
					                    <select class="input-box" name="city" id="city" required>
			                          <option value=""></option>
			                            <?php if(isset($citylist)) { 
					                          foreach ($citylist as $key => $value) { ?>
					                      	    <option value="<?php echo $value['id'];?>" <?php echo (!isset($posted['city'])) ? '' : $posted['city'] ?>><?php echo $value['name'];?></option>
					                        <?php } } ?>
			                          </select>
					                    <span class="highlight"></span>
					                    <span class="bar"></span>
					                    <label class="label-text">City</label>
						                </div>
							          	</div>
						          		<?php } ?>
						          		<?php if(isset($venueEnable) && $venueEnable->meta_value == 'on') { ?>
							          		<div class="col-lg-6 col-md-6">
							          			<!-- <label><h5>Selected Venue</h5></label> -->
							              	<div class="input-group">
							                  <select class="input-box" id="preferedArea1" name="preferedArea1" required>
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <!-- <label class="label-text">Venue</label> -->
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							              <div class="col-lg-6 col-md-12">
							              	<div class="input-group">
							                  <select class="input-box" id="meal" name="meal" ></select>
							                  <span class="highlight"></span>
								                <label class="label-text">Gender</label>
							                </div>
							            	</div>
						              <?php } ?>

						              <?php if(isset($productList)) { ?>
							              <div class="col-lg-6 col-md-12">
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedMovie1" name="offer_id" required>
							                  	<?php if(count($productList)==1) { ?>
									                <?php foreach ($productList as $key => $value) { ?>
									                  <option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
									                  <?php } ?>
									                  <?php } else { ?>
									                    <option value="">Select Your offer</option>
									                      <?php foreach ($productList as $key => $value) { ?>
									                      <option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
									                    <?php } } ?>                    	
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prodname1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							            <?php } ?>
							            
						                <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate1" name="prefereddate1" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						                </div>
						              
						                <?php
															$dt = date("d-m-Y");
											        $dt1 = strtotime($dt);
											        $dt2 = date("l", $dt1);
											        $dt3 = strtolower($dt2);						        
														?>
						                <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime1" name="preferedTime1" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {              			
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>

						              <?php if(isset($peopleEnable)) { ?>
							          		<div class="col-md-6">
							          			<div class="input-group">  
							                  <select class="input-box" name="no_of_person" id="no_of_person" required>
					                        <option value=""></option>
					                           
							                    <?php foreach (range(1, $peopleEnable->meta_value) as $key => $value) { ?>
							                      <option value="<?php echo $value;?>"><?php echo $value;?></option>
							                    <?php  } ?>
					                      </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <label class="label-text">Select No. Of Person</label>
							                </div>
									          </div>
									        <?php } ?>

						            <div class="col-25 checkout" style="-ms-flex: 25%;flex: 25%;display: none;">
										    <div class="container">
										    	<p><a href="#">Total Amount Payable</a> <span class="price" id="total" style="float: right;color: grey;"></span></p>
										      <p><?php if(isset($discountSalonEnable)) { ?><a href="#">Discount(<?php echo $discountSalonEnable->meta_value;?>)%</a><?php } ?> <span class="price" id="total_dist" style="float: right;color: grey;"></span></p>
										      <p><a href="#">Net Payable</a> <span id="net_amt" class="price" style="float: right;color: grey;"></span></p>
										      <hr>
										      <p>Total <span class="price" id="tol_amt" style="color:black;float: right;"><strong>$</strong></span></p>
										    </div>
									    </div>
									    <input type="hidden" id="amount_val" name="amount_val" value="">
									    <input type="hidden" id="total_amount_val" name="total_amount_val" value="">
									    <input type="hidden" id="total_discount_val" name="total_discount_val" value="">

						          <input class="input-box" type="hidden" id="discount" name="discount" placeholder=" " autocomplete="off" value="<?php echo $discountSalonEnable->meta_value;?>" readonly="readonly">
						          		
						            	<div class="col-md-12">
						              	<div class="verify-code">
						              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              	</div>
						              </div>
						            </div>
						            <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            <input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
						            <div class="form-group">
						          		<button type="submit" class="btn btn-primary booking-details" id="booking-details" name="contact-button" disabled="disabled">Submit</button>
						          	</div>
						          </div>
						          <div class="col-25">
    
						      	</form>
									</div>
									 <div class="col-lg-5 col-md-6 mt-4 mb-4">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
										</div>
									</div> 
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingForm').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  = true;
							    var city     		  = $("#city").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var preferedTime1     = $("#preferedTime1").val();
							    

							    if(city == ""){
							      $('#city').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							       
							    if(noError) {
							    	$('#bookingForm')[0].submit();	      
							    }
								});
							});
						</script>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingForm').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  = true;
							    var city     		  = $("#city").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var preferedTime1     = $("#preferedTime1").val();

							    if(city == ""){
							      $('#city').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }   
							    if(noError) {
							    	$('#bookingForm')[0].submit();	      
							    }
								});
								
								//create_custom_dropdowns();
						    $('#preferedArea1').change(function(){
							  var preferedArea1 = $('#preferedArea1').val();
							  if(preferedArea1 != '') {
							  	$.ajaxSetup({
						      	data: {
						        	'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
						      	}
						  		});
									$.ajax({
										url:"<?php echo base_url(); ?>getmeal",
										method:"POST",
										data:{preferedArea1:preferedArea1},
										beforeSend: function () {
		                  $('.loader').removeClass('hide');
		                },
										success:function(data) {
										  $('.loader').addClass('hide');
										  $('#meal').html(data);									 
										}
									});
							  } else {
							    $('.loader').addClass('hide');
							    $('#meal').html('<option value=""></option>');
							  }
					    });
					    
					    $('#no_of_person').change(function(){  
								$(".checkout").show();
								var no_of_person = $(this).val();  
								var per_price = $("#meal").val();  
								var discount = $("#discount").val();  

								var total     =parseInt(no_of_person)*parseFloat(per_price);
								if(isNaN(total)){total=0;}else{total=total;}
								var total_dist     =(discount / 100) * total;
								if(isNaN(total_dist)){total_dist=0;}else{total_dist=total_dist;}
								var net_amt      = parseFloat(total)-parseFloat(total_dist);

								if(isNaN(net_amt)){net_amt=0;}else{net_amt=net_amt;}


								$("#total").text(total);
								$("#total_dist").text(total_dist);
								$("#amount").val(net_amt);
								$("#net_amt").text(net_amt);
								$("#tol_amt").text(net_amt);
								$("#total_amount").val(total);
								$("#total_discount").val(total_dist);

								$('#amount_val').val(net_amt);
								$('#total_amount_val').val(total);
								$('#total_discount_val').val(total_dist);
    				         
				      });  
				      $('#meal').change(function(){  
					      $(".checkout").hide();
					      $("#no_of_person").val("");
								var total = $("#meal").val();  
								var discount = $("#discount").val();  
								if(isNaN(total)){total=0;}else{total=total;}
								if(isNaN(discount)){discount=0;}else{discount=discount;}
								var total_dist     =(discount / 100) * total;
								if(isNaN(total_dist)){total_dist=0;}else{total_dist=total_dist;}
								var net_amt      = parseInt(total)-parseInt(total_dist);

								if(isNaN(net_amt)){net_amt=0;}else{net_amt=net_amt;}

								$("#total").text(total);
								$("#total_dist").text(total_dist);
								$("#amount").val(net_amt);
								$("#net_amt").text(net_amt);
								$("#tol_amt").text(net_amt);
								$("#total_amount").val(total);
								$("#total_discount").val(total_dist);
								$('#amount_val').val(net_amt);
								$('#total_amount_val').val(total);
								$('#total_discount_val').val(total_dist);
				      }); 
						});
					
						</script>
					<?php } else if($prodID==2) { ?>
				    
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container">
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">

										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>	
										<div class="card">
						          <div class="card-body">						
												<form id="bookingFormOffer2" method="POST" action="<?php echo base_url().'bookingdetailsforoffer2';?>">
													<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
							          		<div class="form-group">
															<label for="city">City</label> 
							                <select class="form-control" name="city" id="city">
					                      <option value="">--Please Select--</option>
					                      <?php if(isset($citylist)) { 
					                        foreach ($citylist as $key => $value) { ?>
					                          <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
					                      <?php } } ?>
					                    </select>
							          		</div>
							          		<?php } ?>
												  <div class="form-group">
														<?php if (isset($fieldLabelData)) {
					                  	foreach ($fieldLabelData as $key => $val) {
						                  	if ($val['fieldtype']=='prodname1') { ?>
						                  <label><?php echo $val['fieldname'];?></label>
					                	<?php } } } ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
														  <option value="">select your preferred offer </option>
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
												  </div>
													<div class="row mt-5">
														<div class="col-md-6">
						                  <h5>Selected Slot</h5>
															<div class="form-group">
																<?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea1') { ?>
								                  <label><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
																<select class="form-control" name="preferedArea1" id="preferedArea1">
																</select>									  
															</div>
														</div>
														<div class="col-md-6">                     
															<h5>Backup Slot</h5>
															<div class="form-group">
																<?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea2') { ?>
								                  <label><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
																<select class="form-control" name="preferedArea2" id="preferedArea2">
																</select>
															</div>						 
														</div>
													</div>
													<div class="row mt-3">
														<div class="col-md-6">
												     	<div class="form-group">
																<label for="prefereddate1">Preferred Date</label>
																<div class="input-group pd">
																	<input type="text" class="form-control" placeholder="Enter Preferred date" id="prefereddate1" name="prefereddate1" readonly="readonly">
																	<!-- <div class="input-group-append">
																		<span class="input-group-text"><em class="fa fa-calendar" aria-hidden="true"></em></span>
																		</div> -->
																</div>
															</div>
														</div>
													<div class="col-md-6">
														<div class="form-group">
													 		<label for="prefereddate2">Preferred Date</label>
													 		<div class="input-group pd">
																<input type="text" class="form-control" placeholder="Enter Preferred date" id="prefereddate2" name="prefereddate2" readonly="readonly">
																	<!-- <div class="input-group-append">
														  			<span class="input-group-text"><em class="fa fa-calendar" aria-hidden="true"></em></span>
																	</div> -->
													  		</div>
															</div>
														</div>
													</div>
													
													<?php
														$dt = date("d-m-Y");
										        $dt1 = strtotime($dt);
										        $dt2 = date("l", $dt1);
										        $dt3 = strtolower($dt2);						        
													?>
													<div class="row mt-3">
														<div class="col-md-6">
												     	<div class="form-group">
																<label for="prefereddate1">Preferred Time</label>
																<div class="input-group pd">
																	<select class="form-control" id="preferedTime1" name="preferedTime1" required>
								                  	<option value=""></option>
								                  	<?php if(isset($daySlot)) {
								                  		foreach($daySlot[0] as $key => $value) {          			
								                  			if ($key==$dt3) {
									                  			$timeSlotData = json_decode($value, true);
									                  			$timeSlotArr = explode(",", $timeSlotData);
									                  			foreach ($timeSlotArr as $k => $val) {
								                  		?>
									                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
									                  <?php } } } } ?>
								                  </select>
																</div>
															</div>
														</div>
													<div class="col-md-6">
														<div class="form-group">
													 		<label for="prefereddate2">Preferred Time</label>
													 		<div class="input-group pd">
																<select class="form-control" id="preferedTime2" name="preferedTime2" required>
								                  <option value=""></option>
								                  	<?php if(isset($daySlot)) {
								                  		foreach($daySlot[0] as $key => $value) {
								                  			if ($key==$dt3) {
									                  			$timeSlotData = json_decode($value, true);
									                  			$timeSlotArr = explode(",", $timeSlotData);
									                  			foreach ($timeSlotArr as $k => $val) {
								                  		?>
									                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
									                  <?php } } } } ?>
								                  </select>
													  		</div>
															</div>
														</div>
													</div>

													<div class="row mt-5">
														<div class="col-md-12">
															<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
														</div>
													</div>			 
											 		<?php if($this->session->flashdata('msgshow')) {
											  		$message = $this->session->flashdata('msgshow'); ?>         
							            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
							            <?php } ?>
							            <input type="hidden" name="booking_time" id="booking_time">
													<div class="control-group mt-5">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-danger reset" style="margin-left:10px">Reset</button>	
														  <input type="submit" class="btn btn-primary" id="booking-details" value="Submit"  disabled="disabled">			 
														</div>
												  </div>
												</form>
						          </div>
										</div>		
									</div>
									<div class="col-lg-5 col-md-6 mt-4 mb-4" style="padding: 0 0 2rem 2rem;"	>
										<div class="registration-detail">
											<?php if(isset($contentData)) {
												  echo $contentData[0]['content'];
												} ?>

										</div>
									</div>
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingFormOffer2').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  		= true;
							    var city     		  		= $("#city").val();
							    var preferedoffer    	= $("#preferedoffer").val();
							    var preferedArea1    	= $("#preferedArea1").val();
							    var preferedArea2    	= $("#preferedArea2").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var prefereddate2     = $("#prefereddate2").val();
							    var preferedTime1     = $("#preferedTime1").val();
							    var preferedTime2     = $("#preferedTime2").val();

							    if(city == ""){
							      $('#city').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedoffer == ""){
							      $('#preferedoffer').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedoffer').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedArea1 == ""){
							      $('#preferedArea1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedArea1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedArea2 == ""){
							      $('#preferedArea2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedArea2').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate2 == ""){
							      $('#prefereddate2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate2').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime2 == ""){
							      $('#preferedTime2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime2').css('border','1px solid #eee');
							      noError = true;
							    }
							       
							    if(noError) {
							    	$('#bookingFormOffer2')[0].submit();	      
							    }
								});
							});
						</script>	
					<!-- <h2>Booking Page Coming Soon For Offer 2</h2> -->
				<?php } else if($prodID==3) { ?>
				    
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container">
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>	
										<div class="card">
						          <div class="card-body">
												<form id="bookingFormOffer3" method="POST" action="<?php echo base_url().'bookingdetailsforoffer3';?>">
													<div class="form-group">
														<label for="preferedoffer">Select your Service</label>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
														  <option value="">select your service </option>
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													</div>
	                        <div class="form-group">
		                        <label for="city">City</label>
		                        <select class="form-control" name="city" id="city">
				                      <option value="">--Please Select--</option>
				                      <?php if(isset($citylist)) { 
				                        foreach ($citylist as $key => $value) { ?>
				                          <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
				                      <?php } } ?>
				                    </select>
	                        </div>
	                        <div class="row mt-5">
														<div class="col-md-6">
						                  <h5>Selected Slot</h5>
															<div class="form-group">
																<?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea1') { ?>
								                  <label><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
																<select class="form-control" name="preferedArea1" id="preferedArea1">
																</select>	
													  	</div>
														</div>
														<div class="col-md-6">
															<h5>Backup Slot</h5>
															<div class="form-group">
																<?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prefarea1') { ?>
								                  <label><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
																<select class="form-control" name="preferedArea2" id="preferedArea2">
																</select>	
															</div>
														</div>
													</div>
													<div class="row mt-3">
														<div class="col-md-6">
												     	<div class="form-group">
																<label for="sel1">Preferred Date</label>
																<div class="input-group pd">
																	<input class="form-control" placeholder="Enter Preferred date" id="prefereddate1" name="prefereddate1">
																	<!-- <div class="input-group-append">
																		<span class="input-group-text"><em class="fa fa-calendar" aria-hidden="true"></em></span>
																	</div> -->
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group">
															 	<label for="sel1">Preferred Date</label>
															 	<div class="input-group pd">
																	<input type="text" class="form-control" placeholder="Enter Preferred date" id="prefereddate2" name="prefereddate2" readonly="readonly">
																	<!-- <div class="input-group-append">
																  	<span class="input-group-text"><em class="fa fa-calendar" aria-hidden="true"></em></span>
																	</div> -->
															  </div>
															</div>
														</div>
													</div>
													<?php
														$dt = date("d-m-Y");
										        $dt1 = strtotime($dt);
										        $dt2 = date("l", $dt1);
										        $dt3 = strtolower($dt2);						        
													?>
						              <div class="row mt-3">
						                <div class="col-md-6">
						                  <div class="form-group">
						                    <label for="sel1">Preferred time</label>
	                              <select class="form-control" id="preferedTime1" name="preferedTime1">
							                  	<option value="">--Please Select Time--</option>
							                  	<?php if(isset($daySlot)) {
							                  		foreach($daySlot[0] as $key => $value) {
							                  			if ($key==$dt3) {
								                  			$timeSlotData = json_decode($value, true);
								                  			$timeSlotArr = explode(",", $timeSlotData);
								                  			foreach ($timeSlotArr as $k => $val) {
							                  		?>
								                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
								                  <?php } } } } ?>
							                  </select>
					                    </div>
						                </div>
						                <div class="col-md-6">
						                	<div class="form-group">
						                    <label for="sel1">Preferred time</label>
	                              <select class="form-control" id="preferedTime2" name="preferedTime2">
							                  	<option value="">--Please Select Time--</option>
							                  	<?php if(isset($daySlot)) {
							                  		foreach($daySlot[0] as $key => $value) {
							                  			if ($key==$dt3) {
								                  			$timeSlotData = json_decode($value, true);
								                  			$timeSlotArr = explode(",", $timeSlotData);
								                  			foreach ($timeSlotArr as $k => $val) {
							                  		?>
								                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
								                  <?php } } } } ?>
							                  </select>
						                  </div>
						                </div>
						              </div>
						              <div class="row mt-5">
														<div class="col-md-12">
															<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
														</div>
													</div>			 
											 		<?php if($this->session->flashdata('msgshow')) {
											  		$message = $this->session->flashdata('msgshow'); ?>         
							            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
							            <?php } ?>
							            <input type="hidden" name="booking_time" id="booking_time">
													<div class="control-group mt-5">
														<!-- Button -->
														<div class="controls">
															<button class="btn btn-danger reset" style="margin-left:10px">Reset</button>	
														  <input type="submit" class="btn btn-primary" id="booking-details" value="Submit"  disabled="disabled">	
														</div>
													</div>
												</form>
			                </div>
										</div>
									</div>
									<div class="col-lg-5 col-md-6 mt-4 mb-4" style="padding: 0 0 2rem 2rem;">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
												  echo $contentData[0]['content'];
												} ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingFormOffer3').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  		= true;
							    var city     		  		= $("#city").val();
							    var preferedoffer    	= $("#preferedoffer").val();						    
							    var preferedArea1    	= $("#preferedArea1").val();
							    var preferedArea2    	= $("#preferedArea2").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var prefereddate2     = $("#prefereddate2").val();
							    var preferedTime1     = $("#preferedTime1").val();
								  var preferedTime2     = $("#preferedTime2").val();

							    if(city == ""){
							      $('#city').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedoffer == ""){
							      $('#preferedoffer').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedoffer').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedArea1 == ""){
							      $('#preferedArea1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedArea1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedArea2 == ""){
							      $('#preferedArea2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedArea2').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate2 == ""){
							      $('#prefereddate2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate2').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime2 == ""){
							      $('#preferedTime2').css('border','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime2').css('border','1px solid #eee');
							      noError = true;
							    }
							       
							    if(noError) {
							    	$('#bookingFormOffer3')[0].submit();	      
							    }
								});
							});
						</script>	
						<!-- <h2>Booking Page Coming Soon For Offer 3</h2> -->
				<?php } else if($prodID==4) { ?>
				    <div class="custom-body-wrapper pt-3 pb-3">
						<div class="container">
							<div class="row">
								<div class="col-lg-7 col-md-6 mt-5 mb-5">
									<?php if($this->session->flashdata('msgshow')) {
									  $message = $this->session->flashdata('msgshow'); ?>
									  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
									<?php } ?>	
									<div class="card">
					          <div class="card-body">
											<form id="bookingFormOffer4" method="POST" action="<?php echo base_url().'bookingdetailsforoffer4';?>">					 
												<div class="form-group">
													<label for="sel1">Preferred Offer</label>
													<?php if(count($productList)==1) { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } else { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
															<option value="">--Please select your offer-- </option>
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } ?>
											  </div>
											  <div class="row mt-5">
													<div class="col-md-12">
														<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
													</div>
												</div>			 
										 		<?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
												<div class="control-group mt-5">
													<!-- Button -->
													<div class="controls">
														<!-- <button class="btn btn-danger reset" style="margin-left:10px">Reset</button>	 -->
													  <input type="submit" class="btn btn-primary" id="booking-details" value="Submit"  disabled="disabled">	
													</div>
												</div>
											</form>
					          </div>
									</div>
								</div>
								<div class="col-lg-5 col-md-6 mt-5 mb-4" style="padding: 0 0 2rem 2rem;"	>
									<div class="registration-detail">
										<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<script type="text/javascript">						  
						$(document).ready(function(){
							$('#bookingFormOffer4').on('submit', function (e) {
								e.preventDefault();              
						    var noError 		  		= true;
						    var preferedoffer    	= $("#preferedoffer").val();

						    if(preferedoffer == ""){
						      $('#preferedoffer').css('border','1px solid red');
						      noError = false;               
						    } else {
						    	$('#preferedoffer').css('border','1px solid #eee');
						      noError = true;
						    }
						       
						    if(noError) {
						    	$('#bookingFormOffer4')[0].submit();	      
						    }
							});
						});
					</script>
					<!-- <h2>Booking Page Coming Soon For Offer 4</h2> -->
				<?php } else if($prodID==5) { ?>
				    
					<div class="custom-body-wrapper pt-3 pb-3">
						<div class="container">
							<div class="row">
								<div class="col-lg-7 col-md-6 mt-5 mb-5">
									<?php if($this->session->flashdata('msgshow')) {
									  $message = $this->session->flashdata('msgshow'); ?>
									  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
									<?php } ?>
									<div class="card">
					            <div class="card-body">
												<form id="bookingFormOffer5" method="POST" action="<?php echo base_url().'bookingdetailsforoffer5';?>">					 
													<div class="form-group">
														<label for="sel1">Preferred Offer</label>
														<?php if(count($productList)==1) { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } else { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer">
															<option value="">--Please select your offer-- </option>
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } ?>
											  	</div>
											  	<div class="row mt-5">
													<div class="col-md-12">
														<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
													</div>
												</div>			 
										 		<?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
												<div class="control-group mt-5">
													<!-- Button -->
													<div class="controls">
														<button class="btn btn-danger reset" style="margin-left:10px">Reset</button>	
													  <input type="submit" class="btn btn-primary" id="booking-details" value="Submit"  disabled="disabled">	
													</div>
												</div>
											</form>
					          </div>
									</div>
								</div>
								<div class="col-lg-5 col-md-6 mt-5 mb-4" style="padding: 0 0 2rem 2rem;"	>
									<div class="registration-detail">
										<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
										} ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<script type="text/javascript">						  
						$(document).ready(function(){
							$('#bookingFormOffer5').on('submit', function (e) {
								e.preventDefault();              
						    var noError 		  		= true;
						    var preferedoffer    	= $("#preferedoffer").val();
						    if(preferedoffer == ""){
						      $('#preferedoffer').css('border','1px solid red');
						      noError = false;               
						    } else {
						    	$('#preferedoffer').css('border','1px solid #eee');
						      noError = true;
						    }						       
						    if(noError) {
						    	$('#bookingFormOffer5')[0].submit();	      
						    }
							});
						});
					</script>
					<!-- <h2>Booking Page Coming Soon For Offer 5</h2> -->
				<?php } elseif($prodID==8){ ?>
							
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container">
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>						
						        <form id="bookingFormMerchandise" method="POST" action="<?php echo base_url().'bookingdetailsformerchandise';?>">
						          <div class="booking-step">
						          	<h4>Booking Detail </h4>
						          	<div class="row flex-wrap">
						          		<?php if(isset($stateEnable) && $stateEnable->meta_value == 'on') { ?>
						          		<div class="col-md-6">
						          			<div class="input-group">  
						                  <select class="input-box" name="state" id="state" required>
				                        <option value=""></option>
				                        <?php if(isset($statelist)) { 
				                          foreach ($statelist as $key => $value) { ?>
				                            <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
				                        <?php } } ?>
				                      </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">State</label>
						                </div>
						          		</div>
						          		<?php } ?>

						          		<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
						          		<div class="col-md-6">
						          			<div class="input-group">  
						                  <select class="input-box" name="city" id="city_id" required>
				                        
				                      </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">City</label>
						                </div>
						          		</div>
						          		<?php } ?>
						              <h6 style="padding-left: 16px;">Enter Your Shipping Address </h6>
						              <div class="row  col-md-12">
						              <div class="col-lg-6">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="address1" name="address1" required placeholder=" " autocomplete="off">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Address 1</label>
						                </div>
						              </div>
						              <div class="col-lg-6">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="address2" name="address2" required placeholder=" " autocomplete="off">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Address 2</label>
						                </div>
						              </div>
						              </div>
						              <div class="row  col-md-12">
							              <div class="col-lg-6">
							              	<div class="input-group">      
							                  <input class="input-box" type="text" id="address3" name="address3" required placeholder=" " autocomplete="off">
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <label class="label-text preferdate">Address 3</label>
							                </div>
							              </div>
							              <div class="col-lg-6">
							              	<div class="input-group">      
							                  <input class="input-box" type="text" id="landmark" name="landmark" required placeholder=" " autocomplete="off">
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <label class="label-text preferdate">Landmark</label>
							                </div>
							              </div>
						              </div>
						              <div class="row  col-md-12">
						              <div class="col-lg-6">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="pin_code" name="pin_code" required placeholder=" " autocomplete="off">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Pincode</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="alternate_phone_no" name="alternate_phone_no" required placeholder=" " autocomplete="off">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Alternate Phone Number</label>
						                </div>
						              </div>
						          </div>
						              <!-- <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate2" name="prefereddate2" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						              </div> -->
						              <?php
														$dt = date("d-m-Y");
										        $dt1 = strtotime($dt);
										        $dt2 = date("l", $dt1);
										        $dt3 = strtolower($dt2);						        
													?>
						              <!-- <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime1" name="preferedTime1" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {              			
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime2" name="preferedTime2" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div> -->
						            	<div class="col-md-12">
						              	 <div class="verify-code">
						              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              	</div> 
						              </div>
						            </div>
						            <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            <input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
						            <div class="form-group">
						          		<button type="submit" class="btn btn-primary booking-details" id="booking-details" name="contact-button" disabled="disabled">Submit</button>
						          	</div>
						          </div>
						      	</form>
									</div>
									<div class="col-lg-5 col-md-6 mt-4 mb-4">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingFormMerchandise').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  		= true;
							    var reg = /^[0-9]+$/;
							    var state     		  		= $("#state").val();
							    var city_id     		  	= $("#city_id").val();
							    var address1                = $("#address1").val();
							    var address2                = $("#address2").val();
							    var address3                = $("#address3").val();
							    var landmark                = $("#landmark").val();
							    var pin_code                = $("#pin_code").val();
							    var alternate_phone_no      = $("#alternate_phone_no").val();

							    if(city_id == ""){
							      $('#city_id').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city_id').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(state == ""){
							      $('#state').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#state').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(address1 == ""){
							      $('#address1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#address1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(address2 == ""){
							      $('#address2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#address2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(address3 == ""){
							      $('#address3').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#address3').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(landmark == ""){
							      $('#landmark').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#landmark').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(pin_code == ""){
							      $('#pin_code').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#pin_code').css('border-bottom','1px solid #eee');
							      noError = true;
							    }

							    if((pin_code.length) >= 6){

							      $('#pin_code').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#pin_code').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(!(reg.test(pin_code))){

							      $('#pin_code').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#pin_code').css('border-bottom','1px solid #eee');
							      noError = true;
							    }

							    if(alternate_phone_no == ""){
							      $('#alternate_phone_no').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#alternate_phone_no').css('border-bottom','1px solid #eee');
							      noError = true;
							    }	    
							    if(noError) {
							    	$('#bookingFormMerchandise')[0].submit();	      
							    }
								});
						    //create_custom_dropdowns();
						  	$('#state').change(function(){
								  var state_id = $('#state').val();
								  if(state_id != '') {
								  	$.ajaxSetup({
							      	data: {
							        	'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
							      	}
							  		});
										$.ajax({
											url:"<?php echo base_url(); ?>getcity",
											method:"POST",
											data:{state_id:state_id},
											beforeSend: function () {
    	                  $('.loader').removeClass('hide');
    	                },
											success:function(data) {
											  $('.loader').addClass('hide');
											  $('#city_id').html(data);
											  $('#city_id').html(data);
											}
										});
								  } else {
								    $('.loader').addClass('hide');
								    $('#city_id').html('<option value=""></option>');
								    $('#city_id').html('<option value=""></option>');
								  }
							  });		 
							});
						</script>
					<?php } else if($prodID==6) { ?>
				  
					<div class="custom-body-wrapper pt-3 pb-3">
				  <div class="container">
				    <div class="row">
				      <div class="col-lg-7 col-md-6 mt-5 mb-5">
				        <div class="bcd">
				          <?php if (isset($bookingform6banner)) { ?>				        	
				          <img src="<?php echo base_url().'assets/uploads/'.$bookingform6banner[0]['banner_img'];?>" class="img1" alt="banner"/>
				          <?php } ?>
				          <?php if($this->session->flashdata('msgshow')) {
									  $message = $this->session->flashdata('msgshow'); ?>
									  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
									<?php } ?>
				          <p class="bottom">Select any of the payment modes listed below to request your cashback</p>  
				        </div>        
				        <div class="card ">
				          <div class="card-header">
				            <div class="pt-4 pl-2 pr-2 pb-2">
				              <!-- Credit card form tabs -->
				              <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
				                  <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <em class="fas fa-university mr-2"></em> NEFT</a> </li>                   
				                  <li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link "> <em class="fas fa-mobile-alt mr-2"></em> UPI </a> </li>
				                  <li class="nav-item"> <a data-toggle="pill" href="#paypal" class="nav-link "> <em class="fas fa-wallet mr-2"></em> E-WALLET CASH BACK </a> </li>               
				              </ul>
				            </div> <!-- End -->
				            <!-- Credit card form content -->
				            <div class="tab-content">
				              <!-- credit card info-->
				              <div id="credit-card" class="tab-pane fade show active pt-3">
				                <form id="bookingFormOffer6AccountNumber" method="POST" action="<?php echo base_url().'bookingdetailsforoffer6accountnumber';?>" role="form">
				                  <div class="form-group"> <label for="userfname">
				                    <h6>Full Name</h6>
				                    </label> <input type="text" name="userfname" id="userfname" placeholder="Enter Full Name" required class="form-control "> 
				                  </div>
				                  <div class="form-group"> <label for="accountnumber">
				                    <h6>A/C Number </h6>
				                    </label> <input type="text" name="accountnumber" id="accountnumber" placeholder="Enter Valid Account Number" required class="form-control "> 
				                  </div>
				                  <div class="form-group"> <label for="ifsccode">
				                    <h6>IFSC Code </h6>
				                    </label> <input type="text" name="ifsccode" id="ifsccode" placeholder="Enter IFSC Code" required class="form-control "> 
				                  </div>
				                  
				                  <div class="row mt-5">
														<div class="col-md-12">
															<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
														</div>
													</div>
				                  <?php if($this->session->flashdata('msgshow')) {
											  		$message = $this->session->flashdata('msgshow'); ?>
							            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
							            <?php } ?>
							            <input type="hidden" name="booking_time" id="booking_time">
				                  <div class="card-footer">
				                  	<input type="submit" class="subscribe btn btn-primary shadow-sm" id="booking-details" value="Proceed Payment" disabled="disabled" >
				                </form>
				              </div>
				            </div> <!-- End -->
				            <!-- Paypal info -->
				            <div id="paypal" class="tab-pane fade pt-3">
				              <h6 class="pb-2">Select Your e-Wallet type</h6>
				              <form role="form" id="bookingFormOffer6eWallets" method="POST" action="<?php echo base_url().'bookingdetailsforoffer6ewallets';?>">
				                <div class="form-group ">
				                  <?php if(count($productList)==1) { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer" required="required">
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } else { ?>
														<select class="form-control" name="preferedoffer" id="preferedoffer" required="required">
															<option value="">--Please select your offer-- </option>
														  <?php foreach ($productList as $key => $value) { ?>
						                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
						                  <?php } ?>
														</select>
													<?php } ?>
				                </div>
				                <div class="form-group"> <label for="phonenumber">
				                  <h6>Enter Phone Number</h6>
				                  </label> <input type="number" name="phonenumber" id="phonenumber" placeholder="Enter Phone Number" required class="form-control" minlength="10" maxlength="10"> 
				                </div>
				                
				                <div class="row mt-5">
													<div class="col-md-12">
														<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn1"></div>
													</div>
												</div>
			                  <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>
						            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time1">
				                <p><input type="submit" class="btn btn-primary" id="booking-details1" value="Proceed Payment" disabled="disabled" > </p>
				              </form>
				            </div> <!-- End -->
				            <!-- bank transfer info -->
				            <div id="net-banking" class="tab-pane fade pt-3">
				            	<form role="form" id="bookingFormOffer6UPI" method="POST" action="<?php echo base_url().'bookingdetailsforoffer6upi';?>">
					              <div class="form-group"> <label for="username">
					                <h6>Enter UPI VPN </h6>
					                </label> <input type="text" name="upivpn" id="upivpn" placeholder="Enter UPI VPN" required class="form-control "> 
					              </div>
					              
					              <div class="row mt-5">
													<div class="col-md-12">
														<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn2"></div>
													</div>
												</div>
			                  <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>
						            	<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time2">
					              <div class="form-group">
					                <p><input type="submit" class="btn btn-primary" id="booking-details2" value="Proceed Payment" disabled="disabled" ></p>
					              </div>
					            </form>
				            </div> <!-- End -->
				            <!-- End -->
				          </div>
				        </div>
				      </div>        
				    </div>
				  </div>
				</div>
				<script type="text/javascript">						  
					$(document).ready(function() {
						$("#accountnumber").keyup(function () {
					    var val = $(this).val();
					      if(isNaN(val)){
					        val = val.replace(/[^0-9\.]/g,'');
					        if(val.split('.').length>2) 
					        val =val.replace(/\.+$/,"");
					      }
					    $(this).val(val);       
					  });
						$("#phonenumber").keyup(function () {
					    var val = $(this).val();
					      if(isNaN(val)){
					        val = val.replace(/[^0-9\.]/g,'');
					        if(val.split('.').length>2) 
					        val =val.replace(/\.+$/,"");
					      }
					    $(this).val(val);       
					  });
						$('#bookingFormOffer6AccountNumber').on('submit', function (e) {
							e.preventDefault();              
					    var noError 		  = true;
					    var userfname    	= $("#userfname").val();
					    var accountnumber = $("#accountnumber").val();
					    var ifsccode    	= $("#ifsccode").val();

					    if(userfname == ""){
					      $('#userfname').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#userfname').css('border','1px solid #eee');
					      noError = true;
					    }
					    if(accountnumber == ""){
					      $('#accountnumber').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#accountnumber').css('border','1px solid #eee');
					      noError = true;
					    }
					    if(ifsccode == ""){
					      $('#ifsccode').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#ifsccode').css('border','1px solid #eee');
					      noError = true;
					    }				       
					    if(noError) {
					    	$('#bookingFormOffer6AccountNumber')[0].submit();	      
					    }
						});
						$('#bookingFormOffer6eWallets').on('submit', function (e) {
							e.preventDefault();              
					    var noError 		  		= true;
					    var preferedoffer    	= $("#preferedoffer").val();
					    var phonenumber 			= $("#phonenumber").val();

					    if(preferedoffer == ""){
					      $('#preferedoffer').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#preferedoffer').css('border','1px solid #eee');
					      noError = true;
					    }
					    if(phonenumber == ""){
					      $('#phonenumber').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#phonenumber').css('border','1px solid #eee');
					      noError = true;
					    }					       
					    if(noError) {
					    	$('#bookingFormOffer6eWallets')[0].submit();	      
					    }
						});
						$('#bookingFormOffer6UPI').on('submit', function (e) {
							e.preventDefault();              
					    var noError 		= true;
					    var upivpn    	= $("#upivpn").val();
					    if(upivpn == ""){
					      $('#upivpn').css('border','1px solid red');
					      noError = false;               
					    } else {
					    	$('#upivpn').css('border','1px solid #eee');
					      noError = true;
					    }						       
					    if(noError) {
					    	$('#bookingFormOffer6UPI')[0].submit();	      
					    }
						});
					});
				</script>
				<?php } elseif($prodID==7){ ?>
							
						<div class="custom-body-wrapper pt-3 pb-3">
							<div class="container">
								<div class="row">
									<div class="col-lg-7 col-md-6 mt-4 mb-4">
										<?php if($this->session->flashdata('msgshow')) {
										  $message = $this->session->flashdata('msgshow'); ?>
										  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
										<?php } ?>						
						        <form id="bookingFormHealthCare" method="POST" action="<?php echo base_url().'bookingdetailsforhealthcheckup';?>">
						          <div class="booking-step">
						          	<h4>Booking Detail </h4>
						          	<div class="row flex-wrap">
						          		<?php if(isset($cityEnable) && $cityEnable->meta_value == 'on') { ?>
						          		<div class="col-md-12">
						          			<div class="input-group">  
						                  <select class="input-box" name="city" id="city" required>
				                        <option value=""></option>
				                        <?php if(isset($citylist)) { 
				                          foreach ($citylist as $key => $value) { ?>
				                            <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
				                        <?php } } ?>
				                      </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">City</label>
						                </div>
						          		</div>
						          		<?php } ?>
						          			              
						              <?php if(isset($productList)) { ?>                    
							              <div class="col-lg-6 col-md-12">
							              	<label><h5>Selected Slot</h5></label>
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedMovie1" name="preferedMovie1" required>
							                  	<option value=""></option>
							                  	<?php foreach ($productList as $key => $value) { ?>
								                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
								                  <?php } ?>		                  	
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prodname1') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							            <?php } ?>
							            <?php if(isset($productList)) { ?>
							              <div class="col-lg-6 col-md-12">
							              	<label><h5>Backup Slot</h5></label>
							              	<div class="input-group">      
							                  <select class="input-box" id="preferedMovie2" name="preferedMovie2" required>
							                  	<option value=""></option>
							                  	<?php foreach ($productList as $key => $value) { ?>
								                  	<option value="<?php echo $value['id'];?>"><?php echo $value['producttitle'];?></option>
								                  <?php } ?>
							                  </select>
							                  <span class="highlight"></span>
							                  <span class="bar"></span>
							                  <?php if (isset($fieldLabelData)) {
							                  	foreach ($fieldLabelData as $key => $val) {
								                  	if ($val['fieldtype']=='prodname2') { ?>
								                  <label class="label-text"><?php echo $val['fieldname'];?></label>
							                	<?php } } } ?>
							                </div>
							              </div>
							            <?php } ?>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate1" name="prefereddate1" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <input class="input-box" type="text" id="prefereddate2" name="prefereddate2" required placeholder=" " autocomplete="off" readonly="readonly">
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text preferdate">Preferred Date</label>
						                </div>
						              </div>
						              <?php
														$dt = date("d-m-Y");
										        $dt1 = strtotime($dt);
										        $dt2 = date("l", $dt1);
										        $dt3 = strtolower($dt2);						        
													?>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime1" name="preferedTime1" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {              			
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>
						              <div class="col-lg-6 col-md-12">
						              	<div class="input-group">      
						                  <select class="input-box" id="preferedTime2" name="preferedTime2" required>
						                  	<option value=""></option>
						                  	<?php if(isset($daySlot)) {
						                  		foreach($daySlot[0] as $key => $value) {
						                  			if ($key==$dt3) {
							                  			$timeSlotData = json_decode($value, true);
							                  			$timeSlotArr = explode(",", $timeSlotData);
							                  			foreach ($timeSlotArr as $k => $val) {
						                  		?>
							                  	<option value="<?php echo $val;?>"><?php echo $val;?></option>
							                  <?php } } } } ?>
						                  </select>
						                  <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">Prefered Time</label>
						                </div>
						              </div>
						            	<div class="col-md-12">
						              	<div class="verify-code">
						              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              	</div>
						              </div>
						            </div>
						            <?php if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            <input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>">
						            <?php } ?>
						            <input type="hidden" name="booking_time" id="booking_time">
						            <div class="form-group">
						          		<button type="submit" class="btn btn-primary booking-details" id="booking-details" name="contact-button" disabled="disabled" >Submit</button>
						          	</div>
						          </div>
						      	</form>
									</div>
									<div class="col-lg-5 col-md-6 mt-4 mb-4">
										<div class="registration-detail">
											<?php if(isset($contentData)) {
											  echo $contentData[0]['content'];
											} ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<script type="text/javascript">						  
							$(document).ready(function(){
								$('#bookingFormHealthCare').on('submit', function (e) {
									e.preventDefault();              
							    var noError 		  		= true;
							    var city     		  		= $("#city").val();
							    var preferedMovie1    = $("#preferedMovie1").val();
							    var preferedMovie2    = $("#preferedMovie2").val();
							    var prefereddate1     = $("#prefereddate1").val();
							    var prefereddate2     = $("#prefereddate2").val();
							    var preferedTime1     = $("#preferedTime1").val();
							    var preferedTime2     = $("#preferedTime2").val();

							    if(city == ""){
							      $('#city').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#city').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedMovie1 == ""){
							      $('#preferedMovie1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedMovie1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedMovie2 == ""){
							      $('#preferedMovie2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedMovie2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate1 == ""){
							      $('#prefereddate1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(prefereddate2 == ""){
							      $('#prefereddate2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#prefereddate2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime1 == ""){
							      $('#preferedTime1').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime1').css('border-bottom','1px solid #eee');
							      noError = true;
							    }
							    if(preferedTime2 == ""){
							      $('#preferedTime2').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#preferedTime2').css('border-bottom','1px solid #eee');
							      noError = true;
							    }	    
							    if(noError) {
							    	$('#bookingFormHealthCare')[0].submit();	      
							    }
								});
							});
						</script>
				<?php }elseif($prodID==11){ ?>
				<div class="custom-body-wrapper pt-3 pb-3">
					<div class="container">
						<div class="row">
							<div class="col-lg-7 col-md-6 mt-4 mb-4">
								<?php 
									if($this->session->flashdata('msgshow')) {
										$message = $this->session->flashdata('msgshow'); ?>
										<div id="success" class="<?php echo $message['class'];?>">
											<?php echo $message['message']; ?>
										</div>
										<?php 
									} 
								?>						
						  		<form id="bookingFormMobileRecharge" method="POST" action="<?php echo base_url().'mobilerecharge';?>">
						          	<div class="booking-step">
						          		<h4>Mobile Recharge </h4>
						          		<div class="row flex-wrap">
						          			<div class="col-lg-6 col-md-12" style="display: none;">
									            	<div class="input-group">      
									                  <input class="input-box" type="hidden" name="nominate_name" id="nominate_name" required placeholder=" " autocomplete="off" value="ttt">
									                  <span class="highlight"></span>
									                  <span class="bar"></span>
									                  <?php if (isset($fieldLabelData)) {
										                  	foreach ($fieldLabelData as $key => $val) {
											                  	if ($val['fieldtype']=='nominate_name') { ?>
											                  <label class="label-text"><?php echo $val['fieldname'];?></label>
										              <?php } } } ?>	

									                  <!-- <label class="label-text preferdate">Enter Nominate Name</label> -->
									                </div>
									              </div>

						          				<div class="col-lg-6 col-md-12">
							              			<div class="input-group">      
											            <select class="input-box" id="circle" name="circle" required>
											               	<option value=""></option>
											               	<?php 
							                        			if(isset($circlelist)) { 
							                          				foreach ($circlelist as $key => $value) { ?>
							                            				<option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
							                        					<?php 
							                        				} 
							                        			} 
							                        		?>
											            </select>
										                <span class="highlight"></span>
										                <span class="bar"></span>
										                <?php if (isset($fieldLabelData)) {
										                  	foreach ($fieldLabelData as $key => $val) {
											                  	if ($val['fieldtype']=='mobile_circle') { ?>
											                  <label class="label-text"><?php echo $val['fieldname'];?></label>
										              <?php } } } ?>
										            </div>
							              		</div>
							            		<div class="col-lg-6 col-md-12">
							              			<div class="input-group">      
							                  			<select class="input-box" id="service_provider" name="service_provider" required>
								                  		<option value=""></option>
								                  		<option value="Airtel">Airtel</option>
								                  		<option value="BSNL">BSNL</option>
								                  		<option value="Vodafone">Vodafone</option>
								                  		<option value="Idea">Idea</option>
								                  		<option value="Jio">Jio</option>
								                  		</select>
							                  			<span class="highlight"></span>
							                  			<span class="bar"></span>
							                  			<?php if (isset($fieldLabelData)) {
										                  	foreach ($fieldLabelData as $key => $val) {
											                  	if ($val['fieldtype']=='mobile_povider') { ?>
											                  <label class="label-text"><?php echo $val['fieldname'];?></label>
										              <?php } } } ?>
							                  		</div>
							              		</div>
							            			
						              		<div class="col-lg-6 col-md-12">
						              			<div class="input-group">      
							                  		<select class="input-box" id="connection_type" name="connection_type" required>
									               		<option value=""></option>
									               		<option value="1">Prepaid</option>
									               		<option value="2">Postpaid</option>
								                	</select>
							                  		<span class="highlight"></span>
							                  		<span class="bar"></span>
							                  		<?php if (isset($fieldLabelData)) {
										                  	foreach ($fieldLabelData as $key => $val) {
											                  	if ($val['fieldtype']=='mobile_connection_type') { ?>
											                  <label class="label-text"><?php echo $val['fieldname'];?></label>
										              <?php } } } ?>
							                  	</div>
						              		</div>
						              		<div class="col-md-12">
						              			<div class="verify-code">
						              	  			<div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
						              			</div>
						              		</div>
						            	</div>
						            	<?php 
						            		if($this->session->flashdata('msgshow')) {
										  		$message = $this->session->flashdata('msgshow'); ?>         
						            			<input type="hidden" name="st_otp_time" value="<?php echo $message['st_otp_time'];?>" >
						            			<?php 
						            		} 
						            	?>
						            	<input type="hidden" name="booking_time" id="booking_time">
							            <div class="form-group">
							          		<button type="submit" class="btn btn-primary booking-details" id="booking-details" name="contact-button" disabled="disabled" >Submit</button>
							          	</div>
						          	</div>
						      	</form>
							</div>
							<div class="col-lg-5 col-md-6 mt-4 mb-4">
								<div class="registration-detail">
									<?php if(isset($contentData)) {
									  echo $contentData[0]['content'];
									} ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">						  
					$(document).ready(function(){
						$('#bookingFormMobileRecharge').on('submit', function (e) {
							e.preventDefault();              
						    var noError 		  = true;
						    var nominate_name     = $("#nominate_name").val();
						    var circle            = $("#circle").val();
						    var service_provider  = $("#service_provider").val();
						    var connection_type   = $("#connection_type").val();
						    

						    if(nominate_name == ""){
						       $('#nominate_name').css('border-bottom','1px solid red');
							      noError = false;               
							    } else {
							    	$('#nominate_name').css('border-bottom','1px solid #eee');
							     	 noError = true;
							    }
							    
							    if(circle == ""){
							      	$('#circle').css('border-bottom','1px solid red');
							      	noError = false;               
							    } else {
							    	$('#circle').css('border-bottom','1px solid #eee');
							      	noError = true;
							    }
							    
							    if(service_provider == ""){
							      	$('#service_provider').css('border-bottom','1px solid red');
							      	noError = false;               
							    } else {
							    	$('#service_provider').css('border-bottom','1px solid #eee');
							      	noError = true;
							    }
							    
							    if(connection_type == ""){
							      	$('#connection_type').css('border-bottom','1px solid red');
							      	noError = false;               
							    } else {
							    	$('#connection_type').css('border-bottom','1px solid #eee');
							      	noError = true;
							    }
							    
							    if(noError) {
							    	$('#bookingFormMobileRecharge')[0].submit();	      
							    }
							});
						});
					</script>
				<?php } ?>
				<script type="text/javascript">
					$(document).ready(function(){
						$(".reset").click(function() {
					    $(this).closest('form').find("input[type=text], select,textarea").val("");
						});
						var counter = 0;
						var interval = setInterval(function() {
					    counter++;
					    $('#booking_time').val(counter);
						}, 1000);
						var interval = setInterval(function() {
					    counter++;
					    $('#booking_time1').val(counter);
						}, 1000);
						var interval = setInterval(function() {
					    counter++;
					    $('#booking_time2').val(counter);
						}, 1000);
						
						//var enableDays = ['2020-05-10'];				
		   			//var disabledDays = ['2020-05-06', '2020-05-07'];
		   			var allEnableDates = '<?php echo $calEnablesDates; ?>';
		   			var allblockdates = '<?php echo $calBlocksDates; ?>';
		   			var disabledDays = allblockdates.replace(/\\/g, "");

				    function formatDate(d) {			    	
					    var day = String(d.getDate());
					    //add leading zero if day is is single digit				   
					    if (day.length == 1)
					      day = '0' + day
					    var month = String((d.getMonth()+1))
					    //add leading zero if month is is single digit
					    if (month.length == 1)
					      month = '0' + month
					    //alert(d.getFullYear() + '-' + month + "-" + day);
					    return month + "/" + day + "/" + d.getFullYear();
					   }

						$('#prefereddate1').datepicker({ 
				      format: 'mm/dd/yyyy',
				      startDate: new Date(),
				      beforeShowDay: function(date){
		            var dayNr = date.getDay();
		            if(disabledDays.indexOf(formatDate(date)) >= 0) {
		              return false;
		            }
		            if(allEnableDates.indexOf(formatDate(date)) >= 0) {
		              return true;
		            } else {
		            	return false;
		            }
		    			}		    			
				    });

				    $('#prefereddate2').datepicker({
				      format: 'mm/dd/yyyy',
				      startDate: new Date(),
				      beforeShowDay: function(date){
		            var dayNr = date.getDay();
		            if (disabledDays.indexOf(formatDate(date)) >= 0) {
		              return false;
		            }
		            if(allEnableDates.indexOf(formatDate(date)) >= 0) {
		              return true;
		            } else {
		            	return false;
		            }
		    			}
				    });

				    $('#prefereddate1').datepicker().on('changeDate', function (e) {
						  var offerId = "<?php echo $offerID; ?>";
						  var selectdate = e.format();
						  if(selectdate != '') {
								$.ajax({
									url:"<?php echo base_url(); ?>dateselect",
									method:"POST",
									data:{offerId:offerId,selectdate:selectdate},
									beforeSend: function () {
                	                  $('.loader').removeClass('hide');
                	                },
									success:function(data) {
									    $('.loader').addClass('hide');
									  $('#preferedTime1').html(data);
									}
								});
						  } else {
						      $('.loader').addClass('hide');
						    $('#preferedTime1').html('<option value=""></option>');
						  }
						});

						$('#prefereddate2').datepicker().on('changeDate', function (e) {
						  var offerId = "<?php echo $offerID; ?>";
						  var selectdate = e.format();
						  if(selectdate != '') {
								$.ajax({
									url:"<?php echo base_url(); ?>dateselect",
									method:"POST",
									data:{offerId:offerId,selectdate:selectdate},
									beforeSend: function () {
                	                  $('.loader').removeClass('hide');
                	                },
									success:function(data) {
									    $('.loader').addClass('hide');
									  $('#preferedTime2').html(data);
									}
								});
						  } else {
						      $('.loader').addClass('hide');
						    $('#preferedTime2').html('<option value=""></option>');
						  }
						});

				  });

				  function enableBtn(){
				    document.getElementById("booking-details").disabled = false;
				  }
				  function enableBtn1(){
				    document.getElementById("booking-details1").disabled = false;
				  }
				  function enableBtn2(){
				    document.getElementById("booking-details2").disabled = false;
				  }

					$(document).ready(function(){
					    //create_custom_dropdowns();
					    $('#city').change(function(){
						  var city = $('#city').val();
						  if(city != '') {
						  	$.ajaxSetup({
					      	data: {
					        	'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					      	}
					  		});
								$.ajax({
									url:"<?php echo base_url(); ?>getvenue",
									method:"POST",
									data:{city:city},
									beforeSend: function () {
                	                  $('.loader').removeClass('hide');
                	                },
									success:function(data) {
									  $('.loader').addClass('hide');
									  $('#preferedArea1').html(data);
									  $('#preferedArea2').html(data);
									}
								});
						  } else {
						    $('.loader').addClass('hide');
						    $('#preferedArea1').html('<option value=""></option>');
						    $('#preferedArea2').html('<option value=""></option>');
						  }
					    });	
					});	
					localStorage.removeItem('total_otp_tries');
				</script>
				