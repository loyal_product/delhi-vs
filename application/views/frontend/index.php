
			<?php if(isset($banner)) {
				if(count($banner)==1){
			  foreach ($banner as $key => $value) { 
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } else { 
			$j = 1; $k = 1;?>
				<div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
        	<?php for($i=0;$i<count($banner);$i++){ ?>
          	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php if($j == 1){ echo "active";} ?>"></li>
        	<?php $j = $j+1; } ?>
        </ul>        
        <!-- The slideshow -->
        <div class="carousel-inner">
        	<?php foreach ($banner as $key => $value) { ?>
	          <div class="carousel-item <?php if($k == 1){ echo "active";} ?>">
	            <img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" alt="">
	          </div>
	        <?php $k = $k+1; } ?>
        </div>        
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
		<?php  } } ?>
		</div>
		<div class="custom-body-wrapper pt-3 pb-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-6 mt-4 mb-4">
						<?php if($this->session->flashdata('msgshow')) {
						  $message = $this->session->flashdata('msgshow'); ?>
						  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
						<?php } ?>

							<form id="loginForm" method="POST" action="<?php echo base_url().'login';?>">
								
								<div class="signup-step">
			          	<h4>Registration Details</h4>
			          	<div class="row flex-wrap" id="step-1">
			            	<div class="col-lg-6 col-md-12">
			            		<div class="input-group">      
			                  <input class="input-box" type="text" id="vouchercode" name="vouchercode" required autocomplete="off">
			                  <span class="highlight"></span>
			                  <span class="bar"></span>
			                  <?php if (isset($fieldLabelData)) {
    		                  	foreach ($fieldLabelData as $key => $val) {
    		                  	if ($val['fieldtype']=='vouchercode') { ?>
    		                  <label class="label-text"><?php echo $val['fieldname'];?></label>
    		                	<?php } } } ?>
			                </div>
			            	</div>
			            	<div class="col-lg-6 col-md-12">
			            		<div class="input-group">      
			                  <input class="input-box" type="text" id="name" name="name" required autocomplete="off" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)">
			                  <span class="highlight"></span>
			                  <span class="bar"></span>
			                  <?php if (isset($fieldLabelData)) {
    		                  	foreach ($fieldLabelData as $key => $val) {
    		                  	if ($val['fieldtype']=='cname') { ?>
    		                  <label class="label-text"><?php echo $val['fieldname'];?></label>
    		                	<?php } } } ?>
			                </div>
			            	</div>
			              <div class="col-lg-6 col-md-12">
			              	<div class="input-group">      
			                  <?php if(isset($emailisrequiredornot) && $emailisrequiredornot=='on') { ?>
			                  	<input class="input-box" type="email" id="email" name="email" required autocomplete="off">
			                  <?php } else { ?>
			                  	<input class="input-box" type="email" id="email" name="email" autocomplete="off">
			                  <?php } ?>
			                  <span class="highlight"></span>
			                  <span class="bar"></span>
			                  <?php if (isset($fieldLabelData)) {
    		                  	foreach ($fieldLabelData as $key => $val) {
    		                  	if ($val['fieldtype']=='emailid') { ?>
    		                  <label class="label-text"><?php echo $val['fieldname'];?></label>
    		                	<?php } } } ?>
			                </div>
			              </div>
			              <div class="col-lg-6 col-md-12">
			              	<div class="input-group">      
			                  <input class="input-box" type="number" id="mobile" name="mobile" required autocomplete="off">
			                  <span class="highlight"></span>
			                  <span class="bar"></span>
			                  <?php if (isset($fieldLabelData)) {
    		                  	foreach ($fieldLabelData as $key => $val) {
    		                  	if ($val['fieldtype']=='mobile') { ?>
    		                  <label class="label-text"><?php echo $val['fieldname'];?></label>
    		                	<?php } } } ?>
			                </div>
			              </div>
			              <?php if(isset($dynamicform->fielddata)){ 
											$jsonformdata = json_decode($dynamicform->fielddata);
											//print "<pre>"; print_r($dynamicform->fielddata); die;
											foreach ($jsonformdata as $k=> $val) {
												if($val->fieldid=='1' || $val->fieldid==1){
													if($val->frequire=='yes') {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"> <input '.$val->styling.' type="text" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required="required" > <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">'.$val->fieldname.'</label>
						                </div>
						              </div>';
													} else {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"><input '.$val->styling.' type="text" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'"> <span class="highlight"></span><span class="bar"></span>
			                  			<label class="label-text">'.$val->fieldname.'</label>
							                </div>
							              </div> ';
													}													
												}
												if($val->fieldid=='2' || $val->fieldid==2){
													if($val->frequire=='yes') {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"> <input '.$val->styling.' type="email" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required="required" > <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">'.$val->fieldname.'</label>
						                </div>
						              </div>';
													} else {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"><input '.$val->styling.' type="email" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'"> <span class="highlight"></span><span class="bar"></span>
			                  			<label class="label-text">'.$val->fieldname.'</label>
							                </div>
							              </div> ';
													}	
												}
												if($val->fieldid=='3' || $val->fieldid==3){
													if($val->frequire=='yes') {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"> <input '.$val->styling.' type="date" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required="required" > <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">'.$val->fieldname.'</label>
						                </div>
						              </div>';
													} else {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"><input '.$val->styling.' type="date" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'"> <span class="highlight"></span><span class="bar"></span>
			                  			<label class="label-text">'.$val->fieldname.'</label>
							                </div>
							              </div> ';
													}	
												}
												if($val->fieldid=='4' || $val->fieldid==4){
													if($val->frequire=='yes') {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"> <input '.$val->styling.' type="tel" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required="required" > <span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">'.$val->fieldname.'</label>
						                </div>
						              </div>';
													} else {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"><input '.$val->styling.' type="tel" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'"> <span class="highlight"></span><span class="bar"></span>
			                  			<label class="label-text">'.$val->fieldname.'</label>
							                </div>
							              </div> ';
													}	
												}

												if($val->fieldid=='8' || $val->fieldid==8){
													if($val->frequire=='yes') {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"> <textarea '.$val->styling.' name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required="required" > </textarea><span class="highlight"></span>
						                  <span class="bar"></span>
						                  <label class="label-text">'.$val->fieldname.'</label>
						                </div>
						              </div>';
													} else {
														echo '<div class="col-lg-6 col-md-12"><div class="input-group"><textarea '.$val->styling.' name="'.strtolower(str_replace(' ', '',$val->fieldname)).'"></textarea> <span class="highlight"></span><span class="bar"></span>
			                  			<label class="label-text">'.$val->fieldname.'</label>
							                </div>
							              </div> ';
													}	
												}
												if($val->fieldid=='5' || $val->fieldid==5){
													$chkval = explode(',', $val->checkboxval);
													if($val->frequire=='yes') {
														foreach ($chkval as $key => $value) {														
															echo '<div class="col-lg-6 col-md-12"><label for="'.strtolower(str_replace(' ', '',$value)).'">
																<input id="'.strtolower(str_replace(' ', '',$value)).'" '.$val->styling.' type="checkbox" name="'.strtolower(str_replace(' ', '',$value)).'" value="'.strtolower(str_replace(' ', '',$value)).'" required="required"> '.$value.'
															</label></div>';
														}
													} else {
														foreach ($chkval as $key => $value) {														
															echo '<div class="col-lg-6 col-md-12"><label for="'.strtolower(str_replace(' ', '',$value)).'">
																<input id="'.strtolower(str_replace(' ', '',$value)).'" '.$val->styling.' type="checkbox" name="'.strtolower(str_replace(' ', '',$value)).'" value="'.strtolower(str_replace(' ', '',$value)).'"> '.$value.'
															</label></div>';
														}
													}	
												}
												if($val->fieldid=='6' || $val->fieldid==6) {
													$radval = explode(',', $val->radioboxval);
													if($val->frequire=='yes') {
														$html = '<div class="col-lg-6 col-md-12">';
														foreach ($radval as $key => $value) {														
															$html .= '<div class="col-lg-6 col-md-6"><label for="'.strtolower(str_replace(' ', '',$val->fieldname)).'">
																<input id="'.strtolower(str_replace(' ', '',$value)).'" '.$val->styling.' type="radio" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" value="'.strtolower(str_replace(' ', '',$value)).'" required="required"> '.$value.'
															</label></div>';
														}
														$html .='</div>';
														echo $html;
													} else {
														$html = '<div class="col-lg-6 col-md-12">';
														foreach ($radval as $key => $value) {														
															$html .= '<div class="col-lg-6 col-md-6"><label for="'.strtolower(str_replace(' ', '',$val->fieldname)).'">
																<input id="'.strtolower(str_replace(' ', '',$value)).'" '.$val->styling.' type="radio" name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" value="'.strtolower(str_replace(' ', '',$value)).'"> '.$value.'
															</label><div>';
														}
														$html .='</div>';
														echo $html;
													}
												}
												if($val->fieldid=='7' || $val->fieldid==7){
													$optval = explode(',', $val->optionboxval);
													if($val->frequire=='yes') {
														$html = '<div class="col-lg-6 col-md-12"><div class="input-group"><select '.$val->styling.' name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" id="'.strtolower(str_replace(' ', '',$val->fieldname)).'" required>';
														$html .= '<option value=""></option>';
														foreach ($optval as $key => $value) {
															$html .= '<option value="'.$value.'">'.$value.'</option>';
								            }
								            $html .= '</select><span class="highlight"></span>
							                  <span class="bar"></span>
							                  <label class="label-text">'.$val->fieldname.'</label>
							                </div>
								              </div>';
								              echo $html;
													} else {
														$html = '<div class="col-lg-6 col-md-12"><div class="input-group"><select '.$val->styling.' name="'.strtolower(str_replace(' ', '',$val->fieldname)).'" id="'.strtolower(str_replace(' ', '',$val->fieldname)).'" >';
														$html .= '<option value=""></option>';
														foreach ($optval as $key => $value) {
															$html .= '<option value="'.$value.'">'.$value.'</option>';
								            }
								            $html .= '</select><span class="highlight"></span>
							                  <span class="bar"></span>
							                  <label class="label-text">'.$val->fieldname.'</label>
							                </div>
								              </div>';
								              echo $html;
													}
												}


											}
										?>

									<?php } ?>
			               <div class="col-md-12">
			              	<div class="verify-code">
			              	  <div class="g-recaptcha" data-sitekey="<?php echo gSiteKey; ?>" data-callback="enableBtn"></div>
			              	</div>
			              </div> 
			            </div>
			            <div class="condition-box">
			            	<p class="mb-1">View <a target="_blank" href="<?php echo base_url().'terms-and-condition';?>" rel="noopener">Terms & Conditions</a></p>
			            </div>
			            <div>
				            <label for="registration-agreeterms">
											<input type="checkbox" id="registration-agreeterms" name="terms_cond" value="1"> I agree to the terms and conditions
										</label>
				          </div>
				          <input type="hidden" name="st_time" id="st_time">
			            <div class="form-group">
			            	<button type="submit" id="enbBtn" class="btn btn-primary register-step" name="contact-button" >Submit</button>
			            </div>
			          </div>
			        
			        </form>
					</div>
					<div class="col-lg-5 col-md-6 mt-4 mb-4">
						<div class="registration-detail">
							<?php if(isset($contentData)) {
							  foreach ($contentData as $key => $value) { 
								if($value['pagename']=='home') {
									echo $value['content'];
							} } } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

<script type="text/javascript">
    function enableBtn(){
       document.getElementById("enbBtn").disabled = false;
    }
    var IDLE_TIMEOUT = '<?php echo $screenrefresh->meta_value; ?>';
    //var IDLE_TIMEOUT = 60; //seconds
	var _idleSecondsTimer = null;
	var _idleSecondsCounter = 0;
	document.onclick = function() {
	  _idleSecondsCounter = 0;
	};
	document.onmousemove = function() {
	  _idleSecondsCounter = 0;
	};
	document.onkeypress = function() {
	  _idleSecondsCounter = 0;
	};
	_idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

	function CheckIdleTime() {
	  _idleSecondsCounter++;
	  if (_idleSecondsCounter >= IDLE_TIMEOUT) {
      window.clearInterval(_idleSecondsTimer);
      //alert("Time expired!");
      document.location.href = "<?php echo base_url();?>";
	  }
	}
	$(document).ready(function(){
		var counter = 0;
		var interval = setInterval(function() {
    	    counter++;
    	    $('#st_time').val(counter);
		}, 1000);

		$("#mobile").keyup(function () {
	    var val = $(this).val();
	      if(isNaN(val)){
	        val = val.replace(/[^0-9\.]/g,'');
	        if(val.split('.').length>2) 
	        val =val.replace(/\.+$/,"");
	      }
	    $(this).val(val);       
	  });
	  
		$('#loginForm').on('submit', function (e) {
			e.preventDefault();              
	    var noError         = true;
	    var vouchercode     = $("#vouchercode").val();
	    var name     				= $("#name").val();
	    //var email     			= $("#email").val();
	    var mobile          = $("#mobile").val();
	    //var captcha         = $("#captcha").val(); 
	    var terms_cond      = $('#registration-agreeterms:checked').val();
	    //var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;


	    if(vouchercode == ""){
	      $('#vouchercode').css('border-bottom','1px solid red');
	      noError = false;               
	    } else {
	    	$('#vouchercode').css('border-bottom','1px solid #eee');
	      noError = true;
	    }
	    if(name == ""){
	      $('#name').css('border-bottom','1px solid red');
	      noError = false;               
	    } else {
	    	$('#name').css('border-bottom','1px solid #eee');
	      noError = true;
	    }
	    /*if(email == ""){
	      $('#email').css('border-bottom','1px solid red');
	      noError = false;               
	    } else if(!pattern.test(email)) {
	    	$('#email').css('border-bottom','1px solid red');
	      noError = false;
	    } else {
	    	$('#email').css('border-bottom','1px solid #eee');
	      noError = true;
	    }*/
	    if(mobile == ""){
	      $('#mobile').css('border-bottom','1px solid red');
	      noError = false;               
	    } else if(mobile.length < 10) {
	    	$('#mobile').css('border-bottom','1px solid red');
	      noError = false;
	  	} else {
	    	$('#mobile').css('border-bottom','1px solid #eee');
	      noError = true;
	    }
	    if(terms_cond == undefined){
	      $('#registration-agreeterms').css('outline','2px solid red');
	      noError = false;               
	    } else {
	    	$('#registration-agreeterms').css('outline','1px solid #eee');
	      noError = true;
	    }
	   /* if(captcha == ""){
	      $('#captcha').css('border-bottom','1px solid red');
	      noError = false;               
	    } else {
	    	$('#captcha').css('border-bottom','1px solid #eee');
	      noError = true;
	    }*/

	    if(noError) {
	    	$('#loginForm')[0].submit();
	    }
		});

		
	});
</script>