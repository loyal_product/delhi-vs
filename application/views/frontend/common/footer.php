<style type="text/css">
  .hide{display: none;}
  .loader{position:fixed;left:0%;top:0%;bottom:0%;border:1px solid #000;background-color:#000;color:#fff;opacity:0.8;-webkit-border-radius:5px;-moz-border-radius:5px;width:100%;height:auto;text-align:center;z-index:100000;border-radius:0px;}
  .middle{background:transparent;width:100%;height:100%;}
  .loader .middle img{top:50%;position:absolute;left:50%;width:35px;margin-top:-50px;margin-left:-50px;}
  .loader .middle span{top:50%;position:absolute;left:50%;font-weight:600;color:#fff;margin-top:-50px;}
</style>
<div class="loader hide">
  <div class="middle">
    <img src="<?php echo base_url();?>/assets/frontend/images/loader.gif" alt="banner"/><span>Please Wait</span>
  </div>
</div>

<footer class="custom-footer pt-2 pb-2">
  <div class="container">
    <p class="mb-0">© BigCity Promotions <?php echo date('Y');?>,All Rights Reserved</p>
  </div>
</footer>

<script src="<?php echo base_url();?>assets/frontend/js/local.js"></script>
<script type="text/javascript">
  $(function(){
    var url = window.location.pathname;
    var urlRegExp = new RegExp(url.replace(/\/$/,'') + "$");
    if (url=='/') {
      $('.nav-link').removeClass('active');
      setTimeout(function(){ $('.nav-link').first().addClass('active'); }, 1000);
    } else {
      $('.menuactive a').each(function(){
        // and test its normalized href against the url pathname regexp
        if(urlRegExp.test(this.href.replace(/\/$/,''))) {
          $(this).addClass('active');
        }
      });
    }
  });
</script>

<?php $chatCodes = $this->session->userdata('chatCodes'); 
    if($chatCodes!=''){echo $chatCodes;}
  ?>
</body>
</html>


 