<?php 
  if(isset($blockdates) && isset($blockdates[0]['meta_value']) && $blockdates[0]['meta_value'] != '') {
    $dateArr = json_decode($blockdates[0]['meta_value'], true);
    $currentDate = strtotime(date("m/d/Y"));
    foreach ($dateArr as $key => $val) {
      $blkDate = strtotime($val);
      if ($currentDate == $blkDate) { ?>
        <style type="text/css">
          .loader{position:fixed;left:0%;top:0%;bottom:0%;border:1px solid #000;background-color:#000;color:#fff;-webkit-border-radius:5px;-moz-border-radius:5px;width:100%;height:auto;text-align:center;z-index:100000;border-radius:0px;opacity:0.8;}
          .middle{background:transparent;width:100%;height:100%;}
          .loader .middle h3{top:50%;position:absolute;left:45%;margin-top:-50px;margin-left:-50px;}
        </style>
        <div class="loader">
          <div class="middle">
            <h3>Today site is down for maintaince.</h3>
          </div>
        </div>
        <?php die; ?>
    <?php }
    }
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/bootstrap.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/frontend/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/font.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/local.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/responsive.css" media="all">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/jquery-ui.css">
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/bootstrap-datepicker.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer integrity="sha384-oqVuAfXRKap7fdgcCY5uykM6+R9GqQ8K/uxy9rx7HNQlGYl1kPzQho1wx4JwY8wC"></script> <!-- Compliant: integrity value should be replaced with the digest of the expected resource -->
    <script type="text/javascript">
      window.history.forward();
      function noBack() { window.history.forward(); }
      $(document).ready(function() {
        function disableBack() { window.history.forward() }
        window.onload = disableBack();
        window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
      });
    </script>
    <?php $gaCodes = $this->session->userdata('gaCodes'); 
      if($gaCodes!=''){echo $gaCodes;}
    ?>
  </head>
  <body onpageshow="if (event.persisted) noBack();" onunload="">
    <div class="main-wrapper">
      <div class="custom-navbar">
        <nav class="navbar navbar-expand-lg navbar-bg">
          <div class="container">
              <a class="navbar-brand" href=""></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"><em class="fas fa-bars"></em></span>
              </button>
              <?php if(isset($menulist)){ ?>
            <div class="collapse navbar-collapse menuactive" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <?php foreach ($menulist as $key => $val) { ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php if($val['menulink']=='home'){ echo base_url();} else {echo base_url().$val['menulink']; }?>"><?php echo $val['menuname']; ?> </a>
                  </li>
                <?php } ?>
              </ul>
            </div>
          <?php } ?>
          </div>
        </nav>
      </div>