
			<?php if(isset($banner)) {
			  foreach ($banner as $key => $value) { 
				if($value['pagename']=='contact-us') {
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } } ?>
		</div>

		<div class="custom-contact-wrapper">
			<div class="grey-bg pt-5 pb-5">
				<div class="container">
					<h3 class="main-heading text-center mt-3 mb-5">Contact Us</h3>
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<?php if(isset($contentData)) {
							  foreach ($contentData as $key => $value) { 
								if($value['pagename']=='contact-us') {
									echo $value['content'];
							} } } ?>
							
					
				</div>
		  </div>
		</div>