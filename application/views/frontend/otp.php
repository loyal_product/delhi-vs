
			<?php if(isset($banner)) {
				if(count($banner)==1){
			  foreach ($banner as $key => $value) { 
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } else { 
			$j = 1; $k = 1;?>
				<div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
        	<?php for($i=0;$i<count($banner);$i++){ ?>
          	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php if($j == 1){ echo "active";} ?>"></li>
        	<?php $j = $j+1; } ?>
        </ul>        
        <!-- The slideshow -->
        <div class="carousel-inner">
        	<?php foreach ($banner as $key => $value) { ?>
	          <div class="carousel-item <?php if($k == 1){ echo "active";} ?>">
	            <img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" alt="">
	          </div>
	        <?php $k = $k+1; } ?>
        </div>        
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
		<?php  } } ?>
		</div>
		<div class="custom-body-wrapper pt-3 pb-3">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-6 mt-4 mb-4">
						<?php if($this->session->flashdata('msgshow')) {
						  $message = $this->session->flashdata('msgshow'); ?>
						  <div id="success" class="<?php echo $message['class'];?>"><?php echo $message['message']; ?></div>
						<?php } ?>
						<span id="tries"></span>
		        <form id="otpForm" method="POST" action="<?php echo base_url().'otp_check';?>">		        	
		          <div class="otp-step">
		          	<h4>OTP Detail</h4>
		          	<div class="row flex-wrap">
		            	<div class="col-md-12">
		            		<div class="input-group">      
		                  <input class="input-box" type="text" id="otp" name="otp" required autocomplete="off" minlength="7" maxlength="7">
		                  <span class="highlight"></span>
		                  <span class="bar"></span>
		                  <?php if (isset($fieldLabelData)) {
		                  	foreach ($fieldLabelData as $key => $val) {
		                  	if ($val['fieldtype']=='otp') { ?>
		                  <label class="label-text"><?php echo $val['fieldname'];?></label>
		                	<?php } } } ?>
		                </div>
		            	</div>
		            </div>
		            <?php if($this->session->flashdata('msgshow')) {
						  		$message = $this->session->flashdata('msgshow'); ?>         
		            <input type="hidden" name="st_time" value="<?php echo $message['st_time'];?>">
		            <?php } ?>
		            <input type="hidden" name="otp_time" id="otp_time">
		            <div class="form-group">
		          		<button type="submit" id="otpcheck" class="btn btn-primary otp-stepBtn">Submit</button>
		          		<button type="button" id="resendotp" onclick="window.location.href='<?php echo base_url(); ?>resendotp'" class="btn btn-primary" style="display:none; margin-right: 10px;">Resend OTP</button>
		          	</div>
		          </div>
		        </form>
					</div>
					<div class="col-lg-5 col-md-6 mt-4 mb-4">
						<div class="registration-detail">
							<?php if(isset($contentData)) {
							  foreach ($contentData as $key => $value) { 
								if($value['pagename']=='home') {
									echo $value['content'];
							} } } ?>
						</div>
					</div>
				</div>
			</div>
		</div>

<script type="text/javascript">
	$(document).ready(function(){
	    var counter = 0;
		var interval = setInterval(function() {
		    counter++;
		    $('#otp_time').val(counter);
		}, 1000);
		$("#otp").keyup(function () {
	    var val = $(this).val();
	      if(isNaN(val)){
	        val = val.replace(/[^0-9\.]/g,'');
	        if(val.split('.').length>2) 
	        val =val.replace(/\.+$/,"");
	      }
	    $(this).val(val);       
	  });

		$('#otpForm').on('submit', function (e) {
			e.preventDefault();              
	    var noError = true;
	    var otp     = $("#otp").val();
	    
	    if(otp == ""){
	      $('#otp').css('border-bottom','1px solid red');
	      noError = false;               
	    } else {
	    	$('#otp').css('border-bottom','1px solid #eee');
	      noError = true;
	    }
	    
	    if(noError){
	    	$('#otpForm')[0].submit();
	    }
		});
	});

	$(function(){
	var otp=$("#otp").val();
  $("#otpcheck").click(function(){
  	if(otp==parseInt($('#otp').val())){
  		$('#tries').text("Success");
  		$(this).prop('disabled', true);
  		$('#otpForm')[0].submit();
  		$("#otpcheck").hide();
  	}else{
  		if(parseInt(localStorage.getItem('total_otp_tries'))<=1){
	  		$('#tries').text("Sorry!!! Please resend the otp.");
	  		localStorage.setItem('total_otp_tries',0);
	  		$('#resendotp').show();
	  		$("#otpcheck").hide();
	  	}else{
	  		localStorage.setItem('total_otp_tries', getTotalTries() - 1);
	  		showTotalTries();
	  	}	
  	}  	
  });

  $("#resendotp").click(function(){
  	localStorage.removeItem('total_otp_tries');
  	$('#otpcheck').prop('disabled', false);
  	$("#otpcheck").show();
  	$('#otp').val('');
  	showTotalTries();
  });
  	
});

function getTotalTries() {
  return parseInt(localStorage.getItem('total_otp_tries') || 3);
}

function showTotalTries() {
	if(parseInt(localStorage.getItem('total_otp_tries'))<1) {
  	$('#tries').text("Sorry!!! Please resend the otp.");
  	localStorage.setItem('total_otp_tries',0);
	}/*else{
		$('#tries').text('You have '+getTotalTries()+'Tries left');
	}*/
  
}
showTotalTries();
</script>