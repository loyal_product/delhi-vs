			<?php if(isset($banner)) {
				if(count($banner)==1){
			  foreach ($banner as $key => $value) { 
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } else { 
			$j = 1; $k = 1;?>
				<div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
        	<?php for($i=0;$i<count($banner);$i++){ ?>
          	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php if($j == 1){ echo "active";} ?>"></li>
        	<?php $j = $j+1; } ?>
        </ul>        
        <!-- The slideshow -->
        <div class="carousel-inner">
        	<?php foreach ($banner as $key => $value) { ?>
	          <div class="carousel-item <?php if($k == 1){ echo "active";} ?>">
	            <img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" alt="">
	          </div>
	        <?php $k = $k+1; } ?>
        </div>        
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
		<?php  } } ?>
		</div>
		
		<div class="container mt-5">
			<div class="row">
        <div class="col-md-2"></div>  
        <div class="col-md-8">
        	<div class="card">
        		<h5 class="card-header bg-success">Checkout Confirmation</h5>
        		<div class="card-body">
        			<form action="<?php echo $action; ?>/_payment" method="post" id="payuForm" name="payuForm">
		                <input type="hidden" name="key" value="<?php echo $mkey; ?>" />
		                <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
		                <input type="hidden" name="txnid" value="<?php echo $tid; ?>" />
		                <input type="hidden" name="amount" value="<?php echo $amount; ?>" />
		                <input type="hidden" name="firstname" value="<?php echo $name; ?>" />
		                <input type="hidden" name="email" value="<?php echo $mailid; ?>" />
		                <input type="hidden" name="phone" value="<?php echo $phoneno; ?>" />
		                <input type="hidden" name="productinfo" value="<?php echo $productinfo; ?>" />
		                
		                <table class="table table-striped" summary="confirmation">
						  <tbody>
						    <tr>
						      <th scope="col" scope="row">Total Payable Amount</th>
						      <td><?php echo $amount; ?></td>
						    </tr>
						    <tr>
						      <th scope="col" scope="row">Name</th>
						      <td><?php echo $name; ?></td>
						    </tr>
						    <tr>
						      <th scope="col" scope="row">Email</th>
						      <td><?php echo $mailid; ?></td>
						    </tr>
						    <tr>
						      <th scope="col" scope="row">Phone</th>
						      <td><?php echo $phoneno; ?></td>
						    </tr>
						  </tbody>
						</table>
		                <div class="form-group">
		                    <input name="surl" value="<?php echo $sucess; ?>" size="64" type="hidden" />
		                    <input name="furl" value="<?php echo $failure; ?>" size="64" type="hidden" />  
		                    <!--for test environment comment  service provider   -->
		                    <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
		                    <input name="curl" value="<?php echo $cancel; ?> " type="hidden" />
		                </div>
		                <div class="form-group float-right">
		                	<input type="submit" value="Pay Now" class="btn btn-success" />
		                </div>
		            </form> 
        		</div>
        	</div>
        	                                   
        </div>
        <div class="col-md-2"></div>
    </div>
  </div>