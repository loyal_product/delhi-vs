					
			<?php if(isset($banner)) {
				if(count($banner)==1){
			  foreach ($banner as $key => $value) { 
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } else { 
			$j = 1; $k = 1;?>
				<div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators">
        	<?php for($i=0;$i<count($banner);$i++){ ?>
          	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php if($j == 1){ echo "active";} ?>"></li>
        	<?php $j = $j+1; } ?>
        </ul>        
        <!-- The slideshow -->
        <div class="carousel-inner">
        	<?php foreach ($banner as $key => $value) { ?>
	          <div class="carousel-item <?php if($k == 1){ echo "active";} ?>">
	            <img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" alt="">
	          </div>
	        <?php $k = $k+1; } ?>
        </div>        
        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>
		<?php  } } ?>
		</div>
		<div class="custom-body-wrapper">
			<div class="grey-bg pt-5 pb-5">
				<div class="container">
					<?php if(isset($contentData)) {
						echo $contentData; } ?>
				</div>
    	</div>
		</div>