
			<?php if(isset($banner)) {
			  foreach ($banner as $key => $value) { 
				if($value['pagename']=='privacy-policy') {
			?>
			<div class="custom-banner">
				<img src="<?php echo base_url().'assets/uploads/'.$value['banner'];?>" class="img-fluid w-100" alt="banner">
			</div>
		<?php } } } ?>
		</div>		
		<div class="custom-body-wrapper">
			<div class="grey-bg pt-5 pb-5">
				<div class="container">
					<?php if(isset($contentData)) {
					  foreach ($contentData as $key => $value) { 
						if($value['pagename']=='privacy-policy') {
							echo $value['content'];
					} } } ?>
				</div>
    	</div>
		</div>
		