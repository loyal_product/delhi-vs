<?php
defined('BASEPATH') || exit('No direct script access allowed');

class BookingMailController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('BookingMailModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->helper("logo_helper");
	}


	# login page 
	public function index()	{
		$data['title'] = "Booking Mail";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/allbookingmail',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function resendDetails(){
		$vouchercode = $this->input->post('vouchercode');
		$this->form_validation->set_rules('vouchercode', 'vouchercode', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('allbookings', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/allbooking");
		}	else {
			$results = $this->BookingMailModel->getvoucherDetails($vouchercode);
			
			if(count($results)>0){
				
				$output = '<div class="row">
		        <div class="col-sm-12">
		          <div class="card-box">
		            <h4 class="m-t-0 m-b-20 header-title"><strong>Resend Mail By Admin</strong>
		            <a href="'.base_url().'site/core/micro/site/lib/controller/type/resendmailbyvoucherid/'.$results[0]["id"].'/'.$results[0]["product_id"].'" style="position: absolute;right: 31px;" class="btn btn-default">Resend Mail</a>       
		            </h4>
		            <div class="row">
		              <div class="col-md-12">	                
		                <div class="table-responsive">
		                  <table id="datatable-buttons1" class="table table-striped table-bordered">
		                    <thead>
		                      <tr>
		                        <th scope="col" class="collapsable">S.No</th>
		                        <th scope="col" class="collapsable">Email</th>
		                        <th scope="col" class="collapsable">Mobile</th>
		                      </tr>
		                    </thead>
		                  <tbody>';
											foreach ($results as $key => $value) {
												$k = $key+1;
												$output .= '<tr>
                          <td class="collapsable">'.$k.'</td>
                          <td class="collapsable">'.$value['email'].'</td>
                          <td class="collapsable">'.$value['mobile'].'</td>
                        </tr>';
											}
											$output .= '</tbody>
						                </table> 
						              </div>
						              </div>    
						            </div>
						          </div>
						        </div>
						      </div>'; 

						    } else {
						    	
								$output = '<div class="row">
						        <div class="col-sm-12">
						          <div class="card-box">
						            <h4 class="m-t-0 m-b-20 header-title"><strong>Resend Mail By Admin</strong>
						            </h4>
						            <div class="row">
						              <div class="col-md-12">						                
						                <div class="table-responsive">
						                  <table id="datatable-buttons1" class="table table-striped table-bordered">
						                    <thead>
						                      <tr>
						                        <th scope="col" class="collapsable">S.No</th>
						                        <th scope="col" class="collapsable">Email</th>
						                        <th scope="col" class="collapsable">Mobile</th>
						                      </tr>
						                    </thead>
						                  <tbody>
						                  <tr><td colspan="3">No Record Found</td></tr>
						                  </tbody>
						                </table> 
						              </div>
						              </div>    
						            </div>
						          </div>
						        </div>
						      </div>';
			}
	    print_r($output);
	    die;
	  }
	}

	public function resendMailByVoucherId($uid,$product_id){
		$allbookingsResultss = $this->BookingMailModel->orderDetails($uid);

	  $uname 	 = $allbookingsResultss[0]['name'];
		$emailId = $allbookingsResultss[0]['email'];
		//$emailId = 'kumarvipin9540@gmail.com';
		$bookingmaildetails = $this->BookingMailModel->getBookingMail($product_id);
		$dynamic_mail = $bookingmaildetails[0]['mail_body'];
		$getRewards = $this->BookingMailModel->getRewardCode($uid);	
					
		if (count($getRewards)>0) {
	    $rCode = $getRewards[0]['reward_code'];
		} else {
	    $rCode = 'processing';
		}
		if($product_id==1) {		    	
	    $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
	          <tr>
	          	<td><strong>Location/City</strong></td>
	            <td><strong>Prefered Area</strong></td>
	            <td><strong>Prefered Backup Area</strong></td>
	            <td><strong>Prefered Movie</strong></td>
	            <td><strong>Prefered Backup Movie</strong></td>
	            <td><strong>Prefered Date</strong></td>
	            <td><strong>Prefered Backup Date</strong></td>
	            <td><strong>Prefered Time</strong></td>
	            <td><strong>Prefered Backup Time</strong></td>
	            <td><strong>Booking Date & Time</strong></td>
	          </tr>';
	        foreach($allbookingsResultss as $key=>$v) {
	          $codeMessage .= "<tr>
	          	<td>".$v['city']."</td>
	          	<td>".$v['preferedArea1']."</td>
	          	<td>".$v['preferedArea2']."</td>
	          	<td>".$v['preferedMovie1']."</td>
	          	<td>".$v['preferedMovie2']."</td>
	          	<td>".$v['prefereddate1']."</td>
	          	<td>".$v['prefereddate2']."</td>
	          	<td>".$v['preferedTime1']."</td>
	          	<td>".$v['preferedTime2']."</td>
	          	<td>".$v['orderdatetime']."</td>
	          	</tr>";
	          }
	    $codeMessage .= '</table></td></tr>';
	    
	  	$replacements = [
		    "{{ConsumerName}}" => $uname,
		    "{{BookingDetails}}" => $codeMessage,
		    "{{RewardCode}}"  => $rCode,
		    "{{emailId}}" => $emailId,
			];

			if($emailId){
				$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
							  <tr>
							    <td align="center">
							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
							      <tr>
							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
        $message .= strtr($dynamic_mail, $replacements);
      	$message .= '</table></td></tr></table></td></tr></table>';

        $senderVar = 'bookingsuccesssendermail';
        $bookingSender = $this->BookingMailModel->getBookingSenderEmail($senderVar);
        
	    	$to = $emailId;
				$subject = $bookingmaildetails[0]['mail_subject'];
				$this->send_Email($to, $subject, $message, $bookingSender->meta_value);


				$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			} else {
				$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			}
		} elseif ($product_id==2) {
			if($emailId){
				$codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
		            <tr>
		            	<td><strong>Location/City</strong></td>
		              <td><strong>Preferred Venue</strong></td>
		              <td><strong>Prefered Backup Venue</strong></td>
		              <td><strong>Prefered Offer</strong></td>
		              <td><strong>Prefered Date</strong></td>
		              <td><strong>Prefered Backup Date</strong></td>
		              <td><strong>Booking Date & Time</strong></td>
		            </tr>';
		          foreach($allbookingsResultss as $key=>$v) {
		            $codeMessage .= "<tr>
		            	<td>".$v['city']."</td>
		            	<td>".$v['preferedArea1']."</td>
		            	<td>".$v['preferedArea2']."</td>
		            	<td>".$v['preferedMovie1']."</td>
		            	<td>".$v['prefereddate1']."</td>
		            	<td>".$v['prefereddate2']."</td>
		            	<td>".$v['orderdatetime']."</td>
		            	</tr>";
		            }
		      $codeMessage .= '</table></td></tr>';
		      
		    	$replacements = [
					    "{{ConsumerName}}" => $uname,
					    "{{BookingDetails}}" => $codeMessage,
					    "{{RewardCode}}" => $rCode,
					    "{{emailId}}" => $emailId,
					];

				$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
							  <tr>
							    <td align="center">
							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
							      <tr>
							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
	          $message .= strtr($dynamic_mail, $replacements);
	        	$message .= '</table></td></tr></table></td></tr></table>';

	        $senderVar = 'bookingsuccesssendermail';
	        $bookingSender = $this->BookingMailModel->getBookingSenderEmail($senderVar);
	        
		    	$to = $emailId;
					$subject = $bookingmaildetails[0]['mail_subject'];
					$this->send_Email($to, $subject, $message, $bookingSender->meta_value);

					$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/allbooking");
				} else {
					$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/allbooking");
				}
		} elseif ($product_id==3) {
			if($emailId){
				$codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
            <tr>
            	<td><strong>Location/City</strong></td>
              <td><strong>Prefered Area</strong></td>
              <td><strong>Prefered Backup Area</strong></td>
              <td><strong>Prefered Offer</strong></td>
              <td><strong>Prefered Date</strong></td>
              <td><strong>Prefered Backup Date</strong></td>
              <td><strong>Prefered Time</strong></td>
              <td><strong>Prefered Backup Time</strong></td>
              <td><strong>Booking Date & Time</strong></td>
            </tr>';
          foreach($allbookingsResultss as $key=>$v) {
            $codeMessage .= "<tr>
            	<td>".$v['city']."</td>
            	<td>".$v['preferedArea1']."</td>
            	<td>".$v['preferedArea2']."</td>
            	<td>".$v['preferedMovie1']."</td>
            	<td>".$v['prefereddate1']."</td>
            	<td>".$v['prefereddate2']."</td>
            	<td>".$v['preferedTime1']."</td>
            	<td>".$v['preferedTime2']."</td>
            	<td>".$v['orderdatetime']."</td>
            	</tr>";
            }
      $codeMessage .= '</table></td></tr>';
      
    	$replacements = [
			    "{{ConsumerName}}" => $uname,
			    "{{BookingDetails}}" => $codeMessage,
			    "{{RewardCode}}" => $rCode,
			    "{{emailId}}" => $emailId,
			];

			$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
						  <tr>
						    <td align="center">
						    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
						      <tr>
						        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
						        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
        $message .= strtr($dynamic_mail, $replacements);
      	$message .= '</table></td></tr></table></td></tr></table>';

        $senderVar = 'bookingsuccesssendermail';
        $bookingSender = $this->BookingMailModel->getBookingSenderEmail($senderVar);
        
	    	$to = $emailId;
				$subject = $bookingmaildetails[0]['mail_subject'];
				$this->send_Email($to, $subject, $message, $bookingSender->meta_value);

				$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			} else {
				$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			}
		} elseif ($product_id==4) {
			if($emailId){
				$codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
              <tr>
                <td><strong>Prefered Offer</strong></td>
                <td><strong>Booking Date & Time</strong></td>
              </tr>';
            foreach($allbookingsResultss as $key=>$v) {
              $codeMessage .= "<tr>
              	<td>".$v['preferedMovie1']."</td>
              	<td>".$v['orderdatetime']."</td>
              	</tr>";
              }
        $codeMessage .= '</table></td></tr>';
        
	    	$replacements = [
				    "{{ConsumerName}}" => $uname,
				    "{{BookingDetails}}" => $codeMessage,
				    "{{RewardCode}}" => $rCode,
				    "{{emailId}}" => $emailId,
				];

			$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
						  <tr>
						    <td align="center">
						    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
						      <tr>
						        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
						        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
        $message .= strtr($dynamic_mail, $replacements);
      	$message .= '</table></td></tr></table></td></tr></table>';

        $senderVar = 'bookingsuccesssendermail';
        $bookingSender = $this->BookingMailModel->getBookingSenderEmail($senderVar);
        
	    	$to = $emailId;
				$subject = $bookingmaildetails[0]['mail_subject'];
				$this->send_Email($to, $subject, $message, $bookingSender->meta_value);

				$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			} else {
				$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			}
		} elseif ($product_id==5) {
			if($emailId){
				$codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
              <tr>
                <td><strong>Prefered Offer</strong></td>
                <td><strong>Booking Date & Time</strong></td>
              </tr>';
            foreach($allbookingsResultss as $key=>$v) {
              $codeMessage .= "<tr>
              	<td>".$v['preferedMovie1']."</td>
              	<td>".$v['orderdatetime']."</td>
              	</tr>";
              }
        $codeMessage .= '</table></td></tr>';
        
	    	$replacements = [
				    "{{ConsumerName}}" => $uname,
				    "{{BookingDetails}}" => $codeMessage,
				    "{{RewardCode}}" => $rCode,
				    "{{emailId}}" => $emailId,
				];

			$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
						  <tr>
						    <td align="center">
						    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
						      <tr>
						        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
						        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
        $message .= strtr($dynamic_mail, $replacements);
      	$message .= '</table></td></tr></table></td></tr></table>';

        $senderVar = 'bookingsuccesssendermail';
        $bookingSender = $this->BookingMailModel->getBookingSenderEmail($senderVar);
        
	    	$to = $emailId;
				$subject = $bookingmaildetails[0]['mail_subject'];
				$this->send_Email($to, $subject, $message, $bookingSender->meta_value);

				$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			} else {
				$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/allbooking");
			}
		} /*elseif ($product_id==6) {
			if($emailId){

			} else {
				$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("admin/allbooking");
			}
		}*/ else {
			$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/allbooking");
		}

		
		
	}
	

	public function resendEmailId($uid,$pid) {
		$allbookingsResultss = $this->BookingMailModel->bookingDetailss($uid,$pid);

		if (count($allbookingsResultss)>0) {
			foreach ($allbookingsResultss as $k => $row) {
				$uname 	 = $row['name'];
				$emailId = $row['email'];

				$dynamic_mail = $row['pro_mail'];
	    	if ($row['reward'] != "") {
        	$rewards = $row['reward'];
        } else {
        	$rewards = 'Processing';
        }
	    	$replacements = [
				    "{{ConsumerName}}" => $row['name'],
				    "{{Code}}" => $rewards,
				];
				$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
									  <tr>
									    <td align="center">
									    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
									      <tr>
									        <td bgcolor="#FFFFFF" style="background-color:#FFF;"><table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">
									          
									          <tr>
									            <td style="padding: 20px 30px 10px 30px;"><img src="'.base_url().'assets/frontend/img/emailer-banner.jpg" style="width:100%; max-width:700px;" /></td>
									          </tr>
									          <tr>
									            <td style="padding:0 30px 10px 30px; font-family:Arial, Helvetica, sans-serif; ">
									              <br>';
							$message .= strtr($dynamic_mail, $replacements);
							$message .= '<tr>
							            <td style="padding:10px 30px;">
							              <table border="0" cellspacing="0" cellpadding="0" style="width:100%;background-color: #e4e5e5">
							                <tr>
							                  <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Arial, Helvetica, sans-serif;font-size: 14px;font-weight: normal;text-align: center;">
							                    <strong><span style="color:#000000">For any queries, please email us at&nbsp;</span></strong>
							                      <a href="mailto:feedback@bigcity.in" target="_blank"><span style="color:#000000">feedback@bigcity.in</span></a>
							                  </td>
							                </tr>
							              </table>
							            </td>
							          </tr>
							          <tr>
							            <td style="padding:10px 30px;">
							              <table border="0" cellspacing="0" cellpadding="0" style="width:100%;background-color: #fcfcfc">
							                <tr>
							                  <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Arial, Helvetica, sans-serif;font-size: 14px;font-weight: normal;text-align: center;">
							                    <img align="center" alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" width="69" style="max-width:150px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
							                  </td>
							                </tr>
							                <tr>
							                  <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
							                        
							                    <div style="text-align: center;"><span style="font-size:10px"><em>© '.date("Y").' BigCity Promotions, All rights reserved.</em></span></div>

							                </td>
							                </tr>
							              </table>
							            </td>
							          </tr>
							        </tbody>
							      </table>
							        <!--[if mso]>
							        </td>
							        <![endif]-->
							                
							        <!--[if mso]>
							        </tr>
							        </table>
							        <![endif]-->
							            </td>
							        </tr>

							        </table>  
							      </td>
							      </tr>							      
							      
							      </table>
							    </td>
							  </tr>
							</table>';

					$to = $emailId;
					$subject = $row['pro_mail_subject'];
					$this->send_Email($to, $subject, $message);
					$this->session->set_flashdata('allbookings', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/allbooking");
				} }else {
					$this->session->set_flashdata('allbookings', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/allbooking");
				}

	}

	//send email function ------- 
	function send_Email($to, $subject, $message) {   
	  require_once(APPPATH."libraries/vendor/autoload.php");    
	  $email = new \SendGrid\Mail\Mail(); 
		$email->setfrom("no-reply@bigcityexperience.com", "BigCity");
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent(
		    "text/html", $message
		);
		$sendgrid = new \SendGrid('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');
		if($sendgrid->send($email)){
			echo "reached";
		}
		else{
			echo "not"; 
		}  
	 } 
}