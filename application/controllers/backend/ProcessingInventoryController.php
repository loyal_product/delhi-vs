<?php
defined('BASEPATH') || exit('No direct script access allowed');

class ProcessingInventoryController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('ProcessingInventoryModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->helper("logo_helper");
	}


	# login page 
	public function index()	{
		$data['title'] = "Processing Inventory";

		$processingInventory = $this->ProcessingInventoryModel->getProcessInventoryDetails();

    $config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/processinginventory";
		$config["total_rows"] = $processingInventory;
    $config["per_page"] = 20;
    $config["uri_segment"] = 3;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else {
			$page = 1;
			$config['num_links'] = 10;	
		}
		$this->pagination->initialize($config);
	  $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
		$data['processInventory'] = $this->ProcessingInventoryModel->getProcessInventoryDetailsLimit($startFrom, $config["per_page"]);

		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/processinginventory/list',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function resendEmail($qty,$uid,$pid) {
		$invent_Avail = $this->ProcessingInventoryModel->getInventoryAvailable($pid,$qty);
		if (count($invent_Avail)>0) {
			$prodData = $this->ProcessingInventoryModel->getUsedResendCodeSendMail($pid,$qty);
			foreach ($prodData as $key => $val) {
				$this->ProcessingInventoryModel->updateRewardCodeById($val['prodid'],$val['rewardcode']);
				$this->ProcessingInventoryModel->updateOrderStatus($uid,$val['prodid']);
			}

			$userDetail = $this->ProcessingInventoryModel->getUserDetails($uid);
			$uname 	 = $userDetail[0]['name'];
			$emailId = $userDetail[0]['email'];
			if($emailId){
				$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
				  <tr>
				    <td align="center">
				    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
				      <tr>
				        <td bgcolor="#FFFFFF" style="background-color:#FFF;"><table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">				          
				          <tr>
				            <td style="padding: 20px 30px 10px 30px;"><img src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/225222b2-c2b3-41dc-a70a-4acc119649c2.jpg" style="width:100%; max-width:700px;" /></td>
				          </tr>
				          <tr>
				            <td style="padding:0 30px 10px 30px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
				              <br>
				              <p style="font-family:Arial, Helvetica, sans-serif; font-size:20px; margin:0 0 10px 0;">Dear '.$uname.', </p>
				              <br>
				              <p style="font-family:Helvetica, sans-serif; font-size:16px; margin:0 0 10px 0;">We have received your request as per below details:</p>
				              <br>				              
				              <p style="font-family:Helvetica, sans-serif; font-size:16px; margin:0 0 0px 0;"><STRONG>Here are your reward details:</STRONG></p>
				              <br><br>              
				            </td>
				          </tr>
				          <tr>
				            <td style="padding:0 30px 10px 30px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
				              <table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center;">
				                <tr>
				                  <td ><strong>Reward Code</strong></td>
				                  <td><strong>QTY</strong></td>
				                  <td><strong>Name</strong></td>
				                </tr>';
			                foreach($prodData as $key=>$v) {
			                  $message .= "<tr><td>".$v['rewardcode']."</td><td>1</td><td>".$v['title']."</td></tr>";
			                  }
	              
	          			$message .='</table>
						            </td>
						          </tr>
						          <tr>
						            <td style="padding:0 30px 10px 30px; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
						              <br>
						              <p style="font-family:Helvetica, sans-serif; font-size:16px; margin:0 0 0px 0;">In case the Reward Code shows "Processing", please allow 24-business hours to process the request. Your Reward Codes will be sent to you via your registered email id '.$emailId.'.</p>
						              <br>
						              <br>
						              <p style="font-family:Helvetica, sans-serif; font-size:16px; margin:0 0 0px 0;">To know the Terms & Conditions and How to Redeem your vouchers, please <a target="_blank" href="https://bata.bigcitypromo.in/assets/how-to-redeem-terms-of-offer.pdf">click here</a>.</p>
						              <br>
						              <br>
						              <p style="font-family:Helvetica, sans-serif; font-size:16px; margin:0 0 0px 0;">Regards <br>Team BigCity</p>
						              </td>
						          </tr>
						          <tr>
						            <td style="padding:10px 30px;">
						              <table border="0" cellspacing="0" cellpadding="0" style="width:100%;background-color: #A0DAF5">
						                <tr>
						                  <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Arial, Helvetica, sans-serif;font-size: 14px;font-weight: normal;text-align: center;">
						                      <strong><span style="color:#000000">For any queries, please email us at&nbsp;</span><a href="mailto:bata@bigcity.in" target="_blank"><span style="color:#000000">bata@bigcity.in</span></a><span style="color:#000000">&nbsp;or call us at 08040554856. Lines open from Monday to Friday between 10:30 AM to 5:30 PM. Lines closed on weekends and public holidays.</span></strong>
						                  </td>
						                </tr>
						              </table>
						            </td>
						          </tr>
						          <tr>
						            <td class="mcnImageContent" valign="top" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">
						              <img align="center" alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" width="69" style="max-width:150px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
						            </td>
						          </tr>
						          <tr>
						            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;padding-bottom: 20px;">
						                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
						                    <tbody><tr>
						                        
						                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
						                        
						                            <div style="text-align: center;"><span style="font-size:10px"><em>© '.date('Y').' BigCity Promotions, All rights reserved.</em></span></div>

						                        </td>
						                    </tr>
						                </tbody></table>
						            </td>
						        </tr>
						        </table>  
						      </td>
						      </tr>      
						      </table>
						    </td>
						  </tr>
						</table>';

				$to = $emailId;
				$subject = 'Voucher Code';
				$this->send_Email($to, $subject, $message);
				$this->session->set_flashdata('processinginventory', array('message' => 'Mail has been sent successfully..!!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/processinginventory");
			} else {
				$this->session->set_flashdata('processinginventory', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/processinginventory");
			}
		} else {
			$this->session->set_flashdata('processinginventory', array('message' => 'Insufficient Inventory..!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/processinginventory");
		}
	}

	//send email function ------- 
	function send_Email($to, $subject, $message) {   
	  require_once(APPPATH."libraries/vendor/autoload.php");    
	  $email = new \SendGrid\Mail\Mail(); 
		$email->setfrom("no-reply@bigcityexperience.com", "BigCity");
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent(
		    "text/html", $message
		);
		$sendgrid = new \SendGrid('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');
		if($sendgrid->send($email)){
			echo "sent";
		}
		else{
			echo "not"; 
		}  
	 }
	
}