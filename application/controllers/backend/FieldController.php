<?php
defined('BASEPATH') || exit('No direct script access allowed');

class FieldController extends MY_Controller {

	public $data = array();
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();		
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('FieldModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper('ckeditor_helper');
   	$this->load->helper("logo_helper");
	}

	// User Registration Page----------------------
	public function fieldsAdd(){
	  $data['title'] = "Add Field Name";
	  $data['productlist'] = $this->FieldModel->getProductList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/fieldname/fieldadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Voucher Insert Process -------------------------
	public function fieldsInsert() {
		$fieldtype 	= $this->input->post('fieldtype');
		$fieldname 	= $this->input->post('fieldname');
		$pid 				= $this->input->post('pid');

		$this->form_validation->set_rules('fieldtype', 'fieldtype', 'required');
		$this->form_validation->set_rules('fieldname', 'fieldname', 'required');
		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/fieldadd");
		}	else {
			$data =array(
				'prod_id' 				=>  $pid,
				'fieldtype' 	   	=> 	$fieldtype,					
				'fieldname' 			=> 	$fieldname,
			);
			$tbl= 'tbl_fieldname';
			$result = $this->FieldModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Field has been inserted Successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/fieldslist");
			} else {
				$this->session->set_flashdata('registration', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/fieldadd");
			}
		}
	}

	// User List ------------------------------------

	public function fieldsList(){
	 	$list = $this->FieldModel->getContentDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/fieldslist";
		$config["total_rows"] = $list;
    $config["per_page"] = 30;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['fieldlists'] = $this->FieldModel->getContentDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Field Name List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/fieldname/fieldlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editFields($id) {
		$data['title'] = "Edit Field Name";
		$data['id']=$id;
		$data['productlist'] = $this->FieldModel->getProductList();
		$data['getFieldsList'] = $this->FieldModel->getEditContent($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/fieldname/fieldedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function fieldsUpdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_fieldname';
		$data['title'] = "Edit Field Name";		
		$fieldtype =$this->input->post('fieldtype');
		$fieldname = $this->input->post('fieldname');
		$pid 				= $this->input->post('pid');

		$this->form_validation->set_rules('fieldtype', 'fieldtype', 'required');
		$this->form_validation->set_rules('fieldname', 'fieldname', 'required');
		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editfields/$id");
		}	else {
			$data =array(
				'prod_id' 				=>  $pid,
				'fieldtype' 	   	=> 	$fieldtype,					
				'fieldname' 			=> 	$fieldname,
			);
		
			$this->FieldModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'Field has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/fieldslist');
		}
		
	}
	
	public function deleteFields($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_fieldname';
		$result =$this->FieldModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Content has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/fieldslist");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/fieldslist");
		}
	}	

}