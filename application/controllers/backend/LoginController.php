<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginController extends CI_Controller {

   function __construct() {
    parent::__construct();
		//print_r($this->router->fetch_class());
		//print_r( $this->session->userdata['logged_in']['isAdmin']);
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('UserModel'); 		
	}
	
 		public function index()  {
			$userType = $this->session->userdata('userType');
			if($userType==1){
				redirect("site/core/micro/site/lib/controller/type/dashboard");
			}	elseif($userType==3){
				redirect("site/core/micro/site/lib/controller/type/dashboard");
			} elseif($userType==4){
				redirect("site/core/micro/site/lib/controller/type/dashboard");
			}		
			$data['title'] = "Admin Login";
			$this->load->view('admin/User/index',$data);					
		}

	public function login() {
		$adminEmail = $this->input->post('emailaddress');
		$pemail = explode('@', $adminEmail);
		$mailmatch = explode('.', $pemail[1]);
		
		if($mailmatch[0]=='bigcity' && $mailmatch[1]=='in') {
			$data = array(
				'emailaddress'=>$this->input->post('emailaddress'),
				'password'=>md5($this->input->post('password')),
				'status'=>1
			);
			$tbl= 'tbl_user';
			$result = $this->UserModel->get_where($tbl,$data);
			if($result) {
				foreach($result as $key=>$row){
					$userType= $row->userType;
					$session_data= array(
						'id'=>$row->id,
						'name'=>$row->name,
						'userType'=>$row->userType,
					);	
					$this->session->set_userdata($session_data);
					if($userType==1){
						redirect("site/core/micro/site/lib/controller/type/dashboard");
					}	elseif($userType==3){
						redirect("site/core/micro/site/lib/controller/type/dashboard");
					} elseif($userType==4){
						redirect("site/core/micro/site/lib/controller/type/dashboard");
					} else {
						redirect("site/core/micro/site/lib/controller/type/error");
					}
				}
			} else {
				$this->session->set_flashdata('user', array('message' => 'Please Enter Valid credentials','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type");
			}
		} else {
			$this->session->set_flashdata('user', array('message' => 'Please Enter Only bigcity email address','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type");
		}		
	}

  public function logout() {
    $data['title'] = "Admin Login";
    $this->session->unset_userdata('userType');
		$this->session->set_userdata(array());
		@session_destroy();
		//$this->load->view('site/core/micro/site/lib/controller/type/logout',$data);
		redirect("site/core/micro/site/lib/controller/type");
	}

}
