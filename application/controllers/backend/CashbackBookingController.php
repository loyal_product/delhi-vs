<?php
defined('BASEPATH') || exit('No direct script access allowed');

class CashbackBookingController extends MY_Controller {

	public $data = array();
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();		
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('CashbackBookingModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper('ckeditor_helper');
   	$this->load->helper("logo_helper");
	}

	// User Registration Page----------------------
	public function addCashbackBooking(){
	  $data['title'] = "Add Cashback Booking Form";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/cashbackbooking/cashbackbookingadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Voucher Insert Process -------------------------
	public function insertCashbackBooking() {
		$price 	= $this->input->post('price');
		$status 	= $this->input->post('status');
		$title 	= $this->input->post('title');

		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"];   
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);   	

		$this->form_validation->set_rules('price', 'price', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cashbackbookingadd");
		}	else {
			move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
			$data =array(
				'title' 			=> $title,
				'banner_img' 	=> $file_store,
				'price' 	   	=> $price,					
				'status' 			=> 	$status,
			);
			$tbl= 'tbl_cahbackform';
			$result = $this->CashbackBookingModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Cashback form data has been inserted successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/listcashbackbooking");
			} else {
				$this->session->set_flashdata('registration', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/cashbackbookingadd");
			}
		}
	}

	// User List ------------------------------------

	public function listCashbackBooking(){	
	 	$list = $this->CashbackBookingModel->getContentDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/listcashbackbooking";
		$config["total_rows"] = $list;
    $config["per_page"] = 30;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['cashbackbookingformlists'] = $this->CashbackBookingModel->getContentDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "List Cashback Booking Form";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/cashbackbooking/cashbackbookinglist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editCashbackBooking($id) {
		$data['title'] = "Edit Cashback Booking Form";
		$data['id']=$id;
		$data['getCashbackList'] = $this->CashbackBookingModel->getEditContent($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/cashbackbooking/cashbackbookingedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function UpdateCashbackBooking($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_cahbackform';
		$data['title'] = "Edit Cashback Booking Form";		
		$price 	= $this->input->post('price');
		$status 	= $this->input->post('status');
		$title 	= $this->input->post('title');
		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"];   
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']); 

		$this->form_validation->set_rules('price', 'price', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editcashbackbooking/$id");
		}	else {
			if($_FILES["image"]["name"]) {
				move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
				$data =array(
					'title' 			=> $title,
					'banner_img' 	=> $file_store,
					'price' 	   	=> $price,					
					'status' 			=> 	$status,
				);
			} else {
				$data =array(
					'title' 			=> $title,
					'price' 	   	=> $price,					
					'status' 			=> 	$status,
				);
			}

			$this->CashbackBookingModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'Cashback form has been updated successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/listcashbackbooking');
		}
		
	}
	
	public function deleteCashbackBooking($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_cahbackform';
		$result =$this->CashbackBookingModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Cashback form has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/listcashbackbooking");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/listcashbackbooking");
		}
	}	

}