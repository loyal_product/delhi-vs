<?php
defined('BASEPATH') || exit('No direct script access allowed');

class ProductController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('ProductModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper("logo_helper");
	}
    
	# login page 
	public function index()	{		
		$data['title'] = "Product List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/productList',$data);
		$this->load->view('admin/common/footer',$data);
	}
	// User Registration Page----------------------
	public function productAdd(){
		$data['title'] = "Product Registration";
		$data['cashbackofferlist'] = $this->ProductModel->getCashbackOfferLists();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/productadd',$data);
		$this->load->view('admin/common/footer', $data);
	}
	
	// product Insert Process -------------------------
	public function ProductInsert() {
		$title =$this->input->post('title');
		$tandc = $this->input->post('tandc');
		$pro_description =$this->input->post('pro_description');
		$pro_mail = $this->input->post('pro_mail');
		$offertilldate = $this->input->post('offertilldate');
		$producttitle = $this->input->post('producttitle');
		$productsms = $this->input->post('productsms');
		$pro_mail_subject = $this->input->post('pro_mail_subject');
		$pstatus = $this->input->post('pstatus');
		$booking_form_id = $this->input->post('booking_form_id');
		$crm_offer_id = $this->input->post('crm_offer_id');
		
		$cashbackid = $this->input->post('cashbackid');

		date_default_timezone_set('Asia/Kolkata');
		$created_date  = date("Y-m-d H:i:s");
		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"];   
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);

   	    move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);

        $this->form_validation->set_rules('crm_offer_id','crm_offer_id','required');
		$this->form_validation->set_rules('booking_form_id','booking_form_id','required');
		$this->form_validation->set_rules('title', 'title', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('registration', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/productadd");
		}	else {
					$data =array(
						'booking_form_id' => $booking_form_id,
						'cashbackid'			=> $cashbackid,
						'crm_offer_id' 		=> $crm_offer_id,
						'title' 	  			=> $title,	
						'tandc'						=> $tandc,
						'pro_description'	=> $pro_description,
						'pro_mail_subject'=> $pro_mail_subject,
						'pro_mail'				=> $pro_mail,
						'image' 	   			=> $file_store,
						'offertilldate'		=> $offertilldate,
						'producttitle' 	  => $producttitle,
						'productsms'		  => $productsms,
						'pstatus' 				=> $pstatus,
						'created_date' 		=> $created_date,
					);
					$tbl= 'tbl_product';
					$result = $this->ProductModel->insert($tbl,$data);
					if($result)	{
						$this->session->set_flashdata('product', array('message' => 'Product has been added successfully!','class' => 'alert alert-success'));
						redirect("site/core/micro/site/lib/controller/type/productList");
					} else {
						$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
						redirect("site/core/micro/site/lib/controller/type/productList");
					}
				}
			//}
	  }

	// Product List ------------------------------------

	public function productList(){
	 	$list = $this->ProductModel->getProductDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/productList";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$this->pagination->initialize($config);
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
		}	else{
			$page = 1;
		}
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['product'] = $this->ProductModel->getProductDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Product List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/productList',$data);
		$this->load->view('admin/common/footer', $data);
	}

	public function editProductList($id) {
		$data['title'] = "Edit Product";
		$data['id']=$id;
		$data['cashbackofferlist'] = $this->ProductModel->getCashbackOfferLists();
		$data['productList'] = $this->ProductModel->getEditProduct($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/productEdit',$data);
		$this->load->view('admin/common/footer', $data);
	}

	// Update Product -----------------------------
	public function updateProduct($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_product';

		$title =$this->input->post('title');
		$tandc = $this->input->post('tandc');
		$pro_description =$this->input->post('pro_description');
		$pro_mail = $this->input->post('pro_mail');
		$offertilldate = $this->input->post('offertilldate');
		$producttitle = $this->input->post('producttitle');
		$productsms = $this->input->post('productsms');
		$pro_mail_subject = $this->input->post('pro_mail_subject');
		$pstatus = $this->input->post('pstatus');
		$booking_form_id = $this->input->post('booking_form_id');
		$cashbackid = $this->input->post('cashbackid');
		$crm_offer_id = $this->input->post('crm_offer_id');

		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"]; 

		$file_store = $file_name;
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);
		move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
        
        $this->form_validation->set_rules('crm_offer_id','crm_offer_id','required');
		$this->form_validation->set_rules('booking_form_id', 'booking_form_id', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');

		$a = $this->ProductModel->mobile_number($where,$tbl);
		if($_FILES["image"]["name"]){
		$data =array(
						'booking_form_id' => $booking_form_id,
						'cashbackid'			=> $cashbackid,
						'crm_offer_id' 		=> $crm_offer_id,
						'title' 	   			=> $title,
						'tandc'			 			=> $tandc,	
						'pro_description'	=> $pro_description,
						'pro_mail_subject'=> $pro_mail_subject,
						'pro_mail'				=> $pro_mail,	
						'offertilldate'		=> $offertilldate,
						'producttitle' 	  => $producttitle,
						'productsms'		  => $productsms,
						'image' 	   			=> 	$file_store,
						'pstatus' 				=> $pstatus,		
					);
	    }else{
	    $data =array(
	    			'booking_form_id' => $booking_form_id,
	    			'cashbackid'			=> $cashbackid,
	    			'crm_offer_id' 		=> $crm_offer_id,
						'title' 	   			=> $title,	
						'tandc'			 			=> $tandc,	
						'pro_description'	=> $pro_description,
						'pro_mail_subject'=> $pro_mail_subject,
						'pro_mail'				=> $pro_mail,
						'offertilldate'		=> $offertilldate,
						'producttitle' 	  => $producttitle,
						'productsms'		  => $productsms,
						'pstatus' 				=> $pstatus,
					);
	    }
			$result = $this->ProductModel->update($tbl,$data,$where);
			
			if($result)	{				
				$this->session->set_flashdata('product', array('message' => 'Product has been updated successfully!','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/productList');
			}	else {	
				$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/productList');
			}
		
	}
	
	public function deleteproductList($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_product';
		$result =$this->ProductModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('product', array('message' => 'Product has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/productList");
		}	else {
			$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/productList");
		}

	}
	
	// Language List ----------------------
	public function languageList() {
	 	$list = $this->ProductModel->getLaguageCount(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/languagelist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['languagelist'] = $this->ProductModel->getLanguageDetailsLimit($startFrom, $config["per_page"]);
		$data['title'] = "Language List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/languagelist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Language Add ----------------------
	public function languageAdd(){
		$data['title'] = "Language Add";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/languageadd',$data);
		$this->load->view('admin/common/footer', $data);
	}
	
	// Language Insert Process -------------------------
	public function languageInsert() {
		$lang_title =$this->input->post('lang_title');
		$status =$this->input->post('status');

		$this->form_validation->set_rules('lang_title', 'lang_title', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('languagemsg', array('message' => 'fields are required.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/languageadd");
		}	else {
			$data =array(
				'lang_title' 	 => $lang_title,
				'status' 			 => $status,
			);
			$tbl= 'tbl_language';
			$result = $this->ProductModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('languagemsg', array('message' => 'Language has been added successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/languagelist");
			} else {
				$this->session->set_flashdata('languagemsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/languagelist");
			}
		}
	}

	public function editLanguageList($id) {
		$data['title'] = "Edit Language";
		$data['id']=$id;
		$data['languageList'] = $this->ProductModel->getEditLanguage($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/languageedit',$data);
		$this->load->view('admin/common/footer', $data);
	}

	// Update Product -----------------------------
	public function updatelanguage($id){
		$where = array('id'=>$id);

		$lang_title =$this->input->post('lang_title');
		$status =$this->input->post('status');

		$this->form_validation->set_rules('lang_title', 'lang_title', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('languagemsg', array('message' => 'fields are required.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/languageedit");
		}	else {
			$data =array(
				'lang_title' 	 => $lang_title,
				'status' 			 => $status,
			);
			$tbl= 'tbl_language';
			$result = $this->ProductModel->update($tbl,$data,$where);
			
			if($result)	{				
				$this->session->set_flashdata('product', array('message' => 'Language has been updated successfully!','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/languagelist');
			}	else {	
				$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/languageedit');
			}
		}
	}
	
	public function deleteLanguage($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_language';
		$result =$this->ProductModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('product', array('message' => 'language has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/languagelist");
		}	else {
			$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("admin/languagelist");
		}
	}

	// Sub Product List ----------------------
	public function subProductList() {
	 	$list = $this->ProductModel->getSubProductCount(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/subproductlist";
		$config["total_rows"] = $list;
    $config["per_page"] = 30;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['subproductlists'] = $this->ProductModel->getSubProductDetailsLimit($startFrom, $config["per_page"]);
		$data['title'] = "Sub Product List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/subproductlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// subproduct Add ----------------------
	public function subProductAdd(){
		$data['title'] = "Sub Product Add";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['languageLists'] 	= $this->ProductModel->getLanguageLists();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/subproductadd',$data);
		$this->load->view('admin/common/footer', $data);
	}
	
	// subproduct Insert Process -------------------------
	public function subProductInsert() {
		$pid 				 	= $this->input->post('pid');
		$title 				= $this->input->post('title');
		$langid 		 	= $this->input->post('langid');
		$subpstatus		= $this->input->post('subpstatus');
		$crm_prod_id	= $this->input->post('crm_prod_id');

		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('subpstatus', 'subpstatus', 'required');
		$this->form_validation->set_rules('crm_prod_id', 'crm_prod_id', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('product', array('message' => 'fields are required.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/subproductadd");
		}	else {
			$data =array(
				'prod_id' 	 				=> $pid,
				'lang_id' 	 				=> $langid,
				'crm_prod_id' 	 		=> $crm_prod_id,
				'sub_prod_name' 	 	=> $title,
				'status' 	 					=> $subpstatus,
			);
			$tbl= 'tbl_subproduct';
			$result = $this->ProductModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('product', array('message' => 'Sub Product has been added successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/subproductlist");
			} else {
				$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/subproductlist");
			}
		}
	}

	public function editSubProduct($id) {
		$data['title'] = "Edit Sub Product";
		$data['id']=$id;
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['languageLists'] 	= $this->ProductModel->getLanguageLists();
		$data['editLists'] 			= $this->ProductModel->getEditSubProduct($id);

		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/subproductedit',$data);
		$this->load->view('admin/common/footer', $data);
	}

	// Update Sub Product -----------------------------
	public function updateSubProduct($id){
		$where 			  = array('id'=>$id);
		$pid 				 	= $this->input->post('pid');
		$title 				= $this->input->post('title');
		$langid 		 	= $this->input->post('langid');
		$subpstatus		= $this->input->post('subpstatus');
		$crm_prod_id	= $this->input->post('crm_prod_id');

		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('subpstatus', 'subpstatus', 'required');
		$this->form_validation->set_rules('crm_prod_id', 'crm_prod_id', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('languagemsg', array('message' => 'fields are required.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editsubproduct/$id");
		}	else {
			$data =array(
				'prod_id' 	 				=> $pid,
				'lang_id' 	 				=> $langid,
				'crm_prod_id' 	 		=> $crm_prod_id,
				'sub_prod_name' 	 	=> $title,
				'status' 	 					=> $subpstatus,
			);
			$tbl= 'tbl_subproduct';
			$result = $this->ProductModel->update($tbl,$data,$where);
			
			if($result)	{				
				$this->session->set_flashdata('product', array('message' => 'Sub Product has been updated successfully!','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/subproductlist');
			}	else {	
				$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/subproductlist');
			}
		}
	}
	
	public function deleteSubProduct($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_subproduct';
		$result =$this->ProductModel->delete($tbl,$where);
		if($result)	{
			$this->session->set_flashdata('product', array('message' => 'Sub Product has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/subproductlist");
		}	else {
			$this->session->set_flashdata('product', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/subproductlist");
		}
	}
	
	public function uploadProduct(){
		$data['title'] = "Upload Product as CSV";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['languageLists'] 	= $this->ProductModel->getLanguageLists();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/product/productcsvupload',$data);
		$this->load->view('admin/common/footer');
	}

	public function uploadProductAsCSV(){
		$pid 				 	= $this->input->post('pid');
		$langid 		 	= $this->input->post('langid');
		$subpstatus		= $this->input->post('subpstatus');

		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('subpstatus', 'subpstatus', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('product', array('message' => 'fields are required.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/uploadproduct");
		}	else {

	    $memData = array();
      // If file uploaded
      if(is_uploaded_file($_FILES['file']['tmp_name'])) {
          // Load CSV reader library
          $this->load->library('CSVReader');
          // Parse data from CSV file
          $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
          
          if(!empty($csvData)){
            foreach($csvData as $key=>$row) {                   
              // Prepare data for DB insertion
            	$memData = array(
              	'prod_id' 			=> $pid,
                'lang_id' 			=> $langid,
                'sub_prod_name'	=> $row['productname'],
                'crm_prod_id' 	=> $row['crm_prod_id'],
	            'status' 				=> $subpstatus,                 
              );
              
              $tbl = 'tbl_subproduct';
              // Insert member data
              $insert = $this->ProductModel->insertCSV($tbl,$memData);
            }
          
          $this->session->set_flashdata('voucher', array('message' => 'Product has been imported successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/subproductlist");
        }
      } else {
        $this->session->set_flashdata('voucher', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/subproductlist");
      }     
		}
	}

	public function multiplVouchereDeactivate(){
		$tblname = 'tbl_subproduct';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->ProductModel->multiplDeactivateByID($id[$count],$tblname);
	   	}
	  }
	}

}