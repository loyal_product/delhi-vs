<?php
defined('BASEPATH') || exit('No direct script access allowed');

class VoucherController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
      	$this->load->helper('string');
      	$this->load->helper('text');
        $this->load->library('form_validation');
        $this->load->library('session'); // loading session library
        date_default_timezone_set("Asia/Kolkata");
        $this->load->database(); //loading database
       	$this->load->model('VoucherModel'); 
       	$this->load->helper(array('cookie', 'url'));
       	$this->load->library('pagination'); // pagination ---
       	// Load file helper
        $this->load->helper('file');
        $this->load->helper("logo_helper");
	}

	# login page 
	public function index()	{		
		$data['title'] = "Voucher List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/voucherList',$data);
		$this->load->view('admin/common/footer');
	}

	// User Registration Page----------------------
	public function voucherRegistration(){
	  $data['title'] = "Voucher";
		$data['productList'] = $this->VoucherModel->getProductListing();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/voucherRegistration',$data);
		$this->load->view('admin/common/footer');
	}
	
	// Voucher Insert Process -------------------------
	public function voucherinsert() {
		$productname =$this->input->post('productname');
		$vouchercode =$this->input->post('vouchercode');
		$stdate =$this->input->post('startdate');
		$date =$this->input->post('expirydate');
		$lastbookdate =$this->input->post('lastdatetobook');
		$expirydate = date("Y-m-d", strtotime($date));
		$startdate = date("Y-m-d", strtotime($stdate));
		$lastdatetobook = date("Y-m-d", strtotime($lastbookdate));
		$batchname =$this->input->post('batchname');		

		$this->form_validation->set_rules('productname', 'productname', 'required');
		$this->form_validation->set_rules('vouchercode', 'vouchercode', 'required');
		$this->form_validation->set_rules('startdate', 'startdate', 'required');
		$this->form_validation->set_rules('expirydate', 'expirydate', 'required');
		$this->form_validation->set_rules('lastdatetobook', 'lastdatetobook', 'required');
		$this->form_validation->set_rules('batchname', 'batchname', 'required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('voucher', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherRegistration");
		}	else {
			$createdBy =$this->session->userdata('id');
			$userType =$this->session->userdata('userType');
	   	
			$data =array(
				'product_id' 			=>  $productname,
				'vouchercode' 	  => 	strtoupper($vouchercode),
				'salt'						=>  SALT,
				'startdate'				=>	$startdate,
				'expirydate'			=>  $expirydate,
				'lastdatetobook'  =>  $lastdatetobook,
				'status'          => '1',
				'batchname'				=> $batchname,
			);

			$tbl= 'tbl_voucher_mng';
			$this->VoucherModel->insert_Encrypt($tbl,$data);
			$this->session->set_flashdata('voucher', array('message' => 'Voucher has been created successfully!!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
			}
    }

	// User List ------------------------------------

	public function voucherList() {
	 	$list = $this->VoucherModel->getvoucherDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/voucherList";
		$config["total_rows"] = $list;
    $config["per_page"] = 50;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['voucherList'] = $this->VoucherModel->getvoucherDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Voucher List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/voucherList',$data);
		$this->load->view('admin/common/footer', $data);	
	}

	public function editVoucherList($id) {
		$data['title'] = "Edit Voucher";
		$data['id']=$id;
		$data['productList'] = $this->VoucherModel->getProductListing();
		$data['editVoucherList'] = $this->VoucherModel->getEditVoucher($id);

		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/voucherEdit',$data);
		$this->load->view('admin/common/footer');
	}

	// Update User -----------------------------
	public function updateVoucher($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_voucher_mng';
		$data['title'] = "Edit Voucher";
		$batchname =$this->input->post('batchname');		
		$productname =$this->input->post('productname');
		$vouchercode =$this->input->post('vouchercode');
		$stdate =$this->input->post('startdate');
		$date =$this->input->post('expirydate');
		$lastbookdate =$this->input->post('lastdatetobook');
		$expirydate = date("Y-m-d", strtotime($date));
		$startdate = date("Y-m-d", strtotime($stdate));
		$lastdatetobook = date("Y-m-d", strtotime($lastbookdate));

		$this->form_validation->set_rules('productname', 'productname', 'required');
 		$this->form_validation->set_rules('vouchercode', 'vouchercode', 'required');
 		$this->form_validation->set_rules('startdate', 'startdate', 'required');
		$this->form_validation->set_rules('expirydate', 'expirydate', 'required');
		$this->form_validation->set_rules('lastdatetobook', 'lastdatetobook', 'required');
		$this->form_validation->set_rules('batchname', 'batchname', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('voucher', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editvoucherList/$id");
		} else {
			$userType = $this->session->userdata('userType');
	   	
			$data =array(
				'product_id' 		=>  $productname,
				'vouchercode' 	=> 	strtoupper($vouchercode),
				'salt'					=>  SALT,
				'startdate'			=>	$startdate,
				'expirydate'		=>  $expirydate,
				'lastdatetobook'=>  $lastdatetobook,
				'batchname'				=> $batchname,
			);		
			$this->VoucherModel->update_Encrypt($tbl,$data,$id);
			$this->session->set_flashdata('voucher', array('message' => 'Voucher has been updated successFully','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/voucherList');
		}		
	}
	
	public function deleteVoucherList($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_voucher_mng';
		$result =$this->VoucherModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('voucher', array('message' => 'Voucher has been deleted successFully','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		}	else {	
			$this->session->set_flashdata('voucher', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		}
	}

	public function uploadCSVView(){
		$data['title'] = "Upload Voucher as CSV";
		$data['productList'] = $this->VoucherModel->getProductListing();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/vouchercsvupload',$data);
		$this->load->view('admin/common/footer');
	}

	public function voucherUploadCSV(){
		$productname = $this->input->post('productname');
		$stDate = $this->input->post('stDate');
		$expDate = $this->input->post('expDate');
		$lastDateToBook = $this->input->post('lastDateToBook');
		$startdate = date("Y-m-d", strtotime($stDate));
		$expirydate = date("Y-m-d", strtotime($expDate));
		$lastDateBook = date("Y-m-d", strtotime($lastDateToBook));

	    $memData = array();
      // If file uploaded
      if(is_uploaded_file($_FILES['file']['tmp_name'])) {
          // Load CSV reader library
          $this->load->library('CSVReader');
          // Parse data from CSV file
          $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
          
          if(!empty($csvData)){
            foreach($csvData as $key=>$row){                   
              // Prepare data for DB insertion
            	$memData = array(
              	'product_id' 		=> $productname,
                'vouchercode' 	=> strtoupper($row['vouchercode']),
                'salt'					=> SALT,
	              'startdate' 		=> $startdate,
	              'expirydate' 		=> $expirydate,
	              'lastdatetobook'=> $lastDateBook,
	              'status' 				=> '1',                   
	              'batchname' 		=> $row['batchname']                   
              );
              
              $vouch_tbl = 'tbl_voucher_mng';
              // Insert member data
              $insert = $this->VoucherModel->insertCSV($vouch_tbl,$memData);
            }
          $json['message'] = "Voucher has been imported successfully!";
	        $json['code'] = 200;
	        $json['status'] = 'success';
	        echo json_decode($json); die;
          /*$this->session->set_flashdata('voucher', array('message' => 'Voucher has been imported successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/voucherList");*/
        }
      } else {
        $this->session->set_flashdata('voucher', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
				redirect("ssite/core/micro/site/lib/controller/type/vouchercsvupload");
      }     
	}

	
	public function searchByMobile(){
		$voucher_mob = $this->input->post('voucher_mob');
		if(!empty($voucher_mob)){
		 	$data['voucher_mob']=$voucher_mob;
  		$data["links"] ='';
  		$data['startFrom']=0;
      $data['voucherList'] = $this->VoucherModel->getvoucherDetailsByMobile($voucher_mob);

      $data['title'] = "Search By Mobile";
      $this->load->view('admin/common/header',$data);
			$this->load->view('admin/common/sidebar',$data);
			$this->load->view('admin/voucher/searchbymobile',$data);
			$this->load->view('admin/common/footer',$data);
		}
	}

	public function searchByVoucherCode() {
		$voucher_code = $this->input->post('voucher_code');
		if (!empty($voucher_code)) {
			$data['voucher_code']=$voucher_code;
  		$data["links"] ='';
  		$data['startFrom']=0;
      $data['voucherList'] = $this->VoucherModel->getvoucherDetailsByVoucher($voucher_code);
      $data['title'] = "Search By Voucher";
      $this->load->view('admin/common/header',$data);
			$this->load->view('admin/common/sidebar',$data);
			$this->load->view('admin/voucher/searchbyvoucher',$data);
			$this->load->view('admin/common/footer',$data);
		}
	}

	// Cap Limit
	public function caplimitList(){
		$data['title'] = "Cap Limit";
		$data['caplimitdata'] = $this->VoucherModel->getCapLimitData();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/caplimitlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function caplimitAdd(){
		$data['title'] = "Cap Limit";		
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/addcaplimit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function caplimitInsert(){
		$data['title'] = "Add Cap Limit";
		$setcaplimit =$this->input->post('setcaplimit');			
		$data =array(
			'caplimit' 	  => 	$setcaplimit,
		);
		$tbl= 'tbl_caplimit';
		$this->VoucherModel->insert($tbl,$data);
		$this->session->set_flashdata('caplimitmsg', array('message' => 'Cap limit has been created successfully!!','class' => 'alert alert-success'));
		redirect("site/core/micro/site/lib/controller/type/caplimitlist");
	}

	public function editCapLimitList($id) {
		$data['title'] = "Edit Cap Limit";
		$data['id']=$id;
		$data['editCapLimitList'] = $this->VoucherModel->getEditCapLimit($id);       
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/editcaplimit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update Cap Limit -----------------------------
	public function updateCapLimit($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_caplimit';
		$data['title'] = "Edit Cap Limit";		
		$setcaplimit =$this->input->post('setcaplimit');

		$data =array(
			'caplimit' 	  => 	$setcaplimit,
		);		
		$this->VoucherModel->update($tbl,$data,$where);
		$this->session->set_flashdata('caplimitmsg', array('message' => 'Cap limit has been updated successfully.','class' => 'alert alert-success'));
		redirect('site/core/micro/site/lib/controller/type/caplimitlist');
		
	}
	
	public function deleteCapLimitList($id){
		$tbl= 'tbl_caplimit';
		$this->VoucherModel->truncateCaplimit($tbl);
		$this->session->set_flashdata('caplimitmsg', array('message' => 'Cap limit has been deleted successfully','class' => 'alert alert-success'));
		redirect("site/core/micro/site/lib/controller/type/caplimitlist");		
	}

	public function VoucherUpdateDate(){
		$data['title'] = "Update Voucher Date"; 
		$data['batchType'] = $this->VoucherModel->getBatchType();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/voucherupdatedate',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function VoucherDateUpdate(){
		//$productname =$this->input->post('productname');
		$stdate =$this->input->post('startdate');
		$date =$this->input->post('expirydate');
		$expirydate = date("Y-m-d", strtotime($date));
		$startdate = date("Y-m-d", strtotime($stdate));

		//$this->form_validation->set_rules('productname', 'domainname', 'required');
		$this->form_validation->set_rules('startdate', 'startdate', 'required');
		$this->form_validation->set_rules('expirydate', 'expirydate', 'required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('voucherdateUp', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherupdatedate");
		}	else {
			$tbl= 'tbl_voucher_mng';
			$this->VoucherModel->updateStartExpiryDate($tbl,$startdate,$expirydate);
			$this->session->set_flashdata('voucher', array('message' => 'Voucher date has been updated successfully.','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		} 
	}

	public function voucherLastDateToBook(){
		//$domainname1 =$this->input->post('domainname1');
		$lastbookdate =$this->input->post('lastdatetobook');
		$lastdatetobook = date("Y-m-d", strtotime($lastbookdate));

		//$this->form_validation->set_rules('domainname1', 'domainname1', 'required');
		$this->form_validation->set_rules('lastdatetobook', 'lastdatetobook', 'required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('lastdatetobook', array('message' => 'Field are required!!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherupdatedate");
		}	else {
			$tbl= 'tbl_voucher_mng';
			$this->VoucherModel->updateLastDateToBook($tbl,$lastdatetobook);
			$this->session->set_flashdata('voucher', array('message' => 'Voucher Last Date has been updated successfully.!!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		}
	}

	public function multipleDelete(){
		$tblname = 'tbl_voucher_mng';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->VoucherModel->multiplDeleteByID($id[$count],$tblname);
	   	}
	  }
	}

	public function batchWiseVoucherDateUpdate(){
		$batchtype 		=	$this->input->post('batchtype');
		$strtdate 		= $this->input->post('strtdate');
		$expdate 			= $this->input->post('expdate');		
		$startdate 		= date("Y-m-d", strtotime($strtdate));
		$expirydate 	= date("Y-m-d", strtotime($expdate));

		$this->form_validation->set_rules('batchtype', 'batchtype', 'required');
		$this->form_validation->set_rules('strtdate', 'strtdate', 'required');
		$this->form_validation->set_rules('expdate', 'expdate', 'required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('voucherdateUp2', array('message' => 'Field are required!!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherupdatedate");
		}	else {
			$tbl= 'tbl_voucher_mng';
			$this->VoucherModel->updateBatchWiseLastDateToBook($tbl,$batchtype,$startdate,$expirydate);
			$this->session->set_flashdata('voucher', array('message' => 'Voucher Date has been updated successfully.!!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		}
	}

	public function voucherWiseDateUpdate(){
		$vouchercode 		=	$this->input->post('vouchercode');
		$strtdate1 		= $this->input->post('strtdate1');
		$expdate1 			= $this->input->post('expdate1');		
		$startdate 		= date("Y-m-d", strtotime($strtdate1));
		$expirydate 	= date("Y-m-d", strtotime($expdate1));

		$this->form_validation->set_rules('vouchercode', 'vouchercode', 'required');
		$this->form_validation->set_rules('strtdate1', 'strtdate1', 'required');
		$this->form_validation->set_rules('expdate1', 'expdate1', 'required');

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('voucherdateUp1', array('message'=>'Field are required!!!', 'class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/voucherupdatedate");
		}	else {
			$tbl= 'tbl_voucher_mng';
			$allVoucher = explode(',', $vouchercode);

			foreach ($allVoucher as $key => $value) {
				$vid = $this->VoucherModel->getVoucherById($tbl,$value);
				$res = $this->VoucherModel->updateVoucherCodeDateUPDATE($tbl,$vid->id,$startdate,$expirydate); 
			}
			$this->session->set_flashdata('voucher', array('message' => 'Voucher Date has been updated successfully.!!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/voucherList");
		}
	}
	
	public function getBatchDates(){
		if($this->input->post('batchtype')) {
   		$getBatchDate = $this->VoucherModel->fetchDatesForBatches($this->input->post('batchtype'));
   		$json['strtdate'] = $getBatchDate->startdate;
   		$json['expdate'] 	= $getBatchDate->expirydate;
   		echo json_encode($json); die;
  	}
	}
	
	public function unblockAndUnusedView(){
		$data['title'] = "Unblock And Unused Voucher";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/voucher/unblockandunusedview',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function unblockUnusedVoucherUpdate(){
		$vcode =$this->input->post('vcode');
		$this->form_validation->set_rules('vcode', 'vcode', 'required');
		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/unblockunusedvoucherview");
		}	else {
			$tbl= 'tbl_voucher_mng';
			$nvcode = strtoupper($vcode);
			$result = $this->VoucherModel->getVoucherDetailsByVoucherCode($tbl,$nvcode);
			if (isset($result) && $result->mobile!="") {
				$newVoucherInsert = array(
					'product_id' 			=>  $result->product_id,
					'vouchercode' 	  => 	$result->vouchercode,
					'salt'						=>  SALT,
					'startdate'				=>	$result->startdate,
					'expirydate'			=>  $result->expirydate,
					'lastdatetobook'  =>  $result->lastdatetobook,
					'status'          => '1',
					'batchname'				=> $result->batchname,
				);

				$this->VoucherModel->insert_Encrypt($tbl,$newVoucherInsert);
				$whereforvoucher = array(
					'id'=> $result->id
				);
				$this->VoucherModel->delete($tbl,$whereforvoucher);
				$wherefororder = array(
					'voucher_id'=> $result->id
				);
				$tblorder = "tbl_orders";
				$this->VoucherModel->delete($tblorder,$wherefororder);

				$this->session->set_flashdata('voucher', array('message' => 'Voucher has been unblock/unused successfully. You can use.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/voucherList");
			} else {
				$this->session->set_flashdata('voucher', array('message' => 'Voucher has been unblock/unused successfully. You can use.','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/unblockunusedvoucherview");
			}

		} 
	}


}