<?php
defined('BASEPATH') || exit('No direct script access allowed');
class LogoutController extends CI_Controller {
  function __construct()  {
    parent::__construct();	
  }

	public function index()	{		
		/*$this->session->unset_userdata['logged_in'];
		$this->session->set_userdata('logged_in',array());
		@session_destroy();
		$data['invalidupass'] = "You have successfully logout";
		$data['pageName'] = $this->router->fetch_method();
		$this->load->view('template/logout',$data);*/
		$data['title'] = "Admin Login";
    	$this->session->unset_userdata('userType');
		$this->session->set_userdata(array());
		@session_destroy();
		$this->load->view('admin/logout',$data);
		//redirect('logout');
	
	}
}



?>
