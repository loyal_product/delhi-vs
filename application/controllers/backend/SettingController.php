<?php
defined('BASEPATH') || exit('No direct script access allowed');

class SettingController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('SettingModel');
   	$this->load->model('ProductModel');
   	$this->load->model('VoucherModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->library("excel");   //excel download
	}


	# login page 
	public function index(){
		$data['title'] = "General Setting";
		$data['generalsettingdata'] = $this->SettingModel->getSettingData();
		$data['timesslotdata'] = $this->SettingModel->getTimeSlotDatas();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/generalsetting',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function generalSettingInsert() {
		$data['title'] 						  = "General Setting";
		$postImg 							  = $this->input->post('postImg');
		/*$preferred_date 					  = $this->input->post('preferred_date');*/
		$no_of_people 						  = $this->input->post('no_of_people');
		$discount_applied 					  = $this->input->post('discount_applied');
		$sitetitle 							  = $this->input->post('sitetitle');
		$sitedesc 							  = $this->input->post('sitedesc');
		$setruleformobandemail 				  = $this->input->post('setruleformobandemail');
		$voucherperday						  = $this->input->post('voucherperday');
		$voucherperweek						  = $this->input->post('voucherperweek');
		$voucherpermonth					  = $this->input->post('voucherpermonth');
		$voucherduringpromotion				  = $this->input->post('voucherduringpromotion');
		$errormessage2voucherperday 	      = $this->input->post('errormessage2voucherperday');
		$errormessage5voucherperweek 	      = $this->input->post('errormessage5voucherperweek');
		$errormessage10voucherpermonth 	      = $this->input->post('errormessage10voucherpermonth');
		$errormessage15voucherduringpromotion = $this->input->post('errormessage15voucherduringpromotion');
		$enabledisablecity 					  = $this->input->post('enabledisablecity');
		$enabledisablevenue 				  = $this->input->post('enabledisablevenue');
		$blockdate 							  = $this->input->post('chooseblockdate');
		$screenrefresh 						  = $this->input->post('screenrefresh');
		$daysavailibility 					  = $this->input->post('daysavailibility');
		$chooseblockdate 					  = json_encode($blockdate);
		//$weekenddisableenable               = $this->input->post('weekenddisableenable');
		$registrationsendermail               = $this->input->post('registrationsendermail');
		$bookingsuccesssendermail             = $this->input->post('bookingsuccesssendermail');
		/*$calenderblockdate     			  = $this->input->post('calenderblockdate');
		$calenderblockjson 					  = json_encode($calenderblockdate);*/		
		$accountnumberrule 					  = $this->input->post('accountnumberrule');
		$uparule 							  = $this->input->post('uparule');
		$ewalletsrule 						  = $this->input->post('ewalletsrule');
		$campaginidforcashbackapi 		      = $this->input->post('campaginidforcashbackapi');
		$campaginidforcrm 					  = $this->input->post('campaginidforcrm');
		$googleanalyticscode 				  = $this->input->post('googleanalyticscode');
		$chatcode 							  = $this->input->post('chatcode');
		$emailrequireornotforregistration     = $this->input->post('emailrequireornotforregistration');
		$setsalondiscount 				      = $this->input->post('setsalondiscount');
		$salonflatdiscount					  = $this->input->post('salonflatdiscount');
		$salonnormaldiscount				  = $this->input->post('salonnormaldiscount');
		//-----------------------------------------------------------------------------------//
		$API_entityID                         = $this->input->post('entity_id');
		$API_OTP_TemplateID                   = $this->input->post('otp_template_id');
		$API_Registration_TemplateID          = $this->input->post('reg_template_id');
		$API_Booking_TemplateID               = $this->input->post('booking_template_id');
		//-----------------------------------------------------------------------------------//

		$file_name = $_FILES["logoimg"]["name"];
		$file_tem_loc = $_FILES["logoimg"]["tmp_name"];
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['logoimg']['name']);

	   	move_uploaded_file($_FILES["logoimg"]["tmp_name"],$target_path);	
	   	
	   	$this->form_validation->set_rules('sitetitle', 'sitetitle', 'required');
	   	//$this->form_validation->set_rules('logoimg', 'logoimg', 'required');

	   	if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('generalsettingmsg', array('message' => 'Field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/generalsetting");
		}	else {
	   	$tbl= 'tbl_settings';
	   	$getMetaValue = $this->SettingModel->getMetaVal($tbl);
	   	$userType = $this->session->userdata('userType');
	   	if($userType==1){
				$userid = 1;
			}	elseif($userType==3){
				$userid = 3;
			} elseif($userType==4){
				$userid = 4;
			}

	   	if ( $getMetaValue == 0 ) {
		   	$meta_data =array('logoimg','sitetitle','sitedesc','setruleformobandemail','voucherperday','voucherperweek','voucherpermonth','voucherduringpromotion','errormessage2voucherperday','errormessage5voucherperweek','errormessage10voucherpermonth','errormessage15voucherduringpromotion','enabledisablecity','enabledisablevenue','chooseblockdate','screenrefresh','registrationsendermail','bookingsuccesssendermail','accountnumberrule','uparule','ewalletsrule','campaginidforcashbackapi','campaginidforcrm','googleanalyticscode','chatcode','discountapplied','noofpeople','preferreddate','setsalondiscount','salonflatdiscount','salonnormaldiscount','emailrequireornotforregistration','entity_id','otp_template_id','registration_template_id','booking_template_id');
		   	
		   	foreach ($meta_data as $key => $value) {
		   		$data = array('userid'=>$userid,'theme_id'=>'','meta_key' => $value);
		   		$this->SettingModel->insert($tbl,$data);
		   	}
		   	
				$data =array(					
					'logoimg' 	  		 				   => $file_store,
					'sitetitle' 	 					   => $sitetitle,
					'sitedesc' 	   						   => $sitedesc,
					'setruleformobandemail' 			   => $setruleformobandemail,
					'voucherperday' 					   => $voucherperday,
					'voucherperweek' 					   => $voucherperweek,
					'voucherpermonth' 					   => $voucherpermonth,
					'voucherduringpromotion' 			   => $voucherduringpromotion,
					'errormessage2voucherperday'	  	   => $errormessage2voucherperday,
					'errormessage5voucherperweek' 	       => $errormessage5voucherperweek,
					'errormessage10voucherpermonth'        => $errormessage10voucherpermonth,
					'errormessage15voucherduringpromotion' => $errormessage15voucherduringpromotion,
					'enabledisablecity' 				   => $enabledisablecity,
					'enabledisablevenue' 				   => $enabledisablevenue,
					'chooseblockdate' 					   => $chooseblockdate,
					'screenrefresh' 					   => $screenrefresh,
					//'weekenddisableenable'			   => $weekenddisableenable,	
					'registrationsendermail'			   => $registrationsendermail,	
					'bookingsuccesssendermail'  		   => $bookingsuccesssendermail,
					//'calenderblockdates' 				   => $calenderblockjson,	
					'accountnumberrule' 				   => $accountnumberrule,
					'uparule' 							   => $uparule,
					'ewalletsrule' 						   => $ewalletsrule,
					'campaginidforcashbackapi' 			   => $campaginidforcashbackapi,
					'campaginidforcrm' 					   => $campaginidforcrm,
					'googleanalyticscode' 				   => $googleanalyticscode,
					'chatcode'							   => $chatcode,
					'emailrequireornotforregistration'     => $emailrequireornotforregistration,
					'noofpeople'						   => $no_of_people,
					/*'preferreddate'					   => $preferred_date,*/
					'discountapplied'					   => $discount_applied,
					'setsalondiscount'					   => $setsalondiscount,
					'salonflatdiscount'					   => $salonflatdiscount,
					'salonnormaldiscount'				   => $salonnormaldiscount,
					//---------------------------------------------------------------//
					'entity_id'                            => $API_entityID,
					'otp_template_id'                      => $API_OTP_TemplateID,
					'registration_template_id'             => $API_Registration_TemplateID,
					'booking_template_id'                  => $API_Booking_TemplateID,
					//---------------------------------------------------------------//
				);

				$result = $this->SettingModel->getMetaUpdate($tbl,$data,$userid);
				//$this->SettingModel->insertTimeSlot($timeslotdata);
				if($result)	{
					$this->session->set_flashdata('domainlisting', array('message' => 'General Setting has been added successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/generalsetting");
				} else {
					$this->session->set_flashdata('generalsettingmsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/generalsetting");
				}
			} else {
				if($file_store!='') {
					//echo "Post Image not Blank!!"; die;
					$data =array(					
						'logoimg' 	  		 				   => $file_store,
						'sitetitle' 	 					   => $sitetitle,
						'sitedesc' 	   						   => $sitedesc,
						'setruleformobandemail' 			   => $setruleformobandemail,
						'voucherperday'                        => $voucherperday,
						'voucherperweek'                       => $voucherperweek,
						'voucherpermonth' 					   => $voucherpermonth,
						'voucherduringpromotion' 			   => $voucherduringpromotion,
						'errormessage2voucherperday'	 	   => $errormessage2voucherperday,
						'errormessage5voucherperweek' 		   => $errormessage5voucherperweek,
						'errormessage10voucherpermonth' 	   => $errormessage10voucherpermonth,
						'errormessage15voucherduringpromotion' => $errormessage15voucherduringpromotion,
						'enabledisablecity' 				   => $enabledisablecity,
						'enabledisablevenue' 				   => $enabledisablevenue,
						'chooseblockdate' 					   => $chooseblockdate,
						'screenrefresh' 					   => $screenrefresh,
						//'weekenddisableenable'			   => $weekenddisableenable,	
						'registrationsendermail'			   => $registrationsendermail,	
						'bookingsuccesssendermail'  		   => $bookingsuccesssendermail,
						//'calenderblockdates' 				   => $calenderblockjson,	
						'accountnumberrule' 				   => $accountnumberrule,
						'uparule' 							   => $uparule,
						'ewalletsrule' 						   => $ewalletsrule,	
						'campaginidforcashbackapi' 			   => $campaginidforcashbackapi,
						'campaginidforcrm' 					   => $campaginidforcrm,
						'googleanalyticscode' 				   => $googleanalyticscode,
						'chatcode'							   => $chatcode,
						'emailrequireornotforregistration'     => $emailrequireornotforregistration,
						'noofpeople'						   => $no_of_people,
						/*'preferreddate'					   => $preferred_date,*/
						'discountapplied'					   => $discount_applied,
						'setsalondiscount'					   => $setsalondiscount,
						'salonflatdiscount'					   => $salonflatdiscount,
						'salonnormaldiscount'				   => $salonnormaldiscount,
						//---------------------------------------------------------------//
						'entity_id'                            => $API_entityID,
						'otp_template_id'                      => $API_OTP_TemplateID,
						'registration_template_id'             => $API_Registration_TemplateID,
						'booking_template_id'                  => $API_Booking_TemplateID,
						//---------------------------------------------------------------//
					);
				} else {
					//echo "Post Image Blank!!"; die;
					$data =array(			
						'logoimg' 	  		 				   => $postImg,
						'sitetitle' 	 					   => $sitetitle,
						'sitedesc' 	   						   => $sitedesc,
						'setruleformobandemail' 			   => $setruleformobandemail,
						'voucherperday' 					   => $voucherperday,
						'voucherperweek' 					   => $voucherperweek,
						'voucherpermonth' 					   => $voucherpermonth,
						'voucherduringpromotion' 			   => $voucherduringpromotion,
						'errormessage2voucherperday'	 	   => $errormessage2voucherperday,
						'errormessage5voucherperweek' 	       => $errormessage5voucherperweek,
						'errormessage10voucherpermonth'        => $errormessage10voucherpermonth,
						'errormessage15voucherduringpromotion' => $errormessage15voucherduringpromotion,
						'enabledisablecity' 				   => $enabledisablecity,
						'enabledisablevenue' 				   => $enabledisablevenue,
						'chooseblockdate' 					   => $chooseblockdate,
						'screenrefresh' 					   => $screenrefresh,
						//'weekenddisableenable' 			   => $weekenddisableenable,							
						'registrationsendermail'			   => $registrationsendermail,	
						'bookingsuccesssendermail'  		   => $bookingsuccesssendermail,
						//'calenderblockdates' 				   => $calenderblockjson,
						'accountnumberrule' 				   => $accountnumberrule,
						'uparule' 							   => $uparule,
						'ewalletsrule' 						   => $ewalletsrule,			
						'campaginidforcashbackapi' 			   => $campaginidforcashbackapi,
						'campaginidforcrm' 					   => $campaginidforcrm,
						'googleanalyticscode' 				   => $googleanalyticscode,
						'chatcode'							   => $chatcode,
						'emailrequireornotforregistration'     => $emailrequireornotforregistration,
						'noofpeople'						   => $no_of_people,
						/*'preferreddate'					   => $preferred_date,*/
						'discountapplied'					   => $discount_applied,
						'setsalondiscount'					   => $setsalondiscount,
						'salonflatdiscount'					   => $salonflatdiscount,
						'salonnormaldiscount'				   => $salonnormaldiscount,
						//---------------------------------------------------------------//
						'entity_id'                            => $API_entityID,
						'otp_template_id'                      => $API_OTP_TemplateID,
						'registration_template_id'             => $API_Registration_TemplateID,
						'booking_template_id'                  => $API_Booking_TemplateID,
						//---------------------------------------------------------------//
					);

				}			

				$result = $this->SettingModel->getMetaUpdate($tbl,$data,$userid);
				//$this->SettingModel->updateTimeSlot($timeslotdata);
				if($result)	{
					$this->session->set_flashdata('domainlisting', array('message' => 'General Setting has been updated successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/generalsetting");
				}
			}
		}
	}

	public function CityVenueList(){
		$data['title'] = "City & Venue Upload";
		/*$city_meta_data = array('cityED');
		$city_Data = $this->SettingModel->getSiteLists($city_meta_data);
		$data['city_Data'] = $city_Data;

		$venue_meta_data = array('venueED');
		$venue_Data = $this->SettingModel->getSiteLists($venue_meta_data);
		$data['venue_Data'] = $venue_Data;*/
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['citiesList'] = $this->SettingModel->getCityList();

		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/cityvenueupload', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function cityUploadCSV() {
		$data['title'] = "City Upload";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/cityuploads', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function venueUploadCSV() {
		$data['title'] = "Venue Upload";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		//$data['citiesList'] = $this->SettingModel->getCityList();
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/venueuploads', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function cityInsert() {
		$data['title'] 	= "City Insert";
		$cityname 			= $this->input->post('cityname');
		$crm_city_id 		= $this->input->post('crm_city_id');
		$status 				= $this->input->post('status');
		$prod_id 				= $this->input->post('pid');
		$state_id 				= $this->input->post('state_id');

		$tbl = 'tbl_cities';
		foreach ($cityname as $key => $value) {
			$data =array(
				'prod_id' => $prod_id,
				'state_id' => $state_id,
				'name'	=> $value,
				'status' => $status,
				'crm_city_id' => $crm_city_id,
				);
			$result = $this->SettingModel->insert($tbl,$data);
		}
		if($result)	{
			$this->session->set_flashdata('citymsg', array('message' => 'City has been added successfully.','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/citylisting");
		} else {
			$this->session->set_flashdata('citymsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
			redirect("site/core/micro/site/lib/controller/type/citylisting");
		}
	}

	//State Insert

	public function stateInsert() {
		$data['title'] = "State Insert";
		//Microsite Type Id
		
		$pid 		= $this->input->post('pid');

		$statename 		= $this->input->post('statename');

		$status 		= $this->input->post('status');

		$crm_state_id 		= $this->input->post('crm_state_id');

		/*$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('statename', 'statename', 'required');*/
		
		
		/*if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('citymsg', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cityvenue");
		}	else {*/
			$tbl = 'tbl_states';

			foreach ($statename as $key => $value) {
				$data =array(
						'name'	=> $value,
						
						'prod_id' => $pid,
						'status' => $status,
						'crm_state_id' => $crm_state_id,
					);
				$result = $this->SettingModel->insert($tbl,$data);
			}
			if($result)	{
				$this->session->set_flashdata('citymsg', array('message' => 'State has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/statelisting");
			} else {
				$this->session->set_flashdata('citymsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/cityvenue");
			}
		// }
	}

	//state Add

	public function stateAdd() {

        
		
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['title'] = "State Add"; 
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/stateadd',$data);
		$this->load->view('admin/common/footer',$data);
	}

	//State Listing

	public function StateListing() {	
	 	$list = $this->SettingModel->getStateListCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/statelisting";
		$config["total_rows"] = $list;
	    $config["per_page"] = 50;
	    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    	$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
	    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
	    $data['startFrom']=$startFrom;
	    $data['stateListsss'] = $this->SettingModel->getStateListDetailsLimit($startFrom, $config["per_page"]);	 
	    $data['offerlists'] = $this->SettingModel->getOfferLists();

		$data['title'] = "State Listing";
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/statelists', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function venueInsert() {
		$data['title']      = "Venue Insert";
		$pid 		        = $this->input->post('pid');
		$state_id 		    = $this->input->post('state_id');
		$city_id 		    = $this->input->post('city_id');
		$venuename 		    = $this->input->post('venuename');
		
		$price_per_person   = $this->input->post('price_per_person');
		$status 		    = $this->input->post('status');
		$crm_venue_id 		= $this->input->post('crm_venue_id');
        $monday_veg_price   = $this->input->post('monday_veg_price');
        $tuesday_veg_price  = $this->input->post('tuesday_veg_price');
        $wednesday_veg_price= $this->input->post('wednesday_veg_price');
        $thursday_veg_price = $this->input->post('thursday_veg_price');
        $friday_veg_price   = $this->input->post('friday_veg_price');
        $saturday_veg_price = $this->input->post('saturday_veg_price');
        $sunday_veg_price   = $this->input->post('sunday_veg_price');

        $monday_nonveg_price      = $this->input->post('monday_nonveg_price');
        $tuesday_nonveg_price     = $this->input->post('tuesday_nonveg_price');
        $wednesday_nonveg_price   = $this->input->post('wednesday_nonveg_price');
        $thursday_nonveg_price    = $this->input->post('thursday_nonveg_price');
        $friday_nonveg_price   	  = $this->input->post('friday_nonveg_price');
        $saturday_nonveg_price    = $this->input->post('saturday_nonveg_price');
        $sunday_nonveg_price   	  = $this->input->post('sunday_nonveg_price');
       


		/*print "<pre>";
		print_r($_POST);
		die;*/

		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('city_id', 'city_id', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('citymsg', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cityvenue");
		}	else {
			$tbl = 'tbl_venues';


			foreach ($venuename as $key => $value) {
				$data =array(
					'name'					   => $value,
					'cities_id' 			   => $city_id,
					'state_id' 				   => $state_id,
					'prod_id' 				   => $pid,
					'monday_veg_price'  	   => $monday_veg_price[$key],
					'tuesday_veg_price'  	   => $tuesday_veg_price[$key],
					'wednesday_veg_price'  	   => $wednesday_veg_price[$key],
					'thursday_veg_price'  	   => $thursday_veg_price[$key],
					'friday_veg_price'  	   => $friday_veg_price[$key],
					'saturday_veg_price'  	   => $saturday_veg_price[$key],
					'sunday_veg_price'  	   => $sunday_veg_price[$key],
					'monday_nonveg_price'  	   => $monday_nonveg_price[$key],
					'tuesday_nonveg_price'     => $tuesday_nonveg_price[$key],
					'wednesday_nonveg_price'   => $wednesday_nonveg_price[$key],
					'thursday_nonveg_price'    => $thursday_nonveg_price[$key],
					'friday_nonveg_price'  	   => $friday_nonveg_price[$key],
					'saturday_nonveg_price'    => $saturday_nonveg_price[$key],
					'sunday_nonveg_price'  	   => $sunday_nonveg_price[$key],
					'status' 				   => $status,
					'crm_venue_id' 			   => $crm_venue_id[$key],
				);
				$result = $this->SettingModel->insert($tbl,$data);
			}
			if($result)	{
				$this->session->set_flashdata('citymsg', array('message' => 'Venue has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/venuelisting");
			} else {
				$this->session->set_flashdata('citymsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/cityvenue");
			}
		}
	}

	public function CityListUpload() {
    $cityData = array();
    $status = $this->input->post('status');
    $pid = $this->input->post('pid');
    // If file uploaded
    if(is_uploaded_file($_FILES['file_city']['tmp_name'])) {
      // Load CSV reader library
      $this->load->library('CSVReader');
      // Parse data from CSV file
      $csvData = $this->csvreader->parse_csv($_FILES['file_city']['tmp_name']);
      
      // Insert/update CSV data into database
      if(!empty($csvData)){
        foreach($csvData as $key=>$row){                   
          // Prepare data for DB insertion
          $cityData = array(
          	'prod_id' 	=> $pid,
            'name' 			=> $row['city'],
            'crm_city_id' => $row['crm_city_id'],
            'state_id' => $row['state_id'],
            'status'		=> $status,
          );          
          $vouch_tbl = 'tbl_cities';
          // Insert city data
          $insert = $this->SettingModel->insertCSV($vouch_tbl,$cityData);              
        }	              
        $this->session->set_flashdata('citymsg', array('message' => 'City has been imported successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/cityvenue");
      }
    } else {
      $this->session->set_flashdata('citymsg', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cityuploadcsv");
    }
	}

	public function VenueListUpload() {
    $venueData = array();
    $pid = $this->input->post('pid');
    $city_id = $this->input->post('city_id');
    $state_id = $this->input->post('state_id');
    
    $status = $this->input->post('status');
    // If file uploaded
    if(is_uploaded_file($_FILES['file_venue']['tmp_name'])){
      // Load CSV reader library
      $this->load->library('CSVReader');
      // Parse data from CSV file
      $venueData = $this->csvreader->parse_csv($_FILES['file_venue']['tmp_name']);
      
      // Insert/update CSV data into database
      if(!empty($venueData)){
        foreach($venueData as $key=>$row){                   
          // Prepare data for DB insertion
          $venueData = array(
            'name' 		   			   => $row['venue'],
            'cities_id'	   			   => $city_id, 
            'crm_venue_id' 			   => $row['crm_venue_id'],
            'state_id'     			   => $state_id,
            'monday_veg_price'  	   => $row['monday_veg_price'],
			'tuesday_veg_price'  	   => $row['tuesday_veg_price'],
			'wednesday_veg_price'  	   => $row['wednesday_veg_price'],
			'thursday_veg_price'  	   => $row['thursday_veg_price'],
			'friday_veg_price'  	   => $row['friday_veg_price'],
			'saturday_veg_price'  	   => $row['saturday_veg_price'],
			'sunday_veg_price'  	   => $row['sunday_veg_price'],
			'monday_nonveg_price'  	   => $row['monday_nonveg_price'],
			'tuesday_nonveg_price'     => $row['tuesday_nonveg_price'],
			'wednesday_nonveg_price'   => $row['wednesday_nonveg_price'],
			'thursday_nonveg_price'    => $row['thursday_nonveg_price'],
			'friday_nonveg_price'  	   => $row['friday_nonveg_price'],
			'saturday_nonveg_price'    => $row['saturday_nonveg_price'],
			'sunday_nonveg_price'  	   => $row['sunday_nonveg_price'],
			'monday_male_price'  	   => $row['monday_male_price'],
			'tuesday_male_price'  	   => $row['tuesday_male_price'],
			'wednesday_male_price'     => $row['wednesday_male_price'],
			'thursday_male_price'  	   => $row['thursday_male_price'],
			'friday_male_price'  	   => $row['friday_male_price'],
			'saturday_male_price'  	   => $row['saturday_male_price'],
			'sunday_male_price'  	   => $row['sunday_male_price'],
			'monday_female_price'  	   => $row['monday_female_price'],
			'tuesday_female_price'     => $row['tuesday_female_price'],
			'wednesday_female_price'   => $row['wednesday_female_price'],
			'thursday_female_price'    => $row['thursday_female_price'],
			'friday_female_price'  	   => $row['friday_female_price'],
			'saturday_female_price'    => $row['saturday_female_price'],
			'sunday_female_price'  	   => $row['sunday_female_price'],
			'prod_id' 	   			   => $pid,                  
            'status'	   			   => $status, 
          );          
          $venues_tbl = 'tbl_venues';
          // Insert venue data
          $insert = $this->SettingModel->insertCSV($venues_tbl,$venueData);
        }	              
        $this->session->set_flashdata('citymsg', array('message' => 'Venue has been imported successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/venuelisting");
      }
    } else {
      $this->session->set_flashdata('citymsg', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/venueuploadcsv");
    }
	}

  public function CityListing() {	
	
	 	$list = $this->SettingModel->getCityListCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/citylisting";
		$config["total_rows"] = $list;
    $config["per_page"] = 50;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['cityListsss'] = $this->SettingModel->getCityListDetailsLimit($startFrom, $config["per_page"]);	  

		$data['title'] = "City Listing";
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/citylists', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function editCity($id,$pid) {
		$data['title'] = "Edit City";
		$data['id']=$id;
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['statesList'] = $this->SettingModel->getStateListsss($pid);
		$data['editcitiesList'] = $this->SettingModel->getEditCities($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/cityedit',$data);
		$this->load->view('admin/common/footer',$data);
	}
    public function editState($id,$pid) {
		$data['title'] = "Edit State";
		$data['id']=$id;
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['editstatesList'] = $this->SettingModel->getEditStates($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/stateedit',$data);
		$this->load->view('admin/common/footer',$data);
	}
	// Update User -----------------------------
	public function cityUpdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_cities';
		$data['title'] = "Edit City";		
		$cityname =$this->input->post('cityname');
		$state_id =$this->input->post('state_id');
		$status =$this->input->post('status'); 
		$crm_city_id =$this->input->post('crm_city_id');  	
    
		$this->form_validation->set_rules('cityname', 'cityname', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cityedit/$id");
		}	else {
			$data =array(
				'state_id' 	   		=> 	$state_id,
				'name' 	   		=> 	$cityname,					
				'status' 			=> 	$status,
				'crm_city_id' => $crm_city_id,
			);
		
			$this->SettingModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'City has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/citylisting');
		}
		
	}

    //Update State

    // Update User -----------------------------
	public function stateUpdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_states';
		$data['title'] = "Edit State";		
		$statename =$this->input->post('statename');
		$status =$this->input->post('status'); 
		$crm_state_id =$this->input->post('crm_state_id');  	
    
		$this->form_validation->set_rules('statename', 'statename', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/stateedit/$id");
		}	else {
			$data =array(
				'name' 	   		=> 	$statename,					
				'status' 			=> 	$status,
				'crm_state_id' => $crm_state_id,
			);
		
			$this->SettingModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'State has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/statelisting');
		}
		
	}	
	public function deleteCity($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_cities';
		$result =$this->SettingModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'City has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/citylisting");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/citylisting");
		}
	}

	public function VenueListing() {	
	
	 	$list = $this->SettingModel->getVenueListCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/venuelisting";
		$config["total_rows"] = $list;
    $config["per_page"] = 50;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['venueListsss'] = $this->SettingModel->getVenueListDetailsLimit($startFrom, $config["per_page"]);	 
    $data['offerlists'] = $this->SettingModel->getOfferLists();

		$data['title'] = "Venue Listing";
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/venuelists', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function editVenue($id,$pid) {
		$data['title'] = "Edit Venue";
		$data['id']=$id;
		$data['productLists'] 	= $this->ProductModel->getProductLists();
        $data['bookingFormId']  = $this->ProductModel->getBookigFormId($pid);
		$data['citiesList'] = $this->SettingModel->getCityListsss($pid);
		$data['statesList'] = $this->SettingModel->getStateListsss($pid);
		$data['editvenuesList'] = $this->SettingModel->getEditVenues($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/venueedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function venueUpdate($id){

		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_venues';
		$data['title'] = "Venue Update";		
		$city_id =$this->input->post('city_id');
		$state_id =$this->input->post('state_id');
		$venuename =$this->input->post('venuename');
		$status =$this->input->post('status');
		
		$pid =$this->input->post('pid'); 
		$crm_venue_id = $this->input->post('crm_venue_id'); 
		$monday_veg_price   = $this->input->post('monday_veg_price');
        $tuesday_veg_price  = $this->input->post('tuesday_veg_price');
        $wednesday_veg_price= $this->input->post('wednesday_veg_price');
        $thursday_veg_price = $this->input->post('thursday_veg_price');
        $friday_veg_price   = $this->input->post('friday_veg_price');
        $saturday_veg_price = $this->input->post('saturday_veg_price');
        $sunday_veg_price   = $this->input->post('sunday_veg_price');

        $monday_nonveg_price      = $this->input->post('monday_nonveg_price');
        $tuesday_nonveg_price     = $this->input->post('tuesday_nonveg_price');
        $wednesday_nonveg_price   = $this->input->post('wednesday_nonveg_price');
        $thursday_nonveg_price    = $this->input->post('thursday_nonveg_price');
        $friday_nonveg_price   	  = $this->input->post('friday_nonveg_price');
        $saturday_nonveg_price    = $this->input->post('saturday_nonveg_price');
        $sunday_nonveg_price   	  = $this->input->post('sunday_nonveg_price');

        $monday_male_price   = $this->input->post('monday_male_price');
        $tuesday_male_price  = $this->input->post('tuesday_male_price');
        $wednesday_male_price= $this->input->post('wednesday_male_price');
        $thursday_male_price = $this->input->post('thursday_male_price');
        $friday_male_price   = $this->input->post('friday_male_price');
        $saturday_male_price = $this->input->post('saturday_male_price');
        $sunday_male_price   = $this->input->post('sunday_male_price');

        $monday_female_price      = $this->input->post('monday_female_price');
        $tuesday_female_price     = $this->input->post('tuesday_female_price');
        $wednesday_female_price   = $this->input->post('wednesday_female_price');
        $thursday_female_price    = $this->input->post('thursday_female_price');
        $friday_female_price   	  = $this->input->post('friday_female_price');
        $saturday_female_price    = $this->input->post('saturday_female_price');
        $sunday_female_price   	  = $this->input->post('sunday_female_price');
        /*print "<pre>";
		print_r($_POST);
		die;*/
        $this->form_validation->set_rules('city_id', 'city_id', 'required');
		$this->form_validation->set_rules('venuename', 'venuename', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/venueedit/$id");
		}	else {
			$data =array(
				'name' 	   		   		   => $venuename,					
				'cities_id' 	   		   => $city_id,
				'state_id' 	       		   => $state_id,	
				'prod_id' 		   		   => $pid,	
				'status' 		   		   => $status,
				'crm_venue_id'     		   => $crm_venue_id,
				'monday_veg_price'  	   => $monday_veg_price,
				'tuesday_veg_price'  	   => $tuesday_veg_price,
				'wednesday_veg_price'  	   => $wednesday_veg_price,
				'thursday_veg_price'  	   => $thursday_veg_price,
				'friday_veg_price'  	   => $friday_veg_price,
				'saturday_veg_price'  	   => $saturday_veg_price,
				'sunday_veg_price'  	   => $sunday_veg_price,
				'monday_nonveg_price'  	   => $monday_nonveg_price,
				'tuesday_nonveg_price'     => $tuesday_nonveg_price,
				'wednesday_nonveg_price'   => $wednesday_nonveg_price,
				'thursday_nonveg_price'    => $thursday_nonveg_price,
				'friday_nonveg_price'  	   => $friday_nonveg_price,
				'saturday_nonveg_price'    => $saturday_nonveg_price,
				'sunday_nonveg_price'  	   => $sunday_nonveg_price,
				'monday_male_price'  	   => $monday_male_price,
				'tuesday_male_price'  	   => $tuesday_male_price,
				'wednesday_male_price'     => $wednesday_male_price,
				'thursday_male_price'  	   => $thursday_male_price,
				'friday_male_price'  	   => $friday_male_price,
				'saturday_male_price'  	   => $saturday_male_price,
				'sunday_male_price'  	   => $sunday_male_price,
				'monday_female_price'  	   => $monday_female_price,
				'tuesday_female_price'     => $tuesday_female_price,
				'wednesday_female_price'   => $wednesday_female_price,
				'thursday_female_price'    => $thursday_female_price,
				'friday_female_price'  	   => $friday_female_price,
				'saturday_female_price'    => $saturday_female_price,
				'sunday_female_price'  	   => $sunday_female_price,
			);
		
			$this->SettingModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'Venue has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/venuelisting');
		}
		
	}
	
	public function deleteVenue($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_cities';
		$result =$this->SettingModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Venue has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/venuelisting");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/venuelisting");
		}
	}

	public function deleteState($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_states';
		$result =$this->SettingModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'State has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/statelisting");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/statelisting");
		}
	}

	public function venueUploadingBulk(){
		$data['title'] = "Bulk Venue Upload";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/venueuploadingbulk',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function venueUploadingBulkInsert(){

		$venueData = array();
		$pid =$this->input->post('pid');
        if(is_uploaded_file($_FILES['file_venue']['tmp_name'])){
            $this->load->library('CSVReader');
		    $venueData = $this->csvreader->parse_csv($_FILES['file_venue']['tmp_name']);

		    // Insert/update CSV data into database
		    if(!empty($venueData)){

	        foreach($venueData as $key=>$row){                   
	          // Prepare data for DB insertion
	          $venueData = array(

	          	'cities_id'		           => $row['cityid'], 
	            'name' 			           => $row['venue'],
	            
	            'crm_venue_id'             => $row['crm_venue_id'],
	            'state_id'                 => $row['state_id'],
	            'monday_veg_price'  	   => $row['monday_veg_price'],
				'tuesday_veg_price'  	   => $row['tuesday_veg_price'],
				'wednesday_veg_price'  	   => $row['wednesday_veg_price'],
				'thursday_veg_price'  	   => $row['thursday_veg_price'],
				'friday_veg_price'  	   => $row['friday_veg_price'],
				'saturday_veg_price'  	   => $row['saturday_veg_price'],
				'sunday_veg_price'  	   => $row['sunday_veg_price'],
				'monday_nonveg_price'  	   => $row['monday_nonveg_price'],
				'tuesday_nonveg_price'     => $row['tuesday_nonveg_price'],
				'wednesday_nonveg_price'   => $row['wednesday_nonveg_price'],
				'thursday_nonveg_price'    => $row['thursday_nonveg_price'],
				'friday_nonveg_price'  	   => $row['friday_nonveg_price'],
				'saturday_nonveg_price'    => $row['saturday_nonveg_price'],
				'sunday_nonveg_price'  	   => $row['sunday_nonveg_price'],
				'monday_male_price'  	   => $row['monday_male_price'],
				'tuesday_male_price'  	   => $row['tuesday_male_price'],
				'wednesday_male_price'     => $row['wednesday_male_price'],
				'thursday_male_price'  	   => $row['thursday_male_price'],
				'friday_male_price'  	   => $row['friday_male_price'],
				'saturday_male_price'  	   => $row['saturday_male_price'],
				'sunday_male_price'  	   => $row['sunday_male_price'],
				'monday_female_price'  	   => $row['monday_female_price'],
				'tuesday_female_price'     => $row['tuesday_female_price'],
				'wednesday_female_price'   => $row['wednesday_female_price'],
				'thursday_female_price'    => $row['thursday_female_price'],
				'friday_female_price'  	   => $row['friday_female_price'],
				'saturday_female_price'    => $row['saturday_female_price'],
				'sunday_female_price'  	   => $row['sunday_female_price'],
	            'status'			=> '1', 
	            'prod_id' 		           => $pid,
	          );    
	          //print_r($venueData); die(); 
	          /*print "<pre>";
		print_r($venueData);
		die;    */
	          $vouch_tbl = 'tbl_venues';
	          // Insert venue data
	          $insert = $this->SettingModel->insertCSV($vouch_tbl,$venueData);             
	        }	              
	        $this->session->set_flashdata('msgshow', array('message' => 'Venue has been imported successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/venuelisting");
	        }
	    } else {
	      $this->session->set_flashdata('msgshow', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/venueuploadingbulk");
	    }
	}

	public function downloadAllCityList(){
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array('City Id', 'City Name');
		$column = 0;
		foreach($table_columns as $field)	{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$user_data = $this->SettingModel->downloadCityExcel();
		
		$excel_row = 2;
		foreach($user_data as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);			
			$excel_row++;
		}
			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-type: application/vnd.ms-excel; charset=UTF-8');
			header('Content-Disposition: attachment;filename="CityList.xls"');
			ob_end_clean();
			$object_writer->save('php://output');

	}

	public function downloadAllStateList(){
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array('State Id', 'State Name');
		$column = 0;
		foreach($table_columns as $field)	{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$user_data = $this->SettingModel->downloadStateExcel();
		
		$excel_row = 2;
		foreach($user_data as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['id']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);			
			$excel_row++;
		}
			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-type: application/vnd.ms-excel; charset=UTF-8');
			header('Content-Disposition: attachment;filename="StateList.xls"');
			ob_end_clean();
			$object_writer->save('php://output');
	}

	public function searchByCityName(){
		$cityname =$this->input->post('cityname');
		$this->form_validation->set_rules('cityname', 'cityname', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/citylisting");
		}	else {
			$data['searchcitylist'] = $this->SettingModel->getCityName($cityname);
			
			$data['title'] = "Search By City Name"; 
			$this->load->view('admin/common/header',$data);
			$this->load->view('admin/common/sidebar',$data);
			$this->load->view('admin/settings/citylists',$data);
			$this->load->view('admin/common/footer',$data);
		}
	}

	public function multipleDelete(){
		$tblname = 'tbl_cities';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->SettingModel->multiplDeleteByID($id[$count],$tblname);
	   	}
	  }
	}

	public function multipleVenueDelete(){
		$tblname = 'tbl_venues';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->SettingModel->multiplDeleteByID($id[$count],$tblname);
	   	}
	  }
	}
    
	public function venueAdd() {
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['title'] = "Venue Add"; 
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/venueadd',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function getCityList() {
		if($this->input->post('state_id')) {
   		echo $this->SettingModel->fetch_CityList($this->input->post('state_id'));
  	}
	}

	//State list according product id

	public function getStateList() {

		if($this->input->post('pid')) {
   		echo $this->SettingModel->fetch_StateList($this->input->post('pid'));
  	}
	}

	// Settings Registration Page----------------------
	public function settingsForOfferAdd(){
		$data['title'] = "Settings For Offer Add";
		$data['productList'] = $this->VoucherModel->getProductListing();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/settingsforofferadd',$data);
		$this->load->view('admin/common/footer', $data);
	}
	
	// Settings Insert Process -------------------------
	public function settingsForOfferInsert() {
		if ($this->input->post('daysavailibility_monday') != '') {
			$daysavailibility_monday = $this->input->post('daysavailibility_monday');
    	$timeslot_monday = $this->input->post('timeslot_monday');
		}
		if ($this->input->post('daysavailibility_tuesday') != '') {
			$daysavailibility_tuesday = $this->input->post('daysavailibility_tuesday');
    	$timeslot_tuesday = $this->input->post('timeslot_tuesday');
		}
		if ($this->input->post('daysavailibility_wednesday') != '') {
			$daysavailibility_wednesday = $this->input->post('daysavailibility_wednesday');
    	$timeslot_wednesday = $this->input->post('timeslot_wednesday');
		}
    if ($this->input->post('daysavailibility_thrusday') != '') {
    	$daysavailibility_thrusday = $this->input->post('daysavailibility_thrusday');
    	$timeslot_thrusday = $this->input->post('timeslot_thrusday');
    }
    if ($this->input->post('daysavailibility_friday') != '') {
    	$daysavailibility_friday = $this->input->post('daysavailibility_friday');
    	$timeslot_friday = $this->input->post('timeslot_friday');
    }
    if ($this->input->post('daysavailibility_saturday') != '') {
    	$daysavailibility_saturday = $this->input->post('daysavailibility_saturday');
    	$timeslot_saturday = $this->input->post('timeslot_saturday');
    }
    if ($this->input->post('daysavailibility_sunday') !='') {
    	$daysavailibility_sunday = $this->input->post('daysavailibility_sunday');
    	$timeslot_sunday = $this->input->post('timeslot_sunday');
    }
    $prod_id = $this->input->post('productname');
    $calenderblockdate = $this->input->post('calenderblockdate');
    $setminimumnumberdaydisable = $this->input->post('setminimumnumberdaydisable');
    $setmaximumnumberdayenable = $this->input->post('setmaximumnumberdayenable');

		$this->form_validation->set_rules('productname','productname','required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/settingsforofferinsert");
		}	else {
			$data =array(
				'prod_id' 									=> $prod_id,
				'monday' 										=> json_encode($timeslot_monday),
	    	'tuesday' 									=> json_encode($timeslot_tuesday),
	    	'wednesday' 								=> json_encode($timeslot_wednesday),
	    	'thursday' 									=> json_encode($timeslot_thrusday),
	    	'friday' 										=> json_encode($timeslot_friday),
	    	'saturday' 									=> json_encode($timeslot_saturday),
	    	'sunday' 										=> json_encode($timeslot_sunday),
	    	'blocksdate' 								=> json_encode($calenderblockdate),
	    	'setminimumnumberdisable' 	=> $setminimumnumberdaydisable,
	    	'setmaximumnumberofenable' 	=> $setmaximumnumberdayenable,
			);
			$tbl= 'tbl_day_timeslot';
			$result = $this->SettingModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Setting has been added successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/settingsforofferlist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/settingsforofferlist");
			}
		}
  }

	// settingsForOfferList ----------
	public function settingsForOfferList(){
	
	 	$list = $this->SettingModel->getSettingsForOfferDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/settingsforofferlist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$this->pagination->initialize($config);
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
		}	else{
			$page = 1;
		}
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['settingsforofferlisting'] = $this->SettingModel->getSettingsForOfferDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Settings For Offer List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/settingsforofferlist',$data);
		$this->load->view('admin/common/footer', $data);
	}

	public function settingsForOfferEdit($id) {
		$data['title'] = "Edit Settings For Offer";
		$data['id']=$id;
		$data['productList'] = $this->VoucherModel->getProductListing();
		$data['editlists'] = $this->SettingModel->getEditSettingDetails($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/settingsforofferedit',$data);
		$this->load->view('admin/common/footer', $data);
	}

	// Update Settings -----------------------------
	public function settingsForOfferUpdate($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_day_timeslot';

		if ($this->input->post('daysavailibility_monday') != '') {
			$daysavailibility_monday = $this->input->post('daysavailibility_monday');
    	$timeslot_monday = $this->input->post('timeslot_monday');
		}
		if ($this->input->post('daysavailibility_tuesday') != '') {
			$daysavailibility_tuesday = $this->input->post('daysavailibility_tuesday');
    	$timeslot_tuesday = $this->input->post('timeslot_tuesday');
		}
		if ($this->input->post('daysavailibility_wednesday') != '') {
			$daysavailibility_wednesday = $this->input->post('daysavailibility_wednesday');
    	$timeslot_wednesday = $this->input->post('timeslot_wednesday');
		}
    if ($this->input->post('daysavailibility_thrusday') != '') {
    	$daysavailibility_thrusday = $this->input->post('daysavailibility_thrusday');
    	$timeslot_thrusday = $this->input->post('timeslot_thrusday');
    }
    if ($this->input->post('daysavailibility_friday') != '') {
    	$daysavailibility_friday = $this->input->post('daysavailibility_friday');
    	$timeslot_friday = $this->input->post('timeslot_friday');
    }
    if ($this->input->post('daysavailibility_saturday') != '') {
    	$daysavailibility_saturday = $this->input->post('daysavailibility_saturday');
    	$timeslot_saturday = $this->input->post('timeslot_saturday');
    }
    if ($this->input->post('daysavailibility_sunday') !='') {
    	$daysavailibility_sunday = $this->input->post('daysavailibility_sunday');
    	$timeslot_sunday = $this->input->post('timeslot_sunday');
    }
    $prod_id = $this->input->post('productname');
    $calenderblockdate = $this->input->post('calenderblockdate');
    $setminimumnumberdaydisable = $this->input->post('setminimumnumberdaydisable');
    $setmaximumnumberdayenable = $this->input->post('setmaximumnumberdayenable');

		$this->form_validation->set_rules('productname','productname','required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/settingsforofferedit/$id");
		}	else {
			$data =array(
				'prod_id' 									=> $prod_id,
				'monday' 										=> json_encode($timeslot_monday),
	    	'tuesday' 									=> json_encode($timeslot_tuesday),
	    	'wednesday' 								=> json_encode($timeslot_wednesday),
	    	'thursday' 									=> json_encode($timeslot_thrusday),
	    	'friday' 										=> json_encode($timeslot_friday),
	    	'saturday' 									=> json_encode($timeslot_saturday),
	    	'sunday' 										=> json_encode($timeslot_sunday),
	    	'blocksdate' 								=> json_encode($calenderblockdate),
	    	'setminimumnumberdisable' 	=> $setminimumnumberdaydisable,
	    	'setmaximumnumberofenable' 	=> $setmaximumnumberdayenable,
			);

			$result = $this->SettingModel->update($tbl,$data,$where);			
			if($result)	{				
				$this->session->set_flashdata('msgshow', array('message' => 'Setting has been updated successfully!','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/settingsforofferlist');
			}	else {	
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/settingsforofferlist');
			}
		}		
	}
	
	public function settingsForOfferDelete($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_day_timeslot';
		$result =$this->SettingModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Setting has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/settingsforofferlist");
		}	else {
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/settingsforofferlist");
		}

	}
	
	public function getCityLists(){
		if($this->input->post('offername')) {
   		echo $this->SettingModel->fetchCitiesLists($this->input->post('offername'));
   		die;
  	}
	}

	public function VenueListingAfterSearching() {
		$offerid 	= $this->input->get('offername');
		$cityid 	= $this->input->get('cityid');

	
	 	$list = $this->SettingModel->getVenueListCountsForSearch($offerid,$cityid);
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/searchbyvenuelists?offername=".$offerid."&cityid=".$cityid."";
		$config["total_rows"] = $list;
    $config["per_page"] = 50;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['venueListsss'] = $this->SettingModel->getVenueListDetailsLimitForSearch($startFrom, $config["per_page"], $offerid,$cityid);

    $data['offerlists'] = $this->SettingModel->getOfferLists();
    $data['offerid'] = $offerid;
    $data['cityid'] = $cityid;

		$data['title'] = "Venue Search Listing";
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/venuesearch', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function multipleVenueDeactivate() {
		$tblname = 'tbl_venues';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->SettingModel->multiplDeactivateByID($id[$count],$tblname);
	   	}
	  }
	}

	public function multipleVenueActivate() {
		$tblname = 'tbl_venues';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->SettingModel->multiplActivateByID($id[$count],$tblname);
	   	}
	  }
	}
	
	public function stateUploadCSV() {
		$data['title'] = "State Upload";
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$this->load->view('admin/common/header', $data);
		$this->load->view('admin/common/sidebar', $data);
		$this->load->view('admin/settings/stateuploads', $data);
		$this->load->view('admin/common/footer', $data);
	}

	public function stateUploadCSVInsert() {
    $cityData = array();
    $status = $this->input->post('status');
    $pid = $this->input->post('pid');
    // If file uploaded
    if(is_uploaded_file($_FILES['file_state']['tmp_name'])) {
      // Load CSV reader library
      $this->load->library('CSVReader');
      // Parse data from CSV file
      $csvData = $this->csvreader->parse_csv($_FILES['file_state']['tmp_name']);
      
      // Insert/update CSV data into database
      if(!empty($csvData)){
        foreach($csvData as $key=>$row){                   
          // Prepare data for DB insertion
          $cityData = array(
          	'prod_id' 	=> $pid,
            'name' 			=> $row['state'],
            'crm_state_id' => $row['crm_state_id'],
            'status'		=> $status,
          );          
          $vouch_tbl = 'tbl_states';
          // Insert city data
          $insert = $this->SettingModel->insertCSV($vouch_tbl,$cityData);              
        }	              
        $this->session->set_flashdata('citymsg', array('message' => 'State has been imported successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/statelisting");
      }
    } else {
      $this->session->set_flashdata('citymsg', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/stateuploadcsv");
    }
	}
	//sonam
	public function salonVenueInsert() {
		$data['title']      = "Salon Venue Insert";
		$pid 		        = $this->input->post('pid');
		$state_id 		    = $this->input->post('state_id');
		$city_id 		    = $this->input->post('city_id');
		$venuename 		    = $this->input->post('venuename');
		
		$status 		    = $this->input->post('status');
		$crm_venue_id 		= $this->input->post('crm_venue_id');
        $monday_male_price   = $this->input->post('monday_male_price');
        $tuesday_male_price  = $this->input->post('tuesday_male_price');
        $wednesday_male_price= $this->input->post('wednesday_male_price');
        $thursday_male_price = $this->input->post('thursday_male_price');
        $friday_male_price   = $this->input->post('friday_male_price');
        $saturday_male_price = $this->input->post('saturday_male_price');
        $sunday_male_price   = $this->input->post('sunday_male_price');

        $monday_female_price      = $this->input->post('monday_female_price');
        $tuesday_female_price     = $this->input->post('tuesday_female_price');
        $wednesday_female_price   = $this->input->post('wednesday_female_price');
        $thursday_female_price    = $this->input->post('thursday_female_price');
        $friday_female_price   	  = $this->input->post('friday_female_price');
        $saturday_female_price    = $this->input->post('saturday_female_price');
        $sunday_female_price   	  = $this->input->post('sunday_female_price');
       


		/*print "<pre>";
		print_r($_POST);
		die;*/

		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('city_id', 'city_id', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('citymsg', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/cityvenue");
		}	else {
			$tbl = 'tbl_venues';


			foreach ($venuename as $key => $value) {
				$data =array(
					'name'					   => $value,
					'cities_id' 			   => $city_id,
					'state_id' 				   => $state_id,
					'prod_id' 				   => $pid,
					'monday_male_price'  	   => $monday_male_price[$key],
					'tuesday_male_price'  	   => $tuesday_male_price[$key],
					'wednesday_male_price'     => $wednesday_male_price[$key],
					'thursday_male_price'  	   => $thursday_male_price[$key],
					'friday_male_price'  	   => $friday_male_price[$key],
					'saturday_male_price'  	   => $saturday_male_price[$key],
					'sunday_male_price'  	   => $sunday_male_price[$key],
					'monday_female_price'  	   => $monday_female_price[$key],
					'tuesday_female_price'     => $tuesday_female_price[$key],
					'wednesday_female_price'   => $wednesday_female_price[$key],
					'thursday_female_price'    => $thursday_female_price[$key],
					'friday_female_price'  	   => $friday_female_price[$key],
					'saturday_female_price'    => $saturday_female_price[$key],
					'sunday_female_price'  	   => $sunday_female_price[$key],
					'status' 				   => $status,
					'crm_venue_id' 			   => $crm_venue_id[$key],
				);
				$result = $this->SettingModel->insert($tbl,$data);
			}
			if($result)	{
				$this->session->set_flashdata('citymsg', array('message' => 'Salon Venue has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/venuelisting");
			} else {
				$this->session->set_flashdata('citymsg', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/cityvenue");
			}
		}
	}
	public function salonVenueAdd() {
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['title'] = "Salon Venue Add"; 
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/salonvenueadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
		//------------------------------------------------------------------------------------------------//
	// CIRCLE
		public function circleList(){

			$list = $this->SettingModel->getCircleListCounts();
		 	$config = array(); 
			$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/circlelist";
			$config["total_rows"] = $list;
	    	$config["per_page"] = 50;
	    	$config["uri_segment"] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string']  = TRUE;
			$config['num_links'] = $config["total_rows"];
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
				$page = $_GET['per_page'];
				$config['num_links'] = 5;
			}	else{
				$page = 1;
				$config['num_links'] = 10;
			}
			$this->pagination->initialize($config);
    		$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
		    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
		    $data['startFrom']=$startFrom;
		    $data['circleList'] = $this->SettingModel->getCircleListDetailsLimit($startFrom, $config["per_page"]);	  

			$data['title'] = "Circle List";
			$this->load->view('admin/common/header', $data);
			$this->load->view('admin/common/sidebar', $data);
			$this->load->view('admin/settings/circlelist', $data);
			$this->load->view('admin/common/footer', $data);	
		}


		public function circleAdd(){
			$data['title']        = "Add Circle Form";
			$data['productLists'] = $this->ProductModel->getProductLists();
			
			$this->load->view('admin/common/header', $data);
			$this->load->view('admin/common/sidebar', $data);
			$this->load->view('admin/settings/circle_addform', $data);
			$this->load->view('admin/common/footer', $data);
		}

		public function circleInsert() {
			$data['title'] 	= "Circle Insert";
			$circlename		= $this->input->post('circlename');
			$crm_city_id 	= $this->input->post('crm_city_id');
			$status 		= $this->input->post('status');
			$prod_id 		= $this->input->post('pid');
			$state_id 		= $this->input->post('state_id');

			$tbl = 'tbl_circle';
			foreach ($circlename as $key => $value) {
				$data =array(
					'prod_id'     => $prod_id,
					'state_id'    => $state_id,
					'name'	      => $value,
					'status'      => $status,
					'crm_city_id' => $crm_city_id,
					);
				$result = $this->SettingModel->insert($tbl,$data);
			}
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Cicle has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/circlelist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/circlelist");
			}
		}

		public function circleUpload() {
			$data['title'] = "Circle Upload";
			$data['productLists'] 	= $this->ProductModel->getProductLists();
			$this->load->view('admin/common/header', $data);
			$this->load->view('admin/common/sidebar', $data);
			$this->load->view('admin/settings/circleuploads', $data);
			$this->load->view('admin/common/footer', $data);
		}


		public function circleUploadAdd() {
		    $cityData = array();
		    $status   = $this->input->post('status');
		    $pid      = $this->input->post('pid');
		    if(is_uploaded_file($_FILES['file_city']['tmp_name'])) {
		      // Load CSV reader library
		      $this->load->library('CSVReader');
		      // Parse data from CSV file
		      $csvData = $this->csvreader->parse_csv($_FILES['file_city']['tmp_name']);
		      
		      // Insert/update CSV data into database
		      if(!empty($csvData)){
		        foreach($csvData as $key=>$row){                   
		          // Prepare data for DB insertion
		          $circleData = array(
		          	'prod_id' 	  => $pid,
		            'name' 		  => $row['circle'],
		            'crm_city_id' => $row['crm_city_id'],
		            'state_id'    => $row['state_id'],
		            'status'	  => $status,
		          );          
		          $vouch_tbl = 'tbl_circle';
		          $insert = $this->SettingModel->insertCSV($vouch_tbl,$circleData);              
		        }	              
		        $this->session->set_flashdata('circlemsg', array('message' => 'Circle has been imported successfully!','class' => 'alert alert-success'));
						redirect("site/core/micro/site/lib/controller/type/circlelist");
		      }
		    } else {
		      $this->session->set_flashdata('circlemsg', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/circleupload");
		    }
		}

	public function multipleDeleteCircle(){
		$tblname = 'tbl_circle';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->SettingModel->multiplDeleteByID($id[$count],$tblname);
	   	}
	  }
	}


	public function editCircle($id,$pid) {
		$data['title']          = "Edit Circle";
		$data['id']             = $id;
		$data['productLists'] 	= $this->ProductModel->getProductLists();
		$data['statesList']     = $this->SettingModel->getStateListsss($pid);
		$data['editcircleList'] = $this->SettingModel->getEditCircle($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/settings/circleedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function circleUpdate($id){
		$where         = array( 'id'=>$id );
		$tbl           = 'tbl_circle';
		$data['title'] = "Edit Circle";		
		$circlename    = $this->input->post('circlename');
		$state_id      = $this->input->post('state_id');
		$status        = $this->input->post('status'); 
		$crm_city_id   = $this->input->post('crm_city_id');  	
    
		$this->form_validation->set_rules('circlename', 'circlename', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/circleedit/$id");
		}	else {
			$data =array(
				'state_id' 	  => $state_id,
				'name' 	   	  => $circlename,					
				'status' 	  => $status,
				'crm_city_id' => $crm_city_id,
			);
		
			$this->SettingModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'Circle has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/circlelist');
		}
	}

	public function deleteCircle($id){ 
		$where = array( 'id'=> $id );
		$tbl= 'tbl_circle';
		$result =$this->SettingModel->delete($tbl,$where);	
		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Circle has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/circlelist");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/circlelist");
		}
	}

	public function searchByCircleName(){
		$circlename =$this->input->post('circlename');
		$this->form_validation->set_rules('circlename', 'circlename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/circlelist");
		}	else {
			$data['circleList'] = $this->SettingModel->getCircleName($circlename);
			
			$data['title'] = "Search By Circle Name"; 
			$this->load->view('admin/common/header',$data);
			$this->load->view('admin/common/sidebar',$data);
			$this->load->view('admin/settings/circlelist',$data);
			$this->load->view('admin/common/footer',$data);
		}
	}
	//------------------------------------------------------------------------------------------------//

}
