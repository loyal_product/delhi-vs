<?php
defined('BASEPATH') || exit('No direct script access allowed');

class ResendCodeController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('ResendCodeModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->helper("logo_helper");
	}


	# login page 
	public function index()	{		
		$data['title'] = "Resend Code";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/resendcode/resendcode',$data);
		$this->load->view('admin/common/footer');
	}

	public function resendCodeDetails(){
		$vouchercode = $this->input->post('vouchercode');
		$registeredemailid = $this->input->post('registeredemailid');
		$registeredmobile = $this->input->post('registeredmobile');

		$results = $this->ResendCodeModel->getvoucherDetails($vouchercode,$registeredemailid,$registeredmobile);
		
		if(count($results)>0){
			$output = '<div class="row">
	        <div class="col-sm-12">
	          <div class="card-box">
	            <h4 class="m-t-0 m-b-20 header-title"><strong>Voucher List</strong>	            
	            </h4>
	            <div class="row">
	              <div class="col-md-12">	                
	                <div class="table-responsive">
	                  <table id="datatable-buttons1" class="table table-striped table-bordered">
	                    <thead>
	                      <tr>
	                        <th scope="col" class="collapsable">S.No</th>
	                        <th scope="col" class="collapsable">Product Title</th>
	                        <th scope="col" class="collapsable">Voucher Code</th>
	                        <th scope="col" align="right" class="collapsable">Action</th>
	                      </tr>
	                    </thead>
	                  <tbody>';

								foreach ($results as $key => $value) {
									$k = $key+1;
									$output .= '<tr>
	                          <td class="collapsable">'.$k.'</td>
	                          <td class="collapsable">'.$value['producttitle'].'</td>
	                          <td class="collapsable">'.$value['reward_code'].'</td>
	                          <td align="right" class="collapsable"><a role="button" class="resendSubmit" data-id="'.base_url().'admin/resendemailsfromadmin/'.$value['id'].'/'.$value['p_id'].' " class="btn btn-default">Resend Email</a></td>
	                        </tr>';
								}
						$output .= '</tbody>
	                </table> 
	              </div>
	              </div>    
	            </div>
	          </div>
	        </div>
	      </div>'; 

	    } else {
			$output = '<div class="row">
	        <div class="col-sm-12">
	          <div class="card-box">
	            <h4 class="m-t-0 m-b-20 header-title"><strong>Voucher List</strong>
	            </h4>
	            <div class="row">
	              <div class="col-md-12">
	                
	                <div class="table-responsive">
	                  <table id="datatable-buttons1" class="table table-striped table-bordered">
	                    <thead>
	                      <tr>
	                        <th scope="col" class="collapsable">S.No</th>
	                        <th scope="col" class="collapsable">Voucher Code</th>
	                      </tr>
	                    </thead>
	                  <tbody>
	                  <tr><td colspan="2">No Record Found</td></tr>
	                  </tbody>
	                </table> 
	              </div>
	              </div>    
	            </div>
	          </div>
	        </div>
	      </div>';
	    }
      print_r($output);;
      die;
	}

	public function resendEmailsFromAdmin($uid,$pid){
		$results = $this->ResendCodeModel->getvoucherDetailsforSendMail($uid,$pid);
		
		if(count($results)>0){
			foreach ($results as $key => $vals) {
				//SMS Sent
				$dynamic_sms = $vals['productsms'];
	    	$replacements_sms = [
				    "{{Code}}" => $vals['reward_code'],
				    " " => '%20',
				    "&" => '%26',
				];

				$d_sms = strtr($dynamic_sms, $replacements_sms);
				$curl = curl_init();
		    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$vals['mobile']."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3";

			    curl_setopt_array($curl, array(
			      CURLOPT_URL => $url,
			      CURLOPT_RETURNTRANSFER => TRUE,
			      CURLOPT_ENCODING => "",
			      CURLOPT_MAXREDIRS => 10,
			      CURLOPT_TIMEOUT => 30,
			      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			      CURLOPT_CUSTOMREQUEST => "GET",)
			    );
			    $response = curl_exec($curl);
			    $err = curl_error($curl);
			    curl_close($curl);
			    if ($err) {
			      echo "cURL Error #:" . $err;
			    } else {
			    	$dynamic_mail = $vals['pro_mail'];
				    	if ($vals['reward_code']!="") {
	            	$rewards = $vals['reward_code'];
	            }
				    	$replacements = [
							    "{{ConsumerName}}" => $vals['name'],
							    "{{Code}}" => $vals['reward_code'],
							];

						$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
											  <tr>
											    <td align="center">
											    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
											      <tr>
											        <td bgcolor="#FFFFFF" style="background-color:#FFF;"><table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">
											          
											          <tr>
											            <td style="padding: 20px 30px 10px 30px;"><img src="'.base_url().'assets/frontend/img/emailer-banner.jpg" style="width:100%; max-width:700px;" /></td>
											          </tr>
											          <tr>
											            <td style="padding:0 30px 10px 30px; font-family:Arial, Helvetica, sans-serif; ">
											              <br>';
							$message .= strtr($dynamic_mail, $replacements);
							$message .= '<tr>
								            <td style="padding:10px 30px;">
								              <table border="0" cellspacing="0" cellpadding="0" style="width:100%;background-color: #fcfcfc">
								                <tr>
								                  <td valign="top" class="mcnTextContent" style="padding: 18px;color: #F2F2F2;font-family: Arial, Helvetica, sans-serif;font-size: 14px;font-weight: normal;text-align: center;">
								                    <img align="center" alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" width="69" style="max-width:150px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage">
								                  </td>
								                </tr>
								                <tr>
								                  <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">
								                        
								                    <div style="text-align: center;"><span style="font-size:10px"><em>© '.date("Y").' BigCity Promotions, All rights reserved.</em></span></div>

								                </td>
								                </tr>
								              </table>
								            </td>
								          </tr>
								        </tbody>
								      </table>
								        <!--[if mso]>
								        </td>
								        <![endif]-->
								                
								        <!--[if mso]>
								        </tr>
								        </table>
								        <![endif]-->
								            </td>
								        </tr>

								        </table>  
								      </td>
								      </tr>							      
								      
								      </table>
								    </td>
								  </tr>
								</table>';


						$to = $vals['email'];
						$subject = $vals['pro_mail_subject'];
						$this->send_Email($to, $subject, $message);						
					}
				} 
		} else {
			$this->session->set_flashdata('resendlist', array('message' => 'Mail has not been sent successfully..!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/resendcode");
		}

	}

	//send email function ------- 
	function send_Email($to, $subject, $message) {   
	  require_once(APPPATH."libraries/vendor/autoload.php");    
	  $email = new \SendGrid\Mail\Mail(); 
		$email->setfrom("no-reply@bigcityexperience.com", "BigCity");
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent(
		    "text/html", $message
		);
		$sendgrid = new \SendGrid('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');
		if($sendgrid->send($email)){
			echo "reached";
		}
		else{
			echo "not"; 
		}  
	 } 
}