<?php
defined('BASEPATH') || exit('No direct script access allowed');

class InventoryController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('InventoryModel'); 
   	$this->load->model('ProductModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->helper("logo_helper");
	}
    
	# login page 
	public function index()	{		
		$data['title'] = "Inventory List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/inventory/inventorylist',$data);
		$this->load->view('admin/common/footer',$data);
	}
	// User Registration Page----------------------
	public function inventoryadd(){
		$data['title'] = "Inventory Registration";
		$data['allProducts'] = $this->InventoryModel->getSubProductDetailsLimit();
		//$data['allProducts'] = $allProducts;
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/inventory/inventoryadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// product Insert Process -------------------------
	public function InventoryInsert() {
		$prodId 		= $this->input->post('prodId');
		$rewardcode = $this->input->post('rewardcode');

		$this->form_validation->set_rules('prodId', 'prodId', 'required');
		//$this->form_validation->set_rules('rewardcode', 'rewardcode', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('inventory', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/inventoryadd");
		}	else {
			$tbl = 'tbl_inventory';
			foreach ($rewardcode as $key => $value) {
				$data =array(
					'sub_prodid' 	   	=> $prodId,	
					'rewardcode'	=> trim($value),
					'salt' 				=> SALT,
					'isused' 			=> '0',
				);
				$result = $this->InventoryModel->insert_Encrypt($tbl,$data);
			}
			if($result)	{
				$this->session->set_flashdata('inventory', array('message' => 'Inventory has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/inventorylist");
			} else {
				$this->session->set_flashdata('inventory', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/inventorylist");
			}
		}
	}

	// User List ------------------------------------

	public function inventorylist(){
	 	$list = $this->InventoryModel->getInventoryDetails();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/inventorylist";
		$config["total_rows"] = $list;
    $config["per_page"] = 100;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
    
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

    $data['inventoryList'] = $this->InventoryModel->getInventoryDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Inventory List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/inventory/inventorylist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editinventory($id) {
		$data['title'] = "Edit Inventory";
		$data['allProducts'] = $this->InventoryModel->getSubProductDetailsLimit();
		//$data['allProducts'] = $allProducts;
		$data['id']=$id;
		$data['inventoryList'] = $this->InventoryModel->getEditInventory($id);
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/inventory/inventoryedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update Product -----------------------------
	public function inventoryupdate($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_inventory';
		$prodId 		= $this->input->post('prodId');
		$rewardcode = $this->input->post('rewardcode');

		$this->form_validation->set_rules('prodId', 'prodId', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('inventory', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/inventoryadd");
		}	else {
			$data = array(
				'sub_prodid' 	   	=> $prodId,	
				'rewardcode'	=> trim($rewardcode),
				'salt' 				=> SALT,
				'isused' 			=> '0',
			);
			$result = $this->InventoryModel->update_Encrypt($tbl,$data,$id);			
			if($result)	{				
				$this->session->set_flashdata('inventory', array('message' => 'Inventory has been updated successfully.','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/inventorylist');
			}	else {	
				$this->session->set_flashdata('inventory', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/inventorylist');
			}
		}
		
	}
	
	public function deleteinventory($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_inventory';
		$result =$this->InventoryModel->delete($tbl,$where);

		if($result)	{
			$this->session->set_flashdata('inventory', array('message' => 'Inventory has been deleted successfully.','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/inventorylist");
		}	else {	
			$this->session->set_flashdata('inventory', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/inventorylist");
		}
	}


	public function inventoryUpload(){
		$data['title'] = "Inventory Upload";
		$data['allProducts'] = $this->InventoryModel->getSubProductDetailsLimit();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/inventory/inventoryupload',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function inventoryUploadCSV(){
		$data = array();
    $memData = array();    
    $prodId = $this->input->post('prodId');
    
    // If import request is submitted
    if($prodId){
	      // If file uploaded
	      if(is_uploaded_file($_FILES['file']['tmp_name'])){
	          // Load CSV reader library
	          $this->load->library('CSVReader');
	          // Parse data from CSV file
	          $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
	          $ErrorArr = array();
	          
	          foreach($csvData as $k=>$v){
				if(empty($v['rewardcode'])){
	          		$ErrorArr[$k] = 0;
	          	}else{
	          		$ErrorArr[$k] = $v['rewardcode'];
	          	}
	          }
	          
	          if(in_array(0, $ErrorArr)){
	          	for($i=1;$i<=count($ErrorArr);$i++){
	          		if($ErrorArr[$i] === 0){
	          			$row[] = $i+1;
	          		}
	          	}
	          }
	          
	          if(empty($row)){
	            // Insert/update CSV data into database
	            if(!empty($csvData)){
                  foreach($csvData as $key=>$row){ 
                  	//$rowCount++;                  
                      // Prepare data for DB insertion
                      $memData = array(
                      	'sub_prodid' 		=> $prodId,
                      	'salt' 				=> SALT,
                        'rewardcode' 		=> trim($row['rewardcode']),
                        'isused' 			=> '0',
                        'created_on' 		=> date("Y-m-d H:i:s"),                    
                      );
                      $invent_tbl = 'tbl_inventory';
                      // Insert member data
                      $insert = $this->InventoryModel->insertCSV($invent_tbl,$memData);
    	              }	              
    	          $this->session->set_flashdata('inventory', array('message' => 'Inventory has been imported successfully!','class' => 'alert alert-success'));
    						redirect("site/core/micro/site/lib/controller/type/inventorylist");
    	        }
	          }else{
	          	$this->session->set_flashdata('inventory', array('message' => 'Blank data in row no : ( '.implode(',',$row).' )','class' => 'alert alert-danger'));
						redirect("site/core/micro/site/lib/controller/type/inventoryupload");
	          }
	      }else{
	        $this->session->set_flashdata('inventory', array('message' => 'Error on file upload, please try again.','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/inventorylist");
	      }
    }
    redirect('site/core/micro/site/lib/controller/type/inventorylist');
	}

  public function multipleDelete(){
		$tblname = 'tbl_inventory';
		if($this->input->post('checkbox_value')) {
	   	$id = $this->input->post('checkbox_value');
	   	for($count = 0; $count < count($id); $count++) {
	    	$this->InventoryModel->multiplDeleteByID($id[$count],$tblname);
	   	}
	  }
	}

}