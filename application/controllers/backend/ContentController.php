<?php
defined('BASEPATH') || exit('No direct script access allowed');

class ContentController extends MY_Controller {

	public $data = array();
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('ContentModel'); 
   	$this->load->model('MenuModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper('ckeditor_helper');
   	$this->load->helper("logo_helper");
	}

	# login page 
	public function index()	{		
		$data['title'] = "Content List";
		$data['menulist'] = $this->MenuModel->getMenuList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// User Registration Page----------------------
	public function contentadd(){
	  $data['title'] = "Add Content";
	  $data['menulist'] = $this->MenuModel->getMenuList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Voucher Insert Process -------------------------
	public function contentInsert() {
		$pagename =$this->input->post('pagename');
		$content = $this->input->post('content');

		$this->form_validation->set_rules('pagename', 'pagename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('content', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/contentadd");
		}	else {
		$getPagename = $this->ContentModel->checkPage($pagename);
			if ($getPagename->pagename == $pagename) {
				$this->session->set_flashdata('content', array('message' => 'This Page is alread exists!!','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/contentadd");
			}
			$data =array(
				'content' 	   	=> 	$content,					
				'pagename' 			=> 	$pagename,
			);
			$tbl= 'tbl_content';
			$result = $this->ContentModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('content', array('message' => 'Content has been inserted SuccessFully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/contentlist");
			} else {
				$this->session->set_flashdata('registration', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/contentlist");
			}
		}
	}

	// User List ------------------------------------

	public function contentlist(){
	 	$list = $this->ContentModel->getContentDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/contentlist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['contentlist'] = $this->ContentModel->getContentDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Content List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editcontent($id) {
		$data['title'] = "Edit Content";
		$data['id']=$id;
		$data['menulist'] = $this->MenuModel->getMenuList();
		$data['editcontentList'] = $this->ContentModel->getEditContent($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function contentupdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_content';
		$data['title'] = "Edit Content";		
		$pagename =$this->input->post('pagename');
		$content = $this->input->post('content');
   	    
		$this->form_validation->set_rules('pagename', 'pagename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('content', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editcontent/$id");
		}	else {
			$data =array(
				'content' 	   	=> 	$content,					
				'pagename' 			=> 	$pagename,
			);
		
			$this->ContentModel->update($tbl,$data,$where);
			$this->session->set_flashdata('content', array('message' => 'Content has been updated SuccessFully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/contentlist');
		}
		
	}
	
	public function deletecontent($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_content';
		$result =$this->ContentModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('content', array('message' => 'Content has been deleted SuccessFully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/contentlist");
		}	else {	
			$this->session->set_flashdata('content', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/contentlist");
		}

	}

	// User Registration Page----------------------
	public function contentaddForBooking(){
	  $data['title'] = "Add Content";
	  $data['productlist'] = $this->ContentModel->getProductList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentbookingadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Voucher Insert Process -------------------------
	public function contentInsertForBooking() {
		$pid =$this->input->post('pid');
		$content = $this->input->post('content');

		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('content', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/contentbookingadd");
		}	else {
			$data =array(
				'content' 	=> 	$content,
				'type' 			=> 	'content',					
				'prod_id' 	=> 	$pid,
			);
			$tbl= 'tbl_booking_banner_and_content';
			$result = $this->ContentModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('content', array('message' => 'Content has been inserted Successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/contentlistforbooking");
			} else {
				$this->session->set_flashdata('content', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/contentlistforbooking");
			}
		}
	}

	// User List ------------------------------------

	public function contentListForBooking(){	
	 	$list = $this->ContentModel->getContentBookingDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/contentlistforbooking";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['contentlist'] = $this->ContentModel->getContentBookingDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Content List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentbookinglist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editcontentForBooking($id) {
		$data['title'] = "Edit Content";
		$data['id']=$id;
		$data['productlist'] = $this->ContentModel->getProductList();
		$data['editcontentList'] = $this->ContentModel->getEditBookingContent($id);      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/content/contentbookingedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function contentupdateForBooking($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_booking_banner_and_content';
		$data['title'] = "Edit Content";		
		$pid =$this->input->post('pid');
		$content = $this->input->post('content');
   	    
		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('content', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editcontentforbooking/$id");
		}	else {
			$data =array(
				'content' 	=> 	$content,
				'type' 			=> 	'content',					
				'prod_id' 	=> 	$pid,
			);		
			$this->ContentModel->update($tbl,$data,$where);
			$this->session->set_flashdata('content', array('message' => 'Content has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/contentlistforbooking');
		}
		
	}
	
	public function deletecontentForBooking($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_booking_banner_and_content';
		$result =$this->ContentModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('content', array('message' => 'Content has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/contentlistforbooking");
		}	else {	
			$this->session->set_flashdata('content', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/contentlistforbooking");
		}

	}
	
	

}