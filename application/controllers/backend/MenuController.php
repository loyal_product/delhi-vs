<?php
defined('BASEPATH') || exit('No direct script access allowed');

class MenuController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('MenuModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
	}

	public function index()	{		
		$data['title'] = "Menu List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/menu/menulist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function menuadd(){
	  $data['title'] = "Add Menu";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/menu/menuadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Menu Insert Process -------------------------
	public function menuInsert() {		
		$menuname =$this->input->post('menuname');
		$menulink =$this->input->post('menulink');
		$sort_order  =$this->input->post('sort_order');
		$status =$this->input->post('status');
   	
		$this->form_validation->set_rules('menuname', 'menuname', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/menuadd");
		}	else {
			$data =array(
				'menuname' 	  => 	$menuname,					
				'menulink' 		=> 	$menulink,
				'sort_order'  =>  $sort_order,
				'status' 			=> 	$status,
			);
			$tbl= 'tbl_menu';
			$result = $this->MenuModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Menu has been updated SuccessFully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/menulist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/menulist");
			}
		}
	}

	// menu List ------------------------------------

	public function menulist(){	
	 	$list = $this->MenuModel->getMenuDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/menulist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['menulist'] = $this->MenuModel->getMenuDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Menu List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/menu/menulist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editMenu($id) {
		$data['title'] = "Edit Menu";
		$data['id']=$id;
		$data['editmenuList'] = $this->MenuModel->getEditMenu($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/menu/menuedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function menuUpdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_menu';
		$data['title'] = "Edit Menu";

		$menuname =$this->input->post('menuname');
		$menulink =$this->input->post('menulink');
		$sort_order  =$this->input->post('sort_order');
		$status =$this->input->post('status');   	
    
		$this->form_validation->set_rules('menuname', 'menuname', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editmenu/$id");
		}	else {
			$data =array(
				'menuname' 	  => 	$menuname,					
				'menulink' 		=> 	$menulink,
				'sort_order'  =>  $sort_order,
				'status' 			=> 	$status,
			);
		
			$this->MenuModel->update($tbl,$data,$where);
			$this->session->set_flashdata('msgshow', array('message' => 'Menu has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/menulist');
		}
		
	}
	
	public function deleteMenu($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_menu';
		$result =$this->MenuModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Menu has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/menulist");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/menulist");
		}
	}

	

}