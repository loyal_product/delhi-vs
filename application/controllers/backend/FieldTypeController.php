<?php
defined('BASEPATH') || exit('No direct script access allowed');

class FieldTypeController extends MY_Controller {

	public $data = array();
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();		
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('FieldTypeModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper("logo_helper");
   	$this->load->dbforge();
	}

	// User Registration Page----------------------
	public function createDynamicForm(){
	  $data['title'] = "Create Dynamic Type";	  
	  $data['fieldlist'] = $this->FieldTypeModel->getFieldsList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/dynamicfield/fieldtypeadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Voucher Insert Process -------------------------
	public function insertDynamicForm() {		
		$pagename 		= $this->input->post('pagename');
		$fieldid 			= $this->input->post('fieldid');
		$optionboxval = $this->input->post('optionboxval');
		$checkboxval 	= $this->input->post('checkboxval');
		$radioboxval 	= $this->input->post('radioboxval');
		$fieldname 		= $this->input->post('fieldname');
		$styling 			= $this->input->post('styling');
		$sortorder 		= $this->input->post('sortorder');
		$frequire 		= $this->input->post('frequire');

		$this->form_validation->set_rules('pagename', 'pagename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/createdynamicform");
		}	else {
			$i=0;
			$newdata = array();
			foreach ($fieldid as $k => $val) {
				if($k==$i){
					$newdata[$i]['fieldid'] = $val;
				}
				$i++;
			}
			$j=0;
			foreach ($optionboxval as $ke => $vals) {
				if($ke==$j){
					$newdata[$j]['optionboxval'] = $vals;
				}
				$j++;
			}
			$a=0;
			foreach ($checkboxval as $ke => $value) {
				if($ke==$a){
					$newdata[$a]['checkboxval'] = $value;
				}
				$a++;
			}
			$b=0;
			foreach ($radioboxval as $ke => $value) {
				if($ke==$b) {
					$newdata[$b]['radioboxval'] = $value;
				}
				$b++;
			}
			$c=0;
			foreach ($fieldname as $ke => $value) {
				if($ke==$c) {
					$newdata[$c]['fieldname'] = $value;
				}
				$c++;
			}
			$d=0;
			foreach ($styling as $ke => $value) {
				if($ke==$d) {
					$newdata[$d]['styling'] = $value;
				}
				$d++;
			}
			$e=0;
			foreach ($sortorder as $ke => $value) {
				if($ke==$e) {
					$newdata[$e]['sortorder'] = $value;
				}
				$e++;
			}
			$f=0;
			foreach ($frequire as $ke => $value) {
				if($ke==$f) {
					$newdata[$f]['frequire'] = $value;
				}
				$f++;
			}
			$fieldjsonData = json_encode($newdata);
			$data =array(
				'pagename' 				=>  $pagename,
				'fielddata' 	   	=> 	$fieldjsonData
			);

			$tbl= 'tbl_dynamicfield';
			$result = $this->FieldTypeModel->insert($tbl,$data);
			if($result)	{
				$tbl_vouch= 'tbl_voucher_mng';
				foreach ($newdata as $key => $val) {
					if ($val['fieldid']=='1' || $val['fieldid']==1) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='2' || $val['fieldid']==2) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='3' || $val['fieldid']==3) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='4' || $val['fieldid']==4) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='5' || $val['fieldid']==5) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='6' || $val['fieldid']==6) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields, TRUE);
					}
					if ($val['fieldid']=='7' || $val['fieldid']==7) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields);
					}	
					if ($val['fieldid']=='8' || $val['fieldid']==8) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'TEXT', 'last' => TRUE)
						);
						$this->dbforge->add_column($tbl_vouch, $fields);
					}				
				}				

				$this->session->set_flashdata('msgshow', array('message' => 'Field has been inserted Successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/listdynamicform");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/createdynamicform");
			}
		}
	}

	// User List ------------------------------------
	public function listDynamicForm(){	
	 	$list = $this->FieldTypeModel->getContentDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/dynamicformlist";
		$config["total_rows"] = $list;
    $config["per_page"] = 30;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['fieldlists'] = $this->FieldTypeModel->getContentDetailsLimit($startFrom, $config["per_page"]);	  

		$data['title'] = "Dynamic Field List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/dynamicfield/fieldtypelist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editDynamicForm($id) {
		$data['title'] = "Edit Dynamic Field";
		$data['id']=$id;
		$data['fieldlist'] = $this->FieldTypeModel->getFieldsList();
		$data['getFieldsList'] = $this->FieldTypeModel->getEditContent($id);
		//print_r($data['getFieldsList']); die();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/dynamicfield/fieldtypeedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function updateDynamicForm($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_dynamicfield';	
        $getFieldsList = $this->FieldTypeModel->getEditContent($id);
        $jsonnewdata = json_decode($getFieldsList->fielddata);
        
		$pagename 		= $this->input->post('pagename');
		$fieldid 			= $this->input->post('fieldid');
		$optionboxval = $this->input->post('optionboxval');
		$checkboxval 	= $this->input->post('checkboxval');
		$radioboxval 	= $this->input->post('radioboxval');
		$fieldname 		= $this->input->post('fieldname');
		$styling 			= $this->input->post('styling');
		$sortorder 		= $this->input->post('sortorder');
		$frequire 		= $this->input->post('frequire');

		$this->form_validation->set_rules('pagename', 'pagename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editdynamicform/$id");
		}	else {
			$i=0;
			
			foreach ($fieldid as $k => $val) {
				if($k==$i){
					$newdata[$i]['fieldid'] = $val;
				}
				$i++;
			}
			$j=0;
			foreach ($optionboxval as $ke => $vals) {
				if($ke==$j){
					$newdata[$j]['optionboxval'] = $vals;
				}
				$j++;
			}
			$a=0;
			foreach ($checkboxval as $ke => $value) {
				if($ke==$a){
					$newdata[$a]['checkboxval'] = $value;
				}
				$a++;
			}
			$b=0;
			foreach ($radioboxval as $ke => $value) {
				if($ke==$b) {
					$newdata[$b]['radioboxval'] = $value;
				}
				$b++;
			}
			$c=0;
			foreach ($fieldname as $ke => $value) {
				if($ke==$c) {
					$newdata[$c]['fieldname'] = $value;
				}
				$c++;
			}
			$d=0;
			foreach ($styling as $ke => $value) {
				if($ke==$d) {
					$newdata[$d]['styling'] = $value;
				}
				$d++;
			}
			$e=0;
			foreach ($sortorder as $ke => $value) {
				if($ke==$e) {
					$newdata[$e]['sortorder'] = $value;
				}
				$e++;
			}
			$f=0;
			foreach ($frequire as $ke => $value) {
				if($ke==$f) {
					$newdata[$f]['frequire'] = $value;
				}
				$f++;
			}
			
			$g=0;
			
            foreach ($jsonnewdata as $json1 => $column) {
            	if($json1==$g) {
					$newdata[$g]['column'] = $column->fieldname;
				}
				$g++;
            }
            $h=0;
			
            foreach ($jsonnewdata as $json => $columnid) {
            	if($json==$h) {
					$newdata[$h]['columnid'] = $columnid->fieldid;
				}
				$h++;
            }
			$fieldjsonData = json_encode($newdata);
//print "<pre>"; print_r($newdata); die;
			$data =array(
				'pagename' 				=>  $pagename,
				'fielddata' 	   	=> 	$fieldjsonData
			);

			$result = $this->FieldTypeModel->update($tbl,$data,$where);

			if($result)	{
				$tbl_vouch= 'tbl_voucher_mng';
				

                //$this->dbforge->drop_column($tbl_vouch, $val['column']);

                       
				foreach ($newdata as $key => $val) {
					
					if ($val['columnid']=='1' || $val['columnid']==1 || $val['fieldid']=='1' || $val['fieldid']==1) {
						if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'VARCHAR','constraint' => '255', 'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
						
						
					}
					if ($val['columnid']=='2' || $val['columnid']==2 || $val['fieldid']=='2' || $val['fieldid']==2) {
						if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'VARCHAR','constraint' => '255', 'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
						
						
					}
					if ($val['columnid']=='3' || $val['columnid']==3 || $val['fieldid']=='3' || $val['fieldid']==3) {
						if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'VARCHAR','constraint' => '255', 'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
											}
					if ($val['columnid']=='4' || $val['columnid']==4 || $val['fieldid']=='4' || $val['fieldid']==4) {
						if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'VARCHAR','constraint' => '255', 'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'VARCHAR','constraint' => '255', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
					}
					if ($val['columnid']=='5' || $val['columnid']==5 || $val['fieldid']=='5' || $val['fieldid']==5) {
						if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'LONGTEXT','last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
						
						
					}
					if ($val['columnid']=='6' || $val['columnid']==6 || $val['fieldid']=='6' || $val['fieldid']==6) {
						$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
						if($this->db->field_exists(str_replace(' ', '', strtolower($val['fieldname'])), $tbl_vouch)){

                        	$this->dbforge->drop_column($tbl_vouch, str_replace(' ', '', strtolower($val['fieldname'])));
                        }

						$this->dbforge->add_column($tbl_vouch, $fields);
					}
					if ($val['columnid']=='8' || $val['columnid']==8 || $val['fieldid']=='8' || $val['fieldid']==8) {

                        if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }
                           
                        	
                    	elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'TEXT', 
					                'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'TEXT', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}

                        	
                        
						
					}	
					if ($val['columnid']=='7' || $val['columnid']==7 || $val['fieldid']=='7' || $val['fieldid']==7) {
                    
                        if($val['fieldname'] == ""){

                        	$this->dbforge->drop_column($tbl_vouch, $val['column']);

                        }elseif($this->db->field_exists($val['column'], $tbl_vouch)){
                            
                            $fields = array(
					        $val['column'] => array(
					                'name' => str_replace(' ', '', strtolower($val['fieldname'])),
					                'type' => 'LONGTEXT', 
					                'last' => TRUE
					        ),
						);
                            
						$this->dbforge->modify_column($tbl_vouch, $fields);

                    	}else{

                    		$fields = array(
	        		str_replace(' ', '', strtolower($val['fieldname'])) => array('type' => 'LONGTEXT', 'last' => TRUE)
						);
                        
						
						$this->dbforge->add_column($tbl_vouch, $fields);
                    	}
                        	
                        
						
					}	
								
				}
				$this->session->set_flashdata('msgshow', array('message' => 'Field has been updated successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/listdynamicform");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/editdynamicform/$id");
			}
		}
		
	}
	
	 public function deleteDynamicForm($id) { 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_dynamicfield';
		$result =$this->FieldTypeModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Form has been deleted successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/listdynamicform");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/listdynamicform");
		}
	}

}