<?php
defined('BASEPATH') || exit('No direct script access allowed');

class BlockUnblockController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('BlockUnblockModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
    $this->load->helper("logo_helper");
	}
    
	// Block/Unblock Add Page----------------------
	public function blockUnblockAdd(){
		$data['title'] = "Block/Unblock Add";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblockadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Block/Unblock Insert Process -------------------------
	public function blockUnblockInsert() {
		$blockfield 		= $this->input->post('blockfield');
		$status 				= $this->input->post('status');
		$this->form_validation->set_rules('blockfield', 'blockfield', 'required');	
		$this->form_validation->set_rules('status', 'status', 'required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/blockunblockadd");
		}	else {
			$tbl = 'tbl_blockunblock';
			$data =array(
				'blockdata' 	=> $blockfield,
				'status'			=> $status,
			);
			$result = $this->BlockUnblockModel->insert($tbl,$data);

			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Block data has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/blockunblocklist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/blockunblockadd");
			}
		}
	}

	// Block / Unblock List ------------------------------------

	public function blockUnblockList(){
	 	$list = $this->BlockUnblockModel->getBlockDetails();
	 	$config = array(); 
		$config["base_url"] = base_url() . "admin/blockunblocklist";
		$config["total_rows"] = $list;
    $config["per_page"] = 100;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
    
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

    $data['blockunblockList'] = $this->BlockUnblockModel->getBlockDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Block/Unblock List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblocklist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editBlockUnblock($id) {
		$data['title'] = "Edit Block/Unblock";
		$data['id']=$id;
		$data['editBlockList'] = $this->BlockUnblockModel->getEditBlockData($id);
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblockedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update Product -----------------------------
	public function updateBlockUnblock($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_blockunblock';
		$blockfield 		= $this->input->post('blockfield');
		$status 				= $this->input->post('status');
		$this->form_validation->set_rules('blockfield', 'blockfield', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editblockunblock/$id");
		}	else {
			$data = array(
				'blockdata' 	=> $blockfield,
				'status'			=> $status,
			);
			$result = $this->BlockUnblockModel->update($tbl,$data,$where);			
			if($result)	{				
				$this->session->set_flashdata('msgshow', array('message' => 'Block data has been updated successfully.','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/blockunblocklist');
			}	else {	
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/editblockunblock/$id');
			}
		}
		
	}
	
	public function deleteBlockUnblock($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_blockunblock';
		$result =$this->BlockUnblockModel->delete($tbl,$where);

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Block data has been deleted successfully.','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/blockunblocklist");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/blockunblocklist");
		}
	}

	// Block/Unblock Add Page----------------------
	public function blockUnblockMsgAdd(){
		$data['title'] = "Block/Unblock Message Add";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblockmsgadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Block/Unblock Insert Process -------------------------
	public function blockUnblockMsgInsert() {
		$blockfieldmsg 		= $this->input->post('blockfieldmsg');
		$mtype 				= $this->input->post('mtype');
		$this->form_validation->set_rules('blockfieldmsg', 'blockfieldmsg', 'required');	
		$this->form_validation->set_rules('mtype', 'mtype', 'required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/blockunblockadd");
		}	else {
			$tbl = 'tbl_blockunblockmsg';
			$data =array(
				'type'			=> $mtype,
				'message' 	=> $blockfieldmsg,
			);
			$result = $this->BlockUnblockModel->insert($tbl,$data);

			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Block message has been added successfully.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/blockunblockmsglist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/blockunblockmsgadd");
			}
		}
	}

	// Block / Unblock List ------------------------------------

	public function blockUnblockMsgList(){
	 	
	 	$list = $this->BlockUnblockModel->getBlockMsgDetails();
	 	$config = array();
		$config["base_url"] = base_url() . "admin/blockunblockmsglist";
		$config["total_rows"] = $list;
    $config["per_page"] = 100;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
    
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

    $data['blockunblockmsgList'] = $this->BlockUnblockModel->getBlockMsgDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "Block/Unblock Message List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblockmsglist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editBlockUnblockMsg($id) {
		$data['title'] = "Edit Block/Unblock Message";
		$data['id']=$id;
		$data['editBlockList'] = $this->BlockUnblockModel->getEditBlockMsgData($id);
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/blockunblock/blockunblockmsgedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update Product -----------------------------
	public function updateBlockUnblockMsg($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_blockunblockmsg';
		$blockfieldmsg 		= $this->input->post('blockfieldmsg');
		$mtype 				= $this->input->post('mtype');
		$this->form_validation->set_rules('blockfieldmsg', 'blockfieldmsg', 'required');	
		$this->form_validation->set_rules('mtype', 'mtype', 'required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editblockunblockmsg/$id");
		}	else {
			$data =array(
				'type'			=> $mtype,
				'message' 	=> $blockfieldmsg,
			);
			$result = $this->BlockUnblockModel->update($tbl,$data,$where);			
			if($result)	{				
				$this->session->set_flashdata('msgshow', array('message' => 'Block message has been updated successfully.','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/blockunblockmsglist');
			}	else {	
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/editblockunblockmsg/$id');
			}
		}
		
	}
	
	public function deleteBlockUnblockMsg($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_blockunblockmsg';
		$result =$this->BlockUnblockModel->delete($tbl,$where);

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Block message has been deleted successfully.','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/blockunblockmsglist");
		}	else {	
			$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/blockunblockmsglist");
		}
	}
	
}