<?php
defined('BASEPATH') || exit('No direct script access allowed');

class UserController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->helper('email_helper'); // calling helper class
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('UserModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->library("excel");   //excel download
	}


	# login page 
	public function index()	{		
		$userType = $this->session->userdata('userType');
		if($userType == 1){			
			redirect('site/core/micro/site/lib/controller/type/dashboard');
		}
		
		$data['title'] = "User Login";
		$this->load->view('admin/User/index',$data);
	}
	

	public function login() {
		$data = array(
		'emailaddress'=>$this->input->post('emailaddress'),
		'password'=>md5($this->input->post('password')),
		'status'=>1
		);
		
		$tbl= 'tbl_user';
		$result = $this->UserModel->get_where($tbl,$data);
		if($result) {
			foreach($result as $key=>$row){
				$userType= $row->userType;
				$session_data= array(
					'id'=>$row->id,
					'name'=>$row->name,
					'code'=>$row->code,
					'userType'=>$row->userType,
				);	
				$this->session->set_userdata($session_data);
				if($userType==1){
					redirect("site/core/micro/site/lib/controller/type/dashboard");
				}/*else{
					redirect("dashboard");
				}*/	
			}
		} else {
			$this->session->set_flashdata('user', array('message' => 'Please Enter Valid credentials','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type");
		}
		
	}
	

	// logout module from dashboard----------
	public function logout ()	{
		$this->session->sess_destroy();
		delete_cookie('visited'); 
    redirect("site/core/micro/site/lib/controller/type");
	}

	// Dashboard------------------------------
	
	public function dashboard(){
		$datas['title'] = "Dashboard";
		$datas['usedVouchers'] = $this->UserModel->usedVoucherDetailsCount();
		$datas['registeredlists'] = $this->UserModel->registeredUserListDetails();

		$datas['morethan5regsameip'] = $this->UserModel->morethan5orequalRegistrationSameIPCounts();
		$datas['morethan5booksameip'] = $this->UserModel->morethan5orequalBookingnSameIPCounts();
		$datas['morethan3regsameemail'] = $this->UserModel->morethan3orequalRegistrationSameEmailCounts();
		$datas['morethan3booksameemail'] = $this->UserModel->morethan3orequalBookingnSameEmailCounts();

		$userType =$this->session->userdata('userType');		
		$this->load->view('admin/User/header',$datas);
		$this->load->view('admin/common/sidebar',$datas);
		$this->load->view('admin/User/dashboard',$datas);
		$this->load->view('admin/common/footer',$datas);
	}
	
	// User Registration Page----------------------
	public function registration(){
		$data['title'] = "User Registration";
		//$tbl= 'tbl_userType';
		//$data['userTypeList'] = $this->UserModel->get($tbl);
		$tbl= 'tbl_status';
		$data['status'] = $this->UserModel->get($tbl);
		$tbl= 'tbl_cities';
		$orderColumn= 'cityName';
		$orderValue= 'asc';
		$data['citiesList'] = $this->UserModel->getByOrder($tbl,$orderColumn,$orderValue);
		$tbl= 'tbl_state';
		$orderColumn= 'stateName';
		$orderValue= 'asc';
		$data['stateList'] = $this->UserModel->getByOrder($tbl,$orderColumn,$orderValue); 
		/*$tbl= 'tbl_channel_classification';
		$data['classification'] = $this->UserModel->get($tbl);*/
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/User/registration',$data);
		$this->load->view('admin/common/footer');
	}
	public function citylist() {
		$output = "";
		$id =$this->input->post('id');
		$tbl= 'tbl_cities';
		$orderColumn= 'cityName';
		$orderValue= 'asc';
		$data = array(
			'cityState' => $id
		);
		
		$citiesList = $this->UserModel->get_othet_with_limit($tbl,$data,$orderColumn,$orderValue);
		
		foreach($citiesList as $row){
			$output .= '<option value="'.$row->id.'" >'.$row->cityName.'</option>';
		}
		print_r($output);
	}
	// User Insert Process -------------------------
	public function insert() {
		$name =$this->input->post('name');
		$emailaddress =$this->input->post('emailaddress');
		$city =$this->input->post('city');
		$state =$this->input->post('state');
		$pincode =$this->input->post('pincode');
		$mobile =$this->input->post('mobile');
		$status =$this->input->post('status');
		$password =$this->input->post('password');

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('emailaddress', 'emailaddress', 'required');
		$this->form_validation->set_rules('mobile', 'mobile', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('registration', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/registration");
		}	else{
			$this->form_validation->set_rules('emailaddress', 'emailaddress', 'is_unique[tbl_user.emailaddress]');
			$this->form_validation->set_rules('mobile', 'mobile', 'is_unique[tbl_user.mobile]');
			if ($this->form_validation->run() == FALSE) {
					$this->session->set_flashdata('registration', array('message' => 'Email Id & Mobile should be unique!!! Please try again.','class' => 'alert alert-danger'));
					redirect("site/core/micro/site/lib/controller/type/registration");
				} else {
					$createdBy =$this->session->userdata('id');
					$userType =$this->session->userdata('userType');
					$data =array(
						'name' 					=> 	$name,						
						'mobile' 				=> 	$mobile,
						'emailaddress' 	=> 	$emailaddress,		
						'password'			=>	md5($password),
						'state'					=>  $state,
						'city'					=>  $city,
						'pincode'				=>  $pincode,
						'status'				=>	$status,
						'createdBy'			=>	$createdBy,
						'userType'			=>	2,
					);
					$tbl= 'tbl_user';
					$result = $this->UserModel->insert($tbl,$data);
					if($result)	{
						$this->session->set_flashdata('registration', array('message' => 'User SuccessFully Registered!! Thank You!!.','class' => 'alert alert-success'));
						redirect("site/core/micro/site/lib/controller/type/userList");
					} else {
						$this->session->set_flashdata('registration', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
						redirect("site/core/micro/site/lib/controller/type/userList");
					}
				}
			}
	    }

	// User List ------------------------------------

	public function userList(){
	 	$list = $this->UserModel->getUserDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/userList";
		$config["total_rows"] = count($list);
    $config["per_page"] = 5;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else {
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
	  $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['userList'] = $this->UserModel->getUserDetailsLimit($startFrom, $config["per_page"]);

		$data['title'] = "User List";
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/User/userList',$data);
		$this->load->view('admin/common/footer');
	}

	public function editUserList($id) {
		/*$data['preUrl'] = $_SERVER['HTTP_REFERER'];
		if($data['preUrl'] == ""){
			redirect("userList");
		}
		else{*/
			$data['title'] = "Edit User";
			$data['id']=$id;
			$data['editUserList'] = $this->UserModel->getEditUser($id);
			$tbl= 'tbl_cities';
			$orderColumn= 'cityName';
			$orderValue= 'asc';
			$data['citiesList'] = $this->UserModel->getByOrder($tbl,$orderColumn,$orderValue);
			$tbl= 'tbl_state';
			$orderColumn= 'stateName';
			$orderValue= 'asc';
			$data['stateList'] = $this->UserModel->getByOrder($tbl,$orderColumn,$orderValue); 
			$tbl= 'tbl_status';
			$data['status'] = $this->UserModel->get($tbl);       
			$this->load->view('admin/User/header',$data);
			$this->load->view('admin/common/sidebar',$data);
			$this->load->view('admin/User/editUser',$data);
			$this->load->view('admin/common/footer');
		//}
	}

	// Update User -----------------------------
	public function updateUser($id){
		$where = array(
				'id'=>$id
			);
		$tbl= 'tbl_user';

		$data['title'] = "Edit Outlets";
		//$preUrl=$this->input->post('preUrl');
		$name =$this->input->post('name');
		$emailaddress =$this->input->post('emailaddress');
		$city =$this->input->post('city');
		$state =$this->input->post('state');
		$pincode =$this->input->post('pincode');
		$mobile =$this->input->post('mobile');
		$status =$this->input->post('status');

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('emailaddress', 'emailaddress', 'required');
		//$this->form_validation->set_rules('city', 'city', 'required');
		//$this->form_validation->set_rules('mobile', 'mobile', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
		//$this->form_validation->set_rules('password', 'password', 'required');

		$a = $this->UserModel->mobile_number($where,$tbl);
		if($a[0]->mobile == $mobile) {
			$this->form_validation->set_rules('mobile', 'mobile', 'trim|required|min_length[10]|max_length[11]');
		}	else {
			$this->form_validation->set_rules('mobile', 'mobile', 'is_unique[tbl_user.mobile]');
		}
		
		/*if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('registration', array('message' => 'Mobile should be unique and 10 digit!!! Please try again.','class' => 'alert alert-danger'));
			redirect("editUserList/$id");
		} else {*/
			$data =array(
							'name' 					=> 	$name,						
							'mobile' 				=> 	$mobile,
							'emailaddress' 	=> 	$emailaddress,		
							'state'					=>  $state,
							'city'					=>  $city,
							'pincode'				=>  $pincode,
							'status'				=>	$status
					);
			
			$result = $this->UserModel->update($tbl,$data,$where);
			
			if($result)	{				
				$this->session->set_flashdata('user', array('message' => 'Update SuccessFully! Thank You!!.','class' => 'alert alert-success'));
				redirect('site/core/micro/site/lib/controller/type/userList');
			}	else {	
				$this->session->set_flashdata('user', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
				redirect('site/core/micro/site/lib/controller/type/userList');
			}
		//}
		
	}
	
	public function deleteUserList($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_user';
		$result =$this->UserModel->delete($tbl,$where);
		$tbl= 'voucher_mng';
		$where = array(
			'u_id'=> $id
		);
		$result =$this->UserModel->delete($tbl,$where);



		if($result)	{
			$this->session->set_flashdata('registration', array('message' => 'Delete User SuccessFully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/userList");
		}	else {	
			$this->session->set_flashdata('registration', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/userList");
		}

	}
	
	

	//Change Password---------------------------------
	function changePassword() {
		$data['title'] = "Change Password";
		$id= $this->session->userdata('id');
		$userType =$this->session->userdata('userType');
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/User/changePassword',$data);
		$this->load->view('admin/common/footer');	
	}

	function changePasswordUpdate() {
		$tbl= 'tbl_user';
		$where = array(
			'id'=>$this->session->userdata('id'),
		);
		$data = array(
			'password'=>md5($this->input->post('password')),
		);
		$result = $this->UserModel->update($tbl,$data,$where);
		if($result)
		{
			$this->session->set_flashdata('user', array('message' => 'Password Change SuccessFully! Thank You!!.','class' => 'alert alert-success'));

			redirect("site/core/micro/site/lib/controller/type/changePassword");
		}
		else
		{	
			$this->session->set_flashdata('user', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/changePassword");
		}
	}


	//Reset User Password---------------------------------
	function resetUserPassword($id){
		$data['title'] = "Reset Password";
		$data['id']=$id;
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/User/resetUserPassword',$data);
		$this->load->view('admin/common/footer');	
	}

	function resetPassword(){
		$tbl= 'tbl_user';
		$where = array(
			'id'=>$this->input->post('id'),
		);
		$data = array(
			'password'=>md5($this->input->post('password')),
		);
		$result = $this->UserModel->update($tbl,$data,$where);
		if($result)
		{
			$this->session->set_flashdata('user', array('message' => 'Password Reset SuccessFully! Thank You!!.','class' => 'alert alert-success'));
		}
		else
		{	
			$this->session->set_flashdata('user', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
		}
	}

	public function usedVoucher() {
		$data['title'] = "Dashboard";
		$results = $this->UserModel->usedVoucherDetailsShow10Records();

		$output = '<div id="printMe">  ';
		$output .= '<table class="table table-striped table-bordered"><tr>';
		$output .= '<th>Sr No.</th>';
		$output .= '<th>Voucher Code</th>';
		$output .= '<th>Reward Code</th>';
		$output .= '<th>Quantity</th>';
		$output .= '<th>Price</th>';
		$output .= '<th>Total</th>';
		$output .= '<th>Voucher Used Date</th>';
		$output .= '<th>Voucher Used Time</th>';
		$output .= '<th>Voucher Expiry Date</th>';
		$output .= '</tr><tbody>';
		foreach($results as $key=>$row){
			$res = explode(' ',$row['created_on']);
			$key = $key+1;
			$output .= '<tr><td>'.$key.'</td>';
			$output .= '<td>'.$row['vouchercode'].'</td>';	
			$output .= '<td>'.$row['rewardcode'].'</td>';	
			$output .= '<td>'.$row['qty'].'</td>';	
			$output .= '<td>'.$row['price'].'</td>';				
			$output .= '<td>'.(int)$row['qty']*(int)$row['price'].'</td>';
			$output .= '<td>'.$res[0].'</td>';
			$output .= '<td>'.$res[1].'</td>';
			$output .= '<td>'.$row['expirydate'].'</td></tr>';
		}
		$output .= '</tbody></table>';
		$output .= '</div>';
		print_r($output);
		die;
	}

	public function usedVoucherList(){
		$data['title'] = "Used Voucher List";
		$from_d = $this->input->post('filterDateTo');
    $to_d = $this->input->post('filterDateFrom');
    $from = date("Y-m-d", strtotime($from_d));
    $to   = date("Y-m-d", strtotime($to_d));
		if(!empty($from_d)){
  		$data["links"] ='';
  		$data['startFrom']=0;
      $data['registeredUserList'] = $this->UserModel->downloadUserExcelByDatePicker($from,$to);
    } else {
		 	$list = $this->UserModel->usedVoucherListCount();
		 	$config = array(); 
			$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/usedvoucherlist";
			$config["total_rows"] = $list;
	    $config["per_page"] = 20;
	    $config["uri_segment"] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string']  = TRUE;
			$config['num_links'] = $config["total_rows"];
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			
			 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
				$page = $_GET['per_page'];
				$config['num_links'] = 5;
			}	else{
				$page = 1;
				$config['num_links'] = 10;
			}
			$this->pagination->initialize($config);
	    $str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
	    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
	    $data['startFrom']=$startFrom;

	  	$data['usedVoucherList'] = $this->UserModel->usedVoucherDetailsLimit($startFrom, $config["per_page"]);
	  }

		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/usedlist/usedvoucherlist',$data);
		$this->load->view('admin/common/footer',$data);	
	}

	public function registeredUserList(){
		$data['title'] = "Used Recent List";
		$list = $this->UserModel->registeredUserListDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/registereduserlist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

  	$data['registeredUserList'] = $this->UserModel->registeredUserListDetailsLimit($startFrom, $config["per_page"]);  
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/usedlist/recentuser',$data);
		$this->load->view('admin/common/footer',$data);	
	}
	
	//download excel
	public function downloadUserVoucherList()	{
    $object = new PHPExcel();
		$object->setActiveSheetIndex(0);

		$table_columns = array('Voucher Code', 'Name', 'Mobile', 'Email', 'Date & Time of Registration','City', 'Prefered Area 1', 'Prefered Area 2', 'Prefered Movie 1', 'Prefered Movie 2', 'Prefered Date 1', 'Prefered Date 2', 'Prefered Time 1', 'Prefered Time 2', 'IP Address', 'Device Type', 'No. Of Seconds', 'Booking Order Date & Time', 'Type', 'Name', 'Account Number', 'IFSC Code', 'Cashback  Amount', 'UPI Details', 'Wallets Mobile', 'API STATUS','State','Address1','Address2','Address3','Landmark','Pin Code','Alternate Phone Number');

		$column = 0;
		foreach($table_columns as $field)	{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$user_data = $this->UserModel->downloadUserExcel();
		
		$excel_row = 2;
		foreach($user_data as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['vouchercode']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['cname']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['email']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['registereddate']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['city']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['preferedArea1']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['preferedArea2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['preferedMovie1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['preferedMovie2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['prefereddate1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['prefereddate2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row['preferedTime1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row['preferedTime2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row['ipaddress']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row['devicetype']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row['noofseconds']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row['orderdatetime']);
			if(isset($row['accountnumber'])) {
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "Account");
			} elseif (isset($row['upivpn'])) { 
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "UPI");
			} elseif (isset($row['walletsmobile'])) { 
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "Wallets");
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row['name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row['accountnumber']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $row['ifsccode']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row['cashbackamount']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row['upivpn']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row['walletsmobile']);
			if($row['api_status']==1){
				$object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, "Sent to API");
			} else {
				$object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, "Not Sent to API");
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(26, $excel_row, $row['state']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(27, $excel_row, $row['address1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(28, $excel_row, $row['address2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(29, $excel_row, $row['address3']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(30, $excel_row, $row['landmark']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(31, $excel_row, $row['pin_code']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(32, $excel_row, $row['alternate_phone_no']);
			$excel_row++;
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="UserVoucherList Data.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

  public function downloadUserExcelByDatePicker() {
		$from_d = $this->input->post('filterDateTo');
    $to_d = $this->input->post('filterDateFrom');
    // $from =  strtotime($from);

    $from = date("Y-m-d", strtotime($from_d));
    $to   = date("Y-m-d", strtotime($to_d));

    $object = new PHPExcel();    
		$table_columns = array('Voucher Code', 'Name', 'Mobile', 'Email', 'Date & Time of Registration', 'City', 'Prefered Area 1', 'Prefered Area 2', 'Prefered Movie 1', 'Prefered Movie 2', 'Prefered Date 1', 'Prefered Date 2', 'Prefered Time 1', 'Prefered Time 2', 'IP Address', 'Device Type', 'No. Of Seconds', 'Booking Order Date & Time', 'Type', 'Name', 'Account Number', 'IFSC Code', 'Cashback  Amount', 'UPI Details', 'Wallets Mobile', 'API STATUS');

		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadUserExcelByDatePicker($from,$to);
		
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['vouchercode']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['cname']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['email']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['registereddate']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['city']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['preferedArea1']);

			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['preferedArea2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row['preferedMovie1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row['preferedMovie2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row['prefereddate1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row['prefereddate2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row['preferedTime1']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row['preferedTime2']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row['ipaddress']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row['devicetype']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row['noofseconds']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row['orderdatetime']);
			if(isset($row['accountnumber'])) {
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "Account");
			} elseif (isset($row['upivpn'])) { 
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "UPI");
			} elseif (isset($row['walletsmobile'])) { 
				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, "Wallets");
			}
			$object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row['name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(20, $excel_row, $row['accountnumber']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(21, $excel_row, $row['ifsccode']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(22, $excel_row, $row['cashbackamount']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(23, $excel_row, $row['upivpn']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(24, $excel_row, $row['walletsmobile']);
			if($row['api_status']==1){
				$object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, "Sent to API");
			} else {
				$object->getActiveSheet()->setCellValueByColumnAndRow(25, $excel_row, "Not Sent to API");
			}
			$excel_row++; 
		}
			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-type: application/vnd.ms-excel; charset=UTF-8');
			header('Content-Disposition: attachment;filename="UserVoucherList Data.xls"');
			ob_end_clean();
			$object_writer->save('php://output');
	}

	public function downloadRegisteredBYDate() {
		$from_d = $this->input->post('filterDateTo');
    $to_d = $this->input->post('filterDateFrom');
    $from = date("Y-m-d", strtotime($from_d));
    $to   = date("Y-m-d", strtotime($to_d));

    $object = new PHPExcel();    
		$table_columns = array('Voucher Code', 'Name', 'Mobile', 'Email', 'Date & Time of Registration', 'IP Address', 'Device Type', 'No. Of Seconds');

		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadUserRegisterUser($from,$to);
		
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['vouchercode']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['email']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['registereddate']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['ipaddress']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['devicetype']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['noofseconds']);
			$excel_row++; 
		}
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="UserVoucherList Data.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

	public function registrationMoreThan5SameIP() {
		$data['title'] = "5 or more registrations from same IP address";
		$list = $this->UserModel->morethan5orequalRegistrationSameIPCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/registrationmorethan5sameip";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

  	$data['lists'] = $this->UserModel->morethan5orequalRegistrationSameIPDetailsLimit($startFrom, $config["per_page"]);  
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/morethan5registrationsameip',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function bookingMoreThan5SameIP() {
		$data['title'] = "5 or more booking from same IP address";
		$list = $this->UserModel->morethan5orequalBookingnSameIPCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/bookingmorethan5sameip";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

  	$data['lists'] = $this->UserModel->morethan5orequalBookingnSameIPDetailsLimit($startFrom, $config["per_page"]);  
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/morethan5bookingsameip',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function registrationMoreThan3SameEmail() {
		$data['title'] = "3 or more registration from same Email";
		$list = $this->UserModel->morethan3orequalRegistrationSameEmailCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/registrationmorethan3sameemail";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

  	$data['lists'] = $this->UserModel->morethan3orequalRegistrationSameEmailDetailsLimit($startFrom, $config["per_page"]);  
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/morethan3registsameemail',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function bookingMoreThan3SameEmail() {
		$data['title'] = "3 or more registration from same Email";
		$list = $this->UserModel->morethan3orequalBookingnSameEmailCounts();
	 	$config = array(); 
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/registrationmorethan3sameemail";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 10;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;

  	$data['lists'] = $this->UserModel->morethan3orequalBookingnSameEmailDetailsLimit($startFrom, $config["per_page"]);  
		$this->load->view('admin/User/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/morethan3bookingsameemail',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function registrationDownloadMoreThan5SameIP(){
    $object = new PHPExcel();    
		$table_columns = array('IP Address');
		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadRegisterMoreThan5SameIP();
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['ipaddress']);

			$excel_row++; 
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="registrationDownloadMoreThan5SameIP.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

	public function bookingDownloadMoreThan5SameIP(){
    $object = new PHPExcel();    
		$table_columns = array('IP Address');
		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadBookingMoreThan5SameIP();
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['ipaddress']);

			$excel_row++; 
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="bookingDownloadMoreThan5SameIP.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

	public function registrationDownloadMoreThan3SameEmail(){
    $object = new PHPExcel();    
		$table_columns = array('Email Id');
		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadRegisterMoreThan3SameEmail();
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['email']);

			$excel_row++; 
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="registrationDownloadMoreThan3SameEmail.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

	public function bookingDownloadMoreThan3SameEmail(){
    $object = new PHPExcel();    
		$table_columns = array('Email ID');
		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->UserModel->downloadBookingMoreThan3SameEmail();
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['email']);

			$excel_row++; 
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="bookingDownloadMoreThan3SameEmail.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

}