<?php
defined('BASEPATH') || exit('No direct script access allowed');

class BookingSuccessController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'	); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('BookingSuccessModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	// Load file helper
    $this->load->helper('file');
	}

	public function bookingSuccessList(){	

	 	$list = $this->BookingSuccessModel->getBookingSuccesCount(); 
	 	$config = array();
		$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/typeadmin/bookingsuccessmaillist";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 5;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['bookingsuccesslists'] = $this->BookingSuccessModel->getBookingSuccesDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Booking Mail List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/bookingsuccesslist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function addBookingSuccessMailbyoffer()	{
		$data['title'] = "Add Booking Success Mail";
		//$data['bookingMailData'] = $this->BookingSuccessModel->getBookingMailDetails();
		$data['productlist'] = $this->BookingSuccessModel->getProductList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/bookingsuccessadd',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function insertBookingSuccessMailbyoffer() {
		$mail_subject 	= $this->input->post('mail_subject');
		$mail_body 			= $this->input->post('mail_body');
		$sms 						= $this->input->post('sms');
		$pid 						= $this->input->post('pid');
		$formail 				= $this->input->post('formail');
		$smstmpid				= $this->input->post('smstmpid');

		$this->form_validation->set_rules('mail_subject', 'mail_subject', 'required');
		$this->form_validation->set_rules('mail_body', 'mail_body', 'required');
		$this->form_validation->set_rules('sms', 'sms', 'required');
		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('smstmpid', 'smstmpid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
		}	else {
			/*$getBookingMailData = $this->BookingSuccessModel->getBookingMailDetails();
			$id = $getBookingMailData[0]['id'];
			$where = array('id' => $id );
			if (count($getBookingMailData)>0) {*/
				$data =array(
					'prod_id'				=> $pid,
					'mail_type'			=> 'booking',
					'new_mail_type' => $formail,
					'mail_subject' 	=> $mail_subject,	
					'mail_body'			=> $mail_body,
					'sms'						=> $sms,
					'smstmpid' 			=> $smstmpid
				);
				$tbl= 'tbl_bookingsuccessmail';
				$result = $this->BookingSuccessModel->insert($tbl,$data);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'Booking mail has been added successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
				}
			/*} else {
				$data =array(
					'prod_id'				=> $prod_id,
					'mail_type'			=> 'booking',
					'mail_subject' 	=> $mail_subject,	
					'mail_body'			=> $mail_body,
					'sms'						=> $sms
				);
				$tbl= 'tbl_bookingsuccessmail';
				$result = $this->BookingSuccessModel->insert($tbl,$data);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'Booking mail has been added successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/bookingsuccessmail");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/bookingsuccessmail");
				}
			}*/
		}
	}

    public function editBookingSuccessMailbyoffer($id)	{
		$data['id']=$id;
		$data['title'] = "Edit Booking Success Mail";
		$data['productlist'] = $this->BookingSuccessModel->getProductList();
		$data['editbookingmaillist'] = $this->BookingSuccessModel->getEditBookingMailList($id);
        foreach ($data['editbookingmaillist'] as $key => $value) {
			$getbookingid = $this->BookingSuccessModel->getBookingFormId($value->prod_id);
			//print "<pre>"; print_r($getbookingid); die;
			if ($getbookingid->booking_form_id == 5) {
				$data['bookingid5'] = $getbookingid->booking_form_id;
			}
		}
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/bookingsuccessedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function updateBookingSuccessMailbyoffer($id){
		$where = array('id'=>$id);
		$tbl= 'tbl_bookingsuccessmail';
		$data['title'] = "Edit Booking Success Mail By Offer";		
		$mail_subject 	= $this->input->post('mail_subject');
		$mail_body 			= $this->input->post('mail_body');
		$sms 						= $this->input->post('sms');
		$pid 						= $this->input->post('pid');
		$formail 				= $this->input->post('formail');
		$smstmpid				= $this->input->post('smstmpid');

		$this->form_validation->set_rules('mail_subject', 'mail_subject', 'required');
		$this->form_validation->set_rules('mail_body', 'mail_body', 'required');
		$this->form_validation->set_rules('sms', 'sms', 'required');
		$this->form_validation->set_rules('pid', 'pid', 'required');
		$this->form_validation->set_rules('smstmpid', 'smstmpid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editbookingsuccessmailbyoffer/$id");
		}	else {
			$data =array(
				'prod_id'				=> $pid,
				'mail_type'			=> 'booking',
				'new_mail_type' => $formail,
				'mail_subject' 	=> $mail_subject,	
				'mail_body'			=> $mail_body,
				'sms'						=> $sms,
				'smstmpid' 			=> $smstmpid
			);
		
			$result = $this->BookingSuccessModel->update($tbl,$data,$where);
			if($result)	{
				$this->session->set_flashdata('msgshow', array('message' => 'Booking mail has been updated successfully!','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
			}
		}		
	}

	public function deleteBookingSuccessMailbyoffer($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_bookingsuccessmail';
		$result =$this->BookingSuccessModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('msgshow', array('message' => 'Booking mail has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
		}	else {	
			$this->session->set_flashdata('banner', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/bookingsuccessmaillist");
		}
	}


	public function registrationSuccessMail()	{
		$data['title'] = "Registration Success Mail";
		$data['bookingMailData'] = $this->BookingSuccessModel->getRegistrationMailDetails();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/registrationsuccess',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function registartionMailInsert() {
		$mail_subject 	= $this->input->post('mail_subject');
		$mail_body 			= $this->input->post('mail_body');
		$sms 						= $this->input->post('sms');

		$this->form_validation->set_rules('mail_subject', 'mail_subject', 'required');
		$this->form_validation->set_rules('mail_body', 'mail_body', 'required');
		$this->form_validation->set_rules('sms', 'sms', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/registartionsuccessmail");
		}	else {
			$getBookingMailData = $this->BookingSuccessModel->getRegistrationMailDetails();
			$id = $getBookingMailData[0]['id'];
			$where = array('id' => $id );
			if (count($getBookingMailData)>0) {
				$data =array(
					'prod_id'				=> 1,
					'mail_type'			=> 'regisrtation',
					'mail_subject' 	=> $mail_subject,	
					'mail_body'			=> $mail_body,
					'sms'						=> $sms
				);
				$tbl= 'tbl_bookingsuccessmail';
				$result = $this->BookingSuccessModel->update($tbl,$data,$where);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'Regisrtation mail has been updated successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/registartionsuccessmail");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/registartionsuccessmail");
				}
			} else {
				$data =array(
					'prod_id'				=> 1,
					'mail_type'			=> 'regisrtation',
					'mail_subject' 	=> $mail_subject,	
					'mail_body'			=> $mail_body,
					'sms'						=> $sms
				);
				$tbl= 'tbl_bookingsuccessmail';
				$result = $this->BookingSuccessModel->insert($tbl,$data);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'Regisrtation mail has been added successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/registartionsuccessmail");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/registartionsuccessmail");
				}
			}
		} 
	}

	public function uploadImgforBookingMail() {
		if(isset($_FILES['upload']['name'])) {
			$file = $_FILES['upload']['tmp_name'];
			$file_name = $_FILES['upload']['name'];
			$file_name_array = explode(".", $file_name);
			$extension = end($file_name_array);
			$new_image_name = rand() . '.' . $extension;
			$uploadDir = $_SERVER['DOCUMENT_ROOT'].'/assets/uploads';
			
			$allowed_extension = array("jpg", "jpeg", "gif", "png");
			if(in_array($extension, $allowed_extension)) {
				move_uploaded_file($file, $uploadDir.'/' . $new_image_name);
				$function_number = $_GET['CKEditorFuncNum'];
				$url = base_url() . 'assets/uploads/' . $new_image_name;
				$safename = htmlspecialchars("<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '');</script>");
				echo $safename;

			}
		}
	}
	
	public function otpMessage()	{
		$data['title'] = "OTP Mesage";
		$data['otpmessagelist'] = $this->BookingSuccessModel->getOTPMessageDetails();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/allbookingmail/otpmessages',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function otpMessageInsert() {
		$otpsms 	= $this->input->post('otpsms');

		$this->form_validation->set_rules('otpsms', 'otpsms', 'required');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => 'Fields are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/otpmessage");
		}	else {
			$getBookingMailData = $this->BookingSuccessModel->getOTPMessageDetails();
			$id = $getBookingMailData->id;
			$where = array('id' => $id );
			if (count($getBookingMailData)>0) {
				$data =array(
					'message' 		=> $otpsms,					
					'created_on'			=> date("Y-m-d h:m:s")
				);
				$tbl= 'tbl_otpsms';
				$result = $this->BookingSuccessModel->update($tbl,$data,$where);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'OTP message has been updated successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/otpmessage");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/otpmessage");
				}
			} else {				
				$data =array(
					'message' 		=> $otpsms
				);
				$tbl= 'tbl_otpsms';
				$result = $this->BookingSuccessModel->insert($tbl,$data);
				if($result)	{
					$this->session->set_flashdata('msgshow', array('message' => 'OTP message has been added successfully!','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/otpmessage");
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/otpmessage");
				}
			}
		} 
	}

	
}