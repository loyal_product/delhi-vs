<?php
defined('BASEPATH') || exit('No direct script access allowed');

class InventoryReportController extends MY_Controller {

	public $data = array();
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('InventoryReportModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper('ckeditor_helper');
   	$this->load->library("excel");   //excel download
   	$this->load->helper("logo_helper");
	}

	# login page 
	public function index()	{
		$data['title'] = "Inventory Report Listing";
		$from_d = $this->input->post('filterDateTo');
    $to_d = $this->input->post('filterDateFrom');
    $from = date("Y-m-d", strtotime($from_d));
    $to   = date("Y-m-d", strtotime($to_d));
		if(!empty($from_d)){
  		$data["links"] ='';
  		$data['startFrom']=0;
      $data['InventoryReportList'] = $this->InventoryReportModel->downloadUserExcelByDatePicker($from,$to);
    } else {
		 	$list = $this->InventoryReportModel->registeredInventoryReportDetails(); 

		 	$config = array(); 
			$config["base_url"] = base_url() . "site/core/micro/site/lib/controller/type/inventoryreport";
			$config["total_rows"] = $list;
	    $config["per_page"] = 20;
	    $config["uri_segment"] = 2;
			$config['use_page_numbers'] = TRUE;
			$config['page_query_string']  = TRUE;
			$config['num_links'] = $config["total_rows"];
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			
			 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
				$page = $_GET['per_page'];
				$config['num_links'] = 5;
			}	else{
				$page = 1;
				$config['num_links'] = 10;
			}
			$this->pagination->initialize($config);
	    $str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
	    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
	    $data['startFrom']=$startFrom;

	  	$data['InventoryReportList'] = $this->InventoryReportModel->registeredInventoryReportDetailsLimit($startFrom, $config["per_page"]);
	  }
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/inventoryreport',$data);
		$this->load->view('admin/common/footer',$data);	

	}
	
	//download excel
	public function downloadInventoryReportList()	{
    $object = new PHPExcel();
		$object->setActiveSheetIndex(0);

		$table_columns = array('Product Type','Gift Voucher No','Date & Time Added','Issued date & time','Customer Mob No','Email ID','Name','Voucher Code');

		$column = 0;
		foreach($table_columns as $field)	{
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$user_data = $this->InventoryReportModel->downloadUserExcel();
		$excel_row = 2;
		foreach($user_data as $key=>$row) {
			//$res = explode(' ',$row['created_on']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['title']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['reward_code']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['registereddate']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['created_on']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['email']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['vouchercode']);
			$excel_row++;
		}

		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-type: application/vnd.ms-excel; charset=UTF-8');
		header('Content-Disposition: attachment;filename="UserVoucherList Data.xls"');
		ob_end_clean();
		$object_writer->save('php://output');
	}

  public function downloadInventoryReportDatePicker() {
  	$object = new PHPExcel();
		$from_d = $this->input->post('filterDateTo');
    $to_d = $this->input->post('filterDateFrom');
    // $from =  strtotime($from);

    $from = date("Y-m-d", strtotime($from_d));
    $to   = date("Y-m-d", strtotime($to_d));

    $object = new PHPExcel();
		$table_columns = array('Product Type','Gift Voucher No','Date & Time Added','Issued date & time','Customer Mob No','Email ID','Name','Voucher Code');

		$column = 0;
		foreach($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}
		$result = $this->InventoryReportModel->downloadUserExcelByDatePicker($from,$to);
		$excel_row = 2;
		foreach($result as $key=>$row) {
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row['title']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row['reward_code']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row['registereddate']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row['created_on']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row['mobile']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row['email']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row['name']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row['vouchercode']);

			$excel_row++;
		}
			$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
			header('Content-type: application/vnd.ms-excel; charset=UTF-8');
			header('Content-Disposition: attachment;filename="UserVoucherList Data.xls"');
			ob_end_clean();
			$object_writer->save('php://output');
	}

	public function inventoryDashboard() {
		$data['title'] = "Inventory Dashborad";
		$data["links"] ='';
  	$data['startFrom']=0;
		$data['resData'] = $this->InventoryReportModel->invntoryDashboard();

		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar');
		$this->load->view('admin/report/inventorydashboard',$data);
		$this->load->view('admin/common/footer',$data);
	}


}