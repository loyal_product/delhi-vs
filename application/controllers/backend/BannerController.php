<?php
defined('BASEPATH') || exit('No direct script access allowed');

class BannerController extends MY_Controller {

	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('BannerModel');
   	$this->load->model('MenuModel');
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->library('pagination'); // pagination ---
   	$this->load->helper("logo_helper");
	}


	# login page 
	public function index()	{		
		$data['title'] = "Banner List";
		$data['menulist'] = $this->MainModel->getMenuList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/bannerlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Banner Registration Page----------------------
	public function banneradd(){
	  $data['title'] = "Add Banner";
	  $data['menulist'] = $this->MenuModel->getMenuList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/banneradd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Banner Insert Process -------------------------
	public function bannerInsert() {
		$pagename =$this->input->post('pagename');
		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"];   
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);

   	move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
			$this->form_validation->set_rules('pagename', 'pagename', 'required');
			if($this->form_validation->run() == FALSE){
				$this->session->set_flashdata('registration', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
				redirect("site/core/micro/site/lib/controller/type/banneradd");
			}	else {
				$data =array(
					'banner' 	   		=> 	$file_store,					
					'pagename' 			=> 	$pagename,
				);
				$tbl= 'tbl_banner';
				$result = $this->BannerModel->insert($tbl,$data);
				if($result)	{
					$this->session->set_flashdata('banner', array('message' => 'Banner has been updated SuccessFully!!.','class' => 'alert alert-success'));
					redirect("site/core/micro/site/lib/controller/type/bannerlist");
				} else {
					$this->session->set_flashdata('banner', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
					redirect("site/core/micro/site/lib/controller/type/bannerlist");
				}
			}
	}

	// User List ------------------------------------

	public function bannerlist(){	
	 	$list = $this->BannerModel->getbannerDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "admin/bannerlist";
		$config["total_rows"] = $list;
    	$config["per_page"] = 20;
    	$config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 5;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['bannerlist'] = $this->BannerModel->getbannerDetailsLimit($startFrom, $config["per_page"]);
	  

		$data['title'] = "Banner List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/bannerlist',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editbanner($id) {
		$data['title'] = "Edit Banner";
		$data['id']=$id;
		$data['menulist'] = $this->MenuModel->getMenuList();
		$data['editbannerList'] = $this->BannerModel->getEditBanner($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/banneredit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update User -----------------------------
	public function bannerupdate($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_banner';
		$data['title'] = "Edit Banner";		
		$pagename =$this->input->post('pagename');
   	if($_FILES['image']['name']!="") {
      $file_name = $_FILES["image"]["name"];
			$file_tem_loc = $_FILES["image"]["tmp_name"];   
			$file_store = $file_name;    
			$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);
	   	move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
    } else {
      $file_store=$this->input->post('oldimg');
    }
    
		$this->form_validation->set_rules('pagename', 'pagename', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('banner', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editbanner/$id");
		}	else {
			$data =array(
				'banner' 	   		=> 	$file_store,					
				'pagename' 			=> 	$pagename,
			);
		
			$this->BannerModel->update($tbl,$data,$where);
			$this->session->set_flashdata('banner', array('message' => 'Banner has been updated SuccessFully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/bannerlist');
		}
		
	}
	
	public function deletebanner($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_banner';
		$result =$this->BannerModel->delete($tbl,$where);	

		if($result)	{
			$this->session->set_flashdata('banner', array('message' => 'Banner has been deleted SuccessFully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/bannerlist");
		}	else {	
			$this->session->set_flashdata('banner', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/bannerlist");
		}
	}


	// Banner For Booking Registration Page----------------------
	public function bannerAddForBooking(){
	  $data['title'] = "Add Booking Banner";
	  $data['productlist'] = $this->BannerModel->getProductList();
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/bannerbookingadd',$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	// Banner Insert Process -------------------------
	public function bannerInsertForBooking() {
		$pid =$this->input->post('pid');
		$file_name = $_FILES["image"]["name"];
		$file_tem_loc = $_FILES["image"]["tmp_name"];   
		$file_store = $file_name;    
		$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);

   	move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
		$this->form_validation->set_rules('pid', 'pid', 'required');
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('banner', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/banneraddforbooking");
		}	else {
			$data =array(
				'prod_id' 	=> 	$pid,
				'type' 			=> 	'banner',
				'content' 	=> 	$file_store,
			);
			$tbl= 'tbl_booking_banner_and_content';
			$result = $this->BannerModel->insert($tbl,$data);
			if($result)	{
				$this->session->set_flashdata('banner', array('message' => 'Banner has been updated Successfully!!.','class' => 'alert alert-success'));
				redirect("site/core/micro/site/lib/controller/type/bannerlistforbooking");
			} else {
				$this->session->set_flashdata('banner', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));		
				redirect("site/core/micro/site/lib/controller/type/bannerlistforbooking");
			}
		}
	}

	// Banner List ------------------------------------
	public function bannerListForBooking(){	
	 	$list = $this->BannerModel->getbannerBookingDetails(); 
	 	$config = array(); 
		$config["base_url"] = base_url() . "admin/bannerlistforbooking";
		$config["total_rows"] = $list;
    $config["per_page"] = 20;
    $config["uri_segment"] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string']  = TRUE;
		$config['num_links'] = $config["total_rows"];
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		
		 if(isset($_GET['per_page']) && !empty($_GET['per_page'])){
			$page = $_GET['per_page'];
			$config['num_links'] = 5;
		}	else{
			$page = 1;
			$config['num_links'] = 5;
		}
		$this->pagination->initialize($config);
    $str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
    $startFrom = ($page * $config["per_page"]) - $config["per_page"];
    $data['startFrom']=$startFrom;
    $data['bannerlist'] = $this->BannerModel->getbannerBookingDetailsLimit($startFrom, $config["per_page"]);  

		$data['title'] = "Banner Booking List";
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/bannerbookinglisting',$data);
		$this->load->view('admin/common/footer',$data);
	}

	public function editbannerForBooking($id) {
		$data['title'] = "Edit Booking Banner";
		$data['id']=$id;
		 $data['productlist'] = $this->BannerModel->getProductList();
		$data['editbannerList'] = $this->BannerModel->getEditBannerBooking($id);		      
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/banner/bannerbookingedit',$data);
		$this->load->view('admin/common/footer',$data);
	}

	// Update Banner -----------------------------
	public function bannerupdateForBooking($id){
		$where = array(
			'id'=>$id
		);
		$tbl= 'tbl_booking_banner_and_content';
		$data['title'] = "Edit Booking Banner";		
		$pid =$this->input->post('pid');
   	if($_FILES['image']['name']!="") {
      $file_name = $_FILES["image"]["name"];
			$file_tem_loc = $_FILES["image"]["tmp_name"];   
			$file_store = $file_name;    
			$target_path = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . basename($_FILES['image']['name']);
	   	move_uploaded_file($_FILES["image"]["tmp_name"],$target_path);
    } else {
      $file_store=$this->input->post('oldimg');
    }
    
		$this->form_validation->set_rules('pid', 'pid', 'required');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('banner', array('message' => 'All field are required!!! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/editbanner/$id");
		}	else {
			
			$data =array(
				'prod_id' 	=> 	$pid,
				'type' 			=> 	'banner',
				'content' 	=> 	$file_store,
			);
		
			$this->BannerModel->update($tbl,$data,$where);
			$this->session->set_flashdata('banner', array('message' => 'Banner has been updated Successfully!!.','class' => 'alert alert-success'));
			redirect('site/core/micro/site/lib/controller/type/bannerlistforbooking');
		}		
	}
	
	public function deletebannerForBooking($id){ 
		$where = array(
			'id'=> $id
		);
		$tbl= 'tbl_booking_banner_and_content';
		$result =$this->BannerModel->delete($tbl,$where);
		if($result)	{
			$this->session->set_flashdata('banner', array('message' => 'Banner has been deleted Successfully!','class' => 'alert alert-success'));
			redirect("site/core/micro/site/lib/controller/type/bannerlistforbooking");
		}	else {	
			$this->session->set_flashdata('banner', array('message' => 'Something went wrong! Please try again.','class' => 'alert alert-danger'));
			redirect("site/core/micro/site/lib/controller/type/bannerlistforbooking");
		}
	}

}