<?php
defined('BASEPATH') || exit('No direct script access allowed');

class MainController extends CI_Controller {
	# constructor
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
    header("Access-Control-Allow-Methods: GET, OPTIONS");
		header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
  	$this->load->helper('string');
  	$this->load->helper('text');
    $this->load->library('form_validation');
    $this->load->library('session'); // loading session library
    date_default_timezone_set("Asia/Kolkata");
    $this->load->database(); //loading database
   	$this->load->model('MainModel'); 
   	$this->load->helper(array('cookie', 'url'));
   	$this->load->helper("security");
   	$this->load->library('pagination'); // pagination ---
   	$this->load->library('email');
   	$this->load->library('user_agent');
   	$this->limit_entry = 7; //total no. of entries by a mobile no.
		$this->block_time = 5; //minutes
	}

	public function checkBlock($mobile='') {
		if ($mobile=='') {
			redirect('');
		}
		$valid_entry = 0;
		$getotpblockedtime = $this->MainModel->getOTPBlockedTimeByMobile($mobile);
		$noofattempts = isset($getotpblockedtime[0]['noofattempts']) ? $getotpblockedtime[0]['noofattempts'] : 0;
		$to_time 	= strtotime(date('Y-m-d H:i:s'));
		$from_time 	= isset($getotpblockedtime[0]['created_on']) ? strtotime($getotpblockedtime[0]['created_on']) : strtotime(date('Y-m-d H:i:s', strtotime('-6 minutes')));
		$time_diff 	= round(abs($to_time - $from_time) / 60,2);
		
		if(!empty($getotpblockedtime) && $noofattempts>=$this->limit_entry && $time_diff<=$this->block_time) {
			$this->session->set_flashdata('msgshow', array('message' => "Your access is blocked. Please try after 5 minutes.",'class' => global_text_method('danger-class')));
			redirect('');
		}
	}

	// Session Check
	public function session_chk(){
		$sess_log = $this->session->userdata('logged_in');
		if ($sess_log == '' || $sess_log == 0) {
		  redirect('');
		}
	}

	public function session_mobile_chk(){
		$mobilelog = (int)$this->session->userdata('mobile');
		if ($mobilelog == '' || $mobilelog == 0) { 
		  redirect('');
		}
	}

	public function session_chk_for_thankyou(){
		$thankslog = (int)$this->session->userdata('thanks');
		if ($thankslog == '' || $thankslog == 0) { 
		  redirect('');
		}
	}

	# login page 
	public function index()	{
		$data['title'] = "Home Page";
		$this->session->set_userdata('logged_in', FALSE);
		$this->session->set_userdata('mobile','');
		$data['emailisrequiredornot'] = $this->MainModel->getEmailIsRequired();
		$screenRef = 'screenrefresh';
		$data['screenrefresh'] = $this->MainModel->getSingleRowFromSettings($screenRef);
		$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		//$data['banner'] = $this->MainModel->getBannerforPage();	
		$pagename = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pagename);
		$data['contentData'] = $this->MainModel->getContentforPage();
		$data['fieldLabelData'] = $this->MainModel->getFieldLabelwithoutpid();
		$pgname = 'home';
		$data['dynamicform'] = $this->MainModel->getDynamicForm($pgname);

		$ga_code = 'googleanalyticscode';
		$gaCodes = $this->MainModel->getCampaignIDforCashbackAPI($ga_code);
		$this->session->unset_userdata('gaCodes');
		$this->session->set_userdata('gaCodes', $gaCodes);
		$chat_code = 'chatcode';
		$chatCodes = $this->MainModel->getCampaignIDforCashbackAPI($chat_code);
		$this->session->unset_userdata('chatCodes');
		$this->session->set_userdata('chatCodes', $chatCodes);

		$this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/index',$data);
		$this->load->view('frontend/common/footer',$data);		
	}

	public function userLogin()	{
		$page = 'home';
		$this->load->library('email');
		$this->session->set_userdata('logged_in', FALSE);
		$this->session->set_userdata('mobile','');
		$vouchercodes = $this->input->post('vouchercode');
		$vouchercode = strtoupper($vouchercodes); 
		
		$name	=	$this->input->post('name');
		$name = $this->security->xss_clean($name);
		$name = str_replace("[removed]", "", $name);
    if(!$this->security->xss_clean($name)) {
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class')));
		} 
		$email	=	$this->input->post('email');
		$mobile = $this->input->post('mobile');
		$secret = htmlspecialchars(gScreteKey);
    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
    $responseData = json_decode($verifyResponse);

	  $st_time = (int)$this->input->post('st_time');

		$this->form_validation->set_rules('vouchercode', 'vouchercode', 'required|xss_clean');
		$this->form_validation->set_rules('name', 'name', "required|xss_clean|regex_match[/^([a-zA-Z ]+)$/]");
		$emailisrequiredornot = $this->MainModel->getEmailIsRequired();
		if(isset($emailisrequiredornot) && $emailisrequiredornot=='on') { 
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|xss_clean');
		}
		$this->form_validation->set_rules('mobile', 'mobile', 'required|xss_clean|regex_match[/^[0-9]{10}$/]');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1) {
				$this->checkBlock($mobile);
			    
			    // After Success/Failed API
				$username = CASHBACKAPIUSERNAME;
				$password = CASHBACKAPIPASSWORD;

				$campaignid = 'campaginidforcashbackapi';
				$campaignIDs = $this->MainModel->getCampaignIDforCashbackAPI($campaignid);
				
				//$campaignIDs = 83;
				$getUrl = "https://cbcapi.bigcityreward.com/api/checkvch?vcode=".$vouchercode."&campaign_id=".$campaignIDs;

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$getUrl);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
				$result = curl_exec($ch);
				$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
				curl_close($ch);
				$resp = json_decode($result, true);

				if (!empty($resp)) {				
					if ($resp['status']==true && $resp['voucher']['current_status']=='Failed' && $resp['voucher']['unblock_voucher']==true) {
						$this->MainModel->updateBookingStatusByVid($vouchercode);
					}
				}
				
				$tbl = 'tbl_voucher_mng';
				$result = $this->MainModel->get_where_like($tbl,$vouchercode);				
				$this->session->unset_userdata('mobile');
				$this->session->set_userdata('mobile', $mobile);
				$this->session->unset_userdata('name');
				$this->session->set_userdata('name', $name);
				$this->session->unset_userdata('email');
				$this->session->set_userdata('email', $email);
				$this->session->unset_userdata('id');
				$this->session->set_userdata('id', $result[0]->id);

				$this->session->set_userdata('limitcount',0);
				$this->session->unset_userdata('selectedoffer');
				$this->session->set_userdata('selectedoffer', $result[0]->product_id);

				if($result) {
					$getVoucher 		= $result[0]->vouchercode;
					$getStatus 		= $result[0]->booking_status;
					//print_r($result[0]->status); die();
					$expiry_date 		= strtotime($result[0]->expirydate);
					$current_date 	= strtotime(date("Y-m-d"));
					$cdate 					= date("Y-m-d");

					$caplimitTypes = $this->MainModel->getCaplimitTypes()->meta_value;
					$caplimitData = $this->MainModel->getCaplimitData($caplimitTypes)->meta_value;
					// For Current Day
					$currentD = date("Y-m-d");
					$CountRegisterDatePerDay  = $this->MainModel->getCountRegisterDatePerDay($mobile,$currentD);
					// For Current Week
					$monday = strtotime("last monday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					$sunday = strtotime(date("Y-m-d",$monday)." +6 days");					 
					$this_week_sd = date("Y-m-d",$monday);
					$this_week_ed = date("Y-m-d",$sunday);
					$CountRegisterDatePerWeek  = $this->MainModel->getCountRegisterDatePerWeek($mobile,$this_week_sd,$this_week_ed);
					// For Current Month
					$first_day_current_month = date('Y-m-01');
					$last_day_current_month  = date('Y-m-t');
					$CountRegisterDatePerMonth  = $this->MainModel->getCountRegisterDatePerMonth($mobile,$first_day_current_month,$last_day_current_month);

					$countMobile  = $this->MainModel->getCountMobileNumber($mobile);
					$countEmail   = $this->MainModel->getCountEmail($email);

					// Get Block Message
					$blockErrorMsg = $this->MainModel->getBlockMsg();

					$uid = $result[0]->id;
					//get Ip Address
					$current_ip = $this->input->ip_address();
					//get Block List
					$blockdata = $this->MainModel->getBlocklist($vouchercode,$email,$mobile,$current_ip);		
					
					if ($getStatus != 1) {
						if (count($blockdata)==0) {
							switch ($caplimitTypes) {
								case 'voucherperday':
									if ($caplimitData > $CountRegisterDatePerDay) {
										if ($expiry_date >= $current_date && $vouchercode == trim($getVoucher)) {
											$this->otpSentValidVoucher($name,$email,$mobile,$st_time);
										} else {
										$this->session->set_flashdata('msgshow', array('message' => 'Sorry, the last date to register the voucher is now over!','class' => global_text_method('danger-class')));
							      redirect('');
										} 
									} else {
										$dta = 'errormessage2voucherperday';
										$erMsg = $this->MainModel->getErrorMsg($dta);
										$this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => global_text_method('danger-class')));
										redirect('');
									}
									break;
								case 'voucherperweek':
									if ($caplimitData > $CountRegisterDatePerWeek) {
										if ($expiry_date >= $current_date && $vouchercode == trim($getVoucher)) {
											$this->otpSentValidVoucher($name,$email,$mobile,$st_time);
										} else {
										$this->session->set_flashdata('msgshow', array('message' => 'Sorry, the last date to register the voucher is now over!','class' => global_text_method('danger-class')));
							      redirect('');
										}
									} else {
										$dta = 'errormessage5voucherperweek';
										$erMsg = $this->MainModel->getErrorMsg($dta);
										$this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => global_text_method('danger-class')));							
							      redirect('');
									}
									break;
								case 'voucherpermonth':
									if ($caplimitData > $CountRegisterDatePerMonth) {
										if ($expiry_date >= $current_date && $vouchercode == trim($getVoucher)) {
											$this->otpSentValidVoucher($name,$email,$mobile,$st_time);
										} else {
										$this->session->set_flashdata('msgshow', array('message' => 'Sorry, the last date to register the voucher is now over!','class' => global_text_method('danger-class')));
							      redirect('');
										} 
									} else {
										$dta = 'errormessage10voucherpermonth';
										$erMsg = $this->MainModel->getErrorMsg($dta);
										$this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => global_text_method('danger-class')));							
							      redirect('');
							    }
									break;
								case 'voucherduringpromotion':
									if ($caplimitData > $countMobile) {
										if ($expiry_date >= $current_date && $vouchercode == trim($getVoucher)) {
											$this->otpSentValidVoucher($name,$email,$mobile,$st_time);
										} else {
										$this->session->set_flashdata('msgshow', array('message' => 'Sorry, the last date to register the voucher is now over!','class' => global_text_method('danger-class')));
							      redirect('');
										} 
									} else {
										$dta = 'errormessage15voucherduringpromotion';
										$erMsg = $this->MainModel->getErrorMsg($dta);
										$this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => global_text_method('danger-class')));						
							      redirect('');
									}
									break;
							}
						} else {
							foreach ($blockErrorMsg as $key => $val) {
								if ($val['type']=='mobile') {
									$blockmsg = $val['message'];
								}
							}
							$this->session->set_flashdata('msgshow', array('message' => $blockmsg , 'class' => global_text_method('danger-class')));						
				      redirect('');
						}
					} else {	
						$this->session->set_flashdata('msgshow', array('message' => 'This voucher code is already registered.','class' => global_text_method('danger-class')));					
						redirect('');
					}
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'Sorry, please enter the correct voucher code to register!','class' => global_text_method('danger-class')));
					redirect('');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class')));
				redirect('');
			}
		}
	}

	protected function otpSentValidVoucher($name,$email,$mobile,$st_time){
		$valid_entry = 0; 
		$u_id = $this->session->userdata('id');
		$getotpblockedtime = $this->MainModel->getOTPBlockedTimeByMobile($mobile);
		$noofattempts = isset($getotpblockedtime[0]['noofattempts']) ? $getotpblockedtime[0]['noofattempts'] : 0;
		$to_time 	= strtotime(date('Y-m-d H:i:s'));
		$from_time 	= isset($getotpblockedtime[0]['created_on']) ? strtotime($getotpblockedtime[0]['created_on']) : strtotime(date('Y-m-d H:i:s', strtotime('-6 minutes')));
		$time_diff 	= round(abs($to_time - $from_time) / 60,2);
		
		if(!empty($getotpblockedtime) && $noofattempts<$this->limit_entry && $time_diff<=$this->block_time) {
			$attemptdata = array(
				'noofattempts' 	=> ($noofattempts+1),
				'created_on'	=> date('Y-m-d H:i:s')
			);
			$where = array('mobile' => $mobile);
			$this->MainModel->update_login_attempts($attemptdata,$where);
			$valid_entry=1;
		} else {
			if(empty($getotpblockedtime) || $time_diff>$this->block_time){
				$attemptdata = array(
					'voucher_id' 	=> $u_id, 
					'mobile' 		=> $mobile,
					'noofattempts' 	=> 1,
				);
				$this->MainModel->otp_insert('tbl_loginattempts', $attemptdata);
				$valid_entry=1;
			} else {
				$this->session->set_flashdata('msgshow', array('message' => "You've already attempted ".$noofattempts." times. Please try after 5 minutes.",'class' => global_text_method('danger-class'), 'st_time' =>$st_time));
			  redirect('');
			}				
		}

		if($valid_entry==1){

			$number = random_int(1000000,9999999);
			$curl = curl_init();
			$otpmsg = $this->MainModel->getOTPMessage();
			$dynamic_otpmsg = trim($otpmsg->message);
			$replacements = [
		    "{{otp}}" => $number,
		    " " => '%20',
				"&" => '%26',
				"#" => '%23',
	      "'" => '%27',
	      "!" => '%21',
	      "*" => '%2A',
	      "(" => '%28',
	      ")" => '%29',
	      ";" => '%3B',
	      ":" => '%3A',
	      "@" => '%40',
	      "=" => '%3D',
	      "+" => '%2B',
	      "$" => '%24',
	      "," => '%2C',
	      "/" => '%2F',
	      "?" => '%3F',
	      "%" => '%25',
	      "#" => '%23',
	      "[" => '%5B',
	      "]" => '%5D',
			];
			$smsText = strtr($dynamic_otpmsg, $replacements);
			//--------------------------------------------------------------------//
			// Entity ID
			$entityID_metakey = 'entity_id';
			$entityID = $this->MainModel->getIDs($entityID_metakey);
			// OTP Template ID
			$OTP_TemplateID_metakey = 'otp_template_id';
			$OTP_TemplateID = $this->MainModel->getIDs($OTP_TemplateID_metakey);

			if ($entityID) {
			 	$smsEID = $entityID->meta_value;
			} else {
				$smsEID = '';
			}
			if ($OTP_TemplateID) {
			 	$smsTID = $OTP_TemplateID->meta_value;
			} else {
				$smsTID = '';
			}	
			
			//--------------------------------------------------------------------//
	    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => $url,
	      CURLOPT_RETURNTRANSFER => TRUE,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",)
	    );
	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);
	    
	    if ($err) {
	      echo "cURL Error #:" . $err;
	    } else {
	      $tbl = 'tbl_user_otp';
	      $mobdata = array(
	      	'user_mobile' => $mobile, 
	      	'user_otp' 		=> $number,
	      );

	      $mobdata = $this->security->xss_clean($mobdata);
	      if($this->security->xss_clean($mobdata)) {
					$arrRowInsert = $this->MainModel->otp_insert($tbl,$mobdata);
	      	$this->session->set_flashdata('msgshow', array('message' => 'OTP has been sent to your registered mobile number.','class' => global_text_method('success-class') ,'st_time' =>$st_time));
	      	redirect('otp');
				} else {
					$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_time' =>$st_time));
	      	redirect('');
				}      
	    }
	  } else {
	  	$this->session->set_flashdata('msgshow', array('message' => 'You have exceeded your limit. Please try after 5 minutes.','class' => global_text_method('danger-class') ,'st_time' =>$st_time));
			$this->session->unset_userdata('mobile');
			redirect('');
			exit();
	  }
	}

	# OTP page 
	public function otp()	{
		$this->session_mobile_chk();
		$mobile = $this->session->userdata('mobile');
		$this->checkBlock($mobile);
		$data['title'] = "OTP Page";
		$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		//$data['banner'] = $this->MainModel->getBannerforPage();	
		$pagename = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pagename);
		$data['contentData'] = $this->MainModel->getContentforPage();
		$data['fieldLabelData'] = $this->MainModel->getFieldLabelwithoutpid();

		$this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/otp',$data);
		$this->load->view('frontend/common/footer',$data);		
	}

	public function otp_check() {
		$this->session_mobile_chk();
		$mobile = $this->session->userdata('mobile');
		$name 	= $this->session->userdata('name');
		$email 	= $this->session->userdata('email');

		$user_otp		=	$this->input->post('otp');
		$st_time		=	(int)$this->input->post('st_time');
		$otp_time		=	(int)$this->input->post('otp_time');
		$sumofseconds = (int)$st_time+(int)$otp_time;
		$u_id = $this->session->userdata('id');

		$this->form_validation->set_rules('otp', 'otp', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("otp");
		}	else {
			$this->checkBlock($mobile);
			$tbl = 'tbl_user_otp';
			$results = $this->MainModel->get_where_otp($tbl,$mobile,$user_otp);

			$limitcounts = (!empty((int)$this->session->userdata('limitcount')) ? (int)$this->session->userdata('limitcount') : 0);

			$valid_entry = 0; 
			$getotpblockedtime = $this->MainModel->getOTPBlockedTimeByMobile($mobile);
			$noofattempts = isset($getotpblockedtime[0]['noofattempts']) ? $getotpblockedtime[0]['noofattempts'] : 0;
			$to_time 	= strtotime(date('Y-m-d H:i:s'));
			$from_time 	= isset($getotpblockedtime[0]['created_on']) ? strtotime($getotpblockedtime[0]['created_on']) : strtotime(date('Y-m-d H:i:s', strtotime('-6 minutes')));
			$time_diff 	= round(abs($to_time - $from_time) / 60,2);
			
			if(!empty($getotpblockedtime) && $noofattempts<$this->limit_entry && $time_diff<=$this->block_time) {
				$attemptdata = array(
					'noofattempts' 	=> ($noofattempts+1),
					'created_on'	=> date('Y-m-d H:i:s')
				);
				$where = array('mobile' => $mobile);
				$this->MainModel->update_login_attempts($attemptdata,$where);
				$valid_entry=1;
			} else {
				if(empty($getotpblockedtime) || $time_diff>$this->block_time){
					$attemptdata = array(
						'voucher_id' 	=> $u_id, 
						'mobile' 		=> $mobile,
						'noofattempts' 	=> 1,
					);
					$this->MainModel->otp_insert('tbl_loginattempts', $attemptdata);
					$valid_entry=1;
				} else {
					$this->session->set_flashdata('msgshow', array('message' => "You've already attempted ".$noofattempts." times. Please try after 5 minutes.",'class' => global_text_method('danger-class'), 'st_time' =>$st_time));
					$this->session->set_userdata('page_restrict', true);
				  redirect('');
				}				
			}

			$current_datetime = strtotime(date('Y-m-d H:i:s'));
			$otp_ctime = strtotime($results[0]->created_at);
			// Add 10 minutes
			$afterOTPadd10Minutes = $otp_ctime+(60*10);

			if($valid_entry==1){
				if($current_datetime <= $afterOTPadd10Minutes) {
					if(count($results)>0) {
						$get_otp = $results[0]->user_otp;
						if (trim($get_otp) == trim($user_otp)) {
							$cdate	= date("Y-m-d H:i:s");
							$ip 		= $this->input->ip_address();
							if ($this->agent->is_mobile()) {
							  $agent = "Mobile ".$this->agent->mobile();
							} else {
							  if ($this->agent->is_browser()) {
								  $agent = "Desktop ".$this->agent->browser().' '.$this->agent->version();
								} 
							}
							$tbl 		= 'tbl_voucher_mng';
							$mdata = array(
								'name' 						=> $name,
								'email' 					=> $email,
								'salt' 						=> SALT,
								'mobile' 					=> $mobile,
								'registereddate' 	=> $cdate,
								'ipaddress' 			=> $ip,
								'devicetype' 			=> $agent,
							);
							$mdata = $this->security->xss_clean($mdata);
			        if($this->security->xss_clean($mdata)) {
								$this->MainModel->update_Encrypt($tbl,$mdata,$u_id);
							} else {
								$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_time' =>$st_time));
				      	redirect('');
							}

							$usr_tbl = 'tbl_voucher_mng';				
							$check_usr = $this->MainModel->get_where_data($usr_tbl,$u_id);

							$getBookingFormID = $this->MainModel->getBookingFormById($check_usr[0]->product_id);

							foreach($check_usr as $key=>$row){
								$session_data= array(
									'id'			    =>	$row->id,
									'mobile'			=>	$row->mobile,
									'vouchercode' =>	$row->vouchercode,
									'name'   			=>  $row->name,
									'email'   		=>  $row->email,
									'product_id'  =>  $row->product_id,
									'bookingformId' => $getBookingFormID->booking_form_id,
									'logged_in'  	=>  TRUE,
								);	
								$this->session->set_userdata($session_data);
							}

							$uname = $check_usr[0]->name;
							$regmaildetails = $this->MainModel->getRegistrationMail();
							$dynamic_mail = $regmaildetails[0]['mail_body'];
			        $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
			              <tr>
			              	<td><strong>Voucher Code</strong></td>
			                <td><strong>Name</strong></td>
			                <td><strong>Mobile Number</strong></td>
			                <td><strong>Email ID</strong></td>
			                <td><strong>Date of Registration</strong></td>
			                <td><strong>Time of Registration</strong></td>
			              </tr>';
			            foreach($check_usr as $key=>$v) {
			            	$dt = explode(" ", $v->registereddate);
			              $codeMessage .= "<tr>
			              	<td>".$v->vouchercode."</td>
			              	<td>".$v->name."</td>
			              	<td>".$v->mobile."</td>
			              	<td>".$v->email."</td>
			              	<td>".$dt[0]."</td>
			              	<td>".$dt[1]."</td>
			              	</tr>";
			              }

				            $codeMessage .= '</table></td></tr>';
				            
							    	$replacements = [
										    "{{ConsumerName}}" => $uname,
										    "{{ConsumerDetails}}" => $codeMessage,
										];

							    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
										  <tr>
										    <td align="center">
										    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
										      <tr>
										        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
										        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
						          $message .= strtr($dynamic_mail, $replacements);
						        	$message .= '</table></td></tr></table></td></tr></table>';

						        $senderVar = 'registrationsendermail';
						        $regSender = $this->MainModel->getBookingSenderEmail($senderVar);
						        if ($check_usr[0]->email!='') {
							    	$to = $check_usr[0]->email;
									$subject = $regmaildetails[0]['mail_subject'];
									$this->send_Email($to, $subject, $message, $regSender->meta_value);
						        }
							$this->MainModel->update_Time($u_id, $sumofseconds);
							$this->session->set_flashdata('msgshow', array('message' => '','class' => '' ,'st_otp_time' =>$sumofseconds, 'prodid'=>$check_usr[0]->product_id));

							$allBookingPageData = '';
							$product_id = $this->session->userdata('product_id');
							$bookingform6banner = $this->MainModel->getBookingForm6Banner($product_id);
							$blockdates = $this->MainModel->getBlockDates();		
							//$commonbanner = $this->MainModel->getBannerforPage();
							$pagename = 'home';
					    $commonbanner = $this->MainModel->getBannerforPage($pagename);
							$banner = $this->MainModel->getBannerBookingforPage($product_id);
							$enbStateData = 'enabledisablestate';
							$stateEnable = $this->MainModel->getStateEnable($enbStateData);
							$enbcityData = 'enabledisablecity';
							$cityEnable = $this->MainModel->getCityEnable($enbcityData);
							$enbVenueData = 'enabledisablevenue';
							$venueEnable = $this->MainModel->getCityEnable($enbVenueData);
							$statelist = $this->MainModel->getStateList($product_id);
							//$statelist = $this->MainModel->getStateList($product_id);
							$citylist = $this->MainModel->getCityList($product_id);
							$circlelist = $this->MainModel->getCircleList($selectedoffer);
							$productList = $this->MainModel->getSubProductList($product_id);
							//print_r(count($productList)); die();
							$enbPeopleData = 'noofpeople';
							$peopleEnable = $this->MainModel->getPeopleEnable($enbPeopleData);
							$enbDiscountData = 'discountapplied';
							$discountEnable = $this->MainModel->getDiscountEnable($enbDiscountData);
							$contentData = $this->MainModel->getContentBookingforPage($product_id);
							$fieldLabelData = $this->MainModel->getFieldLabel($product_id);

							$weekendEnDis = 'weekenddisableenable';
							$weekendDisableEnable = $this->MainModel->getSingleRowFromSettings($weekendEnDis);
							//sonam
							$getSalonDiscountType = $this->MainModel->getSalonDiscountType();
							$enbSalonDiscountData = $getSalonDiscountType->meta_value;
							$discountSalonEnable = $this->MainModel->getSalonDiscountEnable($enbSalonDiscountData);

			                //sonam
							$daySlot = $this->MainModel->getDaySlot();
							$calenderBlocksDate = $this->MainModel->getCalenderBlock($product_id);
							$calBlockssDate = $calenderBlocksDate->blocksdate;
							$arrayBlocksDates = json_decode($calBlockssDate);

							$getDisableDate = $this->MainModel->getDisableDates($product_id);
							$getDateDisabled = $getDisableDate->setminimumnumberdisable;
							if(count($arrayBlocksDates)>0 || $getDateDisabled>0) {
								$blockDates = array();
								for ($i=1; $i <=$getDateDisabled ; $i++) { 
									$n = "+1 day";
									if ($i==1) {
										$currentDate = date("m/d/Y");
										$datetime = new DateTime($currentDate);
									} else {
										$datetime->modify($n);
									}			
								  $blockDates[] = $datetime->format("m/d/Y"); 
								}

								$numItemss = count($blockDates);
								$ii = 0;
								$lastDateDisabled = "";
								foreach ($blockDates as $key => $val) {
									if(++$ii === $numItemss) {
								    $lastDateDisabled = $val;
								  }
								}		
								$BlocksDateAll = array_merge($arrayBlocksDates, $blockDates);
								$numItems = count($BlocksDateAll);
								$iii = 0;
								$blDate = "";
								$blDate .= "[";
								foreach ($BlocksDateAll as $key => $val) {
									if(++$iii === $numItems) {
								    $blDate .= '"'.$val.'"';
								  } else {
										$blDate .= '"'.$val.'",';
									}
								}
								$blDate .= "]";
								$calBlocksDates = $blDate;
							}

							$getEnableDate = $this->MainModel->getEnableDates($product_id);
							$getDateEnabled = $getEnableDate->setmaximumnumberofenable;
							
							if($getDateEnabled>0) {
								if (isset($lastDateDisabled) && !empty($lastDateDisabled)) {
									$date = $lastDateDisabled;
									//$date1 = str_replace('-', '/', $date);
									$startEnable = date('m/d/Y',strtotime($date . "+1 days"));
								} else {
									$startEnable = date("m/d/Y");
								}
								//echo $startEnable; die;

								for ($i=1; $i <=$getDateEnabled ; $i++) { 
									$n = "+1 day";
									if ($i==1) {
										//$startEnable = date("m/d/Y");
										$datetime = new DateTime($startEnable);
									} else {
										$datetime->modify($n);
									}			
								  $enableDates[] = $datetime->format("m/d/Y"); 
								}

								$numItemsss = count($enableDates);
								$iiii = 0;
								$enDate = "";
								$enDate .= "[";
								foreach ($enableDates as $key => $val) {
									if(++$iiii === $numItemsss) {
								    $enDate .= '"'.$val.'"';
								  } else {
										$enDate .= '"'.$val.'",';
									}
								}
								$enDate .= "]";
								$calEnablesDates = $enDate;
							}

							$allBookingPageData = array(
								'bookingform6banner'			=>	$bookingform6banner,
								'blockdates'							=>	$blockdates,
								'commonbanner'   					=>  $commonbanner,
								'banner'   								=>  $banner,
								'stateEnable'  						=>  $stateEnable,
								'cityEnable'  						=>  $cityEnable,
								'venueEnable' 						=>  $venueEnable,
								'statelist'  							=>  $statelist,
								'citylist'  							=>  $citylist,
								'circlelist'  					    =>  $circlelist,
								'productList'   					=>  $productList,
								'contentData'  						=>  $contentData,
								'fieldLabelData' 					=>  $fieldLabelData,
								//'weekendDisableEnable'  	=>  $weekendDisableEnable,
								'daySlot'   							=>  $daySlot,
								'calBlocksDates'  				=>  $calBlocksDates,
								'calEnablesDates' 				=>  $calEnablesDates,
								'peopleEnable'  						=>  $peopleEnable,
								'discountEnable'                => $discountEnable,
								'discountSalonEnable'           =>  $discountSalonEnable,
							);	
							
							$this->session->set_userdata($allBookingPageData);
							$wheremobile = array('user_mobile' => $mobile);
							$this->MainModel->deletebyVoucherID('tbl_user_otp', $wheremobile);
							redirect('booking');
						} else {
							$this->session->set_flashdata('msgshow', array('message' => 'Please Enter Valid OTP','class' => global_text_method('danger-class') ,'st_time' =>$st_time));
							$limitcounts++;
							$newlimit = $this->session->set_userdata('limitcount', $limitcounts);
				      redirect('otp');
						}
					} else {
						$this->session->set_flashdata('msgshow', array('message' => 'Please Enter Valid OTP','class' => global_text_method('danger-class') ,'st_time' =>$st_time));
						$limitcounts++;
						$newlimit = $this->session->set_userdata('limitcount', $limitcounts);
						redirect('otp');
					}
				} else {
					$this->session->set_flashdata('msgshow', array('message' => 'OTP time has been expired. Please resend the OTP.','class' => global_text_method('danger-class') ,'st_time' =>$st_time, 'otp_resend_enable' => 'yes'));
					$limitcounts++;
					$newlimit = $this->session->set_userdata('limitcount', $limitcounts);
					redirect('otp');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => 'You have exceeded your limit. Please try after 5 minutes.','class' => global_text_method('danger-class') ,'st_time' =>$st_time));
				$this->session->unset_userdata('mobile');
				redirect('');
				exit();
			}
		}
	}

	public function booking()	{
		$this->session_chk();
		$mobile = $this->session->userdata('mobile');
		$this->checkBlock($mobile);
		$limitcounts = (!empty((int)$this->session->userdata('limitcount')) ? (int)$this->session->userdata('limitcount') : 0);
		if($limitcounts <= 10) {
			$data['title'] = "Booking Page";
			$data['menulist'] = $this->MainModel->getMenuList();
			$this->load->view('frontend/common/header',$data);
			$this->load->view('frontend/booking',$data);
			$this->load->view('frontend/common/footer',$data);
		} else {			
			$this->session->set_flashdata('msgshow', array('message' => 'You have exceeded your limit. Please try after sometime.','class' => global_text_method('danger-class')));
			redirect('');
			exit();
		}	
	}

	public function getvenue(){
		if($this->input->post('city')) {
   		
   		echo $this->MainModel->fetch_Venue($this->input->post('city'));
   	    }
  	}
  	public function getmeal(){
  	    
  		$t=date('d-m-Y');
        $currentDay = strtolower(date("l", strtotime($t)));
        
		if($this->input->post('preferedArea1')) {
   		
   		echo $this->MainModel->fetch_Meal($this->input->post('preferedArea1'),$currentDay);
  	    }
    }
  	public function getcity(){
		if($this->input->post('state_id')) {
   		echo $this->MainModel->fetch_City($this->input->post('state_id'));
  	}
	}

	public function dateSelect() {
		$selDate = $this->input->post('selectdate');
		$offerID = $this->session->userdata('selectedoffer');
		$getDay = date('l', strtotime($selDate));
		$lowerDay = strtolower($getDay);
		if($lowerDay) {
   		echo $this->MainModel->fetch_Timeslot($offerID,$lowerDay);
  	}
	}

	// For Offer One
	public function bookingDetailsForMovies(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$city							=	$this->input->post('city');
		$preferedArea1		=	$this->input->post('preferedArea1');
		$preferedArea2		=	$this->input->post('preferedArea2');
		$preferedMovie1		=	$this->input->post('preferedMovie1');
		$preferedMovie2		=	$this->input->post('preferedMovie2');
		$prefereddate1		=	$this->input->post('prefereddate1');
		$prefereddate2		=	$this->input->post('prefereddate2');
		$preferedTime1		=	$this->input->post('preferedTime1');
		$preferedTime2		=	$this->input->post('preferedTime2');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
        //$u_id = 5;
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('preferedMovie1', 'preferedMovie1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedMovie2', 'preferedMovie2', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate1', 'prefereddate1', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate2', 'prefereddate2', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime1', 'preferedTime1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime2', 'preferedTime2', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
				if($getuniqueid==0){
				$tbl = 'tbl_orders';

				$orderdata = array(
					'voucher_id' 				=> $u_id,
					'prod_id'						=> $product_id,
					'city' 							=> $city,
					'preferedArea1' 		=> $preferedArea1,
					'preferedArea2' 		=> $preferedArea2,
					'preferedMovie1' 	  => $preferedMovie1,
					'preferedMovie2' 		=> $preferedMovie2,
					'prefereddate1' 		=> $prefereddate1,
					'prefereddate2' 		=> $prefereddate2,
					'preferedTime1' 		=> trim($preferedTime1),
					'preferedTime2' 	  => trim($preferedTime2),
					'noofseconds' 			=> $st_otp_time+$booking_time,
				);

				$result = $this->MainModel->insertOrder($tbl,$orderdata);
				
			    
			    $this->MainModel->updateVoucherCodeById($u_id);
				$getRewards = $this->MainModel->getRewardCode($preferedMovie1,$u_id);				
				if (count($getRewards)>0) {
					$where = array(
			      'voucher_id' => $u_id,
			      'prod_id' => $product_id,
			    );
			    $data = array(
			      'status' => 'completed'
			    );
			    //$this->MainModel->updateProductOrder($where,$data);
			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
			    $rdata = array(
			    	'user_id' => $u_id,
			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
			    	'reward_code' => $getRewards[0]['rewardcode'],
			    );
			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
			    $rCode = $getRewards[0]['rewardcode'];
				} else {
					$where = array(
			      'voucher_id' => $u_id,
			      'prod_id' => $product_id,
			    );
			    $data = array(
			      'status' => 'processing'
			    );
			    $this->MainModel->updateProductOrder($where,$data);
			    $rCode = 'processing';
				}

				if ($result) {
					$usr_tbl = 'tbl_voucher_mng';
					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
					
					$mobile = $this->session->userdata('mobile');
					$uname 	= $this->session->userdata('name');
					$email 	= $this->session->userdata('email');
					$vouchercode	= $this->session->userdata('vouchercode');
					$uid = $u_id;

					$newName = explode(" ",$uname);
					$ufName = ($newName[0]) ? $newName[0] : $uname;

					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);			
					
					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
    		    	$replacements_sms = [
    		    		"{{ConsumerName}}" => $ufName,
    		    		"{{RewardCode}}"  => $rCode,
    				    " " => '%20',
    				    "&" => '%26',
    				    "#" => '%23',
                        "'" => '%27',
                        "!" => '%21',
                        "*" => '%2A',
                        "(" => '%28',
                        ")" => '%29',
                        ";" => '%3B',
                        ":" => '%3A',
                        "@" => '%40',
                        "=" => '%3D',
                        "+" => '%2B',
                        "$" => '%24',
                        "," => '%2C',
                        "/" => '%2F',
                        "?" => '%3F',
                        "%" => '%25',
                        "#" => '%23',
                        "[" => '%5B',
                        "]" => '%5D',
					];
					$d_sms = strtr($dynamic_sms, $replacements_sms);

					$allbookingsResultss = $this->MainModel->orderDetails($u_id);

					if($allbookingsResultss){
						$curl = curl_init();

						//--------------------------------------------------------------------//
						// Entity ID
						$entityID_metakey = 'entity_id';
						$entityID = $this->MainModel->getIDs($entityID_metakey);
						// Booking Template ID
						//$Booking_TemplateID_metakey = 'booking_template_id';
						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

						if ($entityID) {
						 	$smsEID = $entityID->meta_value;
						} else {
							$smsEID = '';
						}
						if ($bookingmaildetails[0]['smstmpid']) {
						 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
						} else {
							$smsTID = '';
						}	
					//--------------------------------------------------------------------//

				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";

				    // Booking Msg
				    $bookingMsg = array(
				    	'userid' => $uid, 
				    	'type' => 'msg', 
				    	'msgmail' => $d_sms,
				    );
				    $this->MainModel->insertMailMsg($bookingMsg);
				    
				    curl_setopt_array($curl, array(
				      CURLOPT_URL => $url,
				      CURLOPT_RETURNTRANSFER => TRUE,
				      CURLOPT_ENCODING => "",
				      CURLOPT_MAXREDIRS => 10,
				      CURLOPT_TIMEOUT => 30,
				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				      CURLOPT_CUSTOMREQUEST => "GET",)
				    );
				    $response = curl_exec($curl);
				    $err = curl_error($curl);
				    curl_close($curl);
				    if ($err) {
				      echo "cURL Error #:" . $err;
				    } else {

				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
				    	
	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
		                <tr>
		                	<td><strong>Location/City</strong></td>
		                  <td><strong>Prefered Area</strong></td>
                      <td><strong>Prefered Backup Area</strong></td>
                      <td><strong>Prefered Movie</strong></td>
                      <td><strong>Prefered Backup Movie</strong></td>
                      <td><strong>Prefered Date</strong></td>
                      <td><strong>Prefered Backup Date</strong></td>
                      <td><strong>Prefered Time</strong></td>
                      <td><strong>Prefered Backup Time</strong></td>
		                  <td><strong>Booking Date & Time</strong></td>
		                </tr>';
	                foreach($allbookingsResultss as $key=>$v) {
	                  $codeMessage .= "<tr>
	                  	<td>".$v['city']."</td>
	                  	<td>".$v['preferedArea1']."</td>
	                  	<td>".$v['preferedArea2']."</td>
	                  	<td>".$v['preferedMovie1']."</td>
	                  	<td>".$v['preferedMovie2']."</td>
	                  	<td>".$v['prefereddate1']."</td>
	                  	<td>".$v['prefereddate2']."</td>
	                  	<td>".$v['preferedTime1']."</td>
	                  	<td>".$v['preferedTime2']."</td>
	                  	<td>".$v['orderdatetime']."</td>
	                  	</tr>";
	                  }
	            $codeMessage .= '</table></td></tr>';
	            
				    	$replacements = [
							    "{{ConsumerName}}" => $uname,
							    "{{BookingDetails}}" => $codeMessage,
							    "{{RewardCode}}"  => $rCode,
							    "{{emailId}}" => $email,
							];

				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
							  <tr>
							    <td align="center">
							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
							      <tr>
							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
			          $message .= strtr($dynamic_mail, $replacements);
			        	$message .= '</table></td></tr></table></td></tr></table>';

			        $senderVar = 'bookingsuccesssendermail';
			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
			        
				    	$to = $email;
						$subject = $bookingmaildetails[0]['mail_subject'];
						if($to != ''){
						    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
						}

							// Booking Msg
				    $bookingMsg = array(
				    	'userid' => $uid, 
				    	'type' => 'mail', 
				    	'msgmail' => $message,
				    );
				    $this->MainModel->insertMailMsg($bookingMsg);
				    
				    $campaignidfrocrm = 'campaginidforcrm';
					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                    if(isset($campaignIdForCRM)){
					$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
					$offerid = $this->MainModel->getOfferIdFroCRM($product_id);

					if($rCode == 'processing') {
						$rrCode = '';
						$rCodeStatus = 'pending';
					} else {
						$rrCode = $rCode;
						$rCodeStatus = 'completed';
					}

				    /* Array Parameter Data */
				    $crmdata = array(
				    	'campaign_id' 				=> $campaignIdForCRM,
							'offer_id' 						=> $offerid->id,
							'voucher_id' 					=> $crmbookingsdata->voucher_id,
							'crm_offer_id' 				=> $offerid->crm_offer_id,
							'voucher_code' 				=> $crmbookingsdata->vouchercode,
							'cname' 							=> $crmbookingsdata->name,
							'cemail' 							=> $crmbookingsdata->email,
							'cmobile' 						=> $crmbookingsdata->mobile,
							'city' 								=> $crmbookingsdata->city,
							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
							'preferedTime2' 			=> $crmbookingsdata->preferedTime2,
							'status' 							=> '0',
							'reward_code' 				=> $rrCode,
							'reward_code_status' 	=> $rCodeStatus,
				    );
                    
                    /*print "<pre>"; print_r($crmdata); die;*/
				    /* CRM API URL */
				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
				    
				    /* CRM User name and password */
				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
						
				    $ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$crmurl);
					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
					$cresult = curl_exec($ch);
					
					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
					curl_close($ch);
					$crmresult = json_decode($cresult, true);
					
                    }
						redirect('thankyou');
				    }
					}
				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
	//Bogo offer nine
	//Method that handle when the payment was successful
	public function checkBookingDetailsForBogo() {
		$this->session_chk();
		ini_set('session.gc_maxlifetime', 604800);
		// each client should remember their session id for EXACTLY 1 week
		//session_set_cookie_params(604800);
		session_start(); // start the session
		
		$this->session->unset_userdata('city');
		$this->session->set_userdata('city', $this->input->post('city'));

		$this->session->unset_userdata('preferedArea1');
		$this->session->set_userdata('preferedArea1', $this->input->post('preferedArea1'));

		$this->session->unset_userdata('prefereddate1');
		$this->session->set_userdata('prefereddate1', $this->input->post('prefereddate1'));

		$this->session->unset_userdata('preferedTime1');
		$this->session->set_userdata('preferedTime1', $this->input->post('preferedTime1'));

		$this->session->unset_userdata('no_of_person');
		$this->session->set_userdata('no_of_person', $this->input->post('no_of_person'));

		$this->session->unset_userdata('discount');
		$this->session->set_userdata('discount', $this->input->post('discount'));

		$this->session->unset_userdata('total_amount_val');
		$this->session->set_userdata('total_amount_val', $this->input->post('total_amount_val'));

		$this->session->unset_userdata('offer_id');
		$this->session->set_userdata('offer_id', $this->input->post('offer_id'));


		$amount = $this->input->post('amount_val');
		$ttlamt = $this->input->post('total_amount_val');
		$dis_amt = $this->input->post('total_discount_val');
		$v_id = $this->session->userdata('id');
		$usr_tbl = 'tbl_voucher_mng';
    $cust_details = $this->MainModel->get_where_data($usr_tbl,$v_id);

    $product_info = $v_id;

    $customer_name = $cust_details[0]->name;
    $customer_emial = $cust_details[0]->email;
    $customer_mobile = $cust_details[0]->mobile;
    //$customer_address = $this->input->post('customer_address');
    
    //payumoney details	    
    $MERCHANT_KEY = PAYU_MONEY_MERCHANT_KEY; //change  merchant with yours
    $SALT = PAYU_MONEY_MERCHANT_SALT;  //change salt with yours 

    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    //optional udf values 
    $udf1 = '';
    $udf2 = '';
    $udf3 = '';
    $udf4 = '';
    $udf5 = '';
    
    $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
    $hash = strtolower(hash('sha512', $hashstring));
     
   	$success = base_url() . 'bookingdetailsforbogo';  
    $fail = base_url() . 'bookingdetailsforbogo';
    $cancel = base_url() . 'bookingdetailsforbogo';

    $PAYU_BASE_URL = PAYU_BASE_URL;

     $data = array(
      'mkey' => $MERCHANT_KEY,
      'tid' => $txnid,
      'hash' => $hash,
      'amount' => $amount,           
      'name' => $customer_name,
      'productinfo' => $product_info,
      'mailid' => $customer_emial,
      'phoneno' => $customer_mobile,
      //'address' => $customer_address,
      'action' => $PAYU_BASE_URL, //for live change action  https://secure.payu.in
      'sucess' => $success,
      'failure' => $fail,
      'cancel' => $cancel
    );

    // Insert Order History
    $tbl = 'tbl_orders';
		$orderdata = array(
			'voucher_id' 				=> $product_info,
			'prod_id'						=> $this->input->post('offer_id'),
			'city' 							=> $this->input->post('city'),
			'preferedArea1' 		=> $this->input->post('preferedArea1'),
			'prefereddate1' 		=> $this->input->post('prefereddate1'),
			'preferedTime1' 		=> $this->input->post('preferedTime1'),
			'no_of_person' 			=> $this->input->post('no_of_person'),
			'status'            =>'processing',
			'net_amount'        => $amount,
			'txnid'             => $txnid,
			'discount'          => $this->input->post('discount'),
			'total_amount'      => $ttlamt,
			'created_on'        => date('Y-m-d H:m:s')
 		);
    
		$result = $this->MainModel->insertOrder($tbl,$orderdata);
		//$this->session->set_flashdata('msg_success', "Payment was successful..");



    $data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		$pgname = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pgname);	
		$data['title']      = "Confirmation";

    $this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/confirmation',$data);
		$this->load->view('frontend/common/footer',$data);   
	}

	// For Bogo Offer Nine
	//Method that handle when the payment was successful
	public function bookingDetailsForBogo(){
		$status 					= $this->input->post('status');
		$offer_id 				= $this->session->userdata('offer_id');
		$city 						= $this->session->userdata('city');
		$preferedArea1 		= $this->session->userdata('preferedArea1');
		$prefereddate1 		= $this->session->userdata('prefereddate1');
		$preferedTime1 		= trim($this->session->userdata('preferedTime1'));
		$no_of_person 		= $this->session->userdata('no_of_person');
		$discount         = $this->session->userdata('discount');
		$total_amount     = $this->session->userdata('total_amount_val');


    if (empty($status)) {
      redirect('booking');
    }
     
      $firstname = $this->input->post('firstname');
      $amount = $this->input->post('amount');
      $txnid = $this->input->post('txnid');
      $posted_hash = $this->input->post('hash');
      $key = $this->input->post('key');
      $productinfo = $this->input->post('productinfo');
      $email = $this->input->post('email');
      $salt = PAYU_MONEY_MERCHANT_SALT; //  Your salt
      $add = $this->input->post('additionalCharges');

      If (isset($add)) {
        $additionalCharges = $this->input->post('additionalCharges');
        $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
      } else {
        $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
      }
     	$data['hash'] = hash("sha512", $retHashSeq);
      $data['amount'] = $amount;
      $data['txnid'] = $txnid;
      $data['posted_hash'] = $posted_hash;
      $data['status'] = $status;

      $v_id = $productinfo;

      if($status == 'success') {
      	$tbl = 'tbl_orders';
				$orderdata = array(
					'status'            =>'completed'
		 		);
		 		$where = array('voucher_id' => $v_id);
    
				$result = $this->MainModel->get_update($tbl,$orderdata,$where);
      	$this->MainModel->updateVoucherCodeById($v_id);

				$usr_tbl = 'tbl_voucher_mng';
				$usr_details = $this->MainModel->get_where_data($usr_tbl,$v_id);

				$uname = $usr_details[0]->name;
				$mobile = $usr_details[0]->mobile;
				$email = $usr_details[0]->email;
				$uid = $usr_details[0]->id;

				$newName = explode(" ",$uname);
				$ufName = ($newName[0]) ? $newName[0] : $uname;

				$bookingmaildetails = $this->MainModel->getBookingMail($offer_id);			
				
				$dynamic_sms = trim($bookingmaildetails[0]['sms']);
    	    	$replacements_sms = [
    	    		"{{ConsumerName}}" => $ufName,
    	    		//"{{RewardCode}}"  => $rCode,
    			    " " => '%20',
    			    "&" => '%26',
    			    "#" => '%23',
                    "'" => '%27',
                    "!" => '%21',
                    "*" => '%2A',
                    "(" => '%28',
                    ")" => '%29',
                    ";" => '%3B',
                    ":" => '%3A',
                    "@" => '%40',
                    "=" => '%3D',
                    "+" => '%2B',
                    "$" => '%24',
                    "," => '%2C',
                    "/" => '%2F',
                    "?" => '%3F',
                    "%" => '%25',
                    "#" => '%23',
                    "[" => '%5B',
                    "]" => '%5D',
				];
				$d_sms = strtr($dynamic_sms, $replacements_sms);
				$allbookingsResultss = $this->MainModel->orderDetails($v_id);

				//--------------------------------------------------------------------//
				// Entity ID
				$entityID_metakey = 'entity_id';
				$entityID = $this->MainModel->getIDs($entityID_metakey);
				// Booking Template ID
				//$Booking_TemplateID_metakey = 'booking_template_id';
				//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

				if ($entityID) {
				 	$smsEID = $entityID->meta_value;
				} else {
					$smsEID = '';
				}
				if ($bookingmaildetails[0]['smstmpid']) {
				 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
				} else {
					$smsTID = '';
				}	
			//--------------------------------------------------------------------//

				if($allbookingsResultss){
					$curl = curl_init();
			    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";

			    // Booking Msg
			    $bookingMsg = array(
			    	'userid' => $v_id, 
			    	'type' => 'msg', 
			    	'msgmail' => $d_sms,
			    );
			    $this->MainModel->insertMailMsg($bookingMsg);
			    
			    curl_setopt_array($curl, array(
			      CURLOPT_URL => $url,
			      CURLOPT_RETURNTRANSFER => TRUE,
			      CURLOPT_ENCODING => "",
			      CURLOPT_MAXREDIRS => 10,
			      CURLOPT_TIMEOUT => 30,
			      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			      CURLOPT_CUSTOMREQUEST => "GET",)
			    );
			    $response = curl_exec($curl);
			    $err = curl_error($curl);
			    curl_close($curl);
			    if ($err) {
			      echo "cURL Error #:" . $err;
			    } else {

			    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
			    	
	          $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
	                <tr>
	                	<td><strong>Location/City</strong></td>
	                  <td><strong>Prefered Offer</strong></td>
	                  <td><strong>Prefered Backup Offer</strong></td>
	                  <td><strong>Prefered Date</strong></td>
	                  <td><strong>Prefered Backup Date</strong></td>
	                  <td><strong>Prefered Time</strong></td>
	                  <td><strong>Prefered Backup Time</strong></td>
	                  <td><strong>Booking Date & Time</strong></td>
	                </tr>';
	              foreach($allbookingsResultss as $key=>$v) {
	                $codeMessage .= "<tr>
	                	<td>".$v['city']."</td>
	                	<td>".$v['preferedMovie1']."</td>
	                	<td>".$v['preferedMovie2']."</td>
	                	<td>".$v['prefereddate1']."</td>
	                	<td>".$v['prefereddate2']."</td>
	                	<td>".$v['preferedTime1']."</td>
	                	<td>".$v['preferedTime2']."</td>
	                	<td>".$v['orderdatetime']."</td>
	                	</tr>";
	                }
	          $codeMessage .= '</table></td></tr>';
	          
			    	$replacements = [
						    "{{ConsumerName}}" => $uname,
						    "{{BookingDetails}}" => $codeMessage,
						    "{{emailId}}" => $email,
						];

			    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
						  <tr>
						    <td align="center">
						    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
						      <tr>
						        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
						        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
		          $message .= strtr($dynamic_mail, $replacements);
		        	$message .= '</table></td></tr></table></td></tr></table>';

		        $senderVar = 'bookingsuccesssendermail';
		        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
		        
			    	$to = $email;
					$subject = $bookingmaildetails[0]['mail_subject'];
					if($to != ''){
					    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
					}
					// Booking Msg
				    $bookingMsg = array(
				    	'userid' => $v_id, 
				    	'type' => 'mail', 
				    	'msgmail' => $message,
				    );
				    $this->MainModel->insertMailMsg($bookingMsg);
				  }

				  $campaignidfrocrm = 'campaginidforcrm';
					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                    if(isset($campaignIdForCRM)){
						$crmbookingsdata = $this->MainModel->orderDetailsForCRM($v_id);
						$offerid = $this->MainModel->getOfferIdFroCRM($crmbookingsdata->product_id);

    				    /* Array Parameter Data */
    				    $crmdata = array(
    				    	'campaign_id' 				=> $campaignIdForCRM,
							'offer_id' 					=> $offerid->id,
							'voucher_id' 				=> $crmbookingsdata->voucher_id,
							'crm_offer_id' 				=> $offerid->crm_offer_id,
							'voucher_code' 				=> $crmbookingsdata->vouchercode,
							'cname' 					=> $crmbookingsdata->name,
							'cemail' 					=> $crmbookingsdata->email,
							'cmobile' 					=> $crmbookingsdata->mobile,
							'city' 						=> $crmbookingsdata->city,
							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
							'preferedTime2' 			=> $crmbookingsdata->preferedTime2,
							'status' 					=> '0',
							'reward_code' 				=> '',
							'reward_code_status' 	=> 'completed',
				    );
            
				    /* CRM API URL */
				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
				    
				    /* CRM User name and password */
				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
						
				    $ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,$crmurl);
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
						curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
						$cresult = curl_exec($ch);
						
						$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
						curl_close($ch);
						$crmresult = json_decode($cresult, true);
					}

					redirect('thankyou');
				}
    } else {
    	$tbl = 'tbl_orders';
	 		$where = array('voucher_id' => $v_id);
	 		$this->MainModel->deletebyVoucherID($tbl,$where);

      $data['blockdates'] = $this->MainModel->getBlockDates();
			$data['menulist'] = $this->MainModel->getMenuList();
			$pgname = 'home';
			$data['banner'] = $this->MainModel->getBannerforPage($pgname);	
	    $this->load->view('frontend/common/header',$data);
			$this->load->view('frontend/failure',$data);
			$this->load->view('frontend/common/footer',$data); 
    }
	}


	//Bogo offer nine
	//Method that handle when the payment was successful
	public function checkBookingDetailsForSalon() {
		$this->session_chk();
		ini_set('session.gc_maxlifetime', 604800);
		// each client should remember their session id for EXACTLY 1 week
		//session_set_cookie_params(604800);
		session_start(); // start the session

		$amount = $this->input->post('amount_val');
		$ttlamt = $this->input->post('total_amount_val');
		$dis_amt = $this->input->post('total_discount_val');
		$v_id = $this->session->userdata('id');
		$usr_tbl = 'tbl_voucher_mng';
    $cust_details = $this->MainModel->get_where_data($usr_tbl,$v_id);

    $product_info = $v_id;

    $customer_name = $cust_details[0]->name;
    $customer_emial = $cust_details[0]->email;
    $customer_mobile = $cust_details[0]->mobile;
    //$customer_address = $this->input->post('customer_address');
    
    //payumoney details	    
    $MERCHANT_KEY = PAYU_MONEY_MERCHANT_KEY; //change  merchant with yours
    $SALT = PAYU_MONEY_MERCHANT_SALT;  //change salt with yours 

    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    //optional udf values 
    $udf1 = '';
    $udf2 = '';
    $udf3 = '';
    $udf4 = '';
    $udf5 = '';
    
    $hashstring = $MERCHANT_KEY . '|' . $txnid . '|' . $amount . '|' . $product_info . '|' . $customer_name . '|' . $customer_emial . '|' . $udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $SALT;
    $hash = strtolower(hash('sha512', $hashstring));
     
   	$success = base_url() . 'bookingdetailsforsalonoff';  
    $fail = base_url() . 'bookingdetailsforsalonoff';
    $cancel = base_url() . 'bookingdetailsforsalonoff';

    $PAYU_BASE_URL = PAYU_BASE_URL;

     $data = array(
      'mkey' => $MERCHANT_KEY,
      'tid' => $txnid,
      'hash' => $hash,
      'amount' => $amount,           
      'name' => $customer_name,
      'productinfo' => $product_info,
      'mailid' => $customer_emial,
      'phoneno' => $customer_mobile,
      //'address' => $customer_address,
      'action' => $PAYU_BASE_URL, //for live change action  https://secure.payu.in
      'sucess' => $success,
      'failure' => $fail,
      'cancel' => $cancel            
    );

    // Insert Order History
    $tbl = 'tbl_orders';
		$orderdata = array(
			'voucher_id' 				=> $product_info,
			'prod_id'						=> $this->input->post('offer_id'),
			'city' 							=> $this->input->post('city'),
			'preferedArea1' 		=> $this->input->post('preferedArea1'),
			'prefereddate1' 		=> $this->input->post('prefereddate1'),
			'preferedTime1' 		=> $this->input->post('preferedTime1'),
			'no_of_person' 			=> $this->input->post('no_of_person'),
			'status'            =>'processing',
			'net_amount'        => $amount,
			'txnid'             => $txnid,
			'discount'          => $this->input->post('discount'),
			'total_amount'      => $ttlamt,
			'created_on'        => date('Y-m-d H:m:s')
 		);
    
		$result = $this->MainModel->insertOrder($tbl,$orderdata);
		//$this->session->set_flashdata('msg_success', "Payment was successful..");
    //$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		$pgname = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pgname);	
		$data['title']      = "Confirmation";

    $this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/confirmation',$data);
		$this->load->view('frontend/common/footer',$data);   
	}

	//Method that handle when the payment was successful
	public function BookingDetailsForSalonOff() {
		$status 					= $this->input->post('status');
		$offer_id 				= $this->session->userdata('offer_id');
		$city 						= $this->session->userdata('city');
		$preferedArea1 		= $this->session->userdata('preferedArea1');
		$prefereddate1 		= $this->session->userdata('prefereddate1');
		$preferedTime1 		= trim($this->session->userdata('preferedTime1'));
		$no_of_person 		= $this->session->userdata('no_of_person');
		$discount         = $this->session->userdata('discount');
		$total_amount     = $this->session->userdata('total_amount_val');


    if (empty($status)) {
      redirect('booking');
    }

      $firstname = $this->input->post('firstname');
      $amount = $this->input->post('amount');
      $txnid = $this->input->post('txnid');
      $posted_hash = $this->input->post('hash');
      $key = $this->input->post('key');
      $productinfo = $this->input->post('productinfo');
      $email = $this->input->post('email');
      $salt = PAYU_MONEY_MERCHANT_SALT; //  Your salt
      $add = $this->input->post('additionalCharges');

      If (isset($add)) {
        $additionalCharges = $this->input->post('additionalCharges');
        $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
      } else {
        $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
      }
     	$data['hash'] = hash("sha512", $retHashSeq);
      $data['amount'] = $amount;
      $data['txnid'] = $txnid;
      $data['posted_hash'] = $posted_hash;
      $data['status'] = $status;

      $v_id = $productinfo;

      if($status == 'success') {
      	$tbl = 'tbl_orders';
				$orderdata = array(
					'status'            =>'completed'
		 		);
		 		$where = array('voucher_id' => $v_id);
    
				$result = $this->MainModel->get_update($tbl,$orderdata,$where);
      	$this->MainModel->updateVoucherCodeById($v_id);

				$usr_tbl = 'tbl_voucher_mng';
				$usr_details = $this->MainModel->get_where_data($usr_tbl,$v_id);

				$uname = $usr_details[0]->name;
				$mobile = $usr_details[0]->mobile;
				$email = $usr_details[0]->email;
				$uid = $usr_details[0]->id;

				$newName = explode(" ",$uname);
				$ufName = ($newName[0]) ? $newName[0] : $uname;

				$bookingmaildetails = $this->MainModel->getBookingMail($offer_id);			
				
				$dynamic_sms = trim($bookingmaildetails[0]['sms']);
    	    	$replacements_sms = [
    	    		"{{ConsumerName}}" => $ufName,
    	    		//"{{RewardCode}}"  => $rCode,
    			    " " => '%20',
    			    "&" => '%26',
    			    "#" => '%23',
                    "'" => '%27',
                    "!" => '%21',
                    "*" => '%2A',
                    "(" => '%28',
                    ")" => '%29',
                    ";" => '%3B',
                    ":" => '%3A',
                    "@" => '%40',
                    "=" => '%3D',
                    "+" => '%2B',
                    "$" => '%24',
                    "," => '%2C',
                    "/" => '%2F',
                    "?" => '%3F',
                    "%" => '%25',
                    "#" => '%23',
                    "[" => '%5B',
                    "]" => '%5D',
				];
				$d_sms = strtr($dynamic_sms, $replacements_sms);
				$allbookingsResultss = $this->MainModel->orderDetails($v_id);

				//--------------------------------------------------------------------//
				// Entity ID
				$entityID_metakey = 'entity_id';
				$entityID = $this->MainModel->getIDs($entityID_metakey);
				// Booking Template ID
				//$Booking_TemplateID_metakey = 'booking_template_id';
				//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

				if ($entityID) {
				 	$smsEID = $entityID->meta_value;
				} else {
					$smsEID = '';
				}
				if ($bookingmaildetails[0]['smstmpid']) {
				 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
				} else {
					$smsTID = '';
				}	
			//--------------------------------------------------------------------//

				if($allbookingsResultss){
					$curl = curl_init();
			    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";

			    // Booking Msg
			    $bookingMsg = array(
			    	'userid' => $v_id, 
			    	'type' => 'msg', 
			    	'msgmail' => $d_sms,
			    );
			    $this->MainModel->insertMailMsg($bookingMsg);
			    
			    curl_setopt_array($curl, array(
			      CURLOPT_URL => $url,
			      CURLOPT_RETURNTRANSFER => TRUE,
			      CURLOPT_ENCODING => "",
			      CURLOPT_MAXREDIRS => 10,
			      CURLOPT_TIMEOUT => 30,
			      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			      CURLOPT_CUSTOMREQUEST => "GET",)
			    );
			    $response = curl_exec($curl);
			    $err = curl_error($curl);
			    curl_close($curl);
			    if ($err) {
			      echo "cURL Error #:" . $err;
			    } else {

			    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
			    	
	          $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
	                <tr>
	                	<td><strong>Location/City</strong></td>
	                  <td><strong>Prefered Offer</strong></td>
	                  <td><strong>Prefered Backup Offer</strong></td>
	                  <td><strong>Prefered Date</strong></td>
	                  <td><strong>Prefered Backup Date</strong></td>
	                  <td><strong>Prefered Time</strong></td>
	                  <td><strong>Prefered Backup Time</strong></td>
	                  <td><strong>Booking Date & Time</strong></td>
	                </tr>';
	              foreach($allbookingsResultss as $key=>$v) {
	                $codeMessage .= "<tr>
	                	<td>".$v['city']."</td>
	                	<td>".$v['preferedMovie1']."</td>
	                	<td>".$v['preferedMovie2']."</td>
	                	<td>".$v['prefereddate1']."</td>
	                	<td>".$v['prefereddate2']."</td>
	                	<td>".$v['preferedTime1']."</td>
	                	<td>".$v['preferedTime2']."</td>
	                	<td>".$v['orderdatetime']."</td>
	                	</tr>";
	                }
	          $codeMessage .= '</table></td></tr>';
	          
			    	$replacements = [
						    "{{ConsumerName}}" => $uname,
						    "{{BookingDetails}}" => $codeMessage,
						    "{{emailId}}" => $email,
						];

			    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
						  <tr>
						    <td align="center">
						    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
						      <tr>
						        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
						        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
		          $message .= strtr($dynamic_mail, $replacements);
		        	$message .= '</table></td></tr></table></td></tr></table>';

		        $senderVar = 'bookingsuccesssendermail';
		        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
		        
			    	$to = $email;
					$subject = $bookingmaildetails[0]['mail_subject'];
					if($to != '') {
					    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
					}

					// Booking Msg
				    $bookingMsg = array(
				    	'userid' => $v_id, 
				    	'type' => 'mail', 
				    	'msgmail' => $message,
				    );
				    $this->MainModel->insertMailMsg($bookingMsg);
				  }

				  $campaignidfrocrm = 'campaginidforcrm';
					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                    if(isset($campaignIdForCRM)){
						$crmbookingsdata = $this->MainModel->orderDetailsForCRM($v_id);
						$offerid = $this->MainModel->getOfferIdFroCRM($crmbookingsdata->product_id);

    				    /* Array Parameter Data */
    				    $crmdata = array(
				    	    'campaign_id' 				=> $campaignIdForCRM,
							'offer_id' 						=> $offerid->id,
							'voucher_id' 					=> $crmbookingsdata->voucher_id,
							'crm_offer_id' 				=> $offerid->crm_offer_id,
							'voucher_code' 				=> $crmbookingsdata->vouchercode,
							'cname' 							=> $crmbookingsdata->name,
							'cemail' 							=> $crmbookingsdata->email,
							'cmobile' 						=> $crmbookingsdata->mobile,
							'city' 								=> $crmbookingsdata->city,
							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
							'preferedTime2' 			=> $crmbookingsdata->preferedTime2,
							'status' 							=> '0',
							'reward_code' 				=> '',
							'reward_code_status' 	=> 'completed',
				    );
            
				    /* CRM API URL */
				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
				    
				    /* CRM User name and password */
				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
						
				    $ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,$crmurl);
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
						curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
						curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
						$cresult = curl_exec($ch);
						
						$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
						curl_close($ch);
						$crmresult = json_decode($cresult, true);
					}

					redirect('thankyou');
				}
    } else {
    	$tbl = 'tbl_orders';
	 		$where = array('voucher_id' => $v_id);
	 		$this->MainModel->deletebyVoucherID($tbl,$where);

      $data['blockdates'] = $this->MainModel->getBlockDates();
			$data['menulist'] = $this->MainModel->getMenuList();
			$pgname = 'home';
			$data['banner'] = $this->MainModel->getBannerforPage($pgname);	
	    $this->load->view('frontend/common/header',$data);
			$this->load->view('frontend/failure',$data);
			$this->load->view('frontend/common/footer',$data); 
    }
	}

    
	//Method that handles when payment was failed
	public function error(){
		unset($_POST);
		$this->session->set_flashdata('msg_error', "Your payment was failed. Try again..");
		redirect('booking');
	}

	//Method that handles when payment was cancelled.
	public function cancel(){
		$this->MainModel->updateVoucherCodeById($this->session->userdata('id'));
		unset($_POST);
		$this->session->set_flashdata('msg_error', "Your payment was cancelled. Try again..");
		redirect('cancel-view');
	}

	// For Offer Two
	public function bookingDetailsForOffer2(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$city							=	$this->input->post('city');
		$preferedArea1		=	$this->input->post('preferedArea1');
		$preferedArea2		=	$this->input->post('preferedArea2');
		$preferedoffer		=	$this->input->post('preferedoffer');
		$prefereddate1		=	$this->input->post('prefereddate1');
		$prefereddate2		=	$this->input->post('prefereddate2');
		$preferedTime1		=	$this->input->post('preferedTime1');
		$preferedTime2		=	$this->input->post('preferedTime2');
		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');

		$secret = htmlspecialchars(gScreteKey);
        $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
        $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
        $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
        //$u_id = 5;
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('preferedArea1', 'preferedArea1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedArea2', 'preferedArea2', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate1', 'prefereddate1', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate2', 'prefereddate2', 'required|xss_clean');
		$this->form_validation->set_rules('preferedoffer', 'preferedoffer', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime1', 'preferedTime1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime2', 'preferedTime2', 'required|xss_clean');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    
    				$orderdata = array(
    					'voucher_id' 				=> $u_id,
    					'prod_id'						=> $product_id,
    					'city' 							=> $city,
    					'preferedArea1' 		=> $preferedArea1,
    					'preferedArea2' 		=> $preferedArea2,
    					'preferedMovie1' 	  => $preferedoffer,
    					'preferedMovie2' 		=> '',
    					'prefereddate1' 		=> $prefereddate1,
    					'prefereddate2' 		=> $prefereddate2,
    					'preferedTime1' 		=> $preferedTime1,
    					'preferedTime2' 	  => $preferedTime2,
    					'noofseconds' 			=> $st_otp_time+$booking_time,
    				);
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    			    $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($preferedoffer,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}" => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    					//--------------------------------------------------------------------//
    					
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
    		                	<td><strong>Location/City</strong></td>
    		                  <td><strong>Preferred Venue</strong></td>
                          <td><strong>Prefered Backup Venue</strong></td>
                          <td><strong>Prefered Offer</strong></td>
                          <td><strong>Prefered Date</strong></td>
                          <td><strong>Prefered Backup Date</strong></td>
                          <td><strong>Prefered Time</strong></td>
                          <td><strong>Prefered Backup Time</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['city']."</td>
    	                  	<td>".$v['preferedArea1']."</td>
    	                  	<td>".$v['preferedArea2']."</td>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['prefereddate1']."</td>
    	                  	<td>".$v['prefereddate2']."</td>
    	                  	<td>".$v['preferedTime1']."</td>
    	                  	<td>".$v['preferedTime2']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}" => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
				    	$to = $email;
						$subject = $bookingmaildetails[0]['mail_subject'];
						if($to != ''){
						    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
						}

						// Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'mail', 
    				    	'msgmail' => $message,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
                        
                        $campaignidfrocrm = 'campaginidforcrm';
    					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                        if(isset($campaignIdForCRM)){
    					$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    					$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    					if($rCode == 'processing') {
    						$rrCode = '';
    						$rCodeStatus = 'pending';
    					} else {
    						$rrCode = $rCode;
    						$rCodeStatus = 'completed';
    					}
    
    				    /* Array Parameter Data */
    				    $crmdata = array(
    				    	'campaign_id' 				=> $campaignIdForCRM,
    							'offer_id' 						=> $offerid->id,
    							'voucher_id' 					=> $crmbookingsdata->voucher_id,
    							'crm_offer_id' 				=> $offerid->crm_offer_id,
    							'voucher_code' 				=> $crmbookingsdata->vouchercode,
    							'cname' 							=> $crmbookingsdata->name,
    							'cemail' 							=> $crmbookingsdata->email,
    							'cmobile' 						=> $crmbookingsdata->mobile,
    							'city' 								=> $crmbookingsdata->city,
    							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
    							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
    							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
    							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
    							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    							'preferedTime2' 				=> $crmbookingsdata->preferedTime2,
    							'status' 							=> '0',
    							'reward_code' 				=> $rrCode,
    							'reward_code_status' 	=> $rCodeStatus,
    				    );
    
    				    /* CRM API URL */
    				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    				    /* CRM User name and password */
    				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    						
    				    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$crmurl);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$cresult = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$crmresult = json_decode($cresult, true);
                        }
    					redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}

	// For Offer Three
	public function bookingDetailsForOffer3(){
		$this->session_chk();
		$product_id 		= $this->session->userdata('product_id');
		$city				=	$this->input->post('city');
		$preferedoffer		=	$this->input->post('preferedoffer');
		$preferedArea1		=	$this->input->post('preferedArea1');
		$preferedArea2		=	$this->input->post('preferedArea2');
		$prefereddate1		=	$this->input->post('prefereddate1');
		$prefereddate2		=	$this->input->post('prefereddate2');
		$preferedTime1		=	$this->input->post('preferedTime1');
		$preferedTime2		=	$this->input->post('preferedTime2');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');

		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
   
		$this->form_validation->set_rules('city', 'city', 'required|xss_clean');
		$this->form_validation->set_rules('preferedoffer', 'preferedoffer', 'required|xss_clean');
		$this->form_validation->set_rules('preferedArea1', 'preferedArea1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedArea2', 'preferedArea2', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate1', 'prefereddate1', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate2', 'prefereddate2', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime1', 'preferedTime1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime2', 'preferedTime2', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    				$orderdata = array(
    					'voucher_id' 				=> $u_id,
    					'prod_id'						=> $product_id,
    					'city' 							=> $city,
    					'preferedArea1' 		=> $preferedArea1,
    					'preferedArea2' 		=> $preferedArea2,
    					'preferedMovie1' 	  => $preferedoffer,
    					'preferedMovie2' 		=> '',
    					'prefereddate1' 		=> $prefereddate1,
    					'prefereddate2' 		=> $prefereddate2,
    					'preferedTime1' 		=> trim($preferedTime1),
    					'preferedTime2' 	  => trim($preferedTime2),
    					'noofseconds' 			=> $st_otp_time+$booking_time,
    				);
    
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				
    		    
    		        $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($preferedoffer,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}" => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    					//--------------------------------------------------------------------//
    
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
    		                	<td><strong>Location/City</strong></td>
    		                  <td><strong>Prefered Area</strong></td>
                          <td><strong>Prefered Backup Area</strong></td>
                          <td><strong>Prefered Offer</strong></td>
                          <td><strong>Prefered Date</strong></td>
                          <td><strong>Prefered Backup Date</strong></td>
                          <td><strong>Prefered Time</strong></td>
                          <td><strong>Prefered Backup Time</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['city']."</td>
    	                  	<td>".$v['preferedArea1']."</td>
    	                  	<td>".$v['preferedArea2']."</td>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['prefereddate1']."</td>
    	                  	<td>".$v['prefereddate2']."</td>
    	                  	<td>".$v['preferedTime1']."</td>
    	                  	<td>".$v['preferedTime2']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}" => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    				    	$to = $email;
    							$subject = $bookingmaildetails[0]['mail_subject'];
    							$this->send_Email($to, $subject, $message, $bookingSender->meta_value);
    
    							// Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'mail', 
    				    	'msgmail' => $message,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    $campaignidfrocrm = 'campaginidforcrm';
    					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                        if(isset($campaignIdForCRM)){
    					$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    					$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    					if($rCode == 'processing') {
    						$rrCode = '';
    						$rCodeStatus = 'pending';
    					} else {
    						$rrCode = $rCode;
    						$rCodeStatus = 'completed';
    					}
    
    				    /* Array Parameter Data */
    				    $crmdata = array(
    				    	'campaign_id' 				=> $campaignIdForCRM,
    							'offer_id' 						=> $offerid->id,
    							'voucher_id' 					=> $crmbookingsdata->voucher_id,
    							'crm_offer_id' 				=> $offerid->crm_offer_id,
    							'voucher_code' 				=> $crmbookingsdata->vouchercode,
    							'cname' 							=> $crmbookingsdata->name,
    							'cemail' 							=> $crmbookingsdata->email,
    							'cmobile' 						=> $crmbookingsdata->mobile,
    							'city' 								=> $crmbookingsdata->city,
    							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
    							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
    							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
    							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
    							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    							'preferedTime2' 				=> $crmbookingsdata->preferedTime2,
    							'status' 							=> '0',
    							'reward_code' 				=> $rrCode,
    							'reward_code_status' 	=> $rCodeStatus,
    				    );
    
    				    /* CRM API URL */
    				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    				    /* CRM User name and password */
    				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    						
    				    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$crmurl);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$cresult = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$crmresult = json_decode($cresult, true);
                        }
    					redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}

	// For Offer Four
	public function bookingDetailsForOffer4(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		//$product_id 			= 4;
		$preferedoffer		=	$this->input->post('preferedoffer');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
    
		$this->form_validation->set_rules('preferedoffer', 'preferedoffer', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    				$orderdata = array(
    					'voucher_id' 				=> $u_id,
    					'prod_id'						=> $product_id,
    					'city' 							=> '',
    					'preferedArea1' 		=> '',
    					'preferedArea2' 		=> '',
    					'preferedMovie1' 	  => $preferedoffer,
    					'preferedMovie2' 		=> '',
    					'prefereddate1' 		=> '',
    					'prefereddate2' 		=> '',
    					'preferedTime1' 		=> '',
    					'preferedTime2' 	  => '',
    					'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    				);
    
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				
    			    
    			    $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($preferedoffer,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);			    
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}" => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    					//--------------------------------------------------------------------//
    
    
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
                          <td><strong>Prefered Offer</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}" => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    			    	$to = $email;
    						$subject = $bookingmaildetails[0]['mail_subject'];
    						$this->send_Email($to, $subject, $message, $bookingSender->meta_value);
    
    						// Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'mail', 
    				    	'msgmail' => $message,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    					
    					
    					$campaignidfrocrm = 'campaginidforcrm';
    					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                        if(isset($campaignIdForCRM)){
    					$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    					$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    					if($rCode == 'processing') {
    						$rrCode = '';
    						$rCodeStatus = 'pending';
    					} else {
    						$rrCode = $rCode;
    						$rCodeStatus = 'completed';
    					}
    
    				    /* Array Parameter Data */
    				    $crmdata = array(
    				    	'campaign_id' 				=> $campaignIdForCRM,
    							'offer_id' 						=> $offerid->id,
    							'voucher_id' 					=> $crmbookingsdata->voucher_id,
    							'crm_offer_id' 				=> $offerid->crm_offer_id,
    							'voucher_code' 				=> $crmbookingsdata->vouchercode,
    							'cname' 							=> $crmbookingsdata->name,
    							'cemail' 							=> $crmbookingsdata->email,
    							'cmobile' 						=> $crmbookingsdata->mobile,
    							'city' 								=> $crmbookingsdata->city,
    							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
    							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
    							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
    							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
    							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    							'preferedTime2' 				=> $crmbookingsdata->preferedTime2,
    							'status' 							=> '0',
    							'reward_code' 				=> $rrCode,
    							'reward_code_status' 	=> $rCodeStatus,
    				    );
    
    				    /* CRM API URL */
    				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    				    /* CRM User name and password */
    				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    						
    				    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$crmurl);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$cresult = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$crmresult = json_decode($cresult, true);
                        }
                        redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}

	// For Offer Five
	public function bookingDetailsForOffer5(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$preferedoffer		=	$this->input->post('preferedoffer');
		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
		$this->form_validation->set_rules('preferedoffer', 'preferedoffer', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    				$orderdata = array(
    					'voucher_id' 				=> $u_id,
    					'prod_id'						=> $product_id,
    					'city' 							=> '',
    					'preferedArea1' 		=> '',
    					'preferedArea2' 		=> '',
    					'preferedMovie1' 	  => $preferedoffer,
    					'preferedMovie2' 		=> '',
    					'prefereddate1' 		=> '',
    					'prefereddate2' 		=> '',
    					'preferedTime1' 		=> '',
    					'preferedTime2' 	  => '',
    					'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    				);
    
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				
    			    
    			    $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($preferedoffer,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);			    
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
        
                        $getPname = $this->MainModel->getProductName($preferedoffer);
    					$prodName = $getPname->sub_prod_name;
    
    					$bookingmaildetails = $this->MainModel->getBookingMailOnlyId5($product_id,$prodName);
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}" => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    						//--------------------------------------------------------------------//
    
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
                          <td><strong>Prefered Offer</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}" => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    			    	$to = $email;
    					$subject = $bookingmaildetails[0]['mail_subject'];
    					$this->send_Email($to, $subject, $message, $bookingSender->meta_value);
    
    						// Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'mail', 
    				    	'msgmail' => $message,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    $campaignidfrocrm = 'campaginidforcrm';
    					$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
                        if(isset($campaignIdForCRM)){
    					$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    					$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    					if($rCode == 'processing') {
    						$rrCode = '';
    						$rCodeStatus = 'pending';
    					} else {
    						$rrCode = $rCode;
    						$rCodeStatus = 'completed';
    					}
    
    				    /* Array Parameter Data */
    				    $crmdata = array(
    				    	'campaign_id' 				=> $campaignIdForCRM,
    							'offer_id' 						=> $offerid->id,
    							'voucher_id' 					=> $crmbookingsdata->voucher_id,
    							'crm_offer_id' 				=> $offerid->crm_offer_id,
    							'voucher_code' 				=> $crmbookingsdata->vouchercode,
    							'cname' 							=> $crmbookingsdata->name,
    							'cemail' 							=> $crmbookingsdata->email,
    							'cmobile' 						=> $crmbookingsdata->mobile,
    							'city' 								=> $crmbookingsdata->city,
    							'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    							'crm_venue_id_1' 			=> $crmbookingsdata->crm_venue_id1,
    							'crm_venue_id_2' 			=> $crmbookingsdata->crm_venue_id2,
    							'preferedArea1' 			=> $crmbookingsdata->preferedArea1,
    							'preferedArea2' 			=> $crmbookingsdata->preferedArea2,
    							'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    							'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    							'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    							'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    							'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    							'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    							'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    							'preferedTime2' 				=> $crmbookingsdata->preferedTime2,
    							'status' 							=> '0',
    							'reward_code' 				=> $rrCode,
    							'reward_code_status' 	=> $rCodeStatus,
    				    );
    
    				    /* CRM API URL */
    				    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    				    /* CRM User name and password */
    				    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    				    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    						
    				    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$crmurl);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$cresult = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$crmresult = json_decode($cresult, true);
                        }
    					redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
	
	// For Offer Six Account Number
	public function bookingDetailsForOffer6AccountNumber(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$userfname				=	htmlspecialchars($this->input->post('userfname'));
		$accountnumber		=	htmlspecialchars($this->input->post('accountnumber'));
		$ifsccode					=	htmlspecialchars($this->input->post('ifsccode'));
		$cashbackdetails	=	$this->session->userdata('bookingform6banner');
		$cashbackamount 	= htmlspecialchars($cashbackdetails[0]['price']);

		$selectedoffer = $this->session->userdata('selectedoffer');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');

		$this->form_validation->set_rules('userfname', 'userfname', 'required|alpha');
		$this->form_validation->set_rules('accountnumber', 'accountnumber', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('ifsccode', 'ifsccode', 'trim|required|xss_clean|regex_match[/^[][a-zA-Z0-9]+$/]');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
				if($getuniqueid==0){
				//Get Limit of Account Number
				$meta_val = 'accountnumberrule';
				$getLimitofAccount = $this->MainModel->getAccountsLimitfromSettings($meta_val);
				//Same Account Number Counts

				$accountnumber = $this->security->xss_clean($accountnumber);
        if($this->security->xss_clean($accountnumber)) {
					$getAccountCount = $this->MainModel->getAccountsCounts($accountnumber);
				} else {
					$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
	      	redirect('booking');
				}

				$this->session->set_userdata('thanks', TRUE);
				if(isset($getLimitofAccount->meta_value) && ($getLimitofAccount->meta_value > $getAccountCount) ) {

					$tbl = 'tbl_voucher_mng';
					//$getCurrentUserDetails = $this->MainModel->get_where_data($tbl, $u_id);
					
					$mobile = $this->session->userdata('mobile');
					$uname 	= $this->session->userdata('name');
					$nemail 	= $this->session->userdata('email');
					$vouchercode	= $this->session->userdata('vouchercode');
                    
          $email = (!empty($this->session->userdata('email')) ? $this->session->userdata('email') : 'no-reply@bigcityexperience.com');
					
					$cdate = date('Y-m-d');

					$campaignid = 'campaginidforcashbackapi';
					$campaignIDs = $this->MainModel->getCampaignIDforCashbackAPI($campaignid);
					
					/* API URL */
			    $url = CASHBACKAPIURL;
			    /* Array Parameter Data */
			    $data = array(
			    	'campaign_id'				=> $campaignIDs, 
			    	'customer_mobile'		=> $mobile, 
			    	'customer_email'		=> $email, 
			    	'customer_name'			=> $uname, 
			    	'voucher_code'			=> $vouchercode, 
			    	'transaction_type'	=> 1, 
			    	'transaction_amt'		=> $cashbackamount, 
			    	'transaction_date'	=> $cdate, 
			    	'account_num'				=> $accountnumber, 
			    	'neft_id'						=> $ifsccode
			    );


			    $username = CASHBACKAPIUSERNAME;
			    $password = CASHBACKAPIPASSWORD;
					
			    $ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
					$result = curl_exec($ch);
					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
					curl_close($ch);
					$resultArr = json_decode($result, true);

						$tbl = 'tbl_orders';
						$orderdata = array(
							'voucher_id' 				=> $u_id,
							'prod_id'						=> $product_id,
							'enc_salt' 					=> SALT,
							'city' 							=> '',
							'preferedArea1' 		=> '',
							'preferedArea2' 		=> '',
							'preferedMovie1' 	  => 'Cash Back on Account',
							'preferedMovie2' 		=> '',
							'prefereddate1' 		=> '',
							'prefereddate2' 		=> '',
							'preferedTime1' 		=> '',
							'preferedTime2' 	  => '',
							'name'							=> $userfname,
							'accountnumber'			=> $accountnumber,
							'ifsccode'					=> $ifsccode,
							'cashbackamount' 		=> $cashbackamount,
							'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
							'api_status'				=> $resultArr['status'],
						);

						$orderdata = $this->security->xss_clean($orderdata);
		        if($this->security->xss_clean($orderdata)) {
							$result = $this->MainModel->insertCashbackOrder($tbl,$orderdata);
						} else {
							$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
			      	redirect('booking');
						}
						
					  $this->MainModel->updateVoucherCodeById($u_id);						

						if ($result) {
							$usr_tbl = 'tbl_voucher_mng';
							//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
							
							$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;

							$newName = explode(" ",$uname);
							$ufName = ($newName[0]) ? $newName[0] : $uname;

							$bookingmaildetails = $this->MainModel->getBookingMail($selectedoffer);
							
							$dynamic_sms = trim($bookingmaildetails[0]['sms']);
    				    	$replacements_sms = [
    				    		"{{ConsumerName}}" => $ufName,
    				    		//"{{RewardCode}}" => $rCode,
    						    " " => '%20',
    						    "&" => '%26',
    						    "#" => '%23',
                    "'" => '%27',
                    "!" => '%21',
                    "*" => '%2A',
                    "(" => '%28',
                    ")" => '%29',
                    ";" => '%3B',
                    ":" => '%3A',
                    "@" => '%40',
                    "=" => '%3D',
                    "+" => '%2B',
                    "$" => '%24',
                    "," => '%2C',
                    "/" => '%2F',
                    "?" => '%3F',
                    "%" => '%25',
                    "#" => '%23',
                    "[" => '%5B',
                    "]" => '%5D',
							];
							$d_sms = strtr($dynamic_sms, $replacements_sms);

							$allbookingsResultss = $this->MainModel->orderDetails($u_id);

							if($allbookingsResultss){
								$curl = curl_init();
								//--------------------------------------------------------------------//
								// Entity ID
								$entityID_metakey = 'entity_id';
								$entityID = $this->MainModel->getIDs($entityID_metakey);
								// Booking Template ID
								//$Booking_TemplateID_metakey = 'booking_template_id';
								//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

								if ($entityID) {
								 	$smsEID = $entityID->meta_value;
								} else {
									$smsEID = '';
								}
								if ($bookingmaildetails[0]['smstmpid']) {
    							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
    							} else {
    								$smsTID = '';
    							}	
							//--------------------------------------------------------------------//

						    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";

						    // Booking Msg
						    $bookingMsg = array(
						    	'userid' => $uid, 
						    	'type' => 'msg', 
						    	'msgmail' => $d_sms,
						    );
						    $this->MainModel->insertMailMsg($bookingMsg);
						    
						    curl_setopt_array($curl, array(
						      CURLOPT_URL => $url,
						      CURLOPT_RETURNTRANSFER => TRUE,
						      CURLOPT_ENCODING => "",
						      CURLOPT_MAXREDIRS => 10,
						      CURLOPT_TIMEOUT => 30,
						      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						      CURLOPT_CUSTOMREQUEST => "GET",)
						    );
						    $response = curl_exec($curl);
						    $err = curl_error($curl);
						    curl_close($curl);
						    if ($err) {
						      echo "cURL Error #:" . $err;
						    } else {

						    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];

			            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
				                <tr>
		                      <td><strong>Prefered Offer</strong></td>
		                      <td><strong>Account Number</strong></td>
		                      <td><strong>IFSC Code</strong></td>
		                      <td><strong>Cashback Amount</strong></td>
				                  <td><strong>Booking Date & Time</strong></td>
				                </tr>';
			                foreach($allbookingsResultss as $key=>$v) {
			                  $codeMessage .= "<tr>
			                  	<td>Account</td>
			                  	<td>".$v['accountnumber']."</td>
			                  	<td>".$v['ifsccode']."</td>
			                  	<td>".$v['cashbackamount']."</td>
			                  	<td>".$v['orderdatetime']."</td>
			                  	</tr>";
			                  }
			            $codeMessage .= '</table></td></tr>';
			            
						    	$replacements = [
									    "{{ConsumerName}}" => $uname,
									    "{{BookingDetails}}" => $codeMessage,
									    //"{{RewardCode}}" => $rCode,
									    "{{emailId}}" => $email,
									];

						    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
									  <tr>
									    <td align="center">
									    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
									      <tr>
									        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
									        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
					          $message .= strtr($dynamic_mail, $replacements);
					        	$message .= '</table></td></tr></table></td></tr></table>';

					        $senderVar = 'bookingsuccesssendermail';
					        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
					        
						    	$to = $email;
								$subject = $bookingmaildetails[0]['mail_subject'];
								if($nemail != ''){
								    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
								}

								// Booking Msg
							    $bookingMsg = array(
							    	'userid' => $uid, 
							    	'type' => 'mail', 
							    	'msgmail' => $message,
							    );
							    $this->MainModel->insertMailMsg($bookingMsg);

							    $campaignidfrocrm = 'campaginidforcrm';
									$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
				          if(isset($campaignIdForCRM)){
										$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
										$offerid = $this->MainModel->getOfferIdFroCRM($product_id);

										if ($resultArr['status']==1) {
											$msg = 'Sent To API';
										} else {
											$msg = 'Not Sent To API';
										}
								    /* Array Parameter Data */
								    $crmdata = array(
								    	'campaign_id' 				=> $campaignIdForCRM,
											'offer_id' 						=> $offerid->id,
											'voucher_id' 					=> $crmbookingsdata->voucher_id,
											'crm_offer_id' 				=> $offerid->crm_offer_id,
											'voucher_code' 				=> $crmbookingsdata->vouchercode,
											'cname' 							=> $crmbookingsdata->name,
											'cemail' 							=> $crmbookingsdata->email,
											'cmobile' 						=> $crmbookingsdata->mobile,
											'accountname' 				=> $userfname,
											'accountnumber' 			=> $accountnumber,
											'ifsccode' 						=> $ifsccode,
											'cashbackamount' 			=> $cashbackamount,
											'cashback_api_status' => $msg,
											'status' 							=> '0',
											'reward_code' 				=> 'booked',
											'reward_code_status' 	=> 'completed',
								    );
				            
								    /* CRM API URL */
								    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
								    
								    /* CRM User name and password */
								    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
								    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
										
								    $ch = curl_init();
										curl_setopt($ch, CURLOPT_URL,$crmurl);
										curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
										curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
										curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
										curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
										curl_setopt($ch, CURLOPT_POST, true);
										curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
										$cresult = curl_exec($ch);
										
										$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
										curl_close($ch);
										$crmresult = json_decode($cresult, true);
									}

									redirect('thankyou');
						        }
							}
						} else {
							$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Please Enter correct information.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
							redirect('booking');
						}
					} else {
						$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Your account limit has been exceed.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
						redirect('booking');
					}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		} 
	}

	// For Offer Six UPI
	public function bookingDetailsForOffer6UPI() {
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$upivpn						=	htmlspecialchars($this->input->post('upivpn'));
		$cashbackdetails	=	$this->session->userdata('bookingform6banner');
		$cashbackamount 	= htmlspecialchars($cashbackdetails[0]['price']);

		$selectedoffer = $this->session->userdata('selectedoffer');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
		$this->form_validation->set_rules('upivpn', 'upivpn', 'trim|required|xss_clean|regex_match[/^[][a-zA-Z0-9@.]+$/]');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
				if($getuniqueid==0){
    				//Get Limit of Account Number
    				$meta_val = 'uparule';
    				$getLimitofAccount = $this->MainModel->getAccountsLimitfromSettings($meta_val);
    				//Same Account Number Counts

    				$upivpn = $this->security->xss_clean($upivpn);
		        if($this->security->xss_clean($upivpn)) {
							$getCountUPI = $this->MainModel->getUPICounts($upivpn);
						} else {
							$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
			      	redirect('booking');
						}
    
    				if(isset($getLimitofAccount->meta_value) && ($getLimitofAccount->meta_value > $getCountUPI) ) {
    					$this->session->set_userdata('thanks', TRUE);
    					$tbl = 'tbl_voucher_mng';
    					//$getCurrentUserDetails = $this->MainModel->get_where_data($tbl, $u_id);
    					
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$nemail 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    					
    					$email = (!empty($this->session->userdata('email')) ? $this->session->userdata('email') : 'no-reply@bigcityexperience.com');
    					
    					$cdate = date('Y-m-d');
    					$campaignid = 'campaginidforcashbackapi';
    					$campaignIDs = $this->MainModel->getCampaignIDforCashbackAPI($campaignid);
    					
    					/* API URL */
    			    $url = CASHBACKAPIURL;
    			    /* Array Parameter Data */
    			    $data = array(
    			    	'campaign_id'				=> $campaignIDs, 
    			    	'customer_mobile'		=> $mobile, 
    			    	'customer_email'		=> $email, 
    			    	'customer_name'			=> $uname, 
    			    	'voucher_code'			=> $vouchercode, 
    			    	'transaction_type'	=> 2, 
    			    	'transaction_amt'		=> $cashbackamount, 
    			    	'transaction_date'	=> $cdate, 
    			    	'utr_num'						=> $upivpn
    			    );
    
    			    $username = CASHBACKAPIUSERNAME;
    			    $password = CASHBACKAPIPASSWORD;
    					
    			    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$url);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$result = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$resultArr = json_decode($result, true);
    
    						$tbl = 'tbl_orders';
    						$orderdata = array(
    							'voucher_id' 				=> $u_id,
    							'prod_id'						=> $product_id,
    							'enc_salt' 					=> SALT,
    							'city' 							=> '',
    							'preferedArea1' 		=> '',
    							'preferedArea2' 		=> '',
    							'preferedMovie1' 	  => 'Cash Back on UPI',
    							'preferedMovie2' 		=> '',
    							'prefereddate1' 		=> '',
    							'prefereddate2' 		=> '',
    							'preferedTime1' 		=> '',
    							'preferedTime2' 	  => '',
    							'cashbackamount' 		=> $cashbackamount,
    							'upivpn'						=> $upivpn,
    							'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    							'api_status'				=> $resultArr['status'],
    						);
    
    						//$result = $this->MainModel->insertOrder($tbl,$orderdata);
    						$orderdata = $this->security->xss_clean($orderdata);
				        if($this->security->xss_clean($orderdata)) {
									$result = $this->MainModel->insertCashbackOrder($tbl,$orderdata);
								} else {
									$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
					      	redirect('booking');
								}

    					  $this->MainModel->updateVoucherCodeById($u_id);
    
    						if ($result) {
    							$usr_tbl = 'tbl_voucher_mng';
    							//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    							$mobile = $this->session->userdata('mobile');
            					$uname 	= $this->session->userdata('name');
            					$email 	= $this->session->userdata('email');
            					$vouchercode	= $this->session->userdata('vouchercode');
            					$uid = $u_id;
    
    							$newName = explode(" ",$uname);
    							$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    							$bookingmaildetails = $this->MainModel->getBookingMail($selectedoffer);
    							
    							$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        				    	$replacements_sms = [
        				    		"{{ConsumerName}}" => $ufName,
        				    		//"{{RewardCode}}" => $rCode,
        						    " " => '%20',
        						    "&" => '%26',
        						    "#" => '%23',
                                    "'" => '%27',
                                    "!" => '%21',
                                    "*" => '%2A',
                                    "(" => '%28',
                                    ")" => '%29',
                                    ";" => '%3B',
                                    ":" => '%3A',
                                    "@" => '%40',
                                    "=" => '%3D',
                                    "+" => '%2B',
                                    "$" => '%24',
                                    "," => '%2C',
                                    "/" => '%2F',
                                    "?" => '%3F',
                                    "%" => '%25',
                                    "#" => '%23',
                                    "[" => '%5B',
                                    "]" => '%5D',
    							];
    							$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    							$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    							if($allbookingsResultss){
    								$curl = curl_init();
    								//--------------------------------------------------------------------//
    								// Entity ID
    								$entityID_metakey = 'entity_id';
    								$entityID = $this->MainModel->getIDs($entityID_metakey);
    								// Booking Template ID
    								//$Booking_TemplateID_metakey = 'booking_template_id';
    								//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    								if ($entityID) {
    								 	$smsEID = $entityID->meta_value;
    								} else {
    									$smsEID = '';
    								}
    								if ($bookingmaildetails[0]['smstmpid']) {
        							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
        							} else {
        								$smsTID = '';
        							}
    							//--------------------------------------------------------------------//
    
    						    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    						    // Booking Msg
    						    $bookingMsg = array(
    						    	'userid' => $uid, 
    						    	'type' => 'msg', 
    						    	'msgmail' => $d_sms,
    						    );
    						    $this->MainModel->insertMailMsg($bookingMsg);
    						    
    						    curl_setopt_array($curl, array(
    						      CURLOPT_URL => $url,
    						      CURLOPT_RETURNTRANSFER => TRUE,
    						      CURLOPT_ENCODING => "",
    						      CURLOPT_MAXREDIRS => 10,
    						      CURLOPT_TIMEOUT => 30,
    						      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    						      CURLOPT_CUSTOMREQUEST => "GET",)
    						    );
    						    $response = curl_exec($curl);
    						    $err = curl_error($curl);
    						    curl_close($curl);
    						    if ($err) {
    						      echo "cURL Error #:" . $err;
    						    } else {
    
    						    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    			            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    				                <tr>
    		                      <td><strong>Prefered Offer</strong></td>
    		                      <td><strong>UPI Details</strong></td>
    		                      <td><strong>Cashback Amount</strong></td>
    				                  <td><strong>Booking Date & Time</strong></td>
    				                </tr>';
    			                foreach($allbookingsResultss as $key=>$v) {
    			                  $codeMessage .= "<tr>
    			                  	<td>UPI</td>
    			                  	<td>".$v['upivpn']."</td>
    			                  	<td>".$v['cashbackamount']."</td>
    			                  	<td>".$v['orderdatetime']."</td>
    			                  	</tr>";
    			                  }
    			            $codeMessage .= '</table></td></tr>';
    			            
    						    	$replacements = [
    									    "{{ConsumerName}}" => $uname,
    									    "{{BookingDetails}}" => $codeMessage,
    									    //"{{RewardCode}}" => $rCode,
    									    "{{emailId}}" => $email,
    									];
    
    						    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    									  <tr>
    									    <td align="center">
    									    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    									      <tr>
    									        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    									        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    					          $message .= strtr($dynamic_mail, $replacements);
    					        	$message .= '</table></td></tr></table></td></tr></table>';
    
    					        $senderVar = 'bookingsuccesssendermail';
    					        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    					        
    						    	$to = $email;
									$subject = $bookingmaildetails[0]['mail_subject'];
									if($nemail != ''){
									    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
									}

									// Booking Msg
    							    $bookingMsg = array(
    							    	'userid' => $uid, 
    							    	'type' => 'mail', 
    							    	'msgmail' => $message,
    							    );
    							    $this->MainModel->insertMailMsg($bookingMsg);
    
    							    $campaignidfrocrm = 'campaginidforcrm';
    									$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
    				          if(isset($campaignIdForCRM)){
    										$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    										$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    										if ($resultArr['status']==1) {
    											$msg = 'Sent To API';
    										} else {
    											$msg = 'Not Sent To API';
    										}
    								    /* Array Parameter Data */
    								    $crmdata = array(
    								    	'campaign_id' 				=> $campaignIdForCRM,
    											'offer_id' 						=> $offerid->id,
    											'voucher_id' 					=> $crmbookingsdata->voucher_id,
    											'crm_offer_id' 				=> $offerid->crm_offer_id,
    											'voucher_code' 				=> $crmbookingsdata->vouchercode,
    											'cname' 							=> $crmbookingsdata->name,
    											'cemail' 							=> $crmbookingsdata->email,
    											'cmobile' 						=> $crmbookingsdata->mobile,
    											'cashbackamount' 			=> $cashbackamount,
    											'upivpn' 							=> $upivpn,
    											'cashback_api_status' => $msg,
    											'status' 							=> '0',
    											'reward_code' 				=> 'booked',
    											'reward_code_status' 	=> 'completed',
    								    );
    				            
    								    /* CRM API URL */
    								    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    								    
    								    /* CRM User name and password */
    								    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    								    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    										
    								    $ch = curl_init();
    										curl_setopt($ch, CURLOPT_URL,$crmurl);
    										curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    										curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    										curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    										curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    										curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    										curl_setopt($ch, CURLOPT_POST, true);
    										curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    										$cresult = curl_exec($ch);
    										
    										$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    										curl_close($ch);
    										$crmresult = json_decode($cresult, true);
    									}
    
    									redirect('thankyou');
    						    }
    							}
    					} else {
    						$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Please Enter correct information.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
    						redirect('booking');
    					}
    				} else {
    					$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Your UPI limit has been exceed.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
    					redirect('booking');
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}

	// For Offer Six E-Wallets
	public function bookingDetailsForOffer6Wallets(){
		$this->session_chk();
		$product_id 			= $this->session->userdata('product_id');
		$walletsmobile		=	htmlspecialchars($this->input->post('phonenumber'));
		$cashbackdetails	=	$this->session->userdata('bookingform6banner');
		$cashbackamount 	= htmlspecialchars($cashbackdetails[0]['price']);

		$selectedoffer = $this->session->userdata('selectedoffer');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
		$this->form_validation->set_rules('preferedoffer', 'preferedoffer', 'required');
		$this->form_validation->set_rules('phonenumber', 'phonenumber', 'required|xss_clean|regex_match[/^[0-9]{10}$/]');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
				if($getuniqueid==0){
    				//Get Limit of Account Number
    				$meta_val = 'ewalletsrule';
    				$getLimitofAccount = $this->MainModel->getAccountsLimitfromSettings($meta_val);
    				//Same Wallets Number Counts
    				$walletsmobile = $this->security->xss_clean($walletsmobile);
		        if($this->security->xss_clean($walletsmobile)) {
							$getCountWalletsMobile = $this->MainModel->getEwalletsMobileCount($walletsmobile);
						} else {
							$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
			      	redirect('booking');
						}
    				
    				
    				if(isset($getLimitofAccount->meta_value) && ($getLimitofAccount->meta_value > $getCountWalletsMobile) ) {
    					$this->session->set_userdata('thanks', TRUE);
    
    					$tbl = 'tbl_voucher_mng';
    					//$getCurrentUserDetails = $this->MainModel->get_where_data($tbl, $u_id);
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$nemail 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    					$cdate = date('Y-m-d');
    					$email = (!empty($this->session->userdata('email')) ? $this->session->userdata('email') : 'no-reply@bigcityexperience.com');
    
    					$getwalletsTypes = $this->MainModel->getWalletsTypesByID($preferedoffer);
    					//print "<pre>"; print_r($getwalletsTypes); die;
    					if (isset($getwalletsTypes)) {
    						if (ucwords($getwalletsTypes->sub_prod_name) == ucwords('Phonepe') || ucwords($getwalletsTypes->sub_prod_name) == ucwords('Phonepay') || ucwords($getwalletsTypes->sub_prod_name) == ucwords('Phonepe wallet') ) {
    							$wallet_type = 2;
    						} else {
    							$wallet_type = 1;
    						}
    					} else {
    						$wallet_type = 0;
    					}
    					$campaignid = 'campaginidforcashbackapi';
    					$campaignIDs = $this->MainModel->getCampaignIDforCashbackAPI($campaignid);
    					
    					/* API URL */
    			    $url = CASHBACKAPIURL;
    			    /* Array Parameter Data */
    			    $data = array(
    			    	'campaign_id'				=> $campaignIDs, 
    			    	'customer_mobile'		=> $mobile, 
    			    	'customer_email'		=> $email, 
    			    	'customer_name'			=> $uname, 
    			    	'voucher_code'			=> $vouchercode, 
    			    	'transaction_type'	=> 3, 
    			    	'transaction_amt'		=> $cashbackamount, 
    			    	'transaction_date'	=> $cdate, 
    			    	'wallet_type'				=> $wallet_type,
    			    	'wallet_mobile' 		=> $walletsmobile
    			    );
    
    			    $username = CASHBACKAPIUSERNAME;
    			    $password = CASHBACKAPIPASSWORD;
    					
    			    $ch = curl_init();
    					curl_setopt($ch, CURLOPT_URL,$url);
    					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    					curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    					curl_setopt($ch, CURLOPT_POST, true);
    					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    					$result = curl_exec($ch);
    					$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    					curl_close($ch);
    					$resultArr = json_decode($result, true);
    
    						$tbl = 'tbl_orders';
    						$orderdata = array(
    							'voucher_id' 				=> $u_id,
    							'prod_id'						=> $product_id,
    							'enc_salt' 					=> SALT,
    							'city' 							=> '',
    							'preferedArea1' 		=> '',
    							'preferedArea2' 		=> '',
    							'preferedMovie1' 	  => $preferedoffer,
    							'preferedMovie2' 		=> '',
    							'prefereddate1' 		=> '',
    							'prefereddate2' 		=> '',
    							'preferedTime1' 		=> '',
    							'preferedTime2' 	  => '',
    							'cashbackamount' 		=> $cashbackamount,
    							//'walletstype'				=> $walletstype,
    							'walletsmobile'			=> $walletsmobile,
    							'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    							'api_status'				=> $resultArr['status'],
    						);
    
    						//$result = $this->MainModel->insertOrder($tbl,$orderdata);
    						$orderdata = $this->security->xss_clean($orderdata);
				        if($this->security->xss_clean($orderdata)) {
									$result = $this->MainModel->insertCashbackOrder($tbl,$orderdata);
								} else {
									$this->session->set_flashdata('msgshow', array('message' => global_text_method('correct-details'),'class' => global_text_method('danger-class') ,'st_otp_time' =>$st_otp_time));
					      	redirect('booking');
								}

    					  $this->MainModel->updateVoucherCodeById($u_id);
    
    						if ($result) {
    							$usr_tbl = 'tbl_voucher_mng';
    							//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    							$mobile = $this->session->userdata('mobile');
            					$uname 	= $this->session->userdata('name');
            					$email 	= $this->session->userdata('email');
            					$vouchercode	= $this->session->userdata('vouchercode');
            					$uid = $u_id;
    
    							$newName = explode(" ",$uname);
    							$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    							$bookingmaildetails = $this->MainModel->getBookingMail($selectedoffer);
    							
    							$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        				    	$replacements_sms = [
        				    		"{{ConsumerName}}" => $ufName,
        				    		//"{{RewardCode}}" => $rCode,
        						    " " => '%20',
        						    "&" => '%26',
        						    "#" => '%23',
                        "'" => '%27',
                        "!" => '%21',
                        "*" => '%2A',
                        "(" => '%28',
                        ")" => '%29',
                        ";" => '%3B',
                        ":" => '%3A',
                        "@" => '%40',
                        "=" => '%3D',
                        "+" => '%2B',
                        "$" => '%24',
                        "," => '%2C',
                        "/" => '%2F',
                        "?" => '%3F',
                        "%" => '%25',
                        "#" => '%23',
                        "[" => '%5B',
                        "]" => '%5D',
    							];
    							$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    							$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    							if($allbookingsResultss){
    								$curl = curl_init();
    								//--------------------------------------------------------------------//
    								// Entity ID
    								$entityID_metakey = 'entity_id';
    								$entityID = $this->MainModel->getIDs($entityID_metakey);
    								// Booking Template ID
    								//$Booking_TemplateID_metakey = 'booking_template_id';
    								//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    								if ($entityID) {
    								 	$smsEID = $entityID->meta_value;
    								} else {
    									$smsEID = '';
    								}
    								if ($bookingmaildetails[0]['smstmpid']) {
        							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
        							} else {
        								$smsTID = '';
        							}	
    							//--------------------------------------------------------------------//
    
    						    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    						    // Booking Msg
    						    $bookingMsg = array(
    						    	'userid' => $uid, 
    						    	'type' => 'msg', 
    						    	'msgmail' => $d_sms,
    						    );
    						    $this->MainModel->insertMailMsg($bookingMsg);
    						    
    						    curl_setopt_array($curl, array(
    						      CURLOPT_URL => $url,
    						      CURLOPT_RETURNTRANSFER => TRUE,
    						      CURLOPT_ENCODING => "",
    						      CURLOPT_MAXREDIRS => 10,
    						      CURLOPT_TIMEOUT => 30,
    						      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    						      CURLOPT_CUSTOMREQUEST => "GET",)
    						    );
    						    $response = curl_exec($curl);
    						    $err = curl_error($curl);
    						    curl_close($curl);
    						    if ($err) {
    						      echo "cURL Error #:" . $err;
    						    } else {
    
    						    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    
    			            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    				                <tr>
    		                      <td><strong>Prefered Offer</strong></td>
    		                      <td><strong>Wallet Type</strong></td>
    		                      <td><strong>Wallet Mobile</strong></td>
    		                      <td><strong>Cashback Amount</strong></td>
    				                  <td><strong>Booking Date & Time</strong></td>
    				                </tr>';
    			                foreach($allbookingsResultss as $key=>$v) {
    			                  $codeMessage .= "<tr>
    			                  	<td>Wallets</td>
    			                  	<td>".$v['walletstype']."</td>
    			                  	<td>".$v['walletsmobile']."</td>
    			                  	<td>".$v['cashbackamount']."</td>
    			                  	<td>".$v['orderdatetime']."</td>
    			                  	</tr>";
    			                  }
    			            $codeMessage .= '</table></td></tr>';
    			            
    						    	$replacements = [
    									    "{{ConsumerName}}" => $uname,
    									    "{{BookingDetails}}" => $codeMessage,
    									    //"{{RewardCode}}" => $rCode,
    									    "{{emailId}}" => $email,
    									];
    
    						    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    									  <tr>
    									    <td align="center">
    									    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    									      <tr>
    									        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    									        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    					          $message .= strtr($dynamic_mail, $replacements);
    					        	$message .= '</table></td></tr></table></td></tr></table>';
    
    					        $senderVar = 'bookingsuccesssendermail';
    					        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    					        
    						    	$to = $email;
									$subject = $bookingmaildetails[0]['mail_subject'];
									if($nemail != ''){
									    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
									}

									// Booking Msg
    							    $bookingMsg = array(
    							    	'userid' => $uid, 
    							    	'type' => 'mail', 
    							    	'msgmail' => $message,
    							    );
    							    $this->MainModel->insertMailMsg($bookingMsg);
    
    							    $campaignidfrocrm = 'campaginidforcrm';
    									$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
    				          if(isset($campaignIdForCRM)){
    										$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    										$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    										if ($resultArr['status']==1) {
    											$msg = 'Sent To API';
    										} else {
    											$msg = 'Not Sent To API';
    										}
    								    /* Array Parameter Data */
    								    $crmdata = array(
    								    	'campaign_id' 				=> $campaignIdForCRM,
    											'offer_id' 						=> $offerid->id,
    											'voucher_id' 					=> $crmbookingsdata->voucher_id,
    											'crm_offer_id' 				=> $offerid->crm_offer_id,
    											'voucher_code' 				=> $crmbookingsdata->vouchercode,
    											'cname' 							=> $crmbookingsdata->name,
    											'cemail' 							=> $crmbookingsdata->email,
    											'cmobile' 						=> $crmbookingsdata->mobile,
    											'cashbackamount' 			=> $cashbackamount,
    											'walletsmobile' 			=> $walletsmobile,
    											'cashback_api_status' => $msg,
    											'status' 							=> '0',
    											'reward_code' 				=> 'booked',
    											'reward_code_status' 	=> 'completed',
    								    );
    				            
    								    /* CRM API URL */
    								    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    								    
    								    /* CRM User name and password */
    								    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    								    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    										
    								    $ch = curl_init();
    										curl_setopt($ch, CURLOPT_URL,$crmurl);
    										curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    										curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    										curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    										curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    										curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    										curl_setopt($ch, CURLOPT_POST, true);
    										curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    										curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    										$cresult = curl_exec($ch);
    										
    										$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    										curl_close($ch);
    										$crmresult = json_decode($cresult, true);
    									}
    									redirect('thankyou');
    						    }
    							}
    					} else {
    						$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Please Enter correct information.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
    						redirect('booking');
    					}
    				} else {
    					$this->session->set_flashdata('msgshow', array('message' => 'Sorry, Your wallet mobile limit has been exceed.','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
    					redirect('booking');
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
	
	// For Offer Seven
	public function bookingDetailsForHealthCheckup(){
		$this->session_chk();
		$product_id 		=   $this->session->userdata('selectedoffer');
		$city				=	$this->input->post('city');
		$preferedMovie1		=	$this->input->post('preferedMovie1');
		$preferedMovie2		=	$this->input->post('preferedMovie2');
		$prefereddate1		=	$this->input->post('prefereddate1');
		$prefereddate2		=	$this->input->post('prefereddate2');
		$preferedTime1		=	$this->input->post('preferedTime1');
		$preferedTime2		=	$this->input->post('preferedTime2');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');

		$this->form_validation->set_rules('city', 'city', 'required|xss_clean');
		$this->form_validation->set_rules('preferedMovie1', 'preferedMovie1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedMovie2', 'preferedMovie2', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate1', 'prefereddate1', 'required|xss_clean');
		$this->form_validation->set_rules('prefereddate2', 'prefereddate2', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime1', 'preferedTime1', 'required|xss_clean');
		$this->form_validation->set_rules('preferedTime2', 'preferedTime2', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    
    				$orderdata = array(
    					'voucher_id' 				=> $u_id,
    					'prod_id'						=> $product_id,
    					'city' 							=> $city,
    					'preferedArea1' 		=> '',
    					'preferedArea2' 		=> '',
    					'preferedMovie1' 	  => $preferedMovie1,
    					'preferedMovie2' 		=> $preferedMovie2,
    					'prefereddate1' 		=> $prefereddate1,
    					'prefereddate2' 		=> $prefereddate2,
    					'preferedTime1' 		=> trim($preferedTime1),
    					'preferedTime2' 	  => trim($preferedTime2),
    					'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    				);
    
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				
    			    
    			    $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($preferedMovie1,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);			
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}"  => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    					//--------------------------------------------------------------------//
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    				    	
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
    		                	<td><strong>Location/City</strong></td>
                          <td><strong>Prefered Offer</strong></td>
                          <td><strong>Prefered Backup Offer</strong></td>
                          <td><strong>Prefered Date</strong></td>
                          <td><strong>Prefered Backup Date</strong></td>
                          <td><strong>Prefered Time</strong></td>
                          <td><strong>Prefered Backup Time</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['city']."</td>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['preferedMovie2']."</td>
    	                  	<td>".$v['prefereddate1']."</td>
    	                  	<td>".$v['prefereddate2']."</td>
    	                  	<td>".$v['preferedTime1']."</td>
    	                  	<td>".$v['preferedTime2']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}"  => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    				    	$to = $email;
							$subject = $bookingmaildetails[0]['mail_subject'];
							if($to != ''){
							    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
							}

							// Booking Msg
    					    $bookingMsg = array(
    					    	'userid' => $uid, 
    					    	'type' => 'mail', 
    					    	'msgmail' => $message,
    					    );
    					    $this->MainModel->insertMailMsg($bookingMsg);
    
    					    $campaignidfrocrm = 'campaginidforcrm';
    							$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
    							if(isset($campaignIdForCRM)){
    								$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    								$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    								if($rCode == 'processing') {
    									$rrCode = '';
    									$rCodeStatus = 'pending';
    								} else {
    									$rrCode = $rCode;
    									$rCodeStatus = 'completed';
    								}
    
    						    /* Array Parameter Data */
    						    $crmdata = array(
    						    	'campaign_id' 				=> $campaignIdForCRM,
    									'offer_id' 						=> $offerid->id,
    									'voucher_id' 					=> $crmbookingsdata->voucher_id,
    									'crm_offer_id' 				=> $offerid->crm_offer_id,
    									'voucher_code' 				=> $crmbookingsdata->vouchercode,
    									'cname' 							=> $crmbookingsdata->name,
    									'cemail' 							=> $crmbookingsdata->email,
    									'cmobile' 						=> $crmbookingsdata->mobile,
    									'city' 								=> $crmbookingsdata->city,
    									'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    									'crm_venue_id_1' 			=> '',
    									'crm_venue_id_2' 			=> '',
    									'preferedArea1' 			=> '',
    									'preferedArea2' 			=> '',
    									'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    									'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    									'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    									'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    									'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    									'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    									'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    									'preferedTime2' 			=> $crmbookingsdata->preferedTime2,
    									'status' 							=> '0',
    									'reward_code' 				=> $rrCode,
    									'reward_code_status' 	=> $rCodeStatus,
    						    );
    
    						    /* CRM API URL */
    						    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    						    /* CRM User name and password */
    						    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    						    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    								
    						    $ch = curl_init();
    								curl_setopt($ch, CURLOPT_URL,$crmurl);
    								curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    								curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    								curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    								curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    								curl_setopt($ch, CURLOPT_POST, true);
    								curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    								$cresult = curl_exec($ch);
    								$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    								curl_close($ch);
    								$crmresult = json_decode($cresult, true);			    
    							}
    							redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
    
    // For Merchandise
	public function bookingDetailsForMerchandise(){
		
		//ini_set('display_errors', 1);
		$this->session_chk();
		$product_id 		=   $this->session->userdata('product_id');
		$city				=	$this->input->post('city');
		$state				=	$this->input->post('state');
		$address1		    =	$this->input->post('address1');
		$address2		    =	$this->input->post('address2');
		$address3		    =	$this->input->post('address3');
		$landmark		    =	$this->input->post('landmark');
		$pin_code		    =	$this->input->post('pin_code');
		$alternate_phone_no =	$this->input->post('alternate_phone_no');

		$st_otp_time		=	(int)$this->input->post('st_otp_time');
		$booking_time		=	(int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
	    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
	    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
	    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
        //$u_id = 5;
		$this->form_validation->set_rules('city', 'city', 'required|xss_clean');
		$this->form_validation->set_rules('state', 'state', 'required|xss_clean');
		$this->form_validation->set_rules('address1', 'address1', 'required|xss_clean');
		$this->form_validation->set_rules('address2', 'address2', 'required|xss_clean');
		$this->form_validation->set_rules('address3', 'address3', 'required|xss_clean');
		$this->form_validation->set_rules('landmark', 'landmark', 'required|xss_clean');
		$this->form_validation->set_rules('pin_code', 'pin_code', 'required|xss_clean');
		$this->form_validation->set_rules('alternate_phone_no', 'alternate_phone_no', 'required|xss_clean');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}	else {
			if($responseData->success == '1' || $responseData->success == 1){ 
			    $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
					if($getuniqueid==0){
    				$tbl = 'tbl_orders';
    
    				$orderdata = array(
    					'voucher_id' 			=> $u_id,
    					'prod_id'				=> $product_id,
    					'city' 		            => $city,
    					'state' 				=> $state,
    					'preferedArea1' 		=> '',
    					'preferedArea2' 		=> '',
    					'address1' 				=> $address1,
    					'address2' 				=> $address2,
    					'address3'              => $address3,
    					'landmark' 				=> $landmark,
    					'pin_code' 				=> $pin_code,
    					'alternate_phone_no'    => $alternate_phone_no,
    					//'preferedMovie1' 	  => $preferedMovie1,
    					/*'preferedMovie2' 		=> $preferedMovie2,
    					'prefereddate1' 		=> $prefereddate1,
    					'prefereddate2' 		=> $prefereddate2,
    					'preferedTime1' 		=> trim($preferedTime1),
    					'preferedTime2' 	  => trim($preferedTime2),*/
    					'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
    				);
    
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				
    			  $this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($product_id,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'completed'
    			    );
    			    //$this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      'voucher_id' => $u_id,
    			      'prod_id' => $product_id,
    			    );
    			    $data = array(
    			      'status' => 'processing'
    			    );
    			    $this->MainModel->updateProductOrder($where,$data);
    			    $rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
    					$mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName = explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);			
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
        		    	$replacements_sms = [
        		    		"{{ConsumerName}}" => $ufName,
        		    		"{{RewardCode}}"  => $rCode,
        				    " " => '%20',
        				    "&" => '%26',
        				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
    					];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);
    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    						//--------------------------------------------------------------------//
    						// Entity ID
    						$entityID_metakey = 'entity_id';
    						$entityID = $this->MainModel->getIDs($entityID_metakey);
    						// Booking Template ID
    						//$Booking_TemplateID_metakey = 'booking_template_id';
    						//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);
    
    						if ($entityID) {
    						 	$smsEID = $entityID->meta_value;
    						} else {
    							$smsEID = '';
    						}
    						if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
    					//--------------------------------------------------------------------//
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    // Booking Msg
    				    $bookingMsg = array(
    				    	'userid' => $uid, 
    				    	'type' => 'msg', 
    				    	'msgmail' => $d_sms,
    				    );
    				    $this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    curl_setopt_array($curl, array(
    				      CURLOPT_URL => $url,
    				      CURLOPT_RETURNTRANSFER => TRUE,
    				      CURLOPT_ENCODING => "",
    				      CURLOPT_MAXREDIRS => 10,
    				      CURLOPT_TIMEOUT => 30,
    				      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      CURLOPT_CUSTOMREQUEST => "GET",)
    				    );
    				    $response = curl_exec($curl);
    				    $err = curl_error($curl);
    				    curl_close($curl);
    				    if ($err) {
    				      echo "cURL Error #:" . $err;
    				    } else {
    
    				    	$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    				    	
    	            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
    		                <tr>
    		                	<td><strong>Location/City</strong></td>
                          <td><strong>Prefered Offer</strong></td>
                          <td><strong>Prefered Backup Offer</strong></td>
                          <td><strong>Prefered Date</strong></td>
                          <td><strong>Prefered Backup Date</strong></td>
                          <td><strong>Prefered Time</strong></td>
                          <td><strong>Prefered Backup Time</strong></td>
    		                  <td><strong>Booking Date & Time</strong></td>
    		                </tr>';
    	                foreach($allbookingsResultss as $key=>$v) {
    	                  $codeMessage .= "<tr>
    	                  	<td>".$v['city']."</td>
    	                  	<td>".$v['preferedMovie1']."</td>
    	                  	<td>".$v['preferedMovie2']."</td>
    	                  	<td>".$v['prefereddate1']."</td>
    	                  	<td>".$v['prefereddate2']."</td>
    	                  	<td>".$v['preferedTime1']."</td>
    	                  	<td>".$v['preferedTime2']."</td>
    	                  	<td>".$v['orderdatetime']."</td>
    	                  	</tr>";
    	                  }
    	            $codeMessage .= '</table></td></tr>';
    	            
    				    	$replacements = [
    							    "{{ConsumerName}}" => $uname,
    							    "{{BookingDetails}}" => $codeMessage,
    							    "{{RewardCode}}"  => $rCode,
    							    "{{emailId}}" => $email,
    							];
    
    				    	$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
    			          $message .= strtr($dynamic_mail, $replacements);
    			        	$message .= '</table></td></tr></table></td></tr></table>';
    
    			        $senderVar = 'bookingsuccesssendermail';
    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    				    	$to = $email;
							$subject = $bookingmaildetails[0]['mail_subject'];
							if($to != ''){
							    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
							}

							// Booking Msg
    					    $bookingMsg = array(
    					    	'userid' => $uid, 
    					    	'type' => 'mail', 
    					    	'msgmail' => $message,
    					    );
    					    $this->MainModel->insertMailMsg($bookingMsg);
    
    					    $campaignidfrocrm = 'campaginidforcrm';
    							$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
    							if(isset($campaignIdForCRM)){
    								$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
    								$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
    
    								if($rCode == 'processing') {
    									$rrCode = '';
    									$rCodeStatus = 'pending';
    								} else {
    									$rrCode = $rCode;
    									$rCodeStatus = 'completed';
    								}
    								
    
    						    /* Array Parameter Data */
    						    $crmdata = array(
    						    	'campaign_id' 				    => $campaignIdForCRM,
    									'offer_id' 					=> $offerid->id,
    									'voucher_id' 				=> $crmbookingsdata->voucher_id,
    									'crm_offer_id' 				=> $offerid->crm_offer_id,
    									'voucher_code' 				=> $crmbookingsdata->vouchercode,
    									'cname' 					=> $crmbookingsdata->name,
    									'cemail' 					=> $crmbookingsdata->email,
    									'cmobile' 					=> $crmbookingsdata->mobile,
    									'city' 						=> $crmbookingsdata->city,
    									'state' 					=> $crmbookingsdata->state,
    									'crm_city_id' 				=> $crmbookingsdata->crm_city_id,
    									'crm_venue_id_1' 			=> '',
    									'crm_venue_id_2' 			=> '',
    									'preferedArea1' 			=> '',
    									'preferedArea2' 			=> '',
    									'crm_prod_id_1' 			=> $crmbookingsdata->crm_prod_id1,
    									'crm_prod_id_2' 			=> $crmbookingsdata->crm_prod_id2,
    									'preferedProd1' 			=> $crmbookingsdata->preferedMovie1,
    									'preferedProd2' 			=> $crmbookingsdata->preferedMovie2,
    									'prefereddate1' 			=> $crmbookingsdata->prefereddate1,
    									'prefereddate2' 			=> $crmbookingsdata->prefereddate2,
    									'preferedTime1' 			=> $crmbookingsdata->preferedTime1,
    									'preferedTime2' 			=> $crmbookingsdata->preferedTime2,
    
    									'address1' 					=> $crmbookingsdata->address1,
    									'address2' 					=> $crmbookingsdata->address2,
    									'address3' 					=> $crmbookingsdata->address3,
    									'landmark' 					=> $crmbookingsdata->landmark,
    									'pin_code' 					=> $crmbookingsdata->pin_code,
    									'alternate_phone_no' 		=> $crmbookingsdata->alternate_phone_no,
    									'status' 					=> '0',
    									'reward_code' 				=> $rrCode,
    									'reward_code_status' 	    => $rCodeStatus,
    						    );
    
    						    /* CRM API URL */
    						    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
    						    /* CRM User name and password */
    						    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
    						    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
    								
    						    $ch = curl_init();
    								curl_setopt($ch, CURLOPT_URL,$crmurl);
    								curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    								curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
    								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    								curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    								curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    								curl_setopt($ch, CURLOPT_POST, true);
    								curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
    								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    								$cresult = curl_exec($ch);
    								$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    								curl_close($ch);
    								$crmresult = json_decode($cresult, true);			    
    							}
    							redirect('thankyou');
    				    }
    					}
    				}
				} else{
				  $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
	
	//--------------------------------------------------------------------------------------------------------//
	// CIRCLE
	public function MobileRecharge(){
		$this->session_chk();
		$data['title']      = "Booking Page"; 
		$product_id 		= $this->session->userdata('selectedoffer');
		$nominate_name		= htmlspecialchars($this->input->post('nominate_name'));
		$circle		        = htmlspecialchars($this->input->post('circle'));
		$service_provider	= htmlspecialchars($this->input->post('service_provider'));
		$connection_type   	= htmlspecialchars($this->input->post('connection_type'));
		
		$st_otp_time		= (int)$this->input->post('st_otp_time');
		$booking_time		= (int)$this->input->post('booking_time');
		
		$secret = htmlspecialchars(gScreteKey);
    $response = htmlspecialchars($this->input->post('g-recaptcha-response'));
    $verifyResponse = htmlspecialchars(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response));
    $responseData = json_decode($verifyResponse);

		$u_id = $this->session->userdata('id');
    $this->form_validation->set_rules('nominate_name', 'nominate_name', 'required|xss_clean');
		$this->form_validation->set_rules('circle', 'circle', 'required|xss_clean');
		$this->form_validation->set_rules('service_provider', 'service_provider', 'required|xss_clean');
		$this->form_validation->set_rules('connection_type', 'connection_type', 'required|xss_clean');
		
		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('msgshow', array('message' => global_text_method('require-field'),'class' => global_text_method('danger-class')));
			redirect("booking");
		}else {
			if($responseData->success == '1' || $responseData->success == 1) {
				$tbl = 'tbl_orders';

				$orderdata = array(
					'voucher_id' 			=> $u_id,
					'prod_id'				=> $product_id,
					'circle' 				=> $circle,
					'preferedMovie1'        => $product_id,
					'nominate_name' 		=> $nominate_name,
					'service_provider' 		=> $service_provider,
					'connection_type' 	    => $connection_type,
					'noofseconds' 			=> (int)$st_otp_time+(int)$booking_time,
				);
        $getuniqueid = $this->MainModel->getUniqueVoucherId($u_id);
				if($getuniqueid==0){
    				$result = $this->MainModel->insertOrder($tbl,$orderdata);
    				$this->MainModel->updateVoucherCodeById($u_id);
    				$getRewards = $this->MainModel->getRewardCode($product_id,$u_id);				
    				if (count($getRewards)>0) {
    					$where = array(
    			      		'voucher_id' => $u_id,
    			      		'prod_id' => $product_id,
    			    	);
    			    $data = array( 'status' => 'completed' );
    			    $this->MainModel->updateProductOrder($where,$data);
    			    $this->MainModel->updateRewardCodeById($getRewards[0]['id']);
    			    $rdata = array(
    			    	'user_id' => $u_id,
    			    	'sub_p_id' => $getRewards[0]['sub_prodid'],
    			    	'reward_code' => $getRewards[0]['rewardcode'],
    			    );
    			    $this->MainModel->insertOrder('tbl_voucher_used',$rdata);
    			    $rCode = $getRewards[0]['rewardcode'];
    				} else {
    					$where = array(
    			      		'voucher_id' => $u_id,
    			      		'prod_id' => $product_id,
    			    	);
    			    	$data = array( 'status' => 'processing' );
    			    	$this->MainModel->updateProductOrder($where,$data);
    			    	$rCode = 'processing';
    				}
    
    				if ($result) {
    					$usr_tbl = 'tbl_voucher_mng';
    					//$usr_details = $this->MainModel->get_where_data($usr_tbl,$u_id);
                        $mobile = $this->session->userdata('mobile');
    					$uname 	= $this->session->userdata('name');
    					$email 	= $this->session->userdata('email');
    					$vouchercode	= $this->session->userdata('vouchercode');
    					$uid = $u_id;
    
    					$newName= explode(" ",$uname);
    					$ufName = ($newName[0]) ? $newName[0] : $uname;
    
    					$bookingmaildetails = $this->MainModel->getBookingMail($product_id);			
    					
    					$dynamic_sms = trim($bookingmaildetails[0]['sms']);
    		    		$replacements_sms = [
	    		    		"{{ConsumerName}}" => $ufName,
	    		    		"{{RewardCode}}"  => $rCode,
	    				    " " => '%20',
	    				    "&" => '%26',
	    				    "#" => '%23',
                            "'" => '%27',
                            "!" => '%21',
                            "*" => '%2A',
                            "(" => '%28',
                            ")" => '%29',
                            ";" => '%3B',
                            ":" => '%3A',
                            "@" => '%40',
                            "=" => '%3D',
                            "+" => '%2B',
                            "$" => '%24',
                            "," => '%2C',
                            "/" => '%2F',
                            "?" => '%3F',
                            "%" => '%25',
                            "#" => '%23',
                            "[" => '%5B',
                            "]" => '%5D',
						];
    					$d_sms = strtr($dynamic_sms, $replacements_sms);    
    					$allbookingsResultss = $this->MainModel->orderDetails($u_id);

    					//--------------------------------------------------------------------//
							// Entity ID
							$entityID_metakey = 'entity_id';
							$entityID = $this->MainModel->getIDs($entityID_metakey);
							// Booking Template ID
							//$Booking_TemplateID_metakey = 'booking_template_id';
							//$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

							if ($entityID) {
							 	$smsEID = $entityID->meta_value;
							} else {
								$smsEID = '';
							}
							if ($bookingmaildetails[0]['smstmpid']) {
							 	$smsTID = trim($bookingmaildetails[0]['smstmpid']);
							} else {
								$smsTID = '';
							}	
						//--------------------------------------------------------------------//
    
    					if($allbookingsResultss){
    						$curl = curl_init();
    				    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$d_sms."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";
    
    				    	// Booking Msg
    				   		$bookingMsg = array(
    				    		'userid' => $uid, 
    				    		'type' => 'msg', 
    				    		'msgmail' => $d_sms,
    				    	);
    				    	$this->MainModel->insertMailMsg($bookingMsg);
    				    
    				    	curl_setopt_array($curl, array(
    				      		CURLOPT_URL => $url,
    				      		CURLOPT_RETURNTRANSFER => TRUE,
    				      		CURLOPT_ENCODING => "",
    				      		CURLOPT_MAXREDIRS => 10,
    				      		CURLOPT_TIMEOUT => 30,
    				      		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    				      		CURLOPT_CUSTOMREQUEST => "GET",)
    				    	);
    				    	$response = curl_exec($curl);
    				    	$err = curl_error($curl);
    				    	curl_close($curl);
    				    	if ($err) {
    				      		echo "cURL Error #:" . $err;
    				    	} else {
    
    				    		$dynamic_mail = $bookingmaildetails[0]['mail_body'];
    				    	
	    	            		$codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
	    		                <tr>
	    		                	<td><strong>Name</strong></td>
	                          		<td><strong>Circle</strong></td>
	                          		<td><strong>Service Provider</strong></td>
	                          		<td><strong>Connection Type</strong></td>
	                          	</tr>';
    	                		foreach($allbookingsResultss as $key=>$v) {
	    	                  		$codeMessage .= "<tr>
	    	                  			<td>".$v['nominate_name']."</td>
	    	                  			<td>".$v['circle']."</td>
	    	                  			<td>".$v['service_provider']."</td>
	    	                  			<td>".$v['connection_type']."</td>
	    	                  		</tr>";
    	                  		}
    	            			$codeMessage .= '</table></td></tr>';
    	            
    				    		$replacements = [
			    							    "{{ConsumerName}}" => $uname,
			    							    "{{BookingDetails}}" => $codeMessage,
			    							    "{{RewardCode}}"  => $rCode,
			    							    "{{emailId}}" => $email,
			    								];
    
    				    		$message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
    							  <tr>
    							    <td align="center">
    							    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
    							      <tr>
    							        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
    							        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
		    			        $message .= strtr($dynamic_mail, $replacements);
		    			        $message .= '</table></td></tr></table></td></tr></table>';
		    
		    			        $senderVar = 'bookingsuccesssendermail';
		    			        $bookingSender = $this->MainModel->getBookingSenderEmail($senderVar);
    			        
    				    		$to = $email;
    							$subject = $bookingmaildetails[0]['mail_subject'];
    							if($to != ''){
    							    $this->send_Email($to, $subject, $message, $bookingSender->meta_value);
    							}
    
    							// Booking Msg
		    					$bookingMsg = array(
		    					  	'userid' => $uid, 
		    					   	'type' => 'mail', 
		    					   	'msgmail' => $message,
		    					   );
		    					$this->MainModel->insertMailMsg($bookingMsg);
    
	    					    $campaignidfrocrm = 'campaginidforcrm';
	    							$campaignIdForCRM = $this->MainModel->getCampaignIDforCashbackAPI($campaignidfrocrm);
	    							if(isset($campaignIdForCRM)){
	    								$crmbookingsdata = $this->MainModel->orderDetailsForCRM($u_id);
	    								$offerid = $this->MainModel->getOfferIdFroCRM($product_id);
	    
	    								if($rCode == 'processing') {
	    									$rrCode = '';
	    									$rCodeStatus = 'pending';
	    								} else {
	    									$rrCode = $rCode;
	    									$rCodeStatus = 'completed';
	    								}
	    								
	    
	    						 	/* Array Parameter Data */
	    						 	$crmdata = array(
	    						 			'campaign_id' 				=> $campaignIdForCRM,
	    									'offer_id' 					=> $offerid->id,
	    									'voucher_id' 				=> $crmbookingsdata->voucher_id,
	    									'crm_offer_id' 				=> $offerid->crm_offer_id,
	    									'voucher_code' 				=> $crmbookingsdata->vouchercode,
	    									'cname' 					=> $crmbookingsdata->name,
	    									'cemail' 					=> $crmbookingsdata->email,
	    									'cmobile' 					=> $crmbookingsdata->mobile,
	    									'status' 							=> '0',
	    									'reward_code' 				=> $rrCode,
	    									'reward_code_status' 	=> $rCodeStatus,
	    									'circle' 							=> $circle,
												'nominate_name' 		=> $nominate_name,
												'service_provider' 		=> $service_provider,
												'connection_type' 	    => $connection_type,
	    						    );

	    						    /* CRM API URL */
	    						    $crmurl = CRMBOOKINGAPIURLFORMIDDLEWARE;
	    						    /* CRM User name and password */
	    						    $crmusername = CRMBOOKINGAPIUSERNAMEFORMIDDLEWARE;
	    						    $crmpassword = CRMBOOKINGAPIPASSWORDFORMIDDLEWARE;
	    								
	    						    $ch = curl_init();
	    								curl_setopt($ch, CURLOPT_URL,$crmurl);
	    								curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    								curl_setopt($ch, CURLOPT_USERPWD, "$crmusername:$crmpassword");
	    								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    								curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
	    								curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	    								curl_setopt($ch, CURLOPT_POST, true);
	    								curl_setopt($ch, CURLOPT_POSTFIELDS, $crmdata);
	    								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
	    								$cresult = curl_exec($ch);
	    								$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
	    								curl_close($ch);
	    								$crmresult = json_decode($cresult, true);			    
	    							}
    							redirect('thankyou');
    				    	}
    					}
    				}
				} else{
				    $this->session->set_flashdata('msgshow', array('message' => 'Sorry, You are already booking this voucher code!','class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
					redirect('booking');
				}
			} else {
				$this->session->set_flashdata('msgshow', array('message' => global_text_method('captcha-error'),'class' => global_text_method('danger-class'), 'st_otp_time' =>$st_otp_time));
				redirect('booking');
			}
		}
	}
//--------------------------------------------------------------------------------------------------------//
	
	// logout module from frontend----------
	public function logout()	{
		$this->clearCache();
		$this->session->sess_destroy();
		delete_cookie('visited'); 
    redirect('');
	}

	protected function clearCache(){
    $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    $this->output->set_header('Pragma: no-cache');
    $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	}

	public function thankyou() {
		$this->session_chk();
		$this->session_chk_for_thankyou();
		$mobile = $this->session->userdata('mobile');
		$this->checkBlock($mobile);
		$this->clearCache();
		$this->session->sess_destroy();
		delete_cookie('visited');
		$data['title'] = "Thank you";	
		$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		//$data['banner'] = $this->MainModel->getBannerforPage();	
		$pagename = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pagename);
		$data['contentData'] = $this->MainModel->getContentforPage();
		$this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/thankyou',$data);
		$this->load->view('frontend/common/footer',$data);
	}
	public function cancelView() {
		$this->session_chk();
		$this->clearCache();
		$this->session->sess_destroy();
		delete_cookie('visited');
		$data['title'] = "Cancel";	
		$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		//$data['banner'] = $this->MainModel->getBannerforPage();	
		$pagename = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pagename);
		$data['contentData'] = $this->MainModel->getContentforPage();
		$this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/cancel',$data);
		$this->load->view('frontend/common/footer',$data);
	}

	

	public function termCondition() {
		$last_index_page_url = $this->uri->uri_string(2);
		$getMenuDetails = $this->MainModel->getMenuDetailss($last_index_page_url);
		$getBnnersDetails = $this->MainModel->getBannerDetailss($last_index_page_url);
		$data['blockdates'] = $this->MainModel->getBlockDates();
		$data['menulist'] = $this->MainModel->getMenuList();
		$data['title'] = $getMenuDetails->menuname;			
		//$data['banner'] = $getBnnersDetails->banner;	
		$pagename = 'home';
		$data['banner'] = $this->MainModel->getBannerforPage($pagename);
		$data['contentData'] = $getMenuDetails->content;
		
		$this->load->view('frontend/common/header',$data);
		$this->load->view('frontend/terms-and-condition',$data);
		$this->load->view('frontend/common/footer',$data);
	}

	public function resendotpRequest(){
		$this->session_mobile_chk();
		$mobile_num = $this->session->userdata('mobile');
		$this->checkBlock($mobile_num);
		$limitcounts = (!empty((int)$this->session->userdata('limitcount')) ? (int)$this->session->userdata('limitcount') : 0);

		$valid_entry = 0; 
		$u_id = $this->session->userdata('id');
		$getotpblockedtime = $this->MainModel->getOTPBlockedTimeByMobile($mobile_num);
		$noofattempts = isset($getotpblockedtime[0]['noofattempts']) ? $getotpblockedtime[0]['noofattempts'] : 0;
		$to_time 	= strtotime(date('Y-m-d H:i:s'));
		$from_time 	= isset($getotpblockedtime[0]['created_on']) ? strtotime($getotpblockedtime[0]['created_on']) : strtotime(date('Y-m-d H:i:s', strtotime('-6 minutes')));
		$time_diff 	= round(abs($to_time - $from_time) / 60,2);
		
		if(!empty($getotpblockedtime) && $noofattempts<$this->limit_entry && $time_diff<=$this->block_time) {
			$attemptdata = array(
				'noofattempts' 	=> ($noofattempts+1),
				'created_on'	=> date('Y-m-d H:i:s')
			);
			$where = array('mobile' => $mobile_num);
			$this->MainModel->update_login_attempts($attemptdata,$where);
			$valid_entry=1;
		} else {
			if(empty($getotpblockedtime) || $time_diff>$this->block_time){
				$attemptdata = array(
					'voucher_id' 	=> $u_id, 
					'mobile' 		=> $mobile_num,
					'noofattempts' 	=> 1,
				);
				$this->MainModel->otp_insert('tbl_loginattempts', $attemptdata);
				$valid_entry=1;
			} else {
				$this->session->set_flashdata('msgshow', array('message' => "You've already attempted ".$noofattempts." times. Please try after 5 minutes.",'class' => global_text_method('danger-class'), 'st_time' =>$st_time));
				$this->session->set_userdata('page_restrict', true);
			  redirect('');
			}				
		}

		if($valid_entry==1){
			if($limitcounts <= 10) {
				$number = random_int(1000000,9999999);
				$otpmsg = $this->MainModel->getOTPMessage();
				$dynamic_otpmsg = trim($otpmsg->message);
				$replacements = [
			    "{{otp}}" => $number,
			    " " => '%20',
					"&" => '%26',
					"#" => '%23',
		      "'" => '%27',
		      "!" => '%21',
		      "*" => '%2A',
		      "(" => '%28',
		      ")" => '%29',
		      ";" => '%3B',
		      ":" => '%3A',
		      "@" => '%40',
		      "=" => '%3D',
		      "+" => '%2B',
		      "$" => '%24',
		      "," => '%2C',
		      "/" => '%2F',
		      "?" => '%3F',
		      "%" => '%25',
		      "#" => '%23',
		      "[" => '%5B',
		      "]" => '%5D',
				];
				$smsText = strtr($dynamic_otpmsg, $replacements);

				//--------------------------------------------------------------------//
				// Entity ID
				$entityID_metakey = 'entity_id';
				$entityID = $this->MainModel->getIDs($entityID_metakey);
				// Booking Template ID
				$Booking_TemplateID_metakey = 'otp_template_id';
				$Booking_TemplateID = $this->MainModel->getIDs($Booking_TemplateID_metakey);

				if ($entityID) {
				 	$smsEID = $entityID->meta_value;
				} else {
					$smsEID = '';
				}
				if ($Booking_TemplateID) {
				 	$smsTID = $Booking_TemplateID->meta_value;
				} else {
					$smsTID = '';
				}	
			//--------------------------------------------------------------------//
		    $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile_num."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=".$smsEID."&template_id=".$smsTID."";

		    $curl = curl_init();
		    curl_setopt_array($curl, array(
		      CURLOPT_URL => $url,
		      CURLOPT_RETURNTRANSFER => TRUE,
		      CURLOPT_ENCODING => "",
		      CURLOPT_MAXREDIRS => 10,
		      CURLOPT_TIMEOUT => 30,
		      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		      CURLOPT_CUSTOMREQUEST => "GET",)
		    );
		    $response = curl_exec($curl);
		    $err = curl_error($curl);
		    curl_close($curl);
		    if ($err) {
		      echo "cURL Error #:" . $err;
		    } else {
		      $tbl = 'tbl_user_otp';
		      $mobdata = array(
		      	'user_mobile' => $mobile_num, 
		      	'user_otp' 		=> $number,
		      );
		      $arrRowInsert = $this->MainModel->otp_insert($tbl,$mobdata);
		      $this->session->set_flashdata(array('msgshow','user_mobile' => $mobile_num));
		      $limitcounts++;
					$newlimit = $this->session->set_userdata('limitcount', $limitcounts);
		      redirect("otp");
		    }
		  } else {				
				$this->session->set_flashdata('msgshow', array('message' => 'You have exceeded your limit. Please try after sometime.','class' => global_text_method('danger-class')));
				redirect('');
				exit();
			}
		} else {
	  	$this->session->set_flashdata('msgshow', array('message' => 'You have exceeded your limit. Please try after 5 minutes.','class' => global_text_method('danger-class')));
			$this->session->unset_userdata('mobile');
			redirect('');
			exit();
	  }
	}
	
	public function otp_Send_Resend_CRM() {
    $mobile = $this->uri->segment(2);
    $otp = $this->uri->segment(3);
    $tbl = 'tbl_user_otp';
    $mobdata = array(
      'user_mobile'     => $mobile, 
      'user_otp' 		=> $otp,
    );
    $arrRowInsert = $this->MainModel->otp_insert($tbl,$mobdata);
	}

//send email function ------- 
function send_Email($to, $subject, $message, $from) {   
  require_once(APPPATH."libraries/vendor/autoload.php");    
  $email = new \SendGrid\Mail\Mail(); 
	$email->setfrom($from);
	$email->setSubject($subject);
	$email->addTo($to);
	$email->addContent(
	    "text/html", $message
	);
	$sendgrid = new \SendGrid('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');
	if($sendgrid->send($email)){
		echo "";
	} else {
		echo "not"; 
	}  
 } 
 
 
}

