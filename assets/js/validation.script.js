$(document).ready(function(){
	
    // proposal form validation         
    $('#login').validate({  
        rules:{
            email:{
                required:true,
                email:true
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                required:"Please enter your email address",
                email:"Please a valid email address"
            },
            password:{
                required:"please enter your password"
            }
        }
    });
	
	 $('#login2').validate({  
        rules:{
            email:{
                required:true,
                email:true
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                required:"Please enter your email address",
                email:"Please a valid email address"
            },
            password:{
                required:"please enter your password"
            }
        }
    });
	
	
    $('#notification').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select notification country"
            },
            title:{
                required:"Please enter notification title"
            },
            description:{
                required:"Please enter notification description"
            },
            image:{
                required:"Please select notification image"
            }
        }
    });
    $('#enotification').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select notification country"
            },
            title:{
                required:"Please enter notification title"
            },
            description:{
                required:"Please enter notification description"
            }
        }
    });
    $('#maincategory').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select main category country"
            },
            title:{
                required:"Please enter main category title"
            },
            description:{
                required:"Please enter main category description"
            },
            image:{
                required:"Please select main category image"
            }
        }
    });
    $('#emaincategory').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select main category country"
            },
            title:{
                required:"Please enter main category title"
            },
            description:{
                required:"Please enter main category description"
            }
        }
    });
    $('#subcategory').validate({
        rules:{
            language:{
                required:true
            },
            maincategory:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select sub category language"
            },
            maincategory:{
                required:"Please select sub category"
            },
            title:{
                required:"Please enter sub category title"
            },
            description:{
                required:"Please enter sub category description"
            },
            image:{
                required:"Please select sub category image"
            }
        }
    });
    $('#esubcategory').validate({
        rules:{
            language:{
                required:true
            },
            maincategory:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select sub category language"
            },
            maincategory:{
                required:"Please select main category"
            },
            title:{
                required:"Please enter sub category title"
            },
            description:{
                required:"Please enter sub category description"
            }
        }
    });
    $('#download').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select download document language"
            },
            title:{
                required:"Please enter download document title"
            },
            image:{
                required:"Please select download document image"
            }
        }
    });
    $('#edownload').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select download document language"
            },
            title:{
                required:"Please enter download document title"
            },
            description:{
                required:"Please enter download document description"
            }
        }
    });
    $('#term').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select terms and conditions language"
            },
            title:{
                required:"Please enter terms and conditions title"
            },
            image:{
                required:"Please select terms and conditions image"
            }
        }
    });
    $('#eterm').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select terms and conditions language"
            },
            title:{
                required:"Please enter terms and conditions title"
            }
        }
    });
    $('#policy').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select Policy language"
            },
            title:{
                required:"Please enter Policy title"
            },
            image:{
                required:"Please select Policy image"
            }
        }
    });
    $('#epolicy').validate({
        rules:{
            language:{
                required:true
            },
            title:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select Policy language"
            },
            title:{
                required:"Please enter Policy title"
            }
        }
    });


    $('#product').validate({
        rules:{
            language:{
                required:true
            },
            m_category:{
                required:true
            },
            s_category:{
                required:true
            },
            product_title:{
                required:true
            },
            product_description:{
                required:true
            },
            product_price:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select lanaguage"
            },
            m_category:{
                required:"Please select main category"
            },
            s_category:{
                required:"Please select sub category"
            },
            product_title:{
                required:"Please enter product title"
            },
            product_description:{
                required:"Please enter product description"
            },
            product_price:{
                required:"Please enter product price"
            }
        }
    });
    $('#eproduct').validate({
        rules:{
            language:{
                required:true
            },
            m_category:{
                required:true
            },
            s_category:{
                required:true
            },
            product_title:{
                required:true
            },
            product_description:{
                required:true
            },
            product_price:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select lanaguage"
            },
            m_category:{
                required:"Please select main category"
            },
            s_category:{
                required:"Please select sub category"
            },
            product_title:{
                required:"Please enter product title"
            },
            product_description:{
                required:"Please enter product description"
            },
            product_price:{
                required:"Please enter product price"
            },
            image:{
                required:"Please select product image"
            }
        }
    });
    $('#enquiry').validate({
        rules:{
            status:{
                required:true
            },
            note:{
                required:true
            }
        },
        messages:{
            status:{
                required:"Please select status for enquiry"
            },
            note:{
                required:"Please enter a note"
            }
        }
    });
    $('#cms').validate({
        rules:{
            language:{
                required:true
            },
            p_title:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            },
            image:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select language"
            },
            p_title:{
                required:"Please select page"
            },
            title:{
                required:"Please enter title"
            },
            description:{
                required:"Please enter description"
            },
            image:{
                required:"Please choose an image"
            }
        }
    });
    $('#ecms').validate({
        rules:{
            language:{
                required:true
            },
            p_title:{
                required:true
            },
            title:{
                required:true
            },
            description:{
                required:true
            }
        },
        messages:{
            language:{
                required:"Please select language"
            },
            p_title:{
                required:"Please select page"
            },
            title:{
                required:"Please enter title"
            },
            description:{
                required:"Please enter description"
            }
        }
    });

});
$(document).ready(function(){
    $('#success').delay(10000).fadeOut();
});
