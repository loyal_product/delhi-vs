$(document).on("click", function(e) 
{
   if (window.matchMedia('(max-width: 768px)').matches) {
    var container = $(".button-menu-mobile, .side-menu");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
      $("#wrapper").addClass("enlarged");
    }
   }
});