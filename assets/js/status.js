$(document).on('click','.label-notification',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "notification_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',notificationid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-maincategory',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "maincategory_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',maincategoryid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-subcategory',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "subcategory_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',subcategoryid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-download',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "download_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',downloadid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-term',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "term_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',termid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-policy',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "policy_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',policyid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-product',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "product_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',productid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-cms',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "cms_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',cmsid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

$(document).on('click','.label-banner',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);

    url = "banner_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',bannerid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});



$(document).on('click','.label-jobsubcatagory',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "jobsubcatagory_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',jobsubcatagoryid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        //console.log(data);
       location.reload();
      }
    });
  }
});
 
$(document).on('click','.label-catagory',function(){    
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){  
    var current_element = $(this);
    url = "catagory_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',catagoryid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        //console.log(data);
       location.reload();
      }
    });
  }
});




$(document).on('click','.label-career',function(){
  var status = ($(this).hasClass("btn-success")) ? '0' : '1';
  var msg = (status=='0')? 'Deactivate' : 'Activate';
  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "career_status";
    $.ajax({
      type:"POST",
      url: url,
      data: {UserActive:'active',careerid:$(current_element).attr('data'),status:status},
      success: function(data)
      {
        location.reload();
      }
    });
  }
});

